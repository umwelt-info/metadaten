{
  "title": "Eckpunktepapier zur Weiterentwicklung des bundesweiten Bodenbiodiversitätsmonitorings durch das Fachgremium „Monitoring der Bodenbiodiversität und -funktionen“",
  "description": "Eckpunktepapier\nzur Weiterentwicklung des bundesweiten\nBodenbiodiversitätsmonitorings\ndurch das Fachgremium „Monitoring der\nBodenbiodiversität und -funktionen“\nLeipzig, 26.09.2022\nEinführung\nDas Eckpunktepapier wurde dem Grundsatzfachgremium zum 03.06.2022 erstmalig vorgelegt\nund in dessen 3.Sitzung am 21.06.2022 diskutiert sowie im Nachgang mit einer Frist von 3,5 Wo-\nchen kommentiert. Am 26.09.2022 erfolgte die Wiedervorlage zur Kenntnisnahme beim Grund-\nsatzfachgremium im Umlaufverfahren.\nDie Fachgremienmitglieder sind Dr. Anneke Beylich, Prof. Dr. Dr. François Buscot, Prof. Dr. Nico\nEisenhauer, Dr. Bernd Hommel, Dr. Heinrich Höper, Dr. Frank Glante, Dr. Erik Grüneberg, Dr.\nMoritz Nabel, Dr. Heike Puhlmann, Prof. Dr. Martina Roß-Nickoll, Dr. Jörg Römbke, Dr. David\nRussell, Prof. Dr. Stefan Scheu, Prof. Dr. Christoph Tebbe, Dr. Andreas Toschki, Dr. Lina Weiß,\nRoswitha Walter, Dr. Christina Weißbecker\nDas Nationale Monitoringzentrum zur Biodiversität (NMZB), das am 26.03.2021 eröffnet wurde,\narbeitet an der Weiterentwicklung des bundesweiten Biodiversitätsmonitorings und wird hierfür\nzum Ende der Aufbauphase im Sommer 2023 ein Gesamtkonzept anfertigen. Hierbei wurde die\nBodenbiodiversität als ein Schwerpunktthema identifiziert und für die unterstützende Bearbei-\ntung die Einrichtung des Fachgremiums „Monitoring der Bodenbiodiversität und -funktionen“\nbeschlossen, das am 29.09.2021 zur konstituierenden Sitzung zusammen trat. Das Fachgremium\nist ein zeitlich befristetes Gremium und wird mindestens bis zur Fertigstellung des Gesamtkon-\nzepts bestehen, möglicherweise auch darüber hinaus bspw. zur weiteren Begleitung von initiier-\nten Projekten. Das Fachgremium wird grundlegende fachliche Inhalte und Empfehlungen für eine\nkurz- bis mittelfristige Umsetzung von bundesweiten Modulen im Bodenmonitoring unter beson-\nderer Berücksichtigung vorhandener Monitoringprogamme und Synergiepotenziale geben und\ndie Grundlagen zur weiteren umfassenden Konzeptionierung bereitstellen. Dies wird in ei-nem\nAbschlussbericht, dem Basiskonzept dokumentiert. Dieses Eckpunktepapier legt die aktuellen\nHintergründe zur 1) Bedeutung der Bodenbiodiversität, Gefährdung und Handlungserforder-\nnisse, den 2) Status Quo der Erfassung der Bodenbiodiversität, und 3) die Aufgaben und Ziele des\nFachgremiums „Monitoring der Bodenbiodiversität und -funktionen“ dar.\n1\n\n1 Bedeutung der Bodenbiodiversität, Gefährdung und Handlungserfordernisse\nBöden sichern die menschliche Ernährungsgrundlage, regulieren über ihre Kohlenstoffspeiche-\nrung das Klima, reinigen Wasser, ermöglichen Stoffkreisläufe und sind Lebensraum für eine Viel-\nzahl von Organismen. Zur Bodenentwicklung tragen sowohl Verwitterung als auch Bodenorganis-\nmen bei. Die Verwitterung der verschiedenen Ausgangsgesteine führt zu deren Zerkleinerung und\ndie Bodenorganismen leisten über Prozesse wie Verklebung und Humusbildung die Bildung eines\nstabilen funktionalen und nährstoffhaltigen Gefüges. Die Bildung von wenigen Zentimetern frucht-\nbaren Bodens benötigt mehrere Jahrhunderte [1]. Ein Verlust der Bodenbiodiversität bedeutet\ngleichsam eine Reduzierung oder gar den Verlust der Bodenbildung. Ebenso wird die Funktionali-\ntät der Böden größtenteils über die Aktivität der Bodenorganismen realisiert. Eine vielfältige Bo-\ndengemeinschaft bringt eine Vielzahl von Ökosystemleistungen hervor, darunter z. B. die Auf-\nrechterhaltung der Bodenfruchtbarkeit, die natürliche Schädlingsregulation, die Pufferfunktion\ngegenüber Klimaeinwirkung und Schadstoffeinträgen und die CO2-Fixierung in Böden. Durch in-\ntensive Nutzung, zunehmende Flächenversiegelung, Nutzungskonversionen, Schadstoffeinträge\nsowie durch den Klimawandel bedingte Veränderungen sind Böden heute im Wandel und hinsicht-\nlich ihrer strukturellen und chemisch-physikalischen Qualität sowie Funktionen stark gefährdet.\nInfolge dessen gehen in gestörten Böden zunehmend Lebensräume für Bodenorganismen verlo-\nren. Somit stellt die Bodenbiodiversität ein Querschnittsthema im Bereich des Umwelt- und Na-\nturschutzes, der Land- und Forstwirtschaft, aber auch im Bereich der Siedlungs- und Verkehrsent-\nwicklung dar.\nDer Verlust der Biodiversität im Boden ist auf internationaler [2], europäischer [3] und nationaler\nEbene [4] dokumentiert. Die Bodenbiodiversität steht aufgrund ihrer Bedeutung mit den interna-\ntionalen Zielen für nachhaltige Entwicklung (SDGs) in enger Verbindung (kein Hunger (SDG 2), Luft-\nqualität (SDG 3,13), Wasserqualität (SDG 6,14), Erneuerbare Energie (SDG 7), Maßnahmen zum\nKlimaschutz (SDG 13), Leben an Land (SDG 15) ([2]). Die Vielfalt und die Entwicklung der Boden-\ngemeinschaften sind bisher nur unzureichend durch nationale Gesetze geschützt [4]. Bisher etab-\nlierte Naturschutzmaßnahmen zielen auf den Erhalt der oberirdischen Artenvielfalt ab [5]. Auf eu-\nropäischer Ebene wurde Ende 2021 die Europäische Bodenstrategie 2030 veröffentlicht, die unter\nanderem den Schutz und die Förderung der Bodenbiodiversität beinhaltet [6] und dazu für 2023\nein Bodengesundheitsgesetz ankündigt [7]. Im Koalitionsvertrag 2021-2025 ist der Aufbau eines\nnationalen Bodenmonitoringzentrums vorgesehen sowie die Abänderung des Bundesboden-\nschutzgesetzes zur besseren Abbildung der Bedeutung der Bodenbiodiversität.\nEin bundesweites, standardisiertes Monitoring von Bodenorganismen und Bodenfunktionen soll\nden Zustand und Veränderungen der Bodenbiodiversität und deren Dynamik dokumentieren. Die\nDaten sollen die Grundlage dafür liefern, Ursachen der Veränderungen zu erforschen, Maßnah-\nmen zur Erhaltung biodiversitätsbedingter Bodenfunktionen und -leistungen abzuleiten und deren\nErfolg zu überprüfen. Hierzu sollen für die jeweiligen Ökosystemfunktionen relevante Indikator-\ngruppen erarbeitet werden.\nBeim Aufbau eines bundesweiten Bodenbiodiversitätsmonitorings könnten mögliche Synergien\nmit bestehenden Monitoringprogrammen genutzt werden. Ebenso ist es möglich, klassische Me-\nthoden-Ansätze mit neuen bzw. noch zu entwickelnden Methoden zu kombinieren. Potenzielle\nSchnittpunkte existieren z. B. mit dem derzeit im Aufbau befindlichen bundesweiten Insektenmo-\nnitoring hinsichtlich bodenlebender Arthropoden. Bereits existierende, aber bundesweit noch zu\netablierende Methoden (z. B. DNA-Metabarcoding) stellen vielversprechende Ansätze dar, deren\n2\n\nNutzen für die Bewertung von ökologischen und naturschutzfachlichen Qualitäten allerdings erst\nnoch zu validieren ist.\n2 Status Quo der Erfassung der Bodenbiodiversität\n2.1 Fortlaufende Erfassungsprogramme von Bundes- und Landesbehörden\nBundesweit sind folgende fortlaufende Erfassungsprogramme von Bundes- und Landesbehörden\nmit dem Thema Bodenzustand befasst:\nDie Bodendauerbeobachtung (Bundesländer, UBA) mit ca. 800 Stichprobenflächen fokussiert\nauf eine Vielzahl chemischer und physikalischer Parameter. Einige Bundesländer erheben zusätz-\nlich bodenbiologische Parameter, wie zum Beispiel Regenwürmer, Kleinringelwürmer oder die\nmikrobielle Biomasse. Es werden nur wenige taxonomische Gruppen untersucht und diese nicht\nbundesweit. Diese Erfassungen variieren stark hinsichtlich ihrer räumlichen und zeitlichen Auflö-\nsung sowie der methodischen Durchführung. Eine länderübergreifende Vereinheitlichung der\nMethoden und zu erfassenden Parameter, auch hinsichtlich der Bodenbiologie, wäre wün-\nschenswert, da sich hierdurch die Möglichkeit zur Integration bereits vorhandener Probeflächen\nin die Kulisse eines Monitorings zur Bodenbiodiversität bietet [8].\nAufbauend auf diesen Flächen der Bodendauerbeobachtung wird vom Umweltbundesamt der-\nzeit die Etablierung eines Klimafolgen-Bodenmonitoring-Verbunds mit den vier Schwerpunktthe-\nmen organische Substanz, Bodenbiologie, Bodenwasserhaushalt und Erosion angestrebt [9, 10].\nHierfür wurden aus den Bundesländern und zahlreichen Institutionen 9.000 potentielle Stand-\norte gemeldet. Dazu gehören auch die weiter unten angeführten Programme (z. B. BZE). Aus\ndem Bericht zum Forschungs- und Entwicklungsprojekt geht hervor, dass für die Bodenbiologie\nin Deutschland derzeit noch keine bundesweiten Aussagen anhand vorliegender Daten möglich\nsind. Die Vereinheitlichung der Methoden und Durchführung von Forschungsvorhaben zur Ver-\ngleichsanalyse verschiedener Methoden ist vorgesehen.\nDie Bodenzustandserhebungen Wald (BMEL, Thünen Institut; ca. 1.900 Probenflächen) und\nLandwirtschaft (BMEL, Thünen Institut; ca. 3.100 Probenflächen) umfassen regelmäßige Erhe-\nbungen von Bodenzustandsgrößen/-indikatoren. Bisher werden keine Bodenorganismen erfasst.\nAktuell laufen jedoch Initiativen, die die Lücke zum Monitoring der Biodiversität schließen sollen\n(s. u. „Übersicht in Entwicklung befindlicher Erfassungsprogramme zur Bodenbiodiversität“).\nDie Umweltprobenbank (BMU, UBA) ist ein Archiv, in dem u. a. Bodenproben von 11 verschiede-\nnen Standorten eingelagert werden [11]. Diese können für spätere Auswertungen, z. B. Analysen\nder darin enthaltenen Organismen, genutzt werden.\nDes Weiteren untersuchen wissenschaftliche Forschungsplattformen den Zustand der Bodenor-\nganismen. So sind zum Beispiel in Deutschland 205 sogenannte Landwirtschaftliche Dauerfeld-\nversuche (DFV) etabliert, die spezifisch zur Beantwortung verschiedener Fragestellungen betrie-\nben werden [8]. Neben physikalisch-chemischen Parametern werden auch biologische Daten zur\nmikrobiellen Biomasse und Diversität, Enzymaktivität, pflanzenpathogenen Fadenwürmern und\nRegenwürmern aufgenommen. Da es sich bei den DVF um experimentelle Feldversuche handelt,\nbilden mögliche Veränderungen in der Bodenbiodiversität nicht das Geschehen in der Gesamt-\nlandschaft ab. Seit 2006 werden auch innerhalb der Biodiversitätsexploratorien Daten zu Bo-\ndenorganismen und deren Funktionen erhoben. Diese umfassen ein Monitoring der Bakterien,\n3",
  "types": [
    {
      "Text": {
        "text_type": "Report"
      }
    }
  ],
  "origins": [
    "/Bund/BfN/NMZB"
  ],
  "license": {
    "path": "/geschlossen/all-rights-reserved",
    "label": "all-rights-reserved",
    "url": "https://en.wikipedia.org/wiki/All_rights_reserved"
  },
  "mandatory_registration": false,
  "source_url": "https://www.monitoringzentrum.de/sites/default/files/2023-04/NMZB_Eckpunktepapier_Bodenmonitoring_2023.pdf",
  "machine_readable_source": false,
  "language": {
    "id": "German",
    "path": "/Deutsch",
    "label": "Deutsch"
  },
  "quality": {
    "findability": {
      "title": 0.0,
      "description": 0.34392786,
      "spatial": "NoRegion",
      "spatial_score": 0.0,
      "temporal": false,
      "keywords": 0.0,
      "identifier": false,
      "score": 0.05732131
    },
    "accessibility": {
      "landing_page": "Specific",
      "landing_page_score": 1.0,
      "direct_access": false,
      "publicly_accessible": true,
      "score": 0.6666667
    },
    "interoperability": {
      "open_file_format": false,
      "media_type": false,
      "machine_readable_data": false,
      "machine_readable_metadata": false,
      "score": 0.0
    },
    "reusability": {
      "license": "UnclearInformation",
      "license_score": 0.33333334,
      "contact_info": false,
      "publisher_info": false,
      "score": 0.11111111
    },
    "score": 0.20877478
  },
  "status": "Active"
}
