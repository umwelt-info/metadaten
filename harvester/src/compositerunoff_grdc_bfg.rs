use std::io::{Cursor, Read};

use anyhow::{Error, Result};
use cap_std::fs::Dir;
use csv::ReaderBuilder;
use serde::Deserialize;
use smallvec::smallvec;
use time::{Date, Month};
use zip::ZipArchive;

use harvester::{
    Source,
    client::Client,
    fetch_many,
    utilities::{make_key, point_like_bounding_box},
    write_dataset,
};
use metadaten::{
    dataset::{
        Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
        Status,
        r#type::{Domain, Station, Type},
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let station_list = fetch_station_list(client, source).await?;

    let count = station_list.len();

    let (results, errors) = fetch_many(0, 0, station_list, |station| {
        translate_station_dataset(dir, client, source, station)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_station_list(client: &Client, source: &Source) -> Result<Vec<Item>> {
    let url = source.url.join("html/Data/stations.zip")?;

    let bytes = client
        .make_request(
            source,
            "station_list".to_owned(),
            Some(&url),
            |client| async {
                let bytes = client
                    .get(url.clone())
                    .send()
                    .await?
                    .error_for_status()?
                    .bytes()
                    .await?;

                let mut archive = ZipArchive::new(Cursor::new(&*bytes))?;

                let mut bytes = Vec::new();

                archive
                    .by_name("GRDC663Sites.txt")?
                    .read_to_end(&mut bytes)?;

                Ok::<_, Error>(bytes)
            },
        )
        .await?;

    let mut rdr = ReaderBuilder::new()
        .delimiter(b'\t')
        .has_headers(true)
        .from_reader(Cursor::new(&*bytes));

    let mut station_list = Vec::new();

    for result in rdr.deserialize::<Item>() {
        let item = result?;

        if item.country == "DL" {
            station_list.push(item);
        }
    }

    Ok(station_list)
}

async fn translate_station_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.station_id).into_owned();
    let station_id = item.station_id;
    let url = source
        .url
        .join(&format!("html/Polygons/P{station_id}.html"))?;

    let title = format!(
        "Gauging Station {} ('Station Number': {}) at the river {}",
        item.name, station_id, item.river
    );
    let description = format!(
        r#"This dataset contains composite runoff data combining observed river discharge from gauging station {} at river {} with a climate-driven Water Balance Model.
        The data are provided by the Global Runoff Data Centre (GRDC).
        Monthly average values can be accessed on the dedicated webpage {} (see below in resources), whereas the whole data set can be aquired from the GRDC."#,
        item.name,
        item.river,
        format_args!("{}html/Polygons/Data{}.txt", source.url, station_id)
    );

    let types = smallvec![Type::Measurements {
        domain: Domain::Rivers,
        station: Some(Station {
            id: Some(station_id.clone().into()),
            ..Default::default()
        }),
        measured_variables: smallvec!["Abfluss".to_owned()],
        methods: Default::default(),
    }];

    let region = Region::Other(item.name.into());
    let mut regions = smallvec![region];

    let bounding_boxes = smallvec![point_like_bounding_box(item.lat, item.lon)];
    regions.extend(WISE.match_shape(item.lon, item.lat).map(Region::Watershed));

    let start_date = Date::from_calendar_date(item.start_year, Month::January, 1)?;
    let end_date = Date::from_calendar_date(item.end_year, Month::December, 31)?;
    let time_ranges = smallvec![(start_date, end_date).into()];

    let resources = smallvec![
        Resource {
            r#type: ResourceType::PlainText,
            description: Some("Averaged monthly values".to_owned()),
            url: format!("{}html/Polygons/Data{}.txt", source.url, station_id),
            primary_content: true,
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::PlainText,
            description: Some("Site description".to_owned()),
            url: format!("{}html/Polygons/D{}.txt", source.url, station_id),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            description: Some("GRDC Station Information".to_owned()),
            url: format!("{}html/Polygons/GRDC-desc.html", source.url),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            description: Some("STN Derived Subbasin Inforamtion".to_owned()),
            url: format!("{}html/Polygons/STN-desc.html", source.url),
            ..Default::default()
        },
    ];

    let provider = Organisation::WikiData {
        identifier: 119010386, // BGE
        role: OrganisationRole::Provider,
    };

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        bounding_boxes,
        regions,
        time_ranges,
        resources,
        organisations: smallvec![provider],
        language: Language::English,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url: url.into(),
        machine_readable_source: false,
        status: Status::Inactive,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Deserialize, Debug, Clone)]
struct Item {
    #[serde(rename = "Country")]
    country: String,
    #[serde(rename = "RiverName")]
    river: String,
    #[serde(rename = "Name")]
    name: String,
    #[serde(rename = "GRDC-ID")]
    station_id: String,
    #[serde(rename = "Y-coord")]
    lat: f64,
    #[serde(rename = "X-coord")]
    lon: f64,
    #[serde(rename = "StartYear")]
    start_year: i32,
    #[serde(rename = "EndYear")]
    end_year: i32,
}
