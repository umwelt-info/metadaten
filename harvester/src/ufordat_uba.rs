use anyhow::{Context, Result};
use cap_std::fs::Dir;
use csv::{Error as CsvError, ReaderBuilder};
use encoding_rs::WINDOWS_1252;
use fantoccini::Locator;
use itertools::Itertools;
use serde::Deserialize;
use smallvec::smallvec;
use tokio::fs::read;
use url::Url;

use harvester::{
    Source,
    browser::Browser,
    client::Client,
    fetch_many, suppress_errors,
    utilities::{GermanDate, yesterday},
    write_dataset,
};
use metadaten::dataset::{
    Alternative, Dataset, Language, License, Organisation, OrganisationRole, Region, Resource,
    ResourceType, SourceUrlExplainer, Tag, r#type::Type,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let (_, results, errors) =
        fetch_datasets(dir, client, source, "1900", "01.01.1900", "31.12.1969").await?;

    let (results, errors) = fetch_many(
        results,
        errors,
        1970..=yesterday().year(),
        |year| async move {
            let key = year.to_string();
            let from = format!("01.01.{year}");
            let until = format!("31.12.{year}");

            fetch_datasets(dir, client, source, &key, &from, &until).await
        },
    )
    .await;

    Ok((results, results, errors))
}

async fn fetch_datasets(
    dir: &Dir,
    client: &Client,
    source: &Source,
    key: &str,
    from: &str,
    until: &str,
) -> Result<(usize, usize, usize)> {
    let text = client
        .make_request(
            source,
            format!("csv-{key}.isol"),
            Some(&source.url),
            |_client| download_csv(&source.url, from, until),
        )
        .await
        .with_context(|| format!("Failed to fetch datasets of {key}"))?;

    let mut reader = ReaderBuilder::new()
        .delimiter(b';')
        .from_reader(text.as_bytes());

    let (results, errors) = fetch_many(0, 0, reader.deserialize(), |record| {
        translate_dataset(dir, client, source, record)
    })
    .await;

    Ok((results, results, errors))
}

async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    record: Result<Record, CsvError>,
) -> Result<(usize, usize, usize)> {
    let record = record?;

    let key = record.id;
    if key.is_empty() {
        tracing::debug!("Skipping record with empty identifier");
        return Ok((1, 0, 0));
    }

    let language;
    let alternatives;
    let title;
    let short_description;

    if !record.subject_eng.is_empty() && !record.description_eng.is_empty() {
        if !record.subject.is_empty() && !record.description.is_empty() {
            title = record.subject;
            short_description = record.description;
            language = Language::German;
            alternatives = vec![Alternative::Language {
                language: Language::English,
                url: source.url.clone().into(),
            }];
        } else {
            title = record.subject_eng;
            short_description = record.description_eng;
            language = Language::English;
            alternatives = Vec::new();
        }
    } else if !record.subject.is_empty() {
        title = record.subject;
        short_description = record.description;
        language = Language::German;
        alternatives = Vec::new();
    } else {
        title = record.subject_eng;
        short_description = record.description_eng;
        language = Language::English;
        alternatives = Vec::new();
    }

    let institutions = record
        .institutions
        .split("^")
        .filter(|name| !name.is_empty());

    let description = format!(
        "Das Projekt \"{title}\" wird vom Umweltbundesamt gefördert und von {institutions} durchgeführt. {short_description}",
        institutions = institutions.clone().join(" / "),
    );

    let resources = record
        .urls
        .split('^')
        .filter_map(|url| Url::parse(url).ok())
        .map(|url| Resource {
            r#type: ResourceType::WebPage,
            description: Some("Webseite zum Förderprojekt".to_owned()),
            url: url.into(),
            ..Default::default()
        })
        .collect();

    // Organisations sind das UBA als fördernde Organisation und die jeweils geförderte(n) Organisation(en)
    let mut organisations = smallvec![Organisation::Other {
        name: "Umweltbundesamt".to_owned(),
        role: OrganisationRole::Provider,
        websites: smallvec!["https://www.umweltbundesamt.de".to_owned()],
    }];

    organisations.extend(institutions.map(|name| Organisation::Other {
        name: name.to_owned(),
        role: OrganisationRole::Operator,
        websites: Default::default(),
    }));

    organisations.extend(
        record
            .sponsors
            .split("^")
            .filter(|name| !name.is_empty())
            .map(|name| Organisation::Other {
                name: name.to_owned(),
                role: OrganisationRole::Provider,
                websites: Default::default(),
            }),
    );

    organisations.extend(
        record
            .project_partner
            .split("^")
            .filter(|name| !name.is_empty())
            .map(|name| Organisation::Other {
                name: name.to_owned(),
                role: OrganisationRole::Contributor,
                websites: Default::default(),
            }),
    );

    let regions = record
        .state
        .split('^')
        .map(|name| Region::Other(name.into()))
        .collect();

    // Zeitbezug kann aus Laufzeit ermittelt werden
    let start_date = record.start_date.parse::<GermanDate>();
    let end_date = record.end_date.parse::<GermanDate>();

    let time_ranges = suppress_errors(|| {
        if let (&Ok(start_date), &Ok(end_date)) = (&start_date, &end_date) {
            smallvec![(start_date, end_date).into()]
        } else if let &Ok(start_date) = &start_date {
            smallvec![(start_date.into(), yesterday()).into()]
        } else {
            Default::default()
        }
    });

    // Verschlagwortung ist bereits erfolgt und kann aus dem Feld Schlagwörter ermittelt werden
    let tags = record
        .key_words
        .split('^')
        .map(|key_word| Tag::Other(key_word.into()))
        .collect();

    let dataset = Dataset {
        title: title.to_string(),
        description: Some(description.to_string()),
        types: smallvec![Type::SupportProgram],
        resources,
        regions,
        time_ranges,
        organisations,
        tags,
        alternatives,
        origins: source.origins.clone(),
        source_url: source.url.clone().into(),
        source_url_explainer: SourceUrlExplainer::CopyTitle("Übersichtsseite: bitte dort die Suchfunktion nutzen (Begriff wird automatisch kopiert)".to_owned()),
        language,
        license: License::CcByNcNd40,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug, Deserialize)]
struct Record {
    #[serde(rename = "UU ID")]
    id: String,
    #[serde(rename = "Projektthema")]
    subject: String,
    #[serde(rename = "Projektthema Übersetzung")]
    subject_eng: String,
    #[serde(rename = "Kurzbeschreibung")]
    description: String,
    #[serde(rename = "Kurzbeschreibung Übersetzung")]
    description_eng: String,
    #[serde(rename = "Durchführende Institutionen")]
    institutions: String,
    #[serde(rename = "Durchführende Institutionen Bundesländer")]
    state: String,
    #[serde(rename = "Projektpartner")]
    project_partner: String,
    #[serde(rename = "Finanzgeber")]
    sponsors: String,
    #[serde(rename = "URLs")]
    urls: String,
    #[serde(rename = "Projekt-Beginn")]
    start_date: String,
    #[serde(rename = "Projekt-Ende")]
    end_date: String,
    #[serde(rename = "Schlagwörter")]
    key_words: String,
}

async fn download_csv(url: &Url, from: &str, until: &str) -> Result<String> {
    let browser = Browser::open().await?;

    browser.clear_downloads().await?;

    browser.goto(url.as_str()).await?;

    browser
        .wait()
        .for_element(Locator::Css("iframe#DV1_C13"))
        .await?;

    browser.enter_frame(Some(0)).await?;

    // click the "Erweiterte Suche" button
    browser
        .find(Locator::Css("input#DV1_C35.DynForm_DfButtons"))
        .await?
        .click()
        .await?;

    browser
        .wait()
        .for_element(Locator::Css("iframe#DV1_C24"))
        .await?;

    browser.enter_frame(Some(0)).await?;

    // Set "Laufzeit Beginn von"
    browser
        .find(Locator::Css("input#DV1_C4.DynForm_DfAttributes"))
        .await?
        .send_keys(from)
        .await?;

    // Set "Laufzeit Beginn bis"
    browser
        .find(Locator::Css("input#DV1_C5.DynForm_DfAttributes"))
        .await?
        .send_keys(until)
        .await?;

    // click the "Suchen" button
    browser
        .find(Locator::Css("input#DV1_C28.DynForm_DfButtons"))
        .await?
        .click()
        .await?;

    // click the "Alle Ergebnisse Exportieren" button
    browser
        .wait()
        .for_element(Locator::Css("input#DV1_C75.DynForm_DfButtons"))
        .await?
        .click()
        .await?;

    let text = browser
        .wait_for_download(|entry| async move {
            let bytes = read(entry.path()).await?;
            let text = WINDOWS_1252.decode(&bytes).0.into_owned();
            Ok(Some(text))
        })
        .await?;

    Ok(text)
}
