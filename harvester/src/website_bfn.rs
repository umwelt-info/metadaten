use anyhow::{Result, anyhow};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use cow_utils::CowUtils;
use hashbrown::HashMap;
use poppler::Document;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{SmallVec, smallvec};
use time::{Date, Month, macros::format_description, util::days_in_month};

use harvester::{
    Source,
    client::Client,
    fetch_many, remove_datasets, selectors,
    utilities::{
        GermanDate, GermanMonth, collect_pages, collect_text, make_key, parse_text, select_text,
        yesterday,
    },
    write_dataset,
};
use metadaten::dataset::{
    Alternative, Dataset, GlobalIdentifier, Language, License, Organisation, OrganisationRole,
    Region, Resource, ResourceType, TimeRange,
    r#type::{TextType, Type},
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();
    let clusters = AtomicRefCell::new(Clusters::default());

    let mut batch_size = 0;

    let (count, results, errors) = fetch_page(
        dir,
        client,
        source,
        &selectors,
        &clusters,
        Some(&mut batch_size),
        0,
    )
    .await?;

    let pages = count.div_ceil(batch_size);
    tracing::info!("Harvesting {count} datasets in batches of {batch_size}");

    let (results, errors) = fetch_many(results, errors, 1..pages, |page| {
        fetch_page(dir, client, source, &selectors, &clusters, None, page)
    })
    .await;

    for (cluster_key, keys) in clusters.into_inner() {
        if keys.len() > 1 {
            tracing::debug!("Merging cluster for '{cluster_key}'");
            merge_cluster(dir, client, source, cluster_key, keys).await?;
        }
    }

    Ok((count, results, errors))
}

async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    clusters: &AtomicRefCell<Clusters>,
    batch_size: Option<&mut usize>,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let page = page.to_string();

    let mut url = source.url.clone();
    url.query_pairs_mut().append_pair("page", &page);

    let count;
    let entries;

    {
        let text = client.fetch_text(source, page.clone(), &url).await?;

        let document = Html::parse_document(&text);

        count = parse_text(
            &document,
            &selectors.results,
            &selectors.results_value,
            "number of results",
        )?;

        entries = document
            .select(&selectors.search_entry)
            .map(|element| {
                if let Some(link) = element.select(&selectors.link_article).next() {
                    let href = link.attr("href").unwrap().to_owned();
                    let category = select_text(element, &selectors.category).into();

                    Ok(Entry::Article(ArticleEntry { href, category }))
                } else if let Some(link) = element.select(&selectors.link_pdf).next() {
                    let href = link.attr("href").unwrap().to_owned();
                    let category = select_text(element, &selectors.category).into();

                    let title = element
                        .select(&selectors.title_pdf)
                        .next()
                        .ok_or_else(|| anyhow!("Missing title"))?
                        .attr("title")
                        .unwrap()
                        .to_owned();

                    Ok(Entry::Document(DocumentEntry {
                        href,
                        title,
                        category,
                    }))
                } else {
                    Err(anyhow!("Missing link for article on page {page}"))
                }
            })
            .collect::<Result<Vec<_>>>()?;
    }

    if let Some(batch_size) = batch_size {
        *batch_size = entries.len();
    }

    let (results, errors) = fetch_many(0, 0, entries, |entry| async move {
        match entry {
            Entry::Document(document_entry) => {
                fetch_document(dir, client, source, document_entry).await
            }
            Entry::Article(article_entry) => {
                fetch_article(dir, client, source, selectors, clusters, article_entry).await
            }
        }
    })
    .await;

    Ok((count, results, errors))
}

enum Entry {
    Article(ArticleEntry),
    Document(DocumentEntry),
}

struct ArticleEntry {
    href: String,
    category: CompactString,
}

struct DocumentEntry {
    href: String,
    title: String,
    category: CompactString,
}

async fn fetch_document(
    dir: &Dir,
    client: &Client,
    source: &Source,
    entry: DocumentEntry,
) -> Result<(usize, usize, usize)> {
    let DocumentEntry {
        href,
        title,
        category,
    } = entry;
    tracing::trace!("Fetching document at {}", href);

    let url = source.url.join(&href)?;
    let key = make_key(url.path()).into_owned();

    let description = {
        let bytes = client
            .fetch_pdf(source, format!("document-{key}.isol"), &url)
            .await?;

        let document = Document::from_bytes(&bytes, None)?;

        collect_pages(&document, 0..3)
    };

    let types = if category == "Dokument" {
        smallvec![Type::Text {
            text_type: TextType::Report,
        }]
    } else {
        tracing::error!("Unknown category {} on page {}", category, href);
        Default::default()
    };

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        origins: source.origins.clone(),
        source_url: url.into(),
        language: Language::German,
        license: License::AllRightsReserved,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_article(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    clusters: &AtomicRefCell<Clusters>,
    entry: ArticleEntry,
) -> Result<(usize, usize, usize)> {
    let ArticleEntry { href, category } = entry;
    tracing::trace!("Fetching article at {}", href);

    let url = source.url.join(&href)?;
    let key = make_key(&href).into_owned();

    let text = client
        .fetch_text(source, format!("article-{key}"), &url)
        .await?;

    let title;
    let description;
    let mut organisations = SmallVec::new();
    let issued;
    let mut resources = SmallVec::new();
    let mut time_ranges = SmallVec::new();
    let region;
    let global_identifier;

    {
        let document = Html::parse_document(&text);

        let element = document
            .select(&selectors.article)
            .next()
            .ok_or_else(|| anyhow!("Missing article on page {}", href))?;

        title = element
            .select(&selectors.title)
            .next()
            .ok_or_else(|| anyhow!("Missing title"))?
            .attr("title")
            .unwrap()
            .to_owned();

        description = element
            .select(&selectors.description)
            .next()
            .map(|element| collect_text(element.text()));

        issued = element
            .select(&selectors.publishing_year)
            .next()
            .map(|element| -> Result<Date> {
                let text = collect_text(element.text());
                let year = text.parse()?;
                let date = Date::from_ordinal_date(year, 1)?;
                Ok(date)
            })
            .transpose()?;

        global_identifier = element
            .select(&selectors.doi)
            .next()
            .map(|elem| GlobalIdentifier::Doi(collect_text(elem.text())));

        let start_date = parse_text::<_, GermanDate>(
            element,
            &selectors.start_date,
            &selectors.date_value,
            "start date",
        );

        let end_date = parse_text::<_, GermanDate>(
            element,
            &selectors.end_date,
            &selectors.date_value,
            "end date",
        );

        match (start_date, end_date) {
            (Ok(start_date), Ok(end_date)) => time_ranges.push((start_date, end_date).into()),
            (Ok(date), Err(_)) => time_ranges.push(date.into()),
            _ => (),
        }

        if let Some(period) = element.select(&selectors.period).next() {
            let text = period.text().collect::<String>();

            match extract_period(selectors, &text) {
                Ok(time_range) => time_ranges.push(time_range),
                Err(err) => {
                    let start_year = if text.contains("kontinuierlich") {
                        match title.as_ref() {
                            "Deutsch-Chinesische Workshopreihe zu Biodiversität und Ökosystemleistungen" => {
                                Some(2002)
                            }
                            "Kooperation des BfN mit Russland" => Some(1992),
                            _ => None,
                        }
                    } else {
                        None
                    };

                    if let Some(start_year) = start_year {
                        let start_date = Date::from_calendar_date(start_year, Month::January, 1)?;
                        let end_date = yesterday();

                        time_ranges.push((start_date, end_date).into());
                    } else {
                        tracing::error!("Failed to parse period of {key}: {err}")
                    }
                }
            }
        }

        region = element
            .select(&selectors.location)
            .next()
            .map(|element| element.text().collect())
            .map(Region::Other);

        if let Some(element) = element.select(&selectors.editor).next() {
            let name = collect_text(element.text());

            organisations.push(Organisation::Other {
                name,
                role: OrganisationRole::Publisher,
                websites: Default::default(),
            });
        }

        if let Some(element) = element.select(&selectors.organiser).next() {
            let name = collect_text(element.text());

            organisations.push(Organisation::Other {
                name,
                role: OrganisationRole::Organiser,
                websites: Default::default(),
            });
        }

        for resource in element.select(&selectors.resource) {
            let text = collect_text(resource.text());
            let url = resource
                .select(&selectors.url)
                .next()
                .ok_or_else(|| anyhow!("Could not find URL for resource {}", text))?
                .attr("href")
                .unwrap();

            resources.push(
                Resource {
                    r#type: ResourceType::Document,
                    url: source.url.join(url)?.into(),
                    description: Some(text),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }

        for link in element.select(&selectors.external_link) {
            let href = link.attr("href").unwrap();
            let text = collect_text(link.text());

            resources.push(
                Resource {
                    r#type: ResourceType::WebPage,
                    url: source.url.join(href)?.into(),
                    description: Some(text),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }
    }

    let cluster_key = generate_cluster_key(selectors, &title);

    let language = if url.as_str().contains("/leichte-sprache") {
        Language::GermanEasy
    } else {
        Language::German
    };

    let mut types = SmallVec::new();
    // Types from the BfN website are "Projekt", "Publikation", "Karte", Diagramm" and "Tabelle", types from the NMZB website are "Veranstaltung", "Programmsteckbrief", "Meldung" and "Seite", while "Dokument" exists in both.
    if category == "Projekt" || category.contains("Dokument") {
        types.push(Type::Text {
            text_type: TextType::Report,
        });
    }
    if category == "Publikation" {
        types.push(Type::Text {
            text_type: TextType::Publication,
        });
    }
    if category.contains("Karte") || category.contains("Diagramm") {
        types.push(Type::Image);
    }
    if category.contains("Tabelle") {
        types.push(Type::Dataset);
    }
    if category.contains("Veranstaltung") {
        types.push(Type::Event);
    }
    if category.contains("Steckbrief")
        || category.contains("Programmsteckbrief")
        || category.contains("Seite")
    {
        types.push(Type::Text {
            text_type: TextType::Editorial,
        });
    }
    if category.contains("Meldung") {
        types.push(Type::Text {
            text_type: TextType::News,
        });
    }
    if types.is_empty() {
        tracing::error!("Unknown category {} on page {}", category, href);
    }

    let dataset = Dataset {
        title,
        description,
        types,
        origins: source.origins.clone(),
        source_url: url.into(),
        language,
        license: License::AllRightsReserved,
        organisations,
        issued,
        resources,
        time_ranges,
        regions: region.into_iter().collect(),
        global_identifier,
        ..Default::default()
    };

    clusters
        .borrow_mut()
        .entry(cluster_key)
        .or_default()
        .push(key.clone());

    write_dataset(dir, client, source, key.clone(), dataset).await
}

fn extract_period(selectors: &Selectors, text: &str) -> Result<TimeRange> {
    // Fix up some invalid dates which will trip up the parser.
    let text = text.cow_replace("31.11.2016", "30.11.2016");
    let text = text.cow_replace("31.09.2024", "30.09.2024");
    let text = text.as_ref();

    if let Some(captures) = selectors.period_dates.captures(text) {
        let start_date = captures[1].parse::<GermanDate>()?;
        let end_date = captures[2].parse::<GermanDate>()?;

        Ok((start_date, end_date).into())
    } else if let Some(captures) = selectors.period_months.captures(text) {
        let start_month = captures[1].parse::<u8>()?.try_into()?;
        let start_year = captures[2].parse()?;
        let start_date = Date::from_calendar_date(start_year, start_month, 1)?;

        let end_month = captures[3].parse::<u8>()?.try_into()?;
        let end_year = captures[4].parse()?;
        let end_date =
            Date::from_calendar_date(end_year, end_month, days_in_month(end_month, end_year))?;

        Ok((start_date, end_date).into())
    } else if let Some(captures) = selectors.period_years.captures(text) {
        let start_year = captures[1].parse()?;
        let start_date = Date::from_calendar_date(start_year, Month::January, 1)?;

        let end_year = captures[2].parse()?;
        let end_date = Date::from_calendar_date(end_year, Month::December, 31)?;

        Ok((start_date, end_date).into())
    } else if let Some(captures) = selectors.since_year.captures(text) {
        let start_year = captures[1].parse()?;
        let start_date = Date::from_calendar_date(start_year, Month::January, 1)?;

        let end_date = yesterday();

        Ok((start_date, end_date).into())
    } else if let Some(captures) = selectors.since_date.captures(text) {
        let start_date = Date::parse(&captures[1], format_description!("[day].[month].[year]"))?;
        let end_date = yesterday();

        Ok((start_date, end_date).into())
    } else if let Some(captures) = selectors.period_months_alt.captures(text) {
        let start_month = captures[1].parse::<GermanMonth>()?.into();
        let start_year = captures[2].parse()?;
        let end_month = captures[3].parse::<GermanMonth>()?.into();
        let end_year = captures[4].parse()?;
        let start_date = Date::from_calendar_date(start_year, start_month, 1)?;
        let end_date =
            Date::from_calendar_date(end_year, end_month, days_in_month(end_month, end_year))?;

        Ok((start_date, end_date).into())
    } else {
        Err(anyhow!("Unexpected format `{text}`"))
    }
}

type Clusters = HashMap<String, SmallVec<[String; 1]>>;

fn generate_cluster_key(selectors: &Selectors, title: &str) -> String {
    let key = selectors.numbering_year_and_quotes.replace_all(title, "");

    let key = selectors
        .whitespace_before_parentheses_or_domain_ending
        .replace_all(&key, ")");

    key.trim().to_owned()
}

async fn merge_cluster(
    dir: &Dir,
    client: &Client,
    source: &Source,
    cluster_key: String,
    mut keys: SmallVec<[String; 1]>,
) -> Result<()> {
    keys.sort_unstable();

    let datasets = remove_datasets(dir, client, source, &keys).await?;

    let base_key = keys.into_iter().next().unwrap();

    let mut datasets = datasets.into_iter();
    let mut base_dataset = datasets.next().unwrap();

    base_dataset.title = cluster_key;

    for dataset in datasets {
        base_dataset.alternatives.push(Alternative::Source {
            source: source.name.clone(),
            title: dataset.title,
            url: dataset.source_url,
        });

        base_dataset.resources.extend(dataset.resources);
        base_dataset.organisations.extend(dataset.organisations);
        base_dataset.bounding_boxes.extend(dataset.bounding_boxes);
        base_dataset.time_ranges.extend(dataset.time_ranges);
    }

    write_dataset(dir, client, source, base_key, base_dataset).await?;

    Ok(())
}

selectors! {
    results: "div.js-search-global-info",
    results_value: r"(\d+)\s+Ergebnisse" as Regex,
    search_entry: "div[class='items']",
    link_article: "article h2[title] a[href]",
    link_pdf: "div[class='s-wrapper--teaser'] a[class='s-file s-icon-animation s-icon-animation--down'][href]",
    category: ".s-search--categorie, .field__items .field__item",
    article: "article.s-node--view-mode--full",
    title_pdf: "article h2[title]",
    title: "h1[title]",
    description: "div.field--name-field-abstract",
    editor: "div.field--name-field-editor div.field__item",
    organiser: "div.field--name-field-event-organiser div.field__item",
    publishing_year: "div.field--name-field-publishing-year div.field__item",
    start_date: "div.field--name-field-event-startdate div.field__item",
    end_date: "div.field--name-field-event-enddate div.field__item",
    date_value: r"(\d{2}\.\d{2}\.\d{4})" as Regex,
    period: "div.field--name-field-period div.field__item",
    // regex checks for whitespace to account for typos found in the dates
    period_dates: r"(\d{1,2}\.\s?\d{1,2}\.\s?\d{4})\s*(?:–|-|bis)\s*(\d{1,2}\.\s?\d{1,2}\.\s?\d{4})" as Regex,
    period_months: r"(\d{1,2})/(\d{4})\s*(?:–|-|bis)\s*(\d{1,2})/(\d{4})" as Regex,
    period_months_alt: r"([ADFJMNOS][a-z]+)\s+([12]\d{3}).*([ADFJMNOS][a-z]+)\s+([12]\d{3})" as Regex,
    period_years: r"(\d{4})\s*(?:–|-|bis)\s*(\d{4})" as Regex,
    since_year: r"Seit\W+(\d{4})" as Regex,
    since_date: r"Seit\W+(\d{1,2}\.\d{1,2}\.\d{4}).*" as Regex,
    location: "div.field--name-field-location div.field__item",
    resource: "div.s-media--type--document",
    external_link: "div.field--name-field-links a[href], div.field--name-field-link a[href]",
    doi: ".field--name-field-doi .field__item",
    url: "a.s-file[href]",
    numbering_year_and_quotes: r#"(\d+(\.|nd|th|rd))|((19|20)\d{2})|(\")"# as Regex,
    whitespace_before_parentheses_or_domain_ending: r"(\s\))|(\.de)" as Regex,
}
