use std::cmp::Ordering;

use anyhow::{Result, anyhow, ensure};
use cap_std::fs::Dir;
use poppler::Document as PdfDocument;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{SmallVec, smallvec};
use time::{Date, format_description::FormatItem, macros::format_description};

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors,
    utilities::{
        collect_pages, collect_text, make_key, make_suffix_key, select_first_text, select_text,
        uppercase_words,
    },
    write_dataset,
};
use metadaten::dataset::{
    Dataset, Language, License, Organisation, OrganisationRole, Person, PersonRole, Region,
    Resource, ResourceType, TimeRange,
    r#type::{TextType, Type},
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let mut entries = fetch_sitemap(client, source, selectors).await?;

    let mut leichte_sprache = fetch_leichte_sprache(client, source, selectors).await?;
    entries.append(&mut leichte_sprache);

    entries.sort_unstable_by(|lhs, rhs| lhs.link.cmp(&rhs.link));
    entries.dedup_by(|lhs, rhs| lhs.link == rhs.link);

    let mut counts = entries.len();

    let (mut results, mut errors) = fetch_many(0, 0, entries, |entry| async move {
        if entry.link.contains("aktuelles") {
            fetch_news_links(dir, client, source, selectors, entry).await
        } else {
            write_mainlink(dir, client, source, selectors, entry).await
        }
    })
    .await;

    let (count1, results1, errors1) = fetch_publications(dir, client, source, selectors).await?;
    counts += count1;
    results += results1;
    errors += errors1;

    Ok((counts, results, errors))
}

async fn fetch_sitemap(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<MainLink>> {
    let url = source.url.join("ueber-uns/sitemap")?;

    let text = client
        .fetch_text(source, "sitemap".to_owned(), &url)
        .await?;

    let document = Html::parse_document(&text);

    let entries = document
        .select(&selectors.sitemap_li)
        .flat_map(|element| {
            element.select(&selectors.sitemap_links).map(|element| {
                let link = element.attr("href").unwrap().to_owned();
                let title = element.attr("title").unwrap().to_owned();

                MainLink { title, link }
            })
        })
        .collect();

    Ok(entries)
}

async fn fetch_leichte_sprache(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<MainLink>> {
    let url = source.url.join("ueber-uns/leichte-sprache")?;

    let text = client
        .fetch_text(source, "leichte_sprache".to_owned(), &url)
        .await?;

    let document = Html::parse_document(&text);

    let entries = document
        .select(&selectors.leichte_sprache_links)
        .map(|element| {
            let link = element.attr("href").unwrap().to_owned();
            let title = collect_text(element.text());

            MainLink { title, link }
        })
        .collect();

    Ok(entries)
}

async fn fetch_news_links(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: MainLink,
) -> Result<(usize, usize, usize)> {
    let mut news_links = Vec::new();

    let mut batch_size = 0;
    let mut page = 1;

    loop {
        let mut news_links_set =
            fetch_news_page(client, source, selectors, &item.link, page).await?;

        let batch_count = news_links_set.len();

        news_links.append(&mut news_links_set);

        match batch_count.cmp(&batch_size) {
            Ordering::Greater => {
                batch_size = batch_count;
                page += 1;
            }
            Ordering::Equal => {
                page += 1;
            }
            Ordering::Less => {
                break;
            }
        }
    }

    let (results, errors) = fetch_many(0, 0, news_links, |news_link| {
        write_news_link(dir, client, source, selectors, news_link)
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_news_page(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: &str,
    page: u32,
) -> Result<Vec<PageLink>> {
    let url = source.url.join(&format!("{}/seite-{}", &link, page))?;

    let key = make_key(url.as_str()).into_owned();

    let text = client.fetch_text(source, key, &url).await?;

    let document = Html::parse_document(&text);

    let pagelinks = document
        .select(&selectors.news_page_item)
        .map(|element| {
            let link = element
                .select(&selectors.news_page_link_item)
                .next()
                .ok_or_else(|| anyhow!("Missing news link on page {page}"))?;
            let href = link.attr("href").unwrap();
            let link = source.url.join(href)?.into();

            let title = select_text(element, &selectors.news_page_title);
            let description = select_text(element, &selectors.news_page_description);

            let date = select_first_text(element, &selectors.news_page_date, "date")
                .ok()
                .as_ref()
                .map(|text| Date::parse(text, DATE_FORMAT))
                .transpose()?;

            Ok(PageLink {
                date,
                title,
                description,
                link,
            })
        })
        .collect::<Result<Vec<_>>>()?;

    ensure!(!pagelinks.is_empty(), "Empty news page");

    Ok(pagelinks)
}

async fn write_news_link(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: PageLink,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.link).into_owned();

    let dataset = {
        let url = source.url.join(&item.link)?;

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let mut resources = document
            .select(&selectors.news_resource)
            .filter_map(|element| {
                let mut href = element.attr("href").unwrap().to_owned();

                if !href.contains("http") {
                    href = format!("{}{}", source.url, href);
                };

                let mut description = collect_text(element.text()).trim().to_owned();

                if description.contains("© LfU") {
                    description = select_text(&document, &selectors.article_figcaption);
                }

                if !description.contains("lfu.rlp.de") {
                    Some(
                        Resource {
                            r#type: ResourceType::WebPage,
                            description: Some(description),
                            url: href,
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    )
                } else {
                    None
                }
            })
            .collect::<SmallVec<_>>();

        let mut description = select_text(&document, &selectors.mainlink_description);

        if description.is_empty() {
            description = item.description;
        };

        let persons = extract_contact(&document, selectors)?;

        resources.extend(document.select(&selectors.video).map(|element| {
            let video_link = element.attr("src").unwrap();
            let video_description = "Zum Youtube-Video".to_owned();

            Resource {
                r#type: ResourceType::Video,
                description: Some(video_description),
                url: video_link.to_owned(),
                ..Default::default()
            }
        }));

        Dataset {
            title: item.title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::News,
            }],
            resources,
            persons,
            issued: item.date,
            regions: smallvec![Region::RLP],
            source_url: url.into(),
            origins: source.origins.clone(),
            language: Language::German,
            license: License::OtherClosed,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn write_mainlink(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: MainLink,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.link).into_owned();

    let dataset = {
        let url = source.url.join(&item.link)?;

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let title = {
            let mut title = select_text(&document, &selectors.mainlink_header);

            if title.is_empty() {
                title = item.title;
            }

            if title == "FAQ" {
                "FAQ der Radon-Informationsstelle Rheinland-Pfalz".to_owned()
            } else if title == "Glossar" {
                "Glossar der Radon-Informationsstelle Rheinland-Pfalz".to_owned()
            } else if title.contains("Häufig gestellte Fragen") {
                "Landesamt für Umwelt Rheinland-Pfalz: Häufig gestellte Fragen".to_owned()
            } else {
                title
            }
        };

        let mut description = select_text(&document, &selectors.editorial_description);

        if description.is_empty() {
            description =
                select_first_text(&document, &selectors.mainlink_description, "description")
                    .unwrap_or_default();
        }

        let persons = extract_contact(&document, selectors)?;

        let resources = document
            .select(&selectors.article_resources)
            .filter_map(|element| {
                let href = element.attr("href").unwrap();
                let url = match source.url.join(href) {
                    Ok(url) => url.into(),
                    Err(err) => return Some(Err(err.into())),
                };

                let mut description = collect_text(element.text()).trim().to_owned();
                if description.contains("© LfU") {
                    description = select_text(&document, &selectors.article_figcaption);
                }
                if description.contains("lfu.rlp.de") {
                    return None;
                }

                Some(Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url,
                    ..Default::default()
                }
                .guess_or_keep_type()))
            })
            .collect::<Result<_>>()?;

        let language = if item.link.contains("leichte-sprache") {
            Language::GermanEasy
        } else {
            Language::German
        };

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Editorial,
            }],
            resources,
            persons,
            regions: smallvec![Region::RLP],
            source_url: url.into(),
            origins: source.origins.clone(),
            language,
            license: License::OtherClosed,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_publications(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let mut publications = Vec::new();

    let mut next_link = Some("service/bestellkatalog/list?currentPage=1".to_owned());

    while let Some(link) = next_link {
        let url = source.url.join(&link)?;
        let key = make_key(&link).into_owned();

        let text = client.fetch_text(source, key, &url).await?;

        let document = Html::parse_document(&text);

        next_link = document
            .select(&selectors.publications_next_link)
            .next()
            .map(|elem| elem.attr("href").unwrap().to_owned());

        publications.extend(
            document
                .select(&selectors.publications_result_item)
                .map(|element| {
                    let title = select_first_text(
                        element,
                        &selectors.publications_title,
                        "Publication title",
                    )?;

                    let description = select_first_text(
                        element,
                        &selectors.publications_subtitle,
                        "Publication description",
                    )?;

                    let source_url = element
                        .select(&selectors.publications_download)
                        .next()
                        .or_else(|| element.select(&selectors.publications_title).next())
                        .ok_or_else(|| anyhow!("No source URL or title link found in publication"))?
                        .attr("href")
                        .unwrap()
                        .to_owned();

                    let prop_text = select_text(element, &selectors.publication_prop_text);

                    let year = selectors
                        .publication_prop_text_year
                        .captures(&prop_text)
                        .map(|captures| captures["year"].parse::<i32>())
                        .transpose()?;

                    let publisher = selectors
                        .publication_prop_text_publisher
                        .captures(&prop_text)
                        .map(|captures| captures["publisher"].to_owned());

                    Ok(PublicationItem {
                        title,
                        description,
                        source_url: source_url.clone(),
                        year,
                        publisher,
                    })
                })
                .collect::<Result<Vec<PublicationItem>>>()?,
        );
    }

    let counts = publications.len();

    let (results, errors) = fetch_many(0, 0, publications, |publication| async move {
        let time_ranges = publication
            .year
            .into_iter()
            .map(TimeRange::whole_year)
            .collect::<Result<_>>()?;

        let organisations = publication
            .publisher
            .into_iter()
            .map(|publisher| Organisation::Other {
                name: publisher,
                role: OrganisationRole::Publisher,
                websites: SmallVec::new(),
            })
            .collect();

        let source_url = publication.source_url;
        let key = make_suffix_key(&source_url, &source.url).into_owned();

        let description = if source_url.ends_with(".pdf") {
            let url = source.url.join(&source_url)?;

            let bytes = client
                .fetch_pdf(source, format!("pdf-{}.isol", key), &url)
                .await?;

            let document = PdfDocument::from_bytes(&bytes, None)?;

            let text = collect_pages(&document, 0..5);

            Some(format!("{} [Redaktioneller Hinweis: Die folgende Beschreibung ist eine unstrukturierte Extraktion aus dem originalem PDF] {}", publication.description, text))
        } else {
            Some(publication.description)
        };

        let dataset = Dataset {
            title: publication.title,
            description,
            types: smallvec![Type::Text {
                text_type: TextType::Publication,
            }],
            organisations,
            time_ranges,
            source_url: source.url.join(&source_url)?.to_string(),
            origins: source.origins.clone(),
            license: License::AllRightsReserved,
            language: Language::German,
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((counts, results, errors))
}

fn extract_contact(document: &Html, selectors: &Selectors) -> Result<Vec<Person>> {
    let mut contacts = Vec::new();

    for element in document.select(&selectors.gray_box_contacts) {
        if !collect_text(element.text()).contains("lfu.rlp.de") {
            continue;
        }

        let contact_form = element
            .select(&selectors.gray_box_contacts_link)
            .map(|elem| collect_text(elem.text()))
            .find(|name| name.contains("lfu.rlp.de"))
            .unwrap_or_else(|| "kein Kontaktname".to_owned());

        let contact_form = contact_form.replace("(at)", "@");
        let contact_form = contact_form.replace("[at]", "@");

        let (name, _) = contact_form
            .split_once("@")
            .ok_or_else(|| anyhow!("No contact"))?;

        let name = name.replace(".", " ").replace("%C2%AD", "");

        let name = uppercase_words(&name);

        contacts.push(Person {
            name,
            role: PersonRole::Contact,
            emails: smallvec![contact_form.to_owned()],
            ..Default::default()
        });
    }

    Ok(contacts)
}

#[derive(Debug)]
struct MainLink {
    title: String,
    link: String,
}

#[derive(Debug)]
struct PageLink {
    title: String,
    link: String,
    date: Option<Date>,
    description: String,
}

#[derive(Debug)]
struct PublicationItem {
    title: String,
    description: String,
    source_url: String,
    year: Option<i32>,
    publisher: Option<String>,
}

const DATE_FORMAT: &[FormatItem] = format_description!("[day].[month].[year]");

selectors! {
    sitemap_links: "a[href][title]",
    sitemap_li : "div.container.frame > ul li:has(ul)",

    mainlink_header: ".row > .col-12 header",
    mainlink_description: ".frame-type-container_accordion .col-12,
                        .col-first .frame-type-textmedia .ce-bodytext,
                        .ce-image-right .ce-bodytext, 
                        .MsoPlainText, .text-black.mb-6, h5.mb-6,
                        .mb-7.news-text-wrap, 
                        .container-bg-gray-bright h2",

    editorial_description: ".frame-type-container_accordion .col-12",

    article_resources: ".frame-type-textmedia .ce-image-left .ce-bodytext a[href],
    				.row .ce-bodytext a[href],
    				.col-last .frame-gray-bright.frame-type-textmedia a[href],
    				.frame-gray-bright.frame-type-uploads a[href]",
    article_figcaption: ".col-last figcaption",

    gray_box_contacts: ".col-last .ce-textpic > .ce-bodytext > p",
    gray_box_contacts_link: "a[href*='#']",

    news_page_item: ".list-group-item",
    news_page_title: "h2",
    news_page_date: "time",
    news_page_link_item: ".text-decoration-none",
    news_page_description: ".text, .news-text-wrap",

    news_resource: ".ce-bodytext a[href], .col-first a[href], .mb-7.news-text-wrap a[href]",

    leichte_sprache_links: ".ce-bodytext li a[href]",

    publications_next_link:  "a[href][title^='Nächste']",
    publications_result_item: ".results-list-publications-item",
    publications_title: ".title a[href]",
    publications_subtitle: ".subtitle",
    publications_download: ".publication-download[href]",
    publication_prop_text: ".title-box+ p",
    publication_prop_text_year: r"Erscheinungsjahr:\s+(?<year>\d+)" as Regex,
    publication_prop_text_publisher: r"Kurzinfo:\s+(?:(?:Herausgeber:)|(?:Herausgegeben vo[nm])?)\s+(?<publisher>.+)" as Regex,

    video: "div.mediaelement-video iframe[src^='https://www.youtube']",
}
