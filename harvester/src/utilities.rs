use std::borrow::Cow;
use std::hash::BuildHasher;
use std::str::FromStr;

use anyhow::{Error, Result, anyhow};
use compact_str::CompactString;
use geo::{Coord, Rect};
use once_cell::sync::Lazy;
use poppler::Document;
use regex::Regex;
use rustc_hash::FxBuildHasher;
use scraper::{Selector, selectable::Selectable};
use shapefile::dbase::Date as DbaseDate;
use time::{Date, Month, error::Parse as ParseError, macros::format_description, parsing::Parsed};
#[cfg(not(feature = "regression-test"))]
use time::{Duration, OffsetDateTime};
use url::Url;

use metadaten::dataset::{Region, TimeRange};

#[macro_export]
macro_rules! selectors {
    {
        $( $name:ident : $( $value:literal ),+ $( as $type:ident )? , )*
    } => {
        struct Selectors {
            $( $name: selectors!( @type $( $value ),+ $( $type )? ), )*
        }

        impl Default for Selectors {
            fn default() -> Self {
                Self {
                    $( $name: selectors!( @parse $( $value ),+ $( $type )? ), )*
                }
            }
        }
    };

    ( @type $value:literal ) => { Selector };

    ( @type $value:literal Regex ) => { Regex };

    ( @type $( $value:literal ),+ RegexSet ) => { RegexSet };

    ( @parse $value:literal ) => { Selector::parse( $value ).unwrap() };

    ( @parse $value:literal Regex ) => { Regex::new( $value ).unwrap() };

    ( @parse $( $value:literal ),+  RegexSet ) => { RegexSet::new([ $( $value ),+ ]).unwrap() };
}

pub fn collect_text<'a, I>(iter: I) -> String
where
    I: IntoIterator<Item = &'a str>,
{
    let mut res = String::new();

    for val in iter {
        let val = val.trim();

        if !val.is_empty() {
            if !res.is_empty() {
                res.push(' ');
            }

            res.push_str(val);
        }
    }

    res
}

pub fn collect_pages<P>(document: &Document, pages: P) -> String
where
    P: IntoIterator<Item = i32>,
{
    let mut res = String::new();

    let num_pages = document.n_pages();

    for index in pages {
        if index >= num_pages {
            break;
        }

        let text = document.page(index).and_then(|page| page.text());

        if let Some(text) = text {
            if !res.is_empty() {
                res.push_str("\n\n");
            }

            res.push_str(&text);
        }
    }

    res
}

pub fn select_first_text<'a, S>(selectable: S, selector: &'a Selector, name: &str) -> Result<String>
where
    S: Selectable<'a>,
{
    selectable
        .select(selector)
        .next()
        .ok_or_else(|| anyhow!("Missing {name}"))
        .map(|element| collect_text(element.text()))
}

pub fn select_text<'a, S>(selectable: S, selector: &'a Selector) -> String
where
    S: Selectable<'a>,
{
    collect_text(
        selectable
            .select(selector)
            .flat_map(|element| element.text()),
    )
}

pub fn parse_texts<'a, S, T>(
    selectable: S,
    selector: &'a Selector,
    regex: &'a Regex,
    name: &'a str,
) -> impl Iterator<Item = Result<T>> + 'a
where
    S: Selectable<'a> + 'a,
    T: FromStr,
    Error: From<T::Err>,
{
    selectable.select(selector).map(move |element| {
        let text = element.text().collect::<String>();

        let captures = regex
            .captures(&text)
            .ok_or_else(|| anyhow!("Malformed {name}"))?;

        let value = captures[1].parse::<T>()?;

        Ok(value)
    })
}

pub fn parse_text<'a, S, T>(
    selectable: S,
    selector: &'a Selector,
    regex: &'a Regex,
    name: &'a str,
) -> Result<T>
where
    S: Selectable<'a> + 'a,
    T: FromStr,
    Error: From<T::Err>,
{
    parse_texts(selectable, selector, regex, name)
        .next()
        .ok_or_else(|| anyhow!("Missing {name}"))?
}

pub fn parse_attributes<'a, S, T>(
    selectable: S,
    selector: &'a Selector,
    regex: &'a Regex,
    attribute: &'a str,
    name: &'a str,
) -> impl Iterator<Item = Result<T>> + 'a
where
    S: Selectable<'a> + 'a,
    T: FromStr,
    Error: From<T::Err>,
{
    selectable.select(selector).map(move |element| {
        let value = regex
            .captures(element.attr(attribute).unwrap())
            .ok_or_else(|| anyhow!("Malformed {name}"))?;

        let value = value[1].parse::<T>()?;

        Ok(value)
    })
}

pub fn parse_attribute<'a, S, T>(
    selectable: S,
    selector: &'a Selector,
    regex: &'a Regex,
    attribute: &'a str,
    name: &'a str,
) -> Result<T>
where
    S: Selectable<'a> + 'a,
    T: FromStr,
    Error: From<T::Err>,
{
    parse_attributes(selectable, selector, regex, attribute, name)
        .next()
        .ok_or_else(|| anyhow!("Missing {name}"))?
}

pub fn make_key(key: &str) -> Cow<'_, str> {
    static KEY: Lazy<Regex> = Lazy::new(|| Regex::new(r"[^-[[:alnum:]]]").unwrap());

    let key = KEY.replace_all(key, "-");

    // Prevent "too long filenames" error while making sure to not create collisions between different keys.
    if key.len() > 235 {
        let hash = FxBuildHasher.hash_one(key.as_ref());

        Cow::Owned(format!("{key:.215}-{hash:016x}"))
    } else {
        key
    }
}

pub fn make_suffix_key<'a>(key: &'a str, source_url: &Url) -> Cow<'a, str> {
    let suffix_key = key.strip_prefix(source_url.as_str()).unwrap_or(key);

    make_key(suffix_key)
}

/// Uppercase first letter and letters after whitespace and punctuation
pub fn uppercase_words(text: &str) -> String {
    let mut result = String::with_capacity(text.len());

    let mut word = true;

    for char_ in text.chars() {
        if word {
            result.extend(char_.to_uppercase());
        } else {
            result.push(char_);
        }

        word = !char_.is_alphanumeric();
    }

    result
}

pub fn strip_jsessionid(url: &mut Url) {
    let old_path = url.path();

    let new_path = old_path
        .split_once(";jsessionid=")
        .map_or(old_path, |(prefix, _suffix)| prefix)
        .to_owned();

    url.set_path(&new_path);
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GermanDate(Date);

impl GermanDate {
    fn parse_numeric_month(text: &str) -> Result<Self> {
        static DATE: Lazy<Regex> =
            Lazy::new(|| Regex::new(r"\d{1,2}\.\s*\d{1,2}\.\s*\d{2,4}").unwrap());

        let match_ = DATE
            .find(text)
            .ok_or_else(|| anyhow!("Malformed date: {text}"))?;

        let date = Date::parse(
            match_.as_str(),
            format_description!(
                version = 2,
                "[first [[day]] [[day padding:none]]].[optional [ ]][first [[month]] [[month padding:none]]].[optional [ ]][year]"
            ),
        )?;

        Ok(Self(date))
    }

    fn parse_julian_month(text: &str) -> Result<Self> {
        static DATE: Lazy<Regex> = Lazy::new(|| {
            Regex::new(r"(?<day>\d{1,2})\.?\s*(?<month>\w+)\.?\s*(?<year>\d{4})").unwrap()
        });

        let captures = DATE
            .captures(text)
            .ok_or_else(|| anyhow!("Malformed date: {text}"))?;

        let day = captures["day"].parse()?;
        let month = captures["month"].parse::<GermanMonth>()?.into();
        let year = captures["year"].parse()?;

        let date = Date::from_calendar_date(year, month, day)?;

        Ok(Self(date))
    }
}
impl From<GermanDate> for Date {
    fn from(val: GermanDate) -> Self {
        val.0
    }
}

impl From<GermanDate> for TimeRange {
    fn from(at: GermanDate) -> Self {
        at.0.into()
    }
}

impl FromStr for GermanDate {
    type Err = Error;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        Self::parse_numeric_month(text).or_else(|_err| Self::parse_julian_month(text))
    }
}

#[derive(Debug, Clone, Copy)]
pub struct GermanMonth(Month);

impl From<GermanMonth> for Month {
    fn from(val: GermanMonth) -> Self {
        val.0
    }
}

impl FromStr for GermanMonth {
    type Err = Error;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        let val = match text {
            "Januar" => Month::January,
            "Februar" => Month::February,
            "März" => Month::March,
            "April" => Month::April,
            "Mai" => Month::May,
            "Juni" => Month::June,
            "Juli" => Month::July,
            "August" => Month::August,
            "September" => Month::September,
            "Oktober" => Month::October,
            "November" => Month::November,
            "Dezember" => Month::December,
            "Jan" => Month::January,
            "Feb" => Month::February,
            "Apr" => Month::April,
            "Jun" => Month::June,
            "Jul" => Month::July,
            "Aug" => Month::August,
            "Sep" => Month::September,
            "Okt" => Month::October,
            "Nov" => Month::November,
            "Dez" => Month::December,
            text => return Err(anyhow!("Unknown German month name `{text}`")),
        };

        Ok(Self(val))
    }
}

pub fn parse_short_year(base_year: i32, text: &str) -> Result<Date, ParseError> {
    let mut parsed = Parsed::new();
    parsed.parse_items(
        text.as_bytes(),
        format_description!("[day].[month].[year repr:last_two]"),
    )?;

    if let Some(year) = parsed.year_last_two() {
        parsed.set_year(base_year + year as i32);
    }

    let date = parsed.try_into()?;

    Ok(date)
}

pub fn parse_year_only(text: &str) -> Result<Date, ParseError> {
    let mut parsed = Parsed::new();
    parsed.parse_items(text.as_bytes(), format_description!("[year]"))?;

    parsed.set_ordinal(1.try_into().unwrap());

    let date = parsed.try_into()?;

    Ok(date)
}

#[cfg(not(feature = "regression-test"))]
pub fn yesterday() -> Date {
    static YESTERDAY: Lazy<Date> = Lazy::new(|| {
        // HACK: We have no time zone data for `Europe/Berlin`, but subtracting two hours will always suffice
        // as we harvest between 01:00 and 02:00 local time which is between 23:00 or 01:00 UTC.
        OffsetDateTime::now_utc()
            .checked_sub(2 * Duration::HOUR)
            .unwrap()
            .date()
    });

    *YESTERDAY
}

#[cfg(feature = "regression-test")]
pub fn yesterday() -> Date {
    Date::from_calendar_date(2024, Month::June, 11).unwrap()
}

pub fn since_year(start_year: i32) -> Result<TimeRange> {
    let from = Date::from_calendar_date(start_year, Month::January, 1)?;
    let until = yesterday();

    Ok(TimeRange { from, until })
}

pub fn from_dbase_date(date: DbaseDate) -> Result<Date> {
    let year = date.year().try_into()?;
    let month = Month::try_from(date.month() as u8)?;
    let day = date.day().try_into()?;

    let date = Date::from_calendar_date(year, month, day)?;

    Ok(date)
}

/// Parses coordinates of the form 52° 7.424' N 9° 18.393' E
pub fn parse_german_coord(text: &str) -> Result<Coord> {
    static COORD: Lazy<Regex> = Lazy::new(|| {
        Regex::new(
            r"(?x)
    (?<lat_deg>\d+) °\s*
    (?<lat_min>[\d\.]+) '\s*
    N\s*
    (?<lon_deg>\d+) °\s*
    (?<lon_min>[\d\.]+) '\s*
    E",
        )
        .unwrap()
    });

    let captures = COORD
        .captures(text)
        .ok_or_else(|| anyhow!("Invalid German coordinate: {text}"))?;

    let lat_deg = captures["lat_deg"].parse::<f64>()?;
    let lat_min = captures["lat_min"].parse::<f64>()?;
    let lon_deg = captures["lon_deg"].parse::<f64>()?;
    let lon_min = captures["lon_min"].parse::<f64>()?;

    let y = lat_deg + lat_min / 60.0;
    let x = lon_deg + lon_min / 60.0;

    Ok(Coord { x, y })
}

pub fn remove_pom_suffix(text: &str) -> Cow<str> {
    static SUFFIXES: Lazy<Regex> =
        Lazy::new(|| Regex::new(r"(?:\s|_)(?:UP|OP|AP|BP|UW|OW|SHW|EP|1)$").unwrap());

    SUFFIXES.replace_all(text, "")
}

pub fn point_like_bounding_box(lat: f64, lon: f64) -> Rect {
    let coord = Coord { x: lon, y: lat };
    Rect::new(coord, coord)
}

/// Resolves the given `region` but uses it only if it is contained in `default`.
pub fn contains_or<R>(region: R, default: Region) -> Region
where
    R: Into<CompactString>,
{
    match Region::Other(region.into()).align_in(Some(&default)) {
        None => default,
        Some(Region::Other(_)) => default,
        Some(region) => region,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use scraper::Html;

    #[test]
    fn collect_text_removes_white_space() {
        assert_eq!(
            collect_text(vec!["\tfoo bar\n", "\t\n", "\tbaz\n"]),
            "foo bar baz"
        );
    }

    #[test]
    fn collect_text_removes_white_space_from_html_fragments() {
        let fragment = Html::parse_fragment(
            r#"<p>
	<b>This text is bold.</b>
	This text is plain.
</p>"#,
        );

        assert_eq!(
            fragment.root_element().text().collect::<Vec<_>>(),
            ["\n\t", "This text is bold.", "\n\tThis text is plain.\n"]
        );

        assert_eq!(
            collect_text(fragment.root_element().text()),
            "This text is bold. This text is plain."
        );
    }

    #[test]
    fn strip_jsessionid_works() {
        let mut url = Url::parse("http://foo/bar;jsessionid=2342?baz=qux").unwrap();
        strip_jsessionid(&mut url);
        assert_eq!(url.as_str(), "http://foo/bar?baz=qux");
    }

    #[test]
    fn german_date_works() {
        let date = "01.01.2023".parse::<GermanDate>().unwrap().0;
        assert_eq!(
            date,
            Date::from_calendar_date(2023, Month::January, 1).unwrap()
        );

        let date = "1.01.2023".parse::<GermanDate>().unwrap().0;
        assert_eq!(
            date,
            Date::from_calendar_date(2023, Month::January, 1).unwrap()
        );

        let date = "01.1.2023".parse::<GermanDate>().unwrap().0;
        assert_eq!(
            date,
            Date::from_calendar_date(2023, Month::January, 1).unwrap()
        );

        let date = "1.1.2023".parse::<GermanDate>().unwrap().0;
        assert_eq!(
            date,
            Date::from_calendar_date(2023, Month::January, 1).unwrap()
        );

        let date = "1. März 2023".parse::<GermanDate>().unwrap().0;
        assert_eq!(
            date,
            Date::from_calendar_date(2023, Month::March, 1).unwrap()
        );

        let date = "01.Jan.2023".parse::<GermanDate>().unwrap().0;
        assert_eq!(
            date,
            Date::from_calendar_date(2023, Month::January, 1).unwrap()
        );

        let date = "01. 01.2023".parse::<GermanDate>().unwrap().0;
        assert_eq!(
            date,
            Date::from_calendar_date(2023, Month::January, 1).unwrap()
        );

        let date = "01.01. 2023".parse::<GermanDate>().unwrap().0;
        assert_eq!(
            date,
            Date::from_calendar_date(2023, Month::January, 1).unwrap()
        );

        let date = "Di, den 01.01.2023 - um 12:00"
            .parse::<GermanDate>()
            .unwrap()
            .0;
        assert_eq!(
            date,
            Date::from_calendar_date(2023, Month::January, 1).unwrap()
        );
    }

    #[test]
    fn parse_short_year_works() {
        assert_eq!(
            parse_short_year(2000, "13.01.24").unwrap(),
            Date::from_calendar_date(2024, Month::January, 13).unwrap()
        );
    }

    #[test]
    fn parse_german_coord_works() {
        assert_eq!(
            parse_german_coord("52° 7.424' N 9° 18.393' E").unwrap(),
            Coord {
                x: 9. + 18.393 / 60.,
                y: 52. + 7.424 / 60.
            }
        );
    }

    #[test]
    fn remove_pom_suffix_works() {
        assert_eq!(remove_pom_suffix("Messpunkt UP"), "Messpunkt");
        assert_eq!(remove_pom_suffix("Messpunkt_UP"), "Messpunkt");
        assert_eq!(remove_pom_suffix("MesspunktUP"), "MesspunktUP");
        assert_eq!(remove_pom_suffix("Messpunkt UP LHW"), "Messpunkt UP LHW");
    }

    #[test]
    fn uppercase_words_works() {
        assert_eq!(
            uppercase_words("this Word  should.be,übergroß\t❤ ❤️"),
            "This Word  Should.Be,Übergroß\t❤ ❤️"
        );
    }

    #[test]
    fn make_key_handles_long_paths() {
        assert_eq!(
            make_key("/a/very/long/URL/path/that/would/exceed/the/path/length/limit/of/the/operation/system/so/that/we/need/to/truncate/it/while/avoiding/to/introduce/collisions/yet/keeping/as/much/as/possible/of/the/human/readable/part/in/the/final/key/which/is/why/we/replace/a/part/of/the/key/by/its/hash").as_ref(),
            "-a-very-long-URL-path-that-would-exceed-the-path-length-limit-of-the-operation-system-so-that-we-need-to-truncate-it-while-avoiding-to-introduce-collisions-yet-keeping-as-much-as-possible-of-the-human-readable-part--a9054e90dda8ae37",
        );
    }
}
