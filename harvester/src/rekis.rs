use anyhow::{Result, anyhow, ensure};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_json::from_str;
use smallvec::{SmallVec, smallvec};
use url::Url;

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors, try_fetch,
    utilities::{collect_text, make_key},
    write_dataset,
};
use metadaten::dataset::{
    Dataset, Language, License, Resource, ResourceType,
    r#type::{TextType, Type},
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let source_url = &Url::parse(source.source_url())?;
    let selectors = &Selectors::default();

    const MEASUREMENTS: &[(&str, &str, &str)] = &[
        ("st", "viewer/ST/st_datasets.json", "Sachsen-Anhalt"),
        ("th", "viewer/TN/tn_datasets.json", "Thüringen"),
        ("sn", "viewer/SN/sn_datasets.json", "Sachsen"),
    ];

    let mut count = MEASUREMENTS.len();

    let (results, errors) = fetch_many(0, 0, MEASUREMENTS, |(key, path, land)| {
        fetch_measurements(dir, client, source, key, path, land)
    })
    .await;

    const ARTICLES: &[(&str, &str)] = &[
        ("wissen/sachsen-w", "Sachsen"),
        ("wissen/thueringen", "Thüringen"),
        ("wissen/sachsen-anhalt", "Sachsen-Anhalt"),
        ("kommunal/sachsen-k", "Sachsen"),
        ("kommunal/thueringen", "Thüringen"),
        ("kommunal/sachsen-anhalt", "Sachsen-Anhalt"),
    ];

    count += ARTICLES.len();

    let menu_links = AtomicRefCell::new(Vec::new());

    let (results, errors) = fetch_many(results, errors, ARTICLES, |(path, land)| {
        fetch_article(
            dir,
            client,
            source,
            source_url,
            selectors,
            &menu_links,
            path,
            land,
        )
    })
    .await;

    let menu_links = menu_links.into_inner();

    count += menu_links.len();

    let (results, errors) = fetch_many(results, errors, menu_links, |(path, land)| {
        fetch_menu_link(dir, client, source, source_url, selectors, path, land)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_measurements(
    dir: &Dir,
    client: &Client,
    source: &Source,
    key: &str,
    path: &str,
    land: &str,
) -> Result<(usize, usize, usize)> {
    let mut count = 0;
    let mut results = 0;
    let mut errors = 0;

    let key = format!("measurements-json-{key}");
    let url = source.url.join(path)?;

    let mut top_level_topic = {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        from_str::<Topic>(&text)?
    };

    ensure!(top_level_topic.name == land);

    match &top_level_topic.children[..] {
        [child] if child.name == "Beobachtungsdaten" => {
            top_level_topic = top_level_topic.children.pop().unwrap();
        }
        _ => (),
    }

    for mut topic in top_level_topic.children {
        let key = format!("{key}-{}", make_key(&topic.name));

        let title = format!(
            "Regionales Klimainformationssystem (REKIS): {} {land}",
            topic.name
        );

        let mut resources = SmallVec::new();

        let mut make_topic_resources = |parent_name: &str, topic: Topic| {
            if let Some(data_url) = topic.data_url {
                resources.push(
                    Resource {
                        r#type: ResourceType::Unknown,
                        description: Some(format!("Daten zu {} {}", parent_name, topic.name)),
                        url: data_url,
                        ..Default::default()
                    }
                    .guess_or_keep_type(),
                );
            }

            for resource in topic.download_urls {
                resources.push(
                    Resource {
                        r#type: ResourceType::Unknown,
                        description: Some(format!("{} {}", topic.name, resource.name)),
                        url: resource.url,
                        ..Default::default()
                    }
                    .guess_or_keep_type(),
                );
            }
        };

        for mut sub_topic in topic.children.drain(..) {
            for sub_sub_topic in sub_topic.children.drain(..) {
                make_topic_resources(&sub_topic.name, sub_sub_topic);
            }

            make_topic_resources(&topic.name, sub_topic);
        }

        make_topic_resources("", topic);

        let dataset = Dataset {
            title,
            types: smallvec![Type::MapService],
            resources,
            origins: source
                .origins
                .iter()
                .map(|origin| origin.as_ref().replace("<land>", land).into())
                .collect(),
            source_url: url.clone().into(),
            license: License::OtherClosed,
            language: Language::German,
            ..Default::default()
        };

        try_fetch(
            &mut count,
            &mut results,
            &mut errors,
            write_dataset(dir, client, source, key, dataset),
        )
        .await;
    }

    Ok((count, results, errors))
}

#[allow(clippy::too_many_arguments)]
async fn fetch_article<'a>(
    dir: &Dir,
    client: &Client,
    source: &Source,
    source_url: &Url,
    selectors: &Selectors,
    menu_links: &AtomicRefCell<Vec<(String, &'a str)>>,
    path: &str,
    land: &'a str,
) -> Result<(usize, usize, usize)> {
    let key = path.replace('/', "-");
    let url = source_url.join(path)?;

    let title;
    let description;

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        menu_links.borrow_mut().extend(
            document
                .select(&selectors.menu_link)
                .map(|element| element.attr("href").unwrap().to_owned())
                .map(|path| (path, land)),
        );

        title = collect_text(
            document
                .select(&selectors.headline)
                .next()
                .ok_or_else(|| anyhow!("Missing title on page {path}"))?
                .text(),
        );

        description = collect_text(
            document
                .select(&selectors.teaser_text)
                .flat_map(|element| element.text()),
        );
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        types: smallvec![Type::Text {
            text_type: TextType::Editorial
        }],
        origins: source
            .origins
            .iter()
            .map(|origin| origin.as_ref().replace("<land>", land).into())
            .collect(),
        source_url: url.into(),
        license: License::OtherClosed,
        language: Language::German,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_menu_link(
    dir: &Dir,
    client: &Client,
    source: &Source,
    source_url: &Url,
    selectors: &Selectors,
    path: String,
    land: &str,
) -> Result<(usize, usize, usize)> {
    let url = source_url.join(&path)?;
    let key = url.path().replace(['/'], "-");

    let title;
    let description;
    let mut resources = SmallVec::new();

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        title = if let Some(headline) = document.select(&selectors.headline).next() {
            collect_text(headline.text())
        } else {
            let title = collect_text(
                document
                    .select(&selectors.title)
                    .next()
                    .ok_or_else(|| anyhow!("Missing title on page {path}"))?
                    .text(),
            );

            format!("{title} ({land})")
        };

        description = collect_text(
            document
                .select(&selectors.teaser_text)
                .flat_map(|element| element.text()),
        );

        for element in document.select(&selectors.button_link) {
            let url = url.join(element.attr("href").unwrap())?;

            let description =
                Some(collect_text(element.text())).filter(|description| description != "LINK");

            resources.push(
                Resource {
                    r#type: ResourceType::WebPage,
                    description,
                    url: url.into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        types: smallvec![Type::Text {
            text_type: TextType::Editorial
        }],
        resources,
        origins: source
            .origins
            .iter()
            .map(|origin| origin.as_ref().replace("<land>", land).into())
            .collect(),
        source_url: url.into(),
        license: License::OtherClosed,
        language: Language::German,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug, Deserialize, Clone)]
struct Topic {
    name: String,
    #[serde(default)]
    children: Vec<Topic>,
    data_url: Option<String>,
    #[serde(default)]
    download_urls: Vec<TopicResource>,
}

#[derive(Debug, Deserialize, Clone)]
struct TopicResource {
    name: String,
    url: String,
}

selectors! {
    title: "head title",
    top_level_item: "body > li[info_text]",
    link: "a[href]",
    headline: "main div.o-globalElements--headline",
    teaser_text: "main div.o-infoTeaser--text",
    menu_link: "ul#menu-hauptmenue-1 > li > ul.sub-menu > li > a[href]",
    button_link: "div.o-globalElements--buttons a[href]",
}
