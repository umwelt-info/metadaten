use anyhow::Result;
use cap_std::fs::Dir;
use scraper::{Html, Selector};
use smallvec::smallvec;
use time::{Date, macros::format_description};

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key, select_text},
    write_dataset,
};
use metadaten::dataset::{
    Dataset, Language, License, Organisation, OrganisationRole, Resource, ResourceType,
    r#type::{TextType, Type},
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let news_list = fetch_news_list(client, source, selectors).await?;
    let mut count = news_list.len();
    let (results, errors) = fetch_many(0, 0, news_list, |news_item| {
        translate_news_dataset(dir, client, source, news_item, selectors)
    })
    .await;

    let publication_list = fetch_publication_list(client, source, selectors).await?;
    count += publication_list.len();
    let (results, errors) = fetch_many(results, errors, publication_list, |publication_item| {
        translate_publication_dataset(dir, client, source, publication_item)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_news_list(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<Item>> {
    let url = source.url.join("news/")?;

    let text = client
        .fetch_text(source, "news_list".to_owned(), &url)
        .await?;
    let document = Html::parse_document(&text);

    let item_list = document
        .select(&selectors.news_item)
        .map(|element| {
            let href = element.attr("href").unwrap();
            let title = collect_text(element.text());

            let url = href.trim_start_matches("../").to_owned();

            Ok(Item { url, title })
        })
        .collect::<Result<Vec<_>>>()?;

    Ok(item_list)
}

async fn translate_news_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    item: Item,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.url).into_owned();
    let url = source.url.join(&item.url)?;

    let text = client.fetch_text(source, key.clone(), &url).await?;
    let document = Html::parse_document(&text);

    let description = select_text(&document, &selectors.news_description);
    let date = select_text(&document, &selectors.news_date);

    let issued = Date::parse(
        &date,
        format_description!("[month repr:long] [day padding:none], [year]"),
    )?;

    let provider = Organisation::WikiData {
        identifier: 119010386, // GRDC
        role: OrganisationRole::Publisher,
    };

    let dataset = Dataset {
        title: item.title,
        description: Some(description),
        types: smallvec![Type::Text {
            text_type: TextType::News,
        }],
        issued: Some(issued),
        organisations: smallvec![provider],
        language: Language::English,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url: url.into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_publication_list(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<Item>> {
    let url = source.url.join("publications/reports/")?;

    let text = client
        .fetch_text(source, "publication_list".to_owned(), &url)
        .await?;
    let document = Html::parse_document(&text);

    let item_list = document
        .select(&selectors.publication_all)
        .map(|element| {
            let href = element.attr("href").unwrap();
            let title = collect_text(element.text());

            let url = href.trim_start_matches("../").to_owned();

            Ok(Item { url, title })
        })
        .collect::<Result<Vec<_>>>()?;

    Ok(item_list)
}

async fn translate_publication_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.title).into_owned();
    let url = source.url.join(&item.url)?;

    let description = "Until 2015, GRDC has published annual reports including GRDC meeting reports. Those can be accessed via the link.".to_owned();

    let provider = Organisation::WikiData {
        identifier: 119010386, // GRDC
        role: OrganisationRole::Publisher,
    };

    let resources = smallvec![Resource {
        r#type: ResourceType::Pdf,
        description: Some("Report of the GRDC".to_owned()),
        url: url.to_string(),
        primary_content: true,
        ..Default::default()
    }];

    let dataset = Dataset {
        title: item.title,
        description: Some(description),
        types: smallvec![Type::Text {
            text_type: TextType::Report,
        }],
        resources,
        organisations: smallvec![provider],
        language: Language::English,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url: url.into(),
        machine_readable_source: false,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug, Clone)]
struct Item {
    title: String,
    url: String,
}

selectors! {
    publication_all: ".table a[href]",
    publication_link: "a",
    news_item: ".listing-title .no-external",
    news_description: ".quarto-figure-center+ p, p+ p",
    news_date: ".date",
}
