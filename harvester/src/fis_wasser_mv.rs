use std::fmt::Write;

use anyhow::Result;
use cap_std::fs::Dir;
use compact_str::CompactString;
use encoding_rs::WINDOWS_1252;
use hashbrown::HashMap;

use miniproj::{Projection, get_projection};
use serde::Deserialize;
use serde_roxmltree::from_str;
use smallvec::{SmallVec, smallvec};
use time::{Date, Month};

use harvester::{
    Source,
    client::Client,
    fetch_many,
    utilities::{GermanDate, point_like_bounding_box},
    write_dataset,
};
use metadaten::{
    dataset::{
        Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
        SourceUrlExplainer,
        r#type::{Domain, Station, Type},
    },
    proj_rect,
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let proj = get_projection(5650).unwrap();

    let source_url = source
        .source_url
        .as_deref()
        .expect("No source URL configured");

    let (mut count, mut results, mut errors) =
        fetch_groundwater(dir, client, source, proj, source_url).await?;

    let (count1, results1, errors1) = fetch_levels(dir, client, source, proj).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_groundwater(
    dir: &Dir,
    client: &Client,
    source: &Source,
    proj: &dyn Projection,
    source_url: &str,
) -> Result<(usize, usize, usize)> {
    // XML file that contains the data on stations that measure groundwater table
    let gw_response = {
        let mut url = source.url.join("/script/mv_a7_hydrogeologie_wfs.php?")?;
        url.query_pairs_mut()
            .append_pair("REQUEST", "GetFeature")
            .append_pair("VERSION", "1.1.0")
            .append_pair("SERVICE", "WFS")
            .append_pair("TYPENAME", "t7_gw_stand");

        let key = "groundwater-wfs".to_owned();
        let bytes = client.fetch_bytes(source, key, &url).await?;
        let (text, _, _) = WINDOWS_1252.decode(&bytes);
        from_str::<GwStand>(&text)?
    };

    // XML file that contains the data on stations that measure groundwater chemistry
    let chem_response = {
        let mut url = source.url.join("/script/mv_a3_messnetze_wfs.php?")?;
        url.query_pairs_mut()
            .append_pair("REQUEST", "GetFeature")
            .append_pair("VERSION", "1.1.0")
            .append_pair("SERVICE", "WFS")
            .append_pair("TYPENAME", "t3_fisg_mn_chm_gw");

        let key = "chemistry-wfs".to_owned();
        let bytes = client.fetch_bytes(source, key, &url).await?;
        let (text, _, _) = WINDOWS_1252.decode(&bytes);
        from_str::<GwChem>(&text)?
    };

    let mut stations = HashMap::<String, (Option<ChemData>, Option<Data>)>::new();

    for station in chem_response.chem_station {
        let entry = stations.entry_ref(&station.data.station_id).or_default();
        entry.0 = Some(station.data);
    }

    for station in gw_response.gw_station {
        let entry = stations.entry_ref(&station.data.station_id).or_default();
        entry.1 = Some(station.data);
    }

    let count = stations.len();

    let (results, errors) = fetch_many(0, 0, stations, |(station_id, data)| async move {
        let mut title = "Grundwassermessstelle ID ".to_owned();
        let mut description = None;

        let mut time_ranges = SmallVec::new();
        let bounding_boxes;
        let mut resources = SmallVec::new();
        let mut measured_variables_chem = SmallVec::new();
        let mut measured_variables_gw = SmallVec::new();

        // matching whether groundwater data, chemical data or both are available for the measurement station
        match data {
            (Some(chem_data), Some(gw_data)) => {
                let name = gw_data.name;
                let anzahl = gw_data.anzahl;

                write!(&mut title, "{station_id} in {name}")?;
                description = Some(format!("Es sind {anzahl} Grundwassermessstände vorhanden."));

                bounding_boxes = smallvec![proj_rect(
                    point_like_bounding_box(gw_data.hochwert, gw_data.rechtswert),
                    proj,
                )];

                if !gw_data.start_date.is_empty() {
                    let start_date = gw_data.start_date.parse::<GermanDate>()?;
                    let end_date = gw_data.end_date.parse::<GermanDate>()?;
                    time_ranges.push((start_date, end_date).into());
                }

                measured_variables_chem.extend(
                    CHEM_MEASUREMENTS
                        .iter()
                        .map(|&measured_variable| measured_variable.to_owned()),
                );
                measured_variables_gw.push("Grundwasserstand".to_owned());

                // the link for the resource is created by us and may not always exist
                // this needs to be handled in the upcoming user story 732
                resources.push(Resource {
                    r#type: ResourceType::Pdf,
                    description: Some("Steckbrief der Grundwassermessstelle".to_owned()),
                    url: format!("{source_url}/doku/gwk_steckbr/{station_id}.pdf"),
                    ..Default::default()
                });
                resources.push(Resource {
                    r#type: ResourceType::Pdf,
                    description: Some(
                        "Schematische Darstellung der Grundwassermessstelle".to_owned(),
                    ),
                    url: format!("{source_url}/doku/SVZ/{station_id}.PDF"),
                    ..Default::default()
                });
                if let Some(resource_link) = chem_data.chart_s1 {
                    resources.push(Resource {
                        r#type: ResourceType::WebPage,
                        description: Some("Tabelle der gemessenen Stoffe (Auswahl)".to_owned()),
                        url: resource_link,
                        primary_content: true,
                        ..Default::default()
                    });
                }
                if let Some(resource_link) = chem_data.chart_m1 {
                    resources.push(Resource {
                        r#type: ResourceType::WebPage,
                        description: Some("Tabelle der gemessenen Stoffe (vollständig)".to_owned()),
                        url: resource_link,
                        primary_content: true,
                        ..Default::default()
                    });
                }
            }
            (Some(chem_data), None) => {
                let name = chem_data.name;

                write!(&mut title, "{station_id} in {name}")?;

                bounding_boxes = smallvec![proj_rect(
                    point_like_bounding_box(chem_data.hochwert, chem_data.rechtswert),
                    proj,
                )];

                let start_date = Date::from_calendar_date(chem_data.start_year, Month::January, 1)?;
                let end_date = Date::from_calendar_date(chem_data.end_year, Month::December, 31)?;
                time_ranges.push((start_date, end_date).into());

                measured_variables_chem.extend(
                    CHEM_MEASUREMENTS
                        .iter()
                        .map(|&measured_variable| measured_variable.to_owned()),
                );

                if let Some(resource_link) = chem_data.chart_s1 {
                    resources.push(Resource {
                        r#type: ResourceType::WebPage,
                        description: Some("Tabelle der gemessenen Stoffe (Auswahl)".to_owned()),
                        url: resource_link,
                        primary_content: true,
                        ..Default::default()
                    });
                }
                if let Some(resource_link) = chem_data.chart_m1 {
                    resources.push(Resource {
                        r#type: ResourceType::WebPage,
                        description: Some("Tabelle der gemessenen Stoffe (vollständig)".to_owned()),
                        url: resource_link,
                        primary_content: true,
                        ..Default::default()
                    });
                }
            }
            (None, Some(gw_data)) => {
                let name = gw_data.name;
                let anzahl = gw_data.anzahl;

                write!(&mut title, "{station_id} in {name}")?;
                description = Some(format!("Es sind {anzahl} Grundwassermessstände vorhanden."));

                bounding_boxes = smallvec![proj_rect(
                    point_like_bounding_box(gw_data.hochwert, gw_data.rechtswert),
                    proj,
                )];

                if !gw_data.start_date.is_empty() {
                    let start_date = gw_data.start_date.parse::<GermanDate>()?;
                    let end_date = gw_data.end_date.parse::<GermanDate>()?;
                    time_ranges.push((start_date, end_date).into());
                }

                measured_variables_gw.push("Grundwasserstand".to_owned());

                // the link for the resource is created by us and may not always exist
                // this needs to be handled in the upcoming user story 732
                resources.push(Resource {
                    r#type: ResourceType::Pdf,
                    description: Some("Steckbrief der Grundwassermessstelle".to_owned()),
                    url: format!("{source_url}/doku/gwk_steckbr/{station_id}.pdf"),
                    ..Default::default()
                });
                resources.push(Resource {
                    r#type: ResourceType::Pdf,
                    description: Some(
                        "Schematische Darstellung der Grundwassermessstelle".to_owned(),
                    ),
                    url: format!("{source_url}/doku/SVZ/{station_id}.PDF"),
                    ..Default::default()
                });
            }
            (None, None) => unreachable!(),
        }

        let mut types = SmallVec::new();

        if !measured_variables_chem.is_empty() {
            types.push(Type::Measurements {
                domain: Domain::Chemistry,
                station: Some(Station {
                    id: Some(station_id.as_str().into()),
                    ..Default::default()
                }),
                measured_variables: measured_variables_chem,
                methods: Default::default(),
            });
        }

        if !measured_variables_gw.is_empty() {
            types.push(Type::Measurements {
                domain: Domain::Groundwater,
                station: Some(Station {
                    id: Some(station_id.as_str().into()),
                    ..Default::default()
                }),
                measured_variables: measured_variables_gw,
                methods: Default::default(),
            });
        }

        let dataset = Dataset {
            title,
            description,
            types,
            regions: smallvec![Region::MV],
            bounding_boxes,
            time_ranges,
            resources,
            language: Language::German,
            license: License::CcBySa30,
            organisations: make_organisations(),
            origins: source.origins.clone(),
            source_url: source.url.clone().into(),
            source_url_explainer: SourceUrlExplainer::CopyStationId(
                "Übersichtskarte: bitte dort die Suchfunktion nutzen (ID wird automatisch kopiert)"
                    .to_owned(),
            ),
            machine_readable_source: true,
            ..Default::default()
        };

        write_dataset(dir, client, source, station_id, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_levels(
    dir: &Dir,
    client: &Client,
    source: &Source,
    proj: &dyn Projection,
) -> Result<(usize, usize, usize)> {
    // XML file that contains the data on levels
    let levels_response = {
        let mut url = source.url.join("/script/mv_a3_messnetze_wfs.php?")?;
        url.query_pairs_mut()
            .append_pair("REQUEST", "GetFeature")
            .append_pair("VERSION", "1.1.0")
            .append_pair("SERVICE", "WFS")
            .append_pair("TYPENAME", "t3_fisg_mn_pegel_fg_sg");

        let key = "levels-wfs".to_owned();
        let bytes = client.fetch_bytes(source, key, &url).await?;
        let (text, _, _) = WINDOWS_1252.decode(&bytes);
        from_str::<Levels>(&text)?
    };

    let count = levels_response.level_station.len();

    let (results, errors) = fetch_many(0, 0, levels_response.level_station, |station| async move {
        let title = format!(
            "Pegelmessstelle ID {} in {}",
            station.data.station_id, station.data.name
        );

        let description = if !station.data.body_name.is_empty() {
            Some(format!("Gewässer {}", station.data.body_name))
        } else if !station.data.lake_name.is_empty() {
            Some(format!("See {}", station.data.lake_name))
        } else {
            None
        };

        let profile_url = if !station.data.profile.is_empty() {
            station
                .data
                .profile
                .replace("www.pegelportal-mv.de", "pegelportal-mv.de")
        } else {
            format!(
                "https://pegelportal-mv.de/pdf/pegelsteckbrief_{}.pdf",
                station.data.station_id
            )
        };

        let chart_w_url = if !station.data.chart_w.is_empty() {
            station
                .data
                .chart_w
                .replace("www.pegelportal-mv.de", "pegelportal-mv.de")
        } else {
            format!("https://pegelportal-mv.de/{}.html", station.data.station_id)
        };

        let chart_q_url = if !station.data.chart_q.is_empty() {
            station
                .data
                .chart_q
                .replace("www.pegelportal-mv.de", "pegelportal-mv.de")
        } else {
            format!(
                "https://pegelportal-mv.de/{}-q.html",
                station.data.station_id
            )
        };

        let resources = smallvec![
            Resource {
                r#type: ResourceType::Pdf,
                description: Some("Steckbrief".to_owned()),
                url: profile_url,
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::InteractivePlot,
                description: Some("Ganglinie Wasserstand".to_owned()),
                url: chart_w_url,
                primary_content: true,
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::InteractivePlot,
                description: Some("Ganglinie Durchfluss".to_owned()),
                url: chart_q_url,
                primary_content: true,
                ..Default::default()
            }
        ];

        let domain = match station.data.r#type.as_ref() {
            "f" => Domain::Rivers,
            "s" => Domain::Surfacewater,
            _ => Domain::Water,
        };

        let types = smallvec![Type::Measurements {
            domain,
            station: Some(Station {
                id: Some(station.data.station_id),
                ..Default::default()
            }),
            measured_variables: smallvec!["Pegelstand".to_owned()],
            methods: Default::default(),
        }];

        let (longitude, latitude) =
            proj.projected_to_deg(station.data.rechtswert, station.data.hochwert);
        let bounding_boxes = smallvec![point_like_bounding_box(latitude, longitude)];

        let mut regions = smallvec![Region::MV];
        regions.extend(WISE.match_shape(longitude, latitude).map(Region::Watershed));

        let dataset = Dataset {
            title,
            description,
            types,
            regions,
            bounding_boxes,
            resources,
            language: Language::German,
            license: License::CcBySa30,
            organisations: make_organisations(),
            origins: source.origins.clone(),
            source_url: source.url.clone().into(),
            source_url_explainer: SourceUrlExplainer::CopyStationId(
                "Übersichtskarte: bitte dort die Suchfunktion nutzen (ID wird automatisch kopiert)"
                    .to_owned(),
            ),
            machine_readable_source: true,
            ..Default::default()
        };

        write_dataset(dir, client, source, station.data.id, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

fn make_organisations() -> SmallVec<[Organisation; 2]> {
    smallvec![
        Organisation::Other {
            name: "Staatliche Ämter für Landwirtschaft und Umwelt Mecklenburg-Vorpommern"
                .to_owned(),
            role: OrganisationRole::Management,
            websites: smallvec!["https://www.stalu-mv.de/".to_owned()],
        },
        Organisation::Other {
            name: "Landesamt für Umwelt, Naturschutz und Geologie Mecklenburg-Vorpommern"
                .to_owned(),
            role: OrganisationRole::Publisher,
            websites: smallvec!["https://www.lung.mv-regierung.de/".to_owned()],
        },
    ]
}

// up to 200 chemical solutes in the groundwater are measured
// they are too much to mention them all but I got an overview of the most important measured solutes from the department
const CHEM_MEASUREMENTS: &[&str] = &[
    "Temperatur",
    "Sauerstoff",
    "Färbung",
    "Trübung",
    "Geruch",
    "Leitfähigkeit",
    "pH-Wert",
    "Redox-Potential",
    "Säurekapazität",
    "Basekapazität",
    "Calcium",
    "Magnesium",
    "Natrium",
    "Kalium",
    "Chlorid",
    "Sulfat",
    "Eisen gesamt",
    "Mangan",
    "Ammonium",
    "Nitrat",
    "Nitrit",
    "Orthophosphat",
    "DOC",
    "Bor",
    "Aluminium",
    "Schwermetalle",
    "Arsen",
    "Pflanzenschutzmittel",
    "Arzneimittel",
    "Röntgenkontrastmittel",
    "Flüchtige Organische Verbindungen (VOC)",
];

#[derive(Debug, Deserialize)]
#[serde(rename = "FeatureCollection")]
struct GwStand {
    #[serde(rename = "featureMember")]
    gw_station: Vec<MeasurementStation>,
}

#[derive(Debug, Deserialize)]
struct MeasurementStation {
    #[serde(rename = "t7_gw_stand")]
    data: Data,
}

#[derive(Debug, Deserialize)]
struct Data {
    #[serde(rename = "MESSSTELLE")]
    name: String,
    #[serde(rename = "KENNZAHL")]
    station_id: String,
    #[serde(rename = "ANZAHL")]
    anzahl: String,
    #[serde(rename = "RECHTSWERT")]
    rechtswert: f64,
    #[serde(rename = "HOCHWERT")]
    hochwert: f64,
    #[serde(rename = "ERSTDAT")]
    start_date: String,
    #[serde(rename = "LASTDAT")]
    end_date: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename = "FeatureCollection")]
struct GwChem {
    #[serde(rename = "featureMember")]
    chem_station: Vec<ChemStation>,
}

#[derive(Debug, Deserialize)]
struct ChemStation {
    #[serde(rename = "t3_fisg_mn_chm_gw")]
    data: ChemData,
}

#[derive(Debug, Deserialize)]
struct ChemData {
    #[serde(rename = "MS_NAME")]
    name: String,
    #[serde(rename = "MS_NR")]
    station_id: String,
    #[serde(rename = "RE")]
    rechtswert: f64,
    #[serde(rename = "HO")]
    hochwert: f64,
    #[serde(rename = "PN_VON")]
    start_year: i32,
    #[serde(rename = "PN_BIS")]
    end_year: i32,
    #[serde(rename = "CHART_S1")]
    chart_s1: Option<String>,
    #[serde(rename = "CHART_M1")]
    chart_m1: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename = "FeatureCollection")]
struct Levels {
    #[serde(rename = "featureMember")]
    level_station: Vec<LevelStation>,
}

#[derive(Debug, Deserialize)]
struct LevelStation {
    #[serde(rename = "t3_fisg_mn_pegel_fg_sg")]
    data: LevelData,
}

#[derive(Debug, Deserialize)]
struct LevelData {
    #[serde(rename = "ID")]
    id: String,
    #[serde(rename = "MS_NAME")]
    name: String,
    #[serde(rename = "GWK_GN")]
    body_name: String,
    #[serde(rename = "SEE_GN")]
    lake_name: String,
    #[serde(rename = "MS_NR")]
    station_id: CompactString,
    #[serde(rename = "TYP")]
    r#type: CompactString,
    #[serde(rename = "RE")]
    rechtswert: f64,
    #[serde(rename = "HO")]
    hochwert: f64,
    #[serde(rename = "STECKBRIEF")]
    profile: String,
    #[serde(rename = "GANGLIN_W")]
    chart_w: String,
    #[serde(rename = "GANGLIN_Q")]
    chart_q: String,
}
