use std::io::{Cursor, Read};

use anyhow::{Error, Result, anyhow};
use calamine::{Data, DataType, Reader, Xlsx, open_workbook_from_rs};
use cap_std::fs::Dir;
use smallvec::smallvec;
use time::{Date, Month};
use zip::ZipArchive;

use harvester::{
    Source,
    client::Client,
    fetch_many,
    utilities::{make_key, point_like_bounding_box},
    write_dataset,
};
use metadaten::{
    dataset::{
        Dataset, Language, License, Organisation, OrganisationRole, Region, SourceUrlExplainer,
        r#type::{Domain, Station, Type},
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let station_list = fetch_station_list(client, source).await?;

    let count = station_list.len();

    let (results, errors) = fetch_many(0, 0, station_list, |station| {
        translate_station_dataset(dir, client, source, station)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_station_list(client: &Client, source: &Source) -> Result<Vec<Item>> {
    let url = source.url.join("grdc/grdc_stations.zip")?;

    let bytes = client
        .make_request(
            source,
            "station_list".to_owned(),
            Some(&url),
            |client| async {
                let bytes = client
                    .get(url.clone())
                    .send()
                    .await?
                    .error_for_status()?
                    .bytes()
                    .await?;

                let mut archive = ZipArchive::new(Cursor::new(&*bytes))?;

                let mut bytes = Vec::new();

                archive
                    .by_name("GRDC_Stations.xlsx")?
                    .read_to_end(&mut bytes)?;

                Ok::<_, Error>(bytes)
            },
        )
        .await?;

    let mut workbook: Xlsx<_> = open_workbook_from_rs(Cursor::new(&*bytes))?;
    let worksheet = workbook.worksheet_range("station_catalogue")?;

    let station_list = worksheet
        .rows()
        .filter(|row| row[5] == "DE")
        .map(|row| Item {
            station_id: row[0].to_string(),
            river: row[3].to_string(),
            name: row[4].to_string(),
            lat: row[6].clone(),
            lon: row[7].clone(),
            start_year: row[18].clone(),
            end_year: row[19].clone(),
        })
        .collect::<Vec<_>>();

    Ok(station_list)
}

async fn translate_station_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.station_id).into_owned();

    let url = source
        .url
        .join("applications/public.html?publicuser=PublicUser#dataDownload/Stations")?;

    let title = format!(
        "Messstation {} ('Station Number': {}) am Fluss {}",
        item.name, item.station_id, item.river
    );
    let description = format!(
        r#"Dieser Datensatz enthält Abflussdaten der Messstation {} am Fluss {}.
        Die Daten werden bereit gestellt vom Global Runoff Data Centre (GRDC).
        Um die Rohdaten für diese Station zu erhalten, muss das Data Portal des GRDC ({}) verwendet werden.
        Dort kann man entweder anhand des 'Station Name': {} oder der 'Station Number': {} nach den Daten suchen."#,
        item.name, item.river, url, item.name, item.station_id
    );

    let types = smallvec![Type::Measurements {
        domain: Domain::Rivers,
        station: Some(Station {
            id: Some(item.station_id.into()),
            ..Default::default()
        }),
        measured_variables: smallvec!["Abfluss".to_owned()],
        methods: Default::default(),
    }];

    let region = Region::Other(item.name.into());
    let mut regions = smallvec![region];

    let lat = item.lat.as_f64().ok_or_else(|| anyhow!("Missing lat"))?;
    let lon = item.lon.as_f64().ok_or_else(|| anyhow!("Missing lon"))?;

    let bounding_boxes = smallvec![point_like_bounding_box(lat, lon)];
    regions.extend(WISE.match_shape(lon, lat).map(Region::Watershed));

    // data source only contains start and end year of the time series
    let start_year = item
        .start_year
        .as_i64()
        .ok_or_else(|| anyhow!("Missing start year"))?;
    let end_year = item
        .end_year
        .as_i64()
        .ok_or_else(|| anyhow!("Missing start year"))?;

    let start_date = Date::from_calendar_date(start_year as i32, Month::January, 1)?;
    let end_date = Date::from_calendar_date(end_year as i32, Month::December, 31)?;
    let time_ranges = smallvec![(start_date, end_date).into()];

    let provider = Organisation::WikiData {
        identifier: 119010386, // GRDC
        role: OrganisationRole::Publisher,
    };

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        bounding_boxes,
        regions,
        time_ranges,
        organisations: smallvec![provider],
        language: Language::English,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url: url.into(),
        source_url_explainer: SourceUrlExplainer::CopyStationId(
            "zum Datenportal: bitte dort die Suchfunktion nutzen (Station Number wird automatisch kopiert)".to_owned(),
        ),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug, Clone)]
struct Item {
    river: String,
    name: String,
    station_id: String,
    lat: Data,
    lon: Data,
    start_year: Data,
    end_year: Data,
}
