use anyhow::{Result, anyhow, ensure};
use cap_std::fs::Dir;
use regex::Regex;
use scraper::{CaseSensitivity, Html, Selector};
use smallvec::{SmallVec, smallvec};
use time::{Date, format_description::well_known::Iso8601};

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors,
    utilities::{
        GermanDate, collect_text, make_key, parse_attributes, select_first_text, select_text,
        strip_jsessionid,
    },
    write_dataset,
};
use metadaten::dataset::{Dataset, Language, License, Region, Resource, ResourceType};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (results, errors) =
        fetch_search(dir, client, source, selectors, "", Language::German).await?;

    let (results1, errors1) =
        fetch_search(dir, client, source, selectors, "EN/", Language::English).await?;

    Ok((results + results1, results + results1, errors + errors1))
}

async fn fetch_search(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    path: &str,
    def_lang: Language,
) -> Result<(usize, usize)> {
    let (pages, results, errors) =
        fetch_search_page(dir, client, source, selectors, path, def_lang, 1).await?;

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| {
        fetch_search_page(dir, client, source, selectors, path, def_lang, page)
    })
    .await;

    Ok((results, errors))
}

async fn fetch_search_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    path: &str,
    def_lang: Language,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let pages;
    let articles;

    {
        let key = format!("search-{}{}", path.replace('/', "-"), page);

        let mut url = source.url.join(&format!(
            "SiteGlobals/Forms/Suche/Expertensuche/{path}Servicesuche_Formular.html"
        ))?;

        url.query_pairs_mut()
            .append_pair("gtp", &format!("154060_list%3D{page}"))
            .append_pair("resultsPerPage", &source.batch_size.to_string());

        let text = client.fetch_text(source, key, &url).await?;

        let document = Html::parse_document(&text);

        pages = parse_attributes(
            &document,
            &selectors.pages,
            &selectors.pages_value,
            "href",
            "pagination button",
        )
        .map(|res| res.unwrap_or(1))
        .max()
        .unwrap_or(1);

        articles = document
            .select(&selectors.articles)
            .map(|article| {
                let title = select_first_text(article, &selectors.title, "title")?;

                let href = article
                    .select(&selectors.link)
                    .next()
                    .ok_or_else(|| anyhow!("Missing link"))?
                    .attr("href")
                    .unwrap()
                    .to_owned();

                Ok(Article { href, title })
            })
            .collect::<Vec<Result<_>>>();
    }

    ensure!(!articles.is_empty(), "No articles found");

    let (results, errors) = fetch_many(0, 0, articles, |article| async move {
        fetch_article(dir, client, source, selectors, def_lang, article?).await
    })
    .await;

    Ok((pages, results, errors))
}

struct Article {
    href: String,
    title: String,
}

async fn fetch_article(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    def_lang: Language,
    article: Article,
) -> Result<(usize, usize, usize)> {
    let mut url = source.url.join(&article.href)?;
    strip_jsessionid(&mut url);

    let key = make_key(url.path().trim_start_matches('/').trim_end_matches(".html")).into_owned();

    let description;
    let mut start: Option<GermanDate> = None;
    let mut end: Option<GermanDate> = None;
    let mut regions = SmallVec::new();
    let modified;
    let mut resources = SmallVec::new();

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        let content = document
            .select(&selectors.content)
            .next()
            .ok_or_else(|| anyhow!("Missing content on page {}", url.path()))?;

        description = select_text(content, &selectors.description);
        if description.contains("Lorem ipsum") {
            return Ok((1, 0, 0));
        }

        let mut properties = content.select(&selectors.properties);

        loop {
            let label = match properties.next() {
                Some(element)
                    if element
                        .value()
                        .has_class("label", CaseSensitivity::AsciiCaseInsensitive) =>
                {
                    collect_text(element.text())
                }
                _ => break,
            };

            let value = match properties.next() {
                Some(element)
                    if element
                        .value()
                        .has_class("value", CaseSensitivity::AsciiCaseInsensitive) =>
                {
                    collect_text(element.text())
                }
                _ => break,
            };

            match label.as_str() {
                "Anfang" => {
                    let value = value.split_ascii_whitespace().next().unwrap_or(&value);
                    start = Some(value.parse()?);
                }
                "Ende" => {
                    let value = value.split_ascii_whitespace().next().unwrap_or(&value);
                    end = Some(value.parse()?);
                }
                "Veranstaltungsort" => {
                    regions.push(Region::Other(value.into()));
                }
                _ => (),
            }
        }

        modified = content
            .select(&selectors.modified)
            .next()
            .map(|meta| {
                let content = meta.attr("content").unwrap();
                Date::parse(content, &Iso8601::DEFAULT)
            })
            .transpose()?;

        for attachment in content.select(&selectors.attachments) {
            let url = source.url.join(attachment.attr("href").unwrap())?;
            let description = collect_text(attachment.text());

            let (description, primary_content) = if description.starts_with("Herunterladen") {
                (None, true)
            } else {
                (Some(description), false)
            };

            resources.push(Resource {
                r#type: ResourceType::Pdf,
                description,
                url: url.into(),
                primary_content,
                ..Default::default()
            });
        }

        for video in document.select(&selectors.videos) {
            let description =
                select_first_text(video, &selectors.video_description, "video description")?;

            let video_source = video
                .select(&selectors.video_source)
                .next()
                .ok_or_else(|| anyhow!("Missing video source"))?;

            let url = source.url.join(video_source.attr("src").unwrap())?;

            resources.push(
                Resource {
                    r#type: ResourceType::Video,
                    description: Some(description),
                    url: url.into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            )
        }
    }

    let time_ranges = if let (Some(start), Some(end)) = (start, end) {
        smallvec![(start, end).into()]
    } else if let Some(start) = start {
        smallvec![(start, start).into()]
    } else {
        Default::default()
    };

    let language = if url.as_str().contains("/leichte-sprache/") {
        Language::GermanEasy
    } else {
        def_lang
    };

    let dataset = Dataset {
        title: article.title,
        description: Some(description),
        time_ranges,
        regions,
        modified,
        resources,
        language,
        license: License::AllRightsReserved,
        source_url: url.into(),
        origins: source.origins.clone(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

selectors! {
    pages: "a.c-pagination__button.is-go-to-index[href]",
    pages_value: r"list%253D(\d+)" as Regex,
    articles: "div.c-teaser-search",
    title: "h3.c-teaser-search__title",
    link: "a.c-teaser-search__link[href]",
    content: "div#content",
    description: "div.l-content-wrapper",
    properties: "p.docData > *:not(br)",
    modified: "meta[property='og:updated_time'][content]",
    attachments: "a[href*='.pdf']",
    videos: "div.mejs__wrapper",
    video_description: "p[id^='video-headline']",
    video_source: "video > source[src]",
}
