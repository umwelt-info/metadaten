use anyhow::{Result, anyhow};
use cap_std::fs::Dir;
use hashbrown::HashSet;
use itertools::Itertools;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{SmallVec, smallvec};
use time::{Date, format_description::well_known::Iso8601};

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors,
    utilities::{
        collect_text, make_key, parse_attribute, select_first_text, select_text, yesterday,
    },
    write_dataset,
};
use metadaten::dataset::{
    Dataset, License, Organisation, OrganisationRole, Person, PersonRole, Resource, ResourceType,
    r#type::{TextType, Type},
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) =
        fetch_activities(dir, client, source, selectors).await?;

    let (count1, results1, errors1) =
        fetch_products_or_projects(dir, client, source, selectors, "de/ergebnisse/produkte")
            .await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) =
        fetch_products_or_projects(dir, client, source, selectors, "de/ergebnisse/zuarbeiten")
            .await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_activities(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let (last_page, results, errors) =
        fetch_activities_page(dir, client, source, selectors, 0).await?;

    let (results, errors) = fetch_many(results, errors, 1..=last_page, |page| {
        fetch_activities_page(dir, client, source, selectors, page)
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_activities_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let last_page;
    let links;

    {
        let mut url = source.url.join("/de/aktivitaeten")?;

        url.query_pairs_mut().append_pair("page", &page.to_string());

        let text = client
            .fetch_text(source, format!("activities-{page}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        last_page = parse_attribute(
            &document,
            &selectors.activities_last_page,
            &selectors.activities_last_page_value,
            "href",
            "last page",
        )
        .unwrap_or(0);

        links = document
            .select(&selectors.activities_links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<HashSet<_>>();
    }

    let (results, errors) = fetch_many(0, 0, links, |href| {
        fetch_activity(dir, client, source, selectors, href)
    })
    .await;

    Ok((last_page, results, errors))
}

async fn fetch_activity(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    href: String,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join(&href)?;

    let key = make_key(&href).into_owned();

    let title;
    let description;
    let time_ranges;
    let persons;
    let organisations;
    let mut resources = SmallVec::new();

    {
        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        title = select_first_text(&document, &selectors.activity_title, "title")?;

        description = select_text(&document, &selectors.activity_description);

        let dates = document
            .select(&selectors.activity_dates)
            .map(|element| {
                let datetime = element.attr("datetime").unwrap();

                Date::parse(datetime, &Iso8601::DEFAULT)
            })
            .collect::<Result<SmallVec<[_; 2]>, _>>()?;

        time_ranges = match dates[..] {
            [from] => smallvec![(from, yesterday()).into()],
            [from, until] => smallvec![(from, until).into()],
            _ => SmallVec::new(),
        };

        persons = document
            .select(&selectors.activity_contacts)
            .filter_map(|element| {
                let forename =
                    select_first_text(element, &selectors.activity_contact_forename, "forename");
                let surname =
                    select_first_text(element, &selectors.activity_contact_surname, "surname");

                let name = forename.into_iter().chain(surname.into_iter()).join(" ");
                if name.is_empty() {
                    return None;
                }

                let emails = element
                    .select(&selectors.activity_contact_emails)
                    .map(|element| collect_text(element.text()))
                    .collect();

                Some(Person {
                    name,
                    role: PersonRole::Contact,
                    emails,
                    ..Default::default()
                })
            })
            .collect();

        organisations = document
            .select(&selectors.activity_organisations)
            .map(|element| {
                let name = collect_text(element.text());

                let website = source.url.join(element.attr("href").unwrap())?;

                Ok(Organisation::Other {
                    name,
                    role: OrganisationRole::Unknown,
                    websites: smallvec![website.into()],
                })
            })
            .collect::<Result<_>>()?;

        for element in document.select(&selectors.activity_attachments) {
            let url = source.url.join(element.attr("href").unwrap())?;

            let description = collect_text(element.text());

            resources.push(
                Resource {
                    r#type: ResourceType::Document,
                    description: Some(description),
                    url: url.into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }

        for element in document.select(&selectors.activity_websites) {
            let url = source.url.join(element.attr("href").unwrap())?;

            resources.push(Resource {
                r#type: ResourceType::WebPage,
                description: None,
                url: url.into(),
                ..Default::default()
            });
        }
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        time_ranges,
        persons,
        organisations,
        resources,
        license: License::AllRightsReserved,
        source_url: url.into(),
        types: smallvec![Type::Event],
        origins: source.origins.clone(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_products_or_projects(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    href: &str,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join(href)?;

    let links = {
        let key = make_key(href).into_owned();

        let text = client.fetch_text(source, key, &url).await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.products_or_projects_links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>()
    };

    let count = links.len();

    let (results, errors) = fetch_many(0, 0, links, |link| async move {
        let url = source.url.join(&link)?;

        let key = make_key(&link).into_owned();

        let title;
        let description;
        let issued;
        let resources;

        {
            let text = client
                .fetch_text(source, format!("{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            title = select_first_text(&document, &selectors.product_or_project_title, "title")?;

            description = select_first_text(
                &document,
                &selectors.product_or_project_description,
                "description",
            )?;

            issued = document
                .select(&selectors.product_or_project_issued)
                .next()
                .map(|element| {
                    let datetime = element.attr("datetime").unwrap();

                    Date::parse(datetime, &Iso8601::DEFAULT)
                })
                .ok_or_else(|| anyhow!("Missing issued date"))??;

            resources = document
                .select(&selectors.product_or_project_resources)
                .map(|element| {
                    let href = element.attr("href").unwrap();

                    let description = collect_text(element.text());

                    Ok(Resource {
                        r#type: ResourceType::Unknown,
                        description: Some(description),
                        url: source.url.join(href)?.into(),
                        ..Default::default()
                    })
                })
                .collect::<Result<_>>()?;
        }

        let dataset = Dataset {
            title,
            description: Some(description),
            issued: Some(issued),
            resources,
            license: License::AllRightsReserved,
            source_url: url.into(),
            types: smallvec![Type::Text {
                text_type: TextType::Report
            }],
            origins: source.origins.clone(),
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

selectors! {
    activities_last_page: "nav li.pager__item--last a[href]",
    activities_last_page_value: r"\?page=(\d+)" as Regex,
    activities_links: "div.view-content article h2 a[href]",
    activity_title: "h1.title.page-title",
    activity_description: "div.field--name-field-beschreibung, div.field--name-field-ergebnisse",
    activity_dates: "time[datetime]",
    activity_contacts: "div.paragraph.paragraph--type--ansprechpartner",
    activity_contact_forename: "div.field--name-field-vorname",
    activity_contact_surname: "div.field--name-field-nachname",
    activity_contact_emails: "div.field--name-field-e-mail",
    activity_organisations: "div.field.field--name-field-kooperation a[href], div.field.field--name-field-verantwortliche-organisati a[href]",
    activity_attachments: "div.field.field--name-field-dateianhang a[href]",
    activity_websites: "div.field.field--name-field-website a[href]",
    products_or_projects_links: "#content article h2 a[href]",
    product_or_project_title: "#block-meerimmuell-page-title h1",
    product_or_project_description: "main div.field--name-body",
    product_or_project_issued: "time[datetime]",
    product_or_project_resources: "span.file a[href]",
}
