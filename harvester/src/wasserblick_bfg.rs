use anyhow::{Result, anyhow, ensure};
use cap_std::fs::Dir;
use hashbrown::HashSet;
use if_chain::if_chain;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{SmallVec, smallvec};

use harvester::{
    Source, client::Client, fetch_many, selectors, utilities::collect_text, write_dataset,
};
use metadaten::dataset::{
    Dataset, License, Resource, ResourceType,
    r#type::{TextType, Type},
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();

    let link_ids = fetch_link_ids(client, source, &selectors).await?;
    let count = link_ids.len();

    let (results, errors) = fetch_many(0, 0, link_ids, |link_id| {
        fetch_document(dir, client, source, &selectors, link_id)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_link_ids(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<HashSet<u64>> {
    let url = source.url.join("servlet/is/1/?command=tree&maxLevel=10")?;

    let mut link_ids = HashSet::new();

    {
        let text = client.fetch_text(source, "tree".to_owned(), &url).await?;

        let document = Html::parse_document(&text);

        let mut link_id_caps = selectors.tree_link_id.capture_locations();
        let mut category_name_caps = selectors.tree_category_name.capture_locations();

        for link in document.select(&selectors.tree_link) {
            if_chain! {
                if let Some(link_href) = link.attr("href");
                if let Some(category) = link.select(&selectors.tree_category).next();
                if let Some(category_src) = category.attr("src");
                if let Some(link_id) = selectors.tree_link_id.captures_read(&mut link_id_caps, link_href);
                if let Some(category_name) = selectors.tree_category_name.captures_read(&mut category_name_caps, category_src);
                then {
                    let (start, end) = category_name_caps.get(1).unwrap();
                    let category_name = &category_name.as_str()[start..end];

                    match category_name {
                        "Document" => {
                            let (start, end) = link_id_caps.get(1).unwrap();
                            let link_id = link_id.as_str()[start..end].parse::<u64>()?;

                            link_ids.insert(link_id);
                        }
                        "Ontology" | "Concept" | "ObjectProperty" | "Relation" |
                        "Glossary" | "Form" | "Image" | "Link" | "GlobalLink" |
                        "Calendar" | "Event" | "Letters" | "Envelope" => (),
                        category_name => tracing::warn!(
                            "Unknown category in WasserBLIcK site map: {}",
                            category_name
                        ),
                    }
                }
            }
        }
    }

    ensure!(
        !link_ids.is_empty(),
        "Found no documents in WasserBLIcK site map"
    );

    Ok(link_ids)
}

async fn fetch_document(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link_id: u64,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join(&format!("servlet/is/{link_id}/"))?;

    let mut title;
    let description;
    let mut resources = SmallVec::<[Resource; 4]>::new();

    {
        let text = client
            .fetch_text(source, format!("document-{link_id}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let breadcrumbs = document
            .select(&selectors.breadcrumb_bar)
            .map(|element| collect_text(element.text()))
            .collect::<Vec<String>>();

        // Link ids without breadcrumb bar lead to sites that are not helpful at all.
        // If the type of the document would be "Service", just skip it but do not return an error as these document contain
        // only help information to use WasserBLIcK
        if breadcrumbs.len() <= 2 || breadcrumbs[1] == "Service" {
            return Ok((0, 0, 0));
        }

        title = collect_text(
            document
                .select(&selectors.title)
                .next()
                .ok_or_else(|| anyhow!("Missing title in document {link_id}"))?
                .text(),
        );

        title = format!(
            "{} [{}]",
            title,
            breadcrumbs
                .into_iter()
                .skip(2)
                .rev()
                .collect::<Vec<_>>()
                .join(" / ")
        );

        description = document.select(&selectors.content).next().map(|element| {
            collect_text(
                element
                    .children()
                    .filter_map(|child| child.value().as_text())
                    .map(|text| text.text.as_ref()),
            )
        });

        for resource in document.select(&selectors.resource) {
            let res_url = resource
                .select(&selectors.resource_url)
                .next()
                .ok_or_else(|| anyhow!("Could not find url for resource for link id {}", link_id))?
                .attr("href")
                .unwrap();

            let res_url = source.url.join(res_url)?.into();

            let res_type_name = resource
                .select(&selectors.resource_type)
                .next()
                .ok_or_else(|| anyhow!("Could not find type of resource for link id {}", link_id))?
                .attr("src")
                .unwrap();

            let mut res_type = ResourceType::Unknown;

            if let Some(res_type_name) = selectors.resource_type_name.captures(res_type_name) {
                res_type = res_type_name.get(1).unwrap().as_str().into();
                if res_type == ResourceType::Unknown {
                    tracing::warn!(
                        "Did not understand MIME type from gif {} for link id {}",
                        res_type_name.get(0).unwrap().as_str(),
                        link_id
                    );
                }
            } else {
                tracing::debug!(
                    "Failed parsing resource type from gif {} for link id {}",
                    res_type_name,
                    link_id
                );
            }

            resources.push(Resource {
                r#type: res_type,
                url: res_url,
                ..Default::default()
            });
        }
    }

    if resources.len() < 4 {
        // The amount of 4 resources was empirically chosen by manually screening the datasets. A lower number of resources is more likely to contain primary content.
        for resource in &mut resources {
            resource.primary_content = true;
        }
    }

    let dataset = Dataset {
        title,
        description,
        origins: source.origins.clone(),
        source_url: url.into(),
        types: smallvec![Type::Text {
            text_type: TextType::Report
        }],
        resources,
        license: License::AllRightsReserved,
        ..Default::default()
    };

    write_dataset(dir, client, source, link_id.to_string(), dataset).await
}

selectors! {
    tree_link: "a[href^='/servlet/is/Entry.'][href$='.Tree']",
    tree_link_id: r"/servlet/is/Entry\.(\d+)\.Tree" as Regex,
    tree_category: r#"img[src^="/Bitmaps/Category/"]"#,
    tree_category_name: r"/Bitmaps/Category/([[:alpha:]]+)\.(?:\d+)\.gif" as Regex,
    title: "div#sitetitle > div.page-header",
    content: "div#content",
    resource: "div#content table > tbody > tr.cT",
    resource_url: "td > a[href]",
    resource_type: "td > img[src]",
    resource_type_name: r"/Bitmaps/Mime/(\S+)\.gif" as Regex,
    breadcrumb_bar: "div.well.pathLink > a[class*='pathLink']",
}
