use std::sync::atomic::{AtomicUsize, Ordering};

use anyhow::{Result, anyhow};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use hashbrown::HashMap;
use hashbrown::HashSet;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{SmallVec, smallvec};
use uuid::Uuid;

use harvester::{
    Source, client::Client, fetch_many, remove_datasets, selectors, utilities::collect_text,
    write_dataset,
};
use metadaten::dataset::{
    Alternative, Dataset, Language, License, Resource, ResourceType,
    r#type::{TextType, Type},
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();

    let (mut count, mut results, mut errors) =
        process_downloadable_lists(dir, client, source, &selectors).await?;

    let clusters = AtomicRefCell::new(HashMap::new());

    let (count1, results1, errors1) = process_species_details_pages(
        dir,
        client,
        source,
        &selectors,
        &LangParams {
            href: "/de/Organismengruppen-1681.html",
            key: "",
            lang: Language::German,
            species_profile: "Artensteckbrief",
            scientific_name: "Wissenschaftlicher Name",
            common_name: "Deutscher Name",
            red_list: "Rote-",
            organisms: "Organismen",
            synonyms: "Synonyme",
            comment: "Kommentar",
            comments: "Kommentare",
        },
        &clusters,
    )
    .await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = process_species_details_pages(
        dir,
        client,
        source,
        &selectors,
        &LangParams {
            href: "/en/Groups-of-Organisms-1681.html",
            key: "-en",
            lang: Language::English,
            species_profile: "Species profile",
            scientific_name: "Scientific name",
            common_name: "German name",
            red_list: "Red",
            organisms: "organisms",
            synonyms: "Synonyms",
            comment: "Comment",
            comments: "comments",
        },
        &clusters,
    )
    .await?;
    count += count1;
    results += results1;
    errors += errors1;

    for (key, langs) in clusters.into_inner() {
        if langs.len() > 1 {
            merge_cluster(dir, client, source, key, langs).await?;
        }
    }

    Ok((count, results, errors))
}

async fn process_downloadable_lists(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join("/de/Die-Roten-Listen-1707.html")?;

    let mut hrefs = Vec::new();

    {
        let text = client
            .fetch_text(source, "-de-Die-Roten-Listen-1707.html".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        for element in document.select(&selectors.download_links) {
            let href = element.attr("href").unwrap();
            hrefs.push(href.to_owned());
        }
    }

    let count = hrefs.len();

    let (results, errors) = fetch_many(0, 0, hrefs, |href| async move {
        let key = href.replace('/', "-");
        let url = source.url.join(&href)?;

        let title;
        let description;

        let mut resources = SmallVec::new();

        {
            let text = client.fetch_text(source, key.clone(), &url).await?;

            let document = Html::parse_document(&text);

            title = collect_text(
                document
                    .select(&selectors.list_title)
                    .next()
                    .ok_or_else(|| anyhow!("Missing title in downloadable lists"))?
                    .text(),
            );
            description = document
                .select(&selectors.list_description)
                .next()
                .map(|element| collect_text(element.text()));

            for element in document.select(&selectors.list_files) {
                let mut items = element.select(&selectors.list_items);
                let family = collect_text(
                    items
                        .next()
                        .ok_or_else(|| anyhow!("Missing family in downloadable lists"))?
                        .text(),
                );
                let year = collect_text(
                    items
                        .next()
                        .ok_or_else(|| anyhow!("Missing family in downloadable lists"))?
                        .text(),
                );

                let href = element
                    .select(&selectors.list_link)
                    .next()
                    .ok_or_else(|| anyhow!("Missing download link in downloadable lists"))?
                    .attr("href")
                    .unwrap();

                resources.push(
                    Resource {
                        r#type: ResourceType::Document,
                        description: format!("{family}, {year}").into(),
                        url: source.url.join(href)?.into(),
                        primary_content: true,
                        ..Default::default()
                    }
                    .guess_or_keep_type(),
                )
            }
        }

        write_dataset(
            dir,
            client,
            source,
            key,
            Dataset {
                title,
                description,
                origins: source.origins.clone(),
                license: License::AllRightsReserved,
                source_url: url.into(),
                types: smallvec![Type::Text {
                    text_type: TextType::Editorial
                }],
                resources,
                language: Language::German,
                ..Default::default()
            },
        )
        .await
    })
    .await;

    Ok((count, results, errors))
}

struct LangParams<'a> {
    href: &'a str,
    key: &'a str,
    lang: Language,
    scientific_name: &'a str,
    common_name: &'a str,
    red_list: &'a str,
    organisms: &'a str,
    synonyms: &'a str,
    comment: &'a str,
    comments: &'a str,
    species_profile: &'a str,
}

async fn process_species_details_pages(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    lang_params: &LangParams<'_>,
    clusters: &AtomicRefCell<HashMap<ClusterKey, SmallVec<[String; 2]>>>,
) -> Result<(usize, usize, usize)> {
    let key = lang_params.href.replace('/', "-");
    let url = source.url.join(lang_params.href)?;

    // collect datasets for organism kingdom, e.g. Wirbeltiere
    let mut hrefs = Vec::new();

    {
        let text = client.fetch_text(source, key, &url).await?;

        let document = Html::parse_document(&text);

        for element in document.select(&selectors.list_kingdom) {
            let href = element.attr("href").unwrap();
            hrefs.push(href.to_owned());
        }
    }

    let count = &AtomicUsize::new(hrefs.len());

    let (results, errors) = fetch_many(0, 0, hrefs, |href| async move {
        let key = href.replace('/', "-");
        let url = source.url.join(&href)?;

        let title;
        let description;

        // collect datasets for organism class, e.g. Amphibien
        let mut hrefs = Vec::new();

        {
            let text = client.fetch_text(source, key.clone(), &url).await?;

            let document = Html::parse_document(&text);

            title = collect_text(
                document
                    .select(&selectors.list_title)
                    .next()
                    .ok_or_else(|| anyhow!("Missing title in organism group"))?
                    .text(),
            );
            description = document
                .select(&selectors.list_description)
                .next()
                .map(|element| collect_text(element.text()));

            for element in document.select(&selectors.list_group) {
                let href = element.attr("href").unwrap();
                hrefs.push(href.to_owned());
            }
        }

        let cms_id = selectors
            .list_cms_id
            .captures(&href)
            .ok_or_else(|| anyhow!("Malformed CMS ID in {href}"))?
            .get(1)
            .unwrap()
            .as_str()
            .parse()?;

        clusters
            .borrow_mut()
            .entry(ClusterKey::Cms(cms_id))
            .or_default()
            .push(key.clone());

        write_dataset(
            dir,
            client,
            source,
            key,
            Dataset {
                title,
                description,
                origins: source.origins.clone(),
                license: License::AllRightsReserved,
                source_url: url.into(),
                types: smallvec![Type::Text {
                    text_type: TextType::Editorial
                }],
                language: lang_params.lang,
                ..Default::default()
            },
        )
        .await?;

        count.fetch_add(hrefs.len(), Ordering::Relaxed);

        let (results, errors) = fetch_many(1, 0, hrefs, |href| async move {
            let key = href.replace('/', "-");
            let url = source.url.join(&href)?;

            let title;
            let description;

            // collect datasets for species, e.g. Bufo bufo
            let mut hrefs = HashSet::new();

            {
                let text = client.fetch_text(source, key.clone(), &url).await?;

                let document = Html::parse_document(&text);

                title = collect_text(
                    document
                        .select(&selectors.list_title)
                        .next()
                        .ok_or_else(|| anyhow!("Missing title in organism class"))?
                        .text(),
                );
                description = document
                    .select(&selectors.list_description)
                    .next()
                    .map(|element| collect_text(element.text()));

                for element in document.select(&selectors.list_class) {
                    let href = element.attr("href").unwrap();
                    if href.starts_with('/') {
                        hrefs.insert(href.to_owned());
                    }
                }
            }

            let cms_id = selectors
                .list_cms_id
                .captures(&href)
                .ok_or_else(|| anyhow!("Malformed CMS ID in {href}"))?
                .get(1)
                .unwrap()
                .as_str()
                .parse()?;

            clusters
                .borrow_mut()
                .entry(ClusterKey::Cms(cms_id))
                .or_default()
                .push(key.clone());

            write_dataset(
                dir,
                client,
                source,
                key,
                Dataset {
                    title,
                    description,
                    origins: source.origins.clone(),
                    license: License::AllRightsReserved,
                    source_url: url.into(),
                    types: smallvec![Type::Text {
                        text_type: TextType::Editorial
                    }],
                    language: lang_params.lang,
                    ..Default::default()
                },
            )
            .await?;

            count.fetch_add(hrefs.len(), Ordering::Relaxed);

            let (results, errors) = fetch_many(1, 0, hrefs, |href| async move {
                let url = source.url.join(&href)?;

                let (_, uuid) = url
                    .query_pairs()
                    .find(|(param, _)| param == "species_uuid")
                    .ok_or_else(|| anyhow!("Failed to extract species UUID from {href}"))?;

                let key = format!("species{}-{}", lang_params.key, uuid);

                let mut title = String::new();
                let mut description = String::new();

                let mut resources = SmallVec::new();

                let mut common_name = String::new();
                let mut scientific_name = String::new();

                {
                    let text = client
                        .fetch_text(source, format!("{key}.isol"), &url)
                        .await?;

                    let document = Html::parse_document(&text);

                    let mut any_match = false;

                    for (key, value) in document
                        .select(&selectors.species_detail_key)
                        .zip(document.select(&selectors.species_detail_value))
                    {
                        let key = collect_text(key.text());
                        let value = collect_text(value.text());

                        if key == lang_params.scientific_name
                            || key == lang_params.common_name
                            || key.starts_with(lang_params.red_list)
                            || key.starts_with(lang_params.organisms)
                            || key.starts_with(lang_params.synonyms)
                        {
                            if !title.is_empty() {
                                title.push(' ');
                            }

                            title.push_str(&value);

                            if key == lang_params.scientific_name {
                                scientific_name = value;
                            } else if key == lang_params.common_name {
                                common_name = value;
                            }
                        } else if key.starts_with(lang_params.comment)
                            || key.ends_with(lang_params.comments)
                        {
                            if !description.is_empty() {
                                description.push(' ');
                            }

                            description.push_str(&value);
                        }

                        any_match = true;
                    }

                    if !any_match {
                        tracing::debug!("Species details page {uuid} is empty");
                        // Sometimes there are old species UUID in our cache that yield empty pages the next time the harvester is run.
                        return Ok((1, 0, 0));
                    }

                    for element in document.select(&selectors.species_letter) {
                        let href = element.attr("href").unwrap();

                        resources.push(
                            Resource {
                                r#type: ResourceType::Document,
                                description: Some(lang_params.species_profile.to_owned()),
                                url: source.url.join(href)?.into(),
                                primary_content: true,
                                ..Default::default()
                            }
                            .guess_or_keep_type(),
                        );
                    }
                }

                let types = smallvec![Type::Taxon {
                    scientific_name,
                    common_names: if !common_name.is_empty() {
                        smallvec![common_name]
                    } else {
                        Default::default()
                    },
                }];

                clusters
                    .borrow_mut()
                    .entry(ClusterKey::Species(uuid.parse()?))
                    .or_default()
                    .push(key.clone());

                write_dataset(
                    dir,
                    client,
                    source,
                    key,
                    Dataset {
                        title,
                        description: Some(description),
                        origins: source.origins.clone(),
                        license: License::AllRightsReserved,
                        source_url: url.into(),
                        types,
                        resources,
                        language: lang_params.lang,
                        ..Default::default()
                    },
                )
                .await
            })
            .await;

            Ok((results, results, errors))
        })
        .await;

        Ok((results, results, errors))
    })
    .await;

    Ok((count.load(Ordering::Relaxed), results, errors))
}

#[derive(Debug, PartialEq, Eq, Hash)]
enum ClusterKey {
    Cms(usize),
    Species(Uuid),
}

async fn merge_cluster(
    dir: &Dir,
    client: &Client,
    source: &Source,
    key: ClusterKey,
    mut langs: SmallVec<[String; 2]>,
) -> Result<()> {
    if langs.len() != 2 {
        tracing::debug!("Unexpected number of languages for {key:?}");
    }

    let mut datasets = remove_datasets(dir, client, source, &langs).await?;

    let base_idx = datasets
        .iter()
        .position(|dataset| dataset.language == Language::German)
        .ok_or_else(|| anyhow!("Missing German language dataset for {key:?}"))?;

    let base_key = langs.swap_remove(base_idx);
    let mut base_dataset = datasets.swap_remove(base_idx);

    for dataset in datasets {
        base_dataset.alternatives.push(Alternative::Language {
            language: dataset.language,
            url: dataset.source_url,
        });
    }

    write_dataset(dir, client, source, base_key, base_dataset).await?;

    Ok(())
}

selectors! {
    download_links: "aside.sidebar ul li a[href^='/de/Download-']",
    list_title: "h1",
    list_description: "div.article-section",
    list_files: "div.download-list table tbody tr:not(.cat_abc)",
    list_items: "td.search-results__item",
    list_link: "td a[href]",
    list_kingdom: "aside nav ul li a[href]",
    list_group: "div.kachel_elem a[href]",
    list_class: "table tbody tr td a[href]",
    list_cms_id: r"\-(\d+)\.html$" as Regex,
    species_detail_key: "div.species-detail dt",
    species_detail_value: "div.species-detail dd",
    species_letter: "div.pdf-export-container a[href^='https://www.rote-liste-zentrum.de/detail']",
}
