use std::cmp::Reverse;

use anyhow::{Context, Error, Result, anyhow, ensure};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use geo::{Coord, Rect};
use hashbrown::HashMap;
use regex::Regex;
use scraper::{Element, ElementRef, Html, Selector};
use serde::Serialize;
use smallvec::{SmallVec, smallvec};
use url::Url;

use harvester::{
    Source,
    client::{Client, HttpRequestBuilderExt, HttpResponseExt},
    fetch_many, remove_datasets, selectors, try_fetch,
    utilities::{collect_text, make_key, make_suffix_key, parse_attributes, select_text},
    write_dataset,
};
use metadaten::{
    dataset::{
        Alternative, Dataset, Language, License, Organisation, OrganisationRole, Region, Resource,
        ResourceType, Tag, TimeRange,
        r#type::{Domain, ReportingObligation, Station, TextType, Type},
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();
    let source_url = &Url::parse(
        source
            .source_url
            .as_deref()
            .expect("No source URL configured"),
    )?;

    let (mut count, mut results, mut errors) = fetch_plants(dir, client, source, selectors).await?;

    try_fetch(
        &mut count,
        &mut results,
        &mut errors,
        fetch_articles(dir, client, source, selectors, source_url),
    )
    .await;

    try_fetch(
        &mut count,
        &mut results,
        &mut errors,
        fetch_downloads(dir, client, source, selectors, source_url),
    )
    .await;

    try_fetch(
        &mut count,
        &mut results,
        &mut errors,
        fetch_main_page(dir, client, source, selectors, source_url),
    )
    .await;

    Ok((count, results, errors))
}

async fn fetch_main_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    source_url: &Url,
) -> Result<(usize, usize, usize)> {
    let key = "main_page".to_owned();

    let text = client.fetch_text(source, key.clone(), source_url).await?;

    let document = Html::parse_document(&text);

    let title = "thru.de Hauptseite".to_owned();
    let description = select_text(&document, &selectors.main_description);

    let resources = document
        .select(&selectors.main_resources)
        .map(|element| {
            let href = element
                .select(&selectors.main_resources_href)
                .next()
                .ok_or_else(|| anyhow!("Missing href"))?
                .attr("href")
                .unwrap();

            let description = select_text(element, &selectors.main_resources_title);

            Ok(Resource {
                description: Some(description),
                url: source.url.join(href)?.into(),
                ..Default::default()
            })
        })
        .collect::<Result<SmallVec<_>>>()?;

    let dataset = Dataset {
        title,
        description: Some(description),
        tags: vec![Tag::PRTR, Tag::IED, Tag::IEP, Tag::BRANCHEN], // tags were suggested by dhS on 06.11.24
        language: Language::German,
        license: License::OtherOpen, // Content from thru.de itself is provided under an open license (see https://thru.de/nutzungsbedingungen/).
        origins: source.origins.clone(),
        source_url: source_url.clone().into(),
        resources,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_plants(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let cookies = AtomicRefCell::new(HashMap::new());

    let mut reporting_years;
    let token;

    {
        let url = source.url.join("/detail-suche")?;

        let text = client
            .make_request(
                source,
                "search-parameters".to_owned(),
                Some(&url),
                |client| async {
                    client
                        .get(url.clone())
                        .send()
                        .await?
                        .error_for_status()?
                        .set_cookies(&mut cookies.borrow_mut())?
                        .text()
                        .await
                        .map_err(Error::from)
                },
            )
            .await?;

        let document = Html::parse_document(&text);

        reporting_years = parse_attributes(
            &document,
            &selectors.reporting_years,
            &selectors.reporting_years_value,
            "value",
            "reporting year",
        )
        .collect::<Result<Vec<i32>>>()?;

        token = document
            .select(&selectors.token)
            .next()
            .ok_or_else(|| anyhow!("CSRF token not found"))?
            .attr("value")
            .unwrap()
            .to_owned();
    }

    reporting_years.sort_unstable();
    reporting_years.dedup();

    ensure!(!reporting_years.is_empty(), "No reporting years scraped");

    let clusters = AtomicRefCell::new(Clusters::new());

    let mut count = 0;
    let mut results = 0;
    let mut errors = 0;

    for &reporting_year in &reporting_years {
        match fetch_plants_pages(
            dir,
            client,
            source,
            selectors,
            &clusters,
            reporting_year,
            &token,
            &cookies,
        )
        .await
        {
            Ok((count1, results1, errors1)) => {
                count += count1;
                results += results1;
                errors += errors1;
            }
            Err(err) => {
                tracing::error!("{:#}", err);

                errors += 1;
            }
        };
    }

    for (plant_id, reports) in clusters.into_inner() {
        if reports.len() > 1 {
            tracing::debug!("Merging cluster for {plant_id}");
            merge_cluster(dir, client, source, reports).await?;
        }
    }

    Ok((count, results, errors))
}

#[allow(clippy::too_many_arguments)]
async fn fetch_plants_pages(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    clusters: &AtomicRefCell<Clusters>,
    reporting_year: i32,
    token: &str,
    cookies: &AtomicRefCell<HashMap<CompactString, String>>,
) -> Result<(usize, usize, usize)> {
    let last_page;
    let mut plant_details_ids = Vec::<u32>::new();

    {
        #[derive(Serialize)]
        struct Params<'a> {
            csrfmiddlewaretoken: &'a str,
            berichtsjahr: i32,
            ergebnis: &'a str,
            darstellung: &'a str,
        }

        let url = source.url.join("/detail-suche")?;

        let text = client
            .make_request(
                source,
                format!("pages-{reporting_year}-1"),
                Some(&url),
                |client| async {
                    let cookies = &mut cookies.borrow_mut();

                    client
                        .post(url.clone())
                        .form(&Params {
                            csrfmiddlewaretoken: token,
                            berichtsjahr: reporting_year,
                            ergebnis: "betriebe_name",
                            darstellung: "B",
                        })
                        .cookies(cookies)
                        .send()
                        .await?
                        .error_for_status()?
                        .set_cookies(cookies)?
                        .text()
                        .await
                        .map_err(Error::from)
                },
            )
            .await?;

        let document = Html::parse_document(&text);

        last_page = parse_attributes(
            &document,
            &selectors.last_page,
            &selectors.last_page_value,
            "href",
            "last page",
        )
        .reduce(|lhs, rhs| {
            let lhs: usize = lhs?;
            let rhs: usize = rhs?;
            Ok(lhs.max(rhs))
        })
        .ok_or_else(|| anyhow!("Missing last page"))??;

        for plant_details_id in parse_attributes(
            &document,
            &selectors.plant_id,
            &selectors.plant_id_value,
            "href",
            "plant ID",
        ) {
            plant_details_ids.push(plant_details_id?);
        }
    }

    for page in 2..=last_page {
        let url = source.url.join(&format!("/detail-suche/seite/{page}"))?;

        let text = client
            .make_request(
                source,
                format!("pages-{reporting_year}-{page}"),
                Some(&url),
                |client| async {
                    client
                        .get(url.clone())
                        .cookies(&cookies.borrow())
                        .send()
                        .await?
                        .error_for_status()?
                        .text()
                        .await
                },
            )
            .await?;

        let document = Html::parse_document(&text);

        for plant_details_id in parse_attributes(
            &document,
            &selectors.plant_id,
            &selectors.plant_id_value,
            "href",
            "plant ID",
        ) {
            plant_details_ids.push(plant_details_id?);
        }
    }

    let count = plant_details_ids.len();

    let (results, errors) = fetch_many(0, 0, plant_details_ids, |plant_details_id| async move {
        fetch_plant_details(
            dir,
            client,
            source,
            selectors,
            clusters,
            reporting_year,
            plant_details_id,
        )
        .await
        .with_context(|| format!("Fetch to fetch plant details of `{}`", plant_details_id))
    })
    .await;

    Ok((count, results, errors))
}

#[allow(clippy::too_many_arguments)]
async fn fetch_plant_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    clusters: &AtomicRefCell<Clusters>,
    reporting_year: i32,
    plant_details_id: u32,
) -> Result<(usize, usize, usize)> {
    let key = format!("plant-details-{reporting_year}-{plant_details_id}");

    let url = source
        .url
        .join(&format!("/details/{plant_details_id}/liste"))?;

    let title;
    let mut description = String::new();
    let mut organisations = SmallVec::new();
    let mut regions = SmallVec::new();
    let mut bounding_boxes = SmallVec::new();
    let mut types = smallvec![Type::Text {
        text_type: TextType::Report
    }];

    {
        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let content = document
            .select(&selectors.plant_details_content)
            .next()
            .ok_or_else(|| anyhow!("Missing content"))?;

        let name = collect_text(
            content
                .select(&selectors.plant_details_name)
                .next()
                .ok_or_else(|| anyhow!("Missing name"))?
                .text(),
        );

        title = format!("{name} ({reporting_year})");

        let mut station_id = None;

        for element in content.select(&selectors.plant_details_properties) {
            let key = collect_text(
                element
                    .select(&selectors.plant_details_key)
                    .next()
                    .ok_or_else(|| anyhow!("Missing property key"))?
                    .text(),
            );
            let value = collect_text(
                element
                    .select(&selectors.plant_details_value)
                    .next()
                    .ok_or_else(|| anyhow!("Missing property value"))?
                    .text(),
            );

            if key == "Kennnummer" {
                clusters
                    .borrow_mut()
                    .entry_ref(&*value)
                    .or_default()
                    .push((reporting_year, plant_details_id));

                station_id = Some(value);
            } else if key.contains("Betreiberinformation") && value != "—" {
                description.push('\n');
                description.push_str(&key);
                description.push_str(": ");
                description.push_str(&value);
            } else if (key == "Muttergesellschaft" || key == "Eigentümer" || key == "Betreiber")
                && value != "-"
                && value != "keine Angabe"
            {
                organisations.push(Organisation::Other {
                    name: value,
                    role: OrganisationRole::Unknown,
                    websites: Default::default(),
                });
            } else if key == "Adresse" {
                if let Some(captures) = selectors.plant_details_city.captures(&value) {
                    regions.push(Region::Other(captures["city"].into()));
                }
            } else if key == "Bundesland" {
                regions.push(Region::Other(value.into()));
            } else if key == "Flusseinzugsgebiet" {
                regions.extend(WISE.match_name(&value).map(Region::Watershed));
            }
        }

        for element in content.select(&selectors.plant_details_activities) {
            let key = collect_text(
                element
                    .select(&selectors.plant_details_key)
                    .next()
                    .ok_or_else(|| anyhow!("Missing activity key"))?
                    .text(),
            );

            if !matches!(
                key.as_str(),
                "Berichtsjahr"
                    | "Betreiber"
                    | "Adresse"
                    | "Bundesland"
                    | "Flusseinzugsgebiet"
                    | "Haupttätigkeit"
            ) {
                continue;
            }

            let value = collect_text(
                element
                    .select(&selectors.plant_details_value)
                    .next()
                    .ok_or_else(|| anyhow!("Missing activity value"))?
                    .text(),
            );

            if value != "—" {
                if !description.is_empty() {
                    description.push('\n');
                }
                description.push_str(&key);
                description.push_str(": ");
                description.push_str(&value);
            }
        }

        for element in content.select(&selectors.plant_details_releases_heading) {
            let text = collect_text(element.text());

            let (nth_var, nth_meth) = if text.contains("Freisetzung") {
                (2, 5)
            } else if text == "Verbringung von Schadstoffen mit dem Abwasser" {
                (1, 4)
            } else {
                continue;
            };

            for element in element
                .next_sibling_element()
                .unwrap()
                .select(&selectors.plant_details_releases_rows)
            {
                let variable = collect_text(
                    element
                        .select(&selectors.plant_details_value)
                        .nth(nth_var)
                        .ok_or_else(|| anyhow!("Missing variable"))?
                        .text(),
                );
                let method = collect_text(
                    element
                        .select(&selectors.plant_details_value)
                        .nth(nth_meth)
                        .ok_or_else(|| anyhow!("Missing method"))?
                        .text(),
                );
                types.push(Type::Measurements {
                    domain: if text.contains("Wasser")
                        || text == "Verbringung von Schadstoffen mit dem Abwasser"
                    {
                        Domain::Water
                    } else if text.contains("Luft") {
                        Domain::Air
                    } else if text.contains("Boden") {
                        Domain::Soil
                    } else {
                        Default::default()
                    },
                    station: Some(Station {
                        id: station_id.as_ref().map(Into::into),
                        measurement_frequency: None,
                        reporting_obligations: smallvec![ReportingObligation::Prtr],
                    }),
                    measured_variables: smallvec![variable],
                    methods: smallvec![method],
                });
            }
        }

        if let Some(map) = content.select(&selectors.plant_details_map).next() {
            let x = map.attr("data-x").unwrap().parse()?;
            let y = map.attr("data-y").unwrap().parse()?;

            bounding_boxes.push(Rect::new(Coord { x, y }, Coord { x, y }));
        }
    }

    let time_ranges = smallvec![TimeRange::whole_year(reporting_year)?];

    let resources = smallvec![
        Resource {
            r#type: ResourceType::WebPage,
            description: Some("thru.de Hauptseite".to_owned()),
            url: source.source_url().to_owned(),
            ..Default::default()
        }
        .guess_or_keep_type()
    ];

    let dataset = Dataset {
        title,
        description: Some(description),
        tags: vec![Tag::PRTR, Tag::IED, Tag::IEP, Tag::BRANCHEN], // tags were suggested by dhS on 06.11.24
        organisations,
        regions,
        bounding_boxes,
        time_ranges,
        language: Language::German,
        license: License::OtherOpen, // Content from thru.de itself is provided under an open license (see https://thru.de/nutzungsbedingungen/).
        origins: source.origins.clone(),
        source_url: url.into(),
        types,
        resources,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

type Clusters = HashMap<CompactString, Vec<(i32, u32)>>;

async fn merge_cluster(
    dir: &Dir,
    client: &Client,
    source: &Source,
    mut reports: Vec<(i32, u32)>,
) -> Result<()> {
    reports.sort_unstable_by_key(|(reporting_year, plant_details_id)| {
        (Reverse(*reporting_year), *plant_details_id)
    });
    reports.dedup();

    let keys = reports
        .iter()
        .map(|(reporting_year, plant_details_id)| {
            format!("plant-details-{reporting_year}-{plant_details_id}")
        })
        .collect::<Vec<_>>();

    let datasets = remove_datasets(dir, client, source, &keys).await?;

    let latest_key = keys.into_iter().next().unwrap();

    let mut datasets = datasets.into_iter();
    let mut latest_dataset = datasets.next().unwrap();

    let latest_year = reports.first().unwrap().0;
    let oldest_year = reports.last().unwrap().0;
    let single_year = format!(" ({latest_year})");
    let range_of_years = format!(" ({oldest_year} - {latest_year})");
    let title = latest_dataset.title.replace(&single_year, &range_of_years);

    latest_dataset.title = title;

    for older_dataset in datasets {
        latest_dataset.alternatives.push(Alternative::Source {
            source: source.name.clone(),
            title: older_dataset.title,
            url: older_dataset.source_url,
        });

        latest_dataset
            .organisations
            .extend(older_dataset.organisations);

        latest_dataset
            .bounding_boxes
            .extend(older_dataset.bounding_boxes);

        latest_dataset.time_ranges.extend(older_dataset.time_ranges);
    }

    write_dataset(dir, client, source, latest_key, latest_dataset).await?;

    Ok(())
}

async fn fetch_articles(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    source_url: &Url,
) -> Result<(usize, usize, usize)> {
    let articles;

    {
        let url = source_url.join("/inhalt/")?;

        let text = client
            .fetch_text(source, "sitemap".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        articles = document
            .select(&selectors.articles)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>();
    }

    let count = articles.len();

    let (results, errors) = fetch_many(0, 0, articles, |link| async move {
        fetch_article(dir, client, source, selectors, source_url, &link)
            .await
            .with_context(|| format!("Failed to fetch article `{link}`"))
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_article(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    source_url: &Url,
    link: &str,
) -> Result<(usize, usize, usize)> {
    let key = make_suffix_key(link, source_url);
    let url = source_url.join(link)?;

    let title;
    let description;
    let mut resources;

    {
        let text = client
            .fetch_text(source, format!("article-{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let content = document
            .select(&selectors.article_content)
            .next()
            .ok_or_else(|| anyhow!("Missing content"))?;

        title = collect_text(
            content
                .select(&selectors.article_title)
                .next()
                .ok_or_else(|| anyhow!("Missing title"))?
                .text(),
        );

        description = collect_text(
            content
                .select(&selectors.article_description)
                .flat_map(|elem| elem.text()),
        );

        resources = content
            .select(&selectors.article_resources)
            .map(|element| {
                let description = collect_text(element.text());
                let href = element.attr("href").unwrap();
                let url = source.url.join(href)?;

                Ok(Resource {
                    r#type: ResourceType::Document,
                    description: Some(description),
                    url: url.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        resources.push(
            Resource {
                r#type: ResourceType::WebPage,
                description: Some("thru.de Hauptseite".to_owned()),
                url: source_url.clone().into(),
                ..Default::default()
            }
            .guess_or_keep_type(),
        );
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        resources,
        language: Language::German,
        license: License::MixedClosed, // Content from thru.de itself is provided under an open license, however this does not apply for the documents of third parties (see https://thru.de/nutzungsbedingungen/).
        origins: source.origins.clone(),
        source_url: url.into(),
        types: smallvec![Type::Text {
            text_type: TextType::Editorial
        }],
        ..Default::default()
    };

    write_dataset(dir, client, source, format!("article-{key}"), dataset).await
}

async fn fetch_downloads(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    source_url: &Url,
) -> Result<(usize, usize, usize)> {
    let mut downloads = Vec::new();

    {
        let url = source_url.join("/downloads/")?;

        let text = client
            .fetch_text(source, "downloads".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        for section in document.select(&selectors.downloads_sections) {
            let title = collect_text(
                section
                    .prev_siblings()
                    .filter_map(ElementRef::wrap)
                    .find(|elem| elem.value().name() == "h2")
                    .ok_or_else(|| anyhow!("Missing download section heading"))?
                    .text(),
            );

            let mut resources = SmallVec::new();

            for entry in section.select(&selectors.downloads_entries) {
                let element = entry
                    .select(&selectors.downloads_description)
                    .nth(1)
                    .ok_or_else(|| anyhow!("Missing description on downloads entry"))?;

                let description = collect_text(element.text());

                for link in element.select(&selectors.downloads_link) {
                    let href = link.attr("href").unwrap();

                    let direct_link = !href.contains("fileadmin/SITE_MASTER"); // these links are broken

                    resources.push(
                        Resource {
                            r#type: ResourceType::WebPage,
                            description: Some(description.clone()),
                            url: source.url.join(href)?.into(),
                            direct_link,
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }
            }

            resources.push(
                Resource {
                    r#type: ResourceType::WebPage,
                    description: Some("thru.de Hauptseite".to_owned()),
                    url: source_url.clone().into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );

            downloads.push(Dataset {
                title: format!("Thru.de Downloads - {title}"),
                resources,
                language: Language::German,
                origins: source.origins.clone(),
                source_url: url.clone().into(),
                ..Default::default()
            })
        }
    }

    let count = downloads.len();
    let (results, errors) = fetch_many(0, 0, downloads, |download| async move {
        let key = format!("download-{}", make_key(&download.title));

        write_dataset(dir, client, source, key, download).await
    })
    .await;

    Ok((count, results, errors))
}

selectors! {
    reporting_years: "select#id_berichtsjahr option[value]",
    reporting_years_value: r"(\d+)" as Regex,
    sectors: "select#id_branche option[value]:not([value=''])",
    sectors_value: r"(\d+)" as Regex,
    token: "input[name='csrfmiddlewaretoken'][value]",
    last_page: "div.pagination-next a[href^='/detail-suche/seite/']",
    last_page_value: r"/detail\-suche/seite/(\d+)" as Regex,
    plant_id: "a[href^='/details/']",
    plant_id_value: r"/details/(\d+)/" as Regex,
    plant_details_content: "main",
    plant_details_name: "h1",
    plant_details_properties: "table#detail-table tbody tr",
    plant_details_activities: "table.table-borderless tbody tr",
    plant_details_releases_heading: "div.row div.col-12 h2:has(+ table)",
    plant_details_releases_rows: "tbody tr",
    plant_details_key: "th",
    plant_details_value: "td",
    plant_details_city: r"\d{5}\s+(?<city>.+)" as Regex,
    plant_details_map: "div#detail-map[data-x][data-y]",
    articles: "li.wp-block-pages-list__item a[href*='/thrude/'], li.wp-block-pages-list__item a[href*='/wissen/']",
    article_content: "article.page",
    article_title: "h1.wp-block-heading, h2.wp-block-heading, h3.wp-block-heading",
    article_description: "p, ul",
    article_resources: "a[href*='.pdf']",
    downloads_sections: "figure.wp-block-table",
    downloads_entries: "table tbody tr",
    downloads_description: "td",
    downloads_link: "a[href]",
    main_description: "p.subheadline",
    main_resources: "div.wp-block-column.highlight-box.is-layout-flow.wp-block-column-is-layout-flow",
    main_resources_title: "p",
    main_resources_href: "a[href]",
}
