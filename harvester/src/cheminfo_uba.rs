use std::fmt::Write;

use anyhow::{Error, Result, anyhow, ensure};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use hashbrown::{HashMap, HashSet};
use scraper::{Element, Html, Selector};
use serde::Serialize;
use smallvec::smallvec;
use url::Url;

use harvester::{
    Source,
    client::{Client, HttpRequestBuilderExt, HttpResponseExt},
    fetch_many, selectors,
    utilities::{collect_text, select_text},
    write_dataset,
};
use metadaten::dataset::{
    Dataset, Language, License, Organisation, OrganisationKey, OrganisationRole, r#type::Type,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let chem_list = fetch_list(client, source, selectors, "public").await?;

    let mut count = chem_list.len();

    let (results, errors) = fetch_many(0, 0, chem_list, |gsbl_id| {
        translate_dataset(dir, client, source, selectors, "public", gsbl_id)
    })
    .await;

    let biozid_list = fetch_list(client, source, selectors, "biu").await?;

    count += biozid_list.len();

    let (results, errors) = fetch_many(results, errors, biozid_list, |gsbl_id| {
        translate_dataset(dir, client, source, selectors, "biu", gsbl_id)
    })
    .await;

    let etox_list = fetch_list(client, source, selectors, "etox").await?;

    count += etox_list.len();

    let (results, errors) = fetch_many(results, errors, etox_list, |gsbl_id| {
        translate_dataset(dir, client, source, selectors, "etox", gsbl_id)
    })
    .await;

    let medic_list = fetch_list(client, source, selectors, "aiu").await?;

    count += medic_list.len();

    let (results, errors) = fetch_many(results, errors, medic_list, |gsbl_id| {
        translate_dataset(dir, client, source, selectors, "aiu", gsbl_id)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_list(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    path: &str,
) -> Result<HashSet<u64>> {
    let cookies = AtomicRefCell::new(HashMap::new());

    let request_verification_token = {
        let url = source.url.join(&format!("{path}/erweitert/abfrage"))?;

        let text = client
            .make_request(
                source,
                format!("{path}-request-verification-token"),
                Some(&url),
                |client| async {
                    client
                        .get(url.clone())
                        .send()
                        .await?
                        .error_for_status()?
                        .set_cookies(&mut cookies.borrow_mut())?
                        .text()
                        .await
                        .map_err(Error::from)
                },
            )
            .await?;

        let document = Html::parse_document(&text);

        let element = document
            .select(&selectors.request_verification_token)
            .next()
            .ok_or_else(|| anyhow!("Missing request verfication token"))?;

        element.attr("value").unwrap().to_owned()
    };

    let final_url = {
        let url = source.url.join(&format!("{path}/erweitert/abfrage"))?;

        #[derive(Serialize)]
        struct Params<'a> {
            #[serde(rename = "Kontext")]
            context: &'a str,
            #[serde(rename = "Query")]
            query: &'a str,
            #[serde(rename = "__RequestVerificationToken")]
            request_verification_token: &'a str,
        }

        let final_url = client
            .make_request(
                source,
                format!("{path}-session-id"),
                Some(&url),
                |client| async {
                    let cookies = &mut cookies.borrow_mut();

                    let response = client
                        .post(url.clone())
                        .form(&Params {
                            context: "Stoff",
                            query: "gsbl.gsblrn>0",
                            request_verification_token: &request_verification_token,
                        })
                        .cookies(cookies)
                        .send()
                        .await?
                        .error_for_status()?
                        .set_cookies(cookies)?;

                    let final_url = String::from(response.url().clone());

                    Ok::<_, Error>(final_url)
                },
            )
            .await?;

        Url::parse(&final_url)?
    };

    let mut ids = HashSet::new();
    let mut page = 1;
    let mut last_page = 0;

    loop {
        let mut url = source.url.join(&format!("{path}/p"))?;

        {
            let mut query_pairs = url.query_pairs_mut();

            for (key, value) in final_url.query_pairs() {
                if matches!(key.as_ref(), "page" | "ps") {
                    continue;
                }

                query_pairs.append_pair(&key, &value);
            }

            query_pairs
                .append_pair("page", &page.to_string())
                .append_pair("ps", &source.batch_size.to_string());
        }

        let text = client
            .make_request(
                source,
                format!("{path}-{page}"),
                Some(&url),
                |client| async {
                    client
                        .get(url.clone())
                        .cookies(&cookies.borrow())
                        .send()
                        .await?
                        .error_for_status()?
                        .text()
                        .await
                },
            )
            .await?;

        let fragment = Html::parse_fragment(&text);

        for element in fragment.select(&selectors.gsbl_id) {
            let id = element.attr("value").unwrap().parse()?;

            ids.insert(id);
        }

        if last_page == 0 {
            for element in fragment.select(&selectors.page_link) {
                let text = element.text().collect::<String>();

                if matches!(text.as_ref(), "Zurück" | "..." | "Weiter") {
                    continue;
                }

                let page = text.parse::<usize>()?;

                last_page = last_page.max(page);
            }

            ensure!(last_page != 0, "Missing last page on {url}");
        }

        if page < last_page {
            page += 1;
        } else {
            break;
        }
    }

    Ok(ids)
}

async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    path: &str,
    gsbl_id: u64,
) -> Result<(usize, usize, usize)> {
    let key = format!("{path}-{gsbl_id}");

    let url = source.url.join(&format!("{}/stoff/{}", path, gsbl_id))?;

    let text = client
        .fetch_text(
            source,
            format!("{key}.isol"),
            &source
                .url
                .join(&format!("{}/stoff/{}?&ansicht=dossier", path, gsbl_id))?,
        )
        .await?;

    let document = Html::parse_document(&text);

    let name = select_text(&document, &selectors.title);

    let title = match path {
        "biu" => format!("Informationen zum Biozid: {}", name),
        "etox" => format!("Informationen zur ökotoxikologischen Verbindung: {}", name),
        "aiu" => format!("Informationen zum Arzneimittel: {}", name),
        "public" => format!("Informationen zur Chemischen Verbindung: {}", name),
        _ => unreachable!(),
    };

    let mut description = match path {
        "biu" => format!(
            "Die verlinkte Webseite enthält Informationen der Website chemikalieninfo.de des Umweltbundesamtes zum Biozid {name} mit der GSLB ID {gsbl_id}."
        ),
        "etox" => format!(
            "Die verlinkte Webseite enthält Informationen der Website chemikalieninfo.de des Umweltbundesamtes zur ökotoxikologischen Verbindung {name} mit der GSLB ID {gsbl_id}."
        ),
        "aiu" => format!(
            "Die verlinkte Webseite enthält Informationen der Website chemikalieninfo.de des Umweltbundesamtes zum Arzneitmittel {name} mit der GSLB ID {gsbl_id}."
        ),
        "public" => format!(
            "Die verlinkte Webseite enthält Informationen der Website chemikalieninfo.de des Umweltbundesamtes zur Chemischen Verbindung {name} mit der GSLB ID {gsbl_id}."
        ),
        _ => unreachable!(),
    };

    let organisations = smallvec![Organisation::WikiData {
        identifier: OrganisationKey::UBA,
        role: OrganisationRole::Provider
    }];

    let mut cas_registry_number = None;
    let mut substance_type = None;
    let mut aggregate = None;
    let mut chem_composition = None;
    let mut boiling_temp = None;
    let mut chem_color = None;
    let mut regelwerk = None;

    {
        let mut properties = document.select(&selectors.properties);

        while let Some(term) = properties.next() {
            ensure!(term.value().name() == "dt");

            let definition = properties
                .next()
                .ok_or_else(|| anyhow!("Term without definition"))?;

            ensure!(definition.value().name() == "dd");

            let term = collect_text(term.text());
            let definition = collect_text(definition.text());
            match term.as_ref() {
                "CAS-RN" | "CAS-Nummer gemäß Regelwerk" | "CAS-Nummer" => {
                    cas_registry_number = Some(definition);
                }
                "Stoffart" => {
                    substance_type = Some(definition);
                }
                "Aggregatzustand" => {
                    aggregate = Some(definition);
                }
                "Stoffbeschaffenheit" => {
                    chem_composition = Some(definition);
                }
                "Siedetemperatur/Kondensationstemperatur" => {
                    boiling_temp = Some(definition);
                }
                "Farbe" => {
                    chem_color = Some(definition);
                }
                "Inhalt des Regelwerks" => {
                    regelwerk = Some(definition);
                }
                "Angaben zur Stoffart" => {
                    substance_type = Some(match substance_type {
                        Some(existing) => format!("{} ({})", existing, definition),
                        None => definition,
                    })
                }
                _ => {}
            }
        }
    }

    if let Some(substance_type) = substance_type {
        write!(&mut description, " Stoffart: {substance_type}.")?;
    }
    if let Some(aggregate) = aggregate {
        write!(&mut description, " Aggregatzustand: {aggregate}.")?;
    }
    if let Some(chem_composition) = chem_composition {
        write!(
            &mut description,
            " Stoffbeschaffenheit: {chem_composition}."
        )?;
    }
    if let Some(boiling_temp) = boiling_temp {
        write!(
            &mut description,
            " Siedetemperatur/Kondensationstemperatur: {boiling_temp}."
        )?;
    }
    if let Some(chem_color) = chem_color {
        write!(&mut description, " Farbe: {chem_color}.")?;
    }
    if let Some(regelwerk) = regelwerk {
        write!(&mut description, " Inhalt des Regelwerks: {regelwerk}.")?;
    }

    for element in document.select(&selectors.main_term) {
        let text = collect_text(element.text());

        if text != "Umweltgefahren" {
            continue;
        }

        let element = element
            .next_sibling_element()
            .ok_or_else(|| anyhow!("Missing Umweltgefahren"))?;

        write!(&mut description, " Es gelten folgende Umweltgefahren: ")?;

        let mut properties = element.select(&selectors.properties);

        while let Some(term) = properties.next() {
            ensure!(term.value().name() == "dt");

            let definition = properties
                .next()
                .ok_or_else(|| anyhow!("Term without definition"))?;

            ensure!(definition.value().name() == "dd");

            let term = collect_text(term.text());
            let definition = collect_text(definition.text());

            write!(&mut description, " {term}: {definition}")?;
        }
    }

    let mut synonyms = Vec::new();

    for element in document.select(&selectors.table) {
        if let Some(table_header) = element.select(&selectors.table_header).next() {
            let text = collect_text(table_header.text());

            if text != "Registriername" {
                continue;
            }

            let element = element
                .next_sibling_element()
                .ok_or_else(|| anyhow!("Missing Registriername"))?;

            ensure!(element.value().name() == "tbody");

            for table_row in element.select(&selectors.table_row) {
                let synonym = collect_text(table_row.text());

                synonyms.push(synonym);
            }
        }
    }

    synonyms.sort_unstable();
    synonyms.dedup();

    let r#type = Type::ChemicalCompound {
        name,
        synonyms,
        cas_registry_number,
    };

    let dataset = Dataset {
        title,
        description: Some(description),
        types: smallvec![r#type],
        organisations,
        language: Language::German,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url: url.into(),
        machine_readable_source: false,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

selectors! {
    request_verification_token: "input[type='hidden'][name='__RequestVerificationToken'][value]",
    gsbl_id: "input[type='checkbox'][id^='cb_result_item_select_'][value]",
    page_link: "li.page-item a.page-link",
    title: "h1",
    properties: "dl.row > *",
    main_term: "h3.oberbegriff",
    table: "table > thead",
    table_header: "tr > th:nth-child(2)",
    table_row: "tr > td:nth-child(2)",
}
