use std::borrow::Cow;

use anyhow::Result;
use bincode::serialize;
use cap_std::fs::Dir;
use hashbrown::HashSet;
use serde::{Deserialize, Serialize};
use serde_json::from_str;

use harvester::{Source, client::Client};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let text = client
        .fetch_text(source, "dictionary".to_owned(), &source.url)
        .await?;

    let mut nouns = Nouns::default();

    let mut count = 0;
    let mut results = 0;
    let mut errors = 0;

    for line in text.lines() {
        count += 1;

        match from_str::<Entry>(line) {
            Ok(entry) if entry.pos == "noun" && entry.word.len() > 3 => {
                let mut tags = entry.senses.iter().flat_map(|sense| &sense.tags);
                let word = entry.word.to_lowercase();

                if tags.any(|tag| *tag == "form-of") {
                    nouns.forms.insert(word);
                } else {
                    nouns.base.insert(word);
                }

                results += 1;
            }
            Ok(_) => (),
            Err(err) => {
                tracing::error!("Failed to parse entry: {err:#}");

                errors += 1;
            }
        }
    }

    const MISSING_BASE: &[&str] = &["messstelle"];
    const MISSING_FORMS: &[&str] = &["messstellen"];

    for &word in MISSING_BASE {
        if !nouns.base.insert(word.to_owned()) {
            tracing::error!("Noun `{word}` is not actually missing in Kaikki dictionary.");
        }
    }

    for &word in MISSING_FORMS {
        if !nouns.forms.insert(word.to_owned()) {
            tracing::error!("Noun `{word}` is not actually missing in Kaikki dictionary.");
        }
    }

    let buf = serialize(&nouns)?;
    dir.write("nouns.bin", &buf)?;

    Ok((count, results, errors))
}

#[derive(Deserialize)]
struct Entry<'a> {
    #[serde(borrow)]
    word: Cow<'a, str>,
    #[serde(borrow)]
    pos: &'a str,
    #[serde(borrow)]
    senses: Vec<Sense<'a>>,
}

#[derive(Deserialize)]
struct Sense<'a> {
    #[serde(default, borrow)]
    tags: Vec<&'a str>,
}

#[derive(Default, Serialize)]
struct Nouns {
    base: HashSet<String>,
    forms: HashSet<String>,
}
