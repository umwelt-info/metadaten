use anyhow::{Result, anyhow};
use cap_std::fs::Dir;
use regex::Regex;
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_json::from_str as from_json_str;
use smallvec::{SmallVec, smallvec};
use time::{Date, format_description::well_known::Iso8601};

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors,
    utilities::{
        GermanDate, collect_text, make_key, parse_text, select_first_text, select_text,
        uppercase_words,
    },
    write_dataset,
};
use metadaten::dataset::{
    Dataset, Language, LanguageDetector, License, Organisation, OrganisationKey, OrganisationRole,
    Person, PersonRole, Resource, ResourceType,
    r#type::{TextType, Type},
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let language_detector = LanguageDetector::build(
        0.0,
        [
            Language::German,
            Language::English,
            Language::Persian,
            Language::Arabic,
            Language::Dutch,
            Language::French,
            Language::Russian,
            Language::Chinese,
        ],
    );

    let (max_page, results1, errors1) =
        fetch_newsletter_page(dir, client, source, selectors, 0).await?;

    let (results1, errors1) = fetch_many(results1, errors1, 1..=max_page, |page| {
        fetch_newsletter_page(dir, client, source, selectors, page)
    })
    .await;

    let (max_year, results2, errors2) =
        fetch_press_releases_page(dir, client, source, selectors, &language_detector, 2008).await?;

    let (results2, errors2) = fetch_many(results2, errors2, 2009..=max_year, |year| {
        fetch_press_releases_page(dir, client, source, selectors, &language_detector, year)
    })
    .await;

    let (max_page, results3, errors3) =
        fetch_publication_page(dir, client, source, selectors, &language_detector, 0).await?;

    let (results3, errors3) = fetch_many(results3, errors3, 1..=max_page, |page| {
        fetch_publication_page(dir, client, source, selectors, &language_detector, page)
    })
    .await;

    let (_max_page, results4, errors4) =
        fetch_press_dossiers(dir, client, source, selectors).await?;

    let results = results1 + results2 + results3 + results4;
    let errors = errors1 + errors2 + errors3 + errors4;

    Ok((results, results, errors))
}

async fn fetch_press_dossiers(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let mut results = 0;
    let mut errors = 0;

    let mut page = 0;

    loop {
        let mut source_url = source.url.join("/views/ajax")?;

        source_url
            .query_pairs_mut()
            .append_pair("page", &page.to_string())
            .append_pair("view_name", "cover")
            .append_pair("view_display_id", "teaser_articles")
            .append_pair("view_args", "63525/article")
            .append_pair("view_path", "article/63525")
            .append_pair("view_base_path", "null")
            .append_pair("view_dom_id", "aced8a98cc0aff9079bc2ea60b13e767")
            .append_pair("pager_element", "0");

        let text = client
            .fetch_text(source, format!("pressedossier-{}", page), &source_url)
            .await?;

        let document = Html::parse_fragment(
            &from_json_str::<Vec<Data>>(&text)?
                .into_iter()
                .find_map(|element| element.data)
                .ok_or_else(|| anyhow!("Failed to fetch HTML structure from JSON"))?,
        );

        let dossier = document
            .select(&selectors.pressdossier_items_list_href)
            .map(|element| {
                let link = element.attr("href").unwrap().to_owned();
                let title = element
                    .attr("title")
                    .unwrap()
                    .to_owned()
                    .replace("Gehe zu ", "");

                Item { link, title }
            })
            .collect::<Vec<_>>();

        if !dossier.is_empty() {
            page += 1;
        } else {
            break;
        }

        (results, errors) = fetch_many(results, errors, dossier, |item| async move {
            write_dossier_details(dir, client, source, selectors, item).await
        })
        .await;
    }

    Ok((page - 1, results, errors))
}

#[tracing::instrument(skip_all, fields(link = item.link.as_str()))]
async fn write_dossier_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.link).into_owned();

    let url = source.url.join(&item.link)?;

    let text = client
        .fetch_text(source, format!("{key}.isol"), &url)
        .await?;

    let document = Html::parse_document(&text);

    let issued = select_first_text(
        &document,
        &selectors.pressdossier_article_detail_issued,
        "Issued",
    )?
    .parse::<GermanDate>()?
    .into();

    let description = select_text(
        &document,
        &selectors.pressdossier_article_detail_description,
    );

    let resources = document
        .select(&selectors.pressdossier_article_detail_resources_href)
        .map(|element| {
            let href = element.attr("href").unwrap();

            Ok(Resource {
                r#type: ResourceType::WebPage,
                description: None,
                url: source.url.join(href)?.into(),
                ..Default::default()
            }
            .guess_or_keep_type())
        })
        .collect::<Result<SmallVec<_>>>()?;

    let dataset = Dataset {
        title: item.title,
        description: Some(description),
        organisations: smallvec![ORGANISATION_UBA],
        language: Language::German,
        license: License::OtherClosed,
        types: smallvec![Type::Text {
            text_type: TextType::PressRelease,
        }],
        origins: source.origins.clone(),
        resources,
        issued: Some(issued),
        source_url: url.into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_newsletter_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let mut source_url = source.url.join("/views/ajax")?;

    source_url
        .query_pairs_mut()
        .append_pair("page", &page.to_string())
        .append_pair("view_name", "overviews")
        .append_pair("view_display_id", "newsletter_archiv")
        .append_pair("view_args", "")
        .append_pair("view_path", "service/newsletter/archiv")
        .append_pair("view_base_path", "service/newsletter/archiv")
        .append_pair("view_dom_id", "d597e4d819be16f3d6804b15bc192f08")
        .append_pair("pager_element", "0");

    let text = client
        .fetch_text(source, format!("newsletters-{}", page), &source_url)
        .await?;

    let document = Html::parse_fragment(
        &from_json_str::<Vec<Data>>(&text)?
            .into_iter()
            .find_map(|element| element.data)
            .ok_or_else(|| anyhow!("Failed to fetch HTML structure from JSON"))?,
    );

    let count = parse_text::<_, usize>(
        &document,
        &selectors.newsletter_search_results,
        &selectors.range_count,
        "number of documents",
    )?;

    let max_page = count.div_ceil(BATCH_SIZE);

    let newsletter = document
        .select(&selectors.newsletter_items)
        .filter_map(|element| {
            element
                .select(&selectors.newsletter_items_list)
                .next()
                .map(|item_list| {
                    item_list
                        .select(&selectors.newsletter_items_href)
                        .filter_map(|element| {
                            let link = element.attr("href")?;
                            if !link.contains("newsletter") {
                                return None;
                            }

                            let title = element.attr("title")?.replace("Gehe zu ", "");

                            Some(Item {
                                link: link.to_owned(),
                                title,
                            })
                        })
                })
        })
        .flatten()
        .collect::<Vec<_>>();

    let (results, errors) = fetch_many(0, 0, newsletter, |item| async move {
        write_newsletter_details(dir, client, source, selectors, item).await
    })
    .await;

    Ok((max_page, results, errors))
}

#[tracing::instrument(skip_all, fields(link = item.link.as_str()))]
async fn write_newsletter_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.link).into_owned();

    let url = source.url.join(&item.link)?;

    let text = client
        .fetch_text(source, format!("{key}.isol"), &url)
        .await?;

    let document = Html::parse_document(&text);

    let issued = select_first_text(
        &document,
        &selectors.newsletter_article_detail_issued,
        "Issued",
    )?
    .parse::<GermanDate>()?
    .into();

    let description = select_text(&document, &selectors.newsletter_article_detail_description);

    let resources = document
        .select(&selectors.newsletter_article_detail_primary_resources_href)
        .map(|element| {
            let href = element.attr("href").unwrap();

            let description = collect_text(element.text());

            Ok(Resource {
                r#type: ResourceType::WebPage,
                description: Some(description),
                url: source.url.join(href)?.into(),
                ..Default::default()
            }
            .guess_or_keep_type())
        })
        .collect::<Result<SmallVec<_>>>()?;

    let persons = document
        .select(&selectors.newsletter_article_detail_persons)
        .filter_map(|element| {
            let newsletter_text = collect_text(element.text());

            newsletter_text
                .split(" ")
                .find(|element| element.contains("@"))
                .map(|email| {
                    let name = uppercase_words(&email.replace("@uba.de", "").replace(".", " "));

                    Person {
                        name,
                        role: PersonRole::Contact,
                        emails: smallvec![email.to_owned()],
                        ..Default::default()
                    }
                })
        })
        .collect::<Vec<_>>();

    let dataset = Dataset {
        title: item.title,
        types: smallvec![Type::Text {
            text_type: TextType::News,
        }],
        description: Some(description),
        persons,
        organisations: smallvec![ORGANISATION_UBA],
        language: Language::German,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        resources,
        issued: Some(issued),
        source_url: url.into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_press_releases_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    language_detector: &LanguageDetector,
    year: usize,
) -> Result<(usize, usize, usize)> {
    let mut source_url = source.url.join("/views/ajax")?;

    source_url
        .query_pairs_mut()
        .append_pair("year[value][year]", &year.to_string())
        .append_pair("view_name", "overviews")
        .append_pair("view_display_id", "pressinformation")
        .append_pair("view_args", "")
        .append_pair("view_path", "presse%2Fpressemitteilungen")
        .append_pair("view_base_path", "presse%2Fpressemitteilungen")
        .append_pair("view_dom_id", "71d0e9bb661c4b9261e19688f73d10d3")
        .append_pair("pager_element", "0");

    let text = client
        .fetch_text(source, format!("year-{}", year), &source_url)
        .await?;

    let document = Html::parse_fragment(
        &from_json_str::<Vec<Data>>(&text)?
            .into_iter()
            .find_map(|element| element.data)
            .ok_or_else(|| anyhow!("Failed to fetch HTML structure from JSON"))?,
    );

    let max_year = collect_text(
        document
            .select(&selectors.press_release_search_results)
            .next()
            .ok_or_else(|| anyhow!("No year of publication available found"))?
            .text(),
    )
    .parse::<usize>()?;

    let press_items = document
        .select(&selectors.press_release_items)
        .filter_map(|element| {
            element
                .select(&selectors.press_release_item_list)
                .next()
                .map(|item_list| {
                    item_list
                        .select(&selectors.press_release_item_article_href)
                        .map(|element| {
                            let link = element.attr("href").unwrap().to_owned();
                            let title = element.attr("title").unwrap().replace("Gehe zu ", "");

                            Item { link, title }
                        })
                })
        })
        .flatten()
        .collect::<Vec<_>>();

    let (results, errors) = fetch_many(0, 0, press_items, |item| async move {
        write_press_release_details(dir, client, source, selectors, language_detector, item).await
    })
    .await;

    Ok((max_year, results, errors))
}

#[tracing::instrument(skip_all, fields(link = item.link.as_str()))]
async fn write_press_release_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    language_detector: &LanguageDetector,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.link).into_owned();

    let url = source.url.join(&item.link)?;

    let text = client
        .fetch_text(source, format!("{key}.isol"), &url)
        .await?;

    let document = Html::parse_document(&text);

    let issued = select_first_text(
        &document,
        &selectors.press_release_article_detail_issued,
        "Issued",
    )?
    .parse::<GermanDate>()?
    .into();

    let description = select_text(
        &document,
        &selectors.press_release_article_detail_description,
    );

    let mut resources = document
        .select(&selectors.press_release_article_detail_resources)
        .filter_map(|element| {
            element
                .select(&selectors.press_release_article_detail_resources_list)
                .next()
                .map(|resource_list| {
                    resource_list
                        .select(&selectors.press_release_article_detail_resources_href)
                        .map(|element| {
                            let url = element.attr("href").unwrap().to_owned();

                            let description = element
                                .attr("title")
                                .unwrap()
                                .to_owned()
                                .replace("Gehe zu ", "");

                            Resource {
                                r#type: ResourceType::Pdf,
                                description: Some(description),
                                url,
                                ..Default::default()
                            }
                            .guess_or_keep_type()
                        })
                })
        })
        .flatten()
        .collect::<SmallVec<[Resource; 4]>>();

    resources.extend(
        document
            .select(&selectors.press_release_article_detail_primary_download_list)
            .flat_map(|element| {
                element
                    .select(&selectors.press_release_article_detail_primary_download_href)
                    .map(|element| {
                        let url = element.attr("href").unwrap().to_owned();

                        let description = element.attr("title").unwrap().to_owned();

                        Resource {
                            r#type: ResourceType::WebPage,
                            description: Some(description),
                            url,
                            primary_content: true,
                            ..Default::default()
                        }
                        .guess_or_keep_type()
                    })
            }),
    );

    let persons = document
        .select(&selectors.press_release_article_detail_persons)
        .filter_map(|element| {
            let names = collect_text(element.text());

            names.split(" +").next().map(|name| {
                let emails = element
                    .select(&selectors.press_release_article_detail_persons_mail)
                    .map(|element| {
                        let mail_text = collect_text(element.text());
                        mail_text.replace(" [dot] ", ".").replace(" [at] ", "@")
                    })
                    .collect();

                Person {
                    name: name.to_owned(),
                    role: PersonRole::Contact,
                    emails,
                    ..Default::default()
                }
            })
        })
        .collect::<Vec<_>>();

    let mut dataset = Dataset {
        title: item.title,
        types: smallvec![Type::Text {
            text_type: TextType::PressRelease,
        }],
        description: Some(description),
        persons,
        organisations: smallvec![ORGANISATION_UBA],
        license: License::OtherClosed,
        origins: source.origins.clone(),
        resources,
        issued: Some(issued),
        source_url: url.into(),
        ..Default::default()
    };

    language_detector.guess_or(&mut dataset, Language::German);

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_publication_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    language_detector: &LanguageDetector,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let mut source_url = source.url.join("/views/ajax")?;

    source_url
        .query_pairs_mut()
        .append_pair("page", &page.to_string())
        .append_pair("view_name", "publications")
        .append_pair("view_display_id", "overview")
        .append_pair("view_args", "")
        .append_pair("view_path", "publikationen")
        .append_pair("view_base_path", "publikationen");

    let text = client
        .fetch_text(source, format!("page-{}", page), &source_url)
        .await?;

    let document = Html::parse_fragment(
        &from_json_str::<Vec<Data>>(&text)?
            .into_iter()
            .find_map(|element| element.data)
            .ok_or_else(|| anyhow!("Failed to fetch HTML structure from JSON"))?,
    );

    let count = parse_text::<_, usize>(
        &document,
        &selectors.publication_search_results,
        &selectors.range_count,
        "number of documents",
    )?;

    let max_page = count.div_ceil(BATCH_SIZE);

    let items = document
        .select(&selectors.publication_item)
        .map(|element| {
            let link_element = element
                .select(&selectors.publication_page_link)
                .next()
                .ok_or_else(|| anyhow!("No link"))?;

            let href = link_element.attr("href").unwrap().to_owned();

            let title = select_text(element, &selectors.publication_page_title);

            Ok(Item { link: href, title })
        })
        .collect::<Result<Vec<_>>>()?;

    let (results, errors) = fetch_many(0, 0, items, |item| async move {
        write_publication_details(dir, client, source, selectors, language_detector, item).await
    })
    .await;

    Ok((max_page, results, errors))
}

#[tracing::instrument(skip_all, fields(link = item.link.as_str()))]
async fn write_publication_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    language_detector: &LanguageDetector,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.link).into_owned();

    let url = source.url.join(&item.link)?;

    let text = client
        .fetch_text(source, format!("{key}.isol"), &url)
        .await?;

    let document = Html::parse_document(&text);

    let mut description = select_text(&document, &selectors.publication_detail_description);

    let mut resources = document
        .select(&selectors.publication_detail_download)
        .map(|element| {
            let url = element.attr("href").unwrap().to_owned();

            Resource {
                r#type: ResourceType::Pdf,
                description: None,
                url,
                primary_content: true,
                ..Default::default()
            }
            .guess_or_keep_type()
        })
        .collect::<SmallVec<_>>();

    let mut series = None;
    let mut language = Language::Unknown;
    let mut persons = Vec::new();
    let mut issued = None;

    {
        let element = document
            .select(&selectors.publication_detail_item)
            .next()
            .ok_or_else(|| anyhow!("Could not find publication_detail_item"))?;

        for info_box in element.select(&selectors.publication_detail_info_box) {
            let key = select_text(info_box, &selectors.publication_detail_info_box_key);

            match key.as_str() {
                "Reihe" => {
                    let value = select_text(info_box, &selectors.publication_detail_info_box_value);

                    let value = selectors
                        .mark_whitespace
                        .replace_all(&value, " ")
                        .into_owned();

                    series = Some(value);
                }
                "Autor(en)" => {
                    let value = select_text(info_box, &selectors.publication_detail_info_box_value);

                    persons = value
                        .split(", ")
                        .map(|element| Person {
                            name: element.to_owned(),
                            role: PersonRole::Creator,
                            ..Default::default()
                        })
                        .collect();
                }
                "Erscheinungsjahr" => {
                    let time = info_box
                        .select(&selectors.publication_detail_info_box_time)
                        .next()
                        .ok_or_else(|| anyhow!("No issued date found for publication"))?;

                    let datetime = time.attr("datetime").unwrap();

                    issued = Some(Date::parse(datetime, &Iso8601::DEFAULT)?);
                }
                "Sprache" => {
                    let value = select_text(info_box, &selectors.publication_detail_info_box_value);

                    language = value.as_str().into();
                }
                _ => (),
            }
        }

        resources.extend(
            element
                .select(&selectors.publication_detail_info_box_links)
                .map(|element| {
                    let url = element.attr("href").unwrap().to_owned();
                    let description = collect_text(element.text());

                    Resource {
                        r#type: ResourceType::WebPage,
                        description: Some(description),
                        url,
                        ..Default::default()
                    }
                    .guess_or_keep_type()
                }),
        );
    }

    if let Some(series) = series {
        description = format!("{} Veröffentlicht in {}.", description, series);
    }

    let mut dataset = Dataset {
        title: item.title,
        types: smallvec![Type::Text {
            text_type: TextType::Publication,
        }],
        description: Some(description),
        persons,
        organisations: smallvec![ORGANISATION_UBA],
        language,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        resources,
        issued,
        source_url: url.into(),
        ..Default::default()
    };

    if dataset.language == Language::Unknown {
        language_detector.guess_or(&mut dataset, Language::Unknown)
    }

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Deserialize)]
struct Data {
    data: Option<String>,
}

#[derive(Debug)]
struct Item {
    link: String,
    title: String,
}

const BATCH_SIZE: usize = 8;

const ORGANISATION_UBA: Organisation = Organisation::WikiData {
    identifier: OrganisationKey::UBA,
    role: OrganisationRole::Publisher,
};

selectors! {
    range_count: r"(\d{1,})" as Regex,

    pressdossier_items_list_href: ".span8.article-text > h2 a[href]",
    pressdossier_article_detail_issued: ".span9 time.date-display-single, .article-meta time",
    pressdossier_article_detail_description: ".article-content > strong",
    pressdossier_article_detail_resources_href: ".row-fluid > .article-content > p a[href]:not(.lexicon.tooltip-link)",

    newsletter_search_results: ".results-total",
    newsletter_items: ".article-list",
    newsletter_items_list: ".view-content",
    newsletter_items_href: ".clearfix a[href]",
    newsletter_article_detail_issued: "p time.date-display-single",
    newsletter_article_detail_description: ".article-content .content-box .row-fluid .article-content > p, .article-content.box > .content-box.no-spacing.centered-image",

    newsletter_article_detail_primary_resources_href: ".row-fluid .article-content .button",
    newsletter_article_detail_resources_href: ".article-content.box > .unstyled-list > li > .content a[href],
                                               .article-list.box > article.clearfix.box > .row-fluid > .span8.article-text a[href],
                                               .article-list.box > .publication > .content-box-content a[href],
                                               .article-list.box > .unstyled-list a[href],
                                               .content-box.no-spacing.centered-image a[href]",

    newsletter_article_detail_persons: ".article-content.box > .content-box > p ",

    press_release_search_results: ".list-tabs.tab-navi.exposed-filter li",
    press_release_items: ".view-content",
    press_release_item_list: ".document-list",
    press_release_item_article_href: ".span10.article-text a[href]",
    press_release_article_detail_issued: ".span9 time.date-display-single, .article-meta time",
    press_release_article_detail_description: ".article-content .subtitle, .article-content strong p, .span9 .article-content p",
    press_release_article_detail_primary_download_list: ".article-sidebar.span3",
    press_release_article_detail_primary_download_href: "a[href]",
    press_release_article_detail_resources: ".article-sidebar.span3",
    press_release_article_detail_resources_list: ".styled-list",
    press_release_article_detail_resources_href: ".link-external a[href]",
    press_release_article_detail_persons: ".sidebar-box-content .unstyled.sections-list li.section ul.unstyled li",
    press_release_article_detail_persons_mail: ".sidebar-box-content .unstyled.sections-list li.section ul.unstyled li .spamspan",

    publication_search_results: ".results-total",
    publication_item: ".no-background .row-fluid .span9",
    publication_page_link: " h2 a[href]",
    publication_page_title: "h2",
    publication_detail_download: ".content-box .content-box-actions a[href]",
    publication_detail_description: ".content-box .span9 .article-content p",
    publication_detail_item: ".content-box-content.document-info",
    publication_detail_info_box: ".row-fluid",
    publication_detail_info_box_key: "div.span3.info-title",
    publication_detail_info_box_value: "div.span9",
    publication_detail_info_box_time: "time[datetime]",
    publication_detail_info_box_links: ".link-external a[href]",

    mark_whitespace: r"\s{2,}" as Regex,
}
