use anyhow::{Result, anyhow};
use cap_std::fs::Dir;
use hashbrown::HashSet;
use itertools::Itertools;
use scraper::{Html, Selector};
use smallvec::smallvec;

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key, select_first_text, select_text},
    write_dataset,
};
use metadaten::dataset::{
    Dataset, Resource, ResourceType, Tag,
    r#type::{TextType, Type},
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) =
        fetch_product_categories(dir, client, source, selectors).await?;

    let (count1, results1, errors1) = fetch_publications(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_media_files(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) =
        fetch_background_information(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_product_categories(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let categories = {
        let url = source.url.join("/de/produktwelt")?;

        let text = client
            .fetch_text(source, "product-categories".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.product_categories)
            .map(|element| {
                let href = element.attr("href").unwrap();

                href.split('/').take(4).join("/")
            })
            .collect::<HashSet<_>>()
    };

    let count = categories.len();

    let (results, errors) = fetch_many(0, 0, categories, |category| async move {
        let url = source.url.join(&category)?;

        let key = make_key(&category);

        let title;
        let description;
        let resources;

        {
            let text = client
                .fetch_text(source, format!("product-category-{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            title = select_first_text(&document, &selectors.product_category_title, "title")?;

            description = select_text(&document, &selectors.product_category_description);

            resources = document
                .select(&selectors.product_category_resources)
                .map(|element| {
                    let text = collect_text(element.text());

                    let href = element.attr("href").unwrap();

                    Ok(Resource {
                        r#type: ResourceType::WebPage,
                        description: Some(text),
                        url: source.url.join(href)?.into(),
                        ..Default::default()
                    }
                    .guess_or_keep_type())
                })
                .collect::<Result<_>>()?;
        }

        let dataset = Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Editorial
            }],
            resources,
            source_url: url.into(),
            origins: source.origins.clone(),
            ..Default::default()
        };

        write_dataset(
            dir,
            client,
            source,
            format!("product-category-{key}"),
            dataset,
        )
        .await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_publications(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let formats = {
        let url = source.url.join("/de/aktuelles/publikationen")?;

        let text = client
            .fetch_text(source, "publication-formats".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.publications_formats)
            .filter_map(|element| {
                let value = element.attr("value").unwrap();

                if value != "All" {
                    Some(value.to_owned())
                } else {
                    None
                }
            })
            .collect::<HashSet<_>>()
    };

    let (results, errors) = fetch_many(0, 0, formats, |format| async move {
        let links = {
            let mut url = source.url.join("/de/aktuelles/publikationen")?;

            url.query_pairs_mut()
                .append_pair("field_publication_format_target_id", &format);

            let key = make_key(&format);

            let text = client
                .fetch_text(source, format!("publications-{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            document
                .select(&selectors.publications_links)
                .map(|element| element.attr("href").unwrap().to_owned())
                .collect::<Vec<_>>()
        };

        let count = links.len();

        let (results, errors) = fetch_many(0, 0, links, |link| async move {
            let url = source.url.join(&link)?;

            let key = make_key(&link);

            let title;
            let description;
            let resources;
            let tags;

            {
                let text = client
                    .fetch_text(source, format!("publications-{key}.isol"), &url)
                    .await?;

                let document = Html::parse_document(&text);

                title = select_first_text(&document, &selectors.publications_title, "title")?;

                description = select_text(&document, &selectors.publications_description);

                resources = document
                    .select(&selectors.publications_resources)
                    .map(|element| {
                        let text = collect_text(element.text());

                        let href = element.attr("href").unwrap();

                        Ok(Resource {
                            r#type: ResourceType::WebPage,
                            description: Some(text),
                            url: source.url.join(href)?.into(),
                            primary_content: true,
                            ..Default::default()
                        }
                        .guess_or_keep_type())
                    })
                    .collect::<Result<_>>()?;

                tags = document
                    .select(&selectors.publications_tags)
                    .map(|element| {
                        let text = collect_text(element.text());
                        Tag::Other(text.into())
                    })
                    .collect();
            }

            let dataset = Dataset {
                title,
                description: Some(description),
                types: smallvec![Type::Text {
                    text_type: TextType::Publication
                }],
                resources,
                tags,
                source_url: url.into(),
                origins: source.origins.clone(),
                ..Default::default()
            };

            write_dataset(dir, client, source, format!("publication-{key}"), dataset).await
        })
        .await;

        Ok((count, results, errors))
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_media_files(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let formats = {
        let url = source.url.join("/de/aktuelles/mediathek")?;

        let text = client
            .fetch_text(source, "media-file-formats".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.media_files_formats)
            .filter_map(|element| {
                if element.attr("data-target-id").unwrap() == "All" {
                    return None;
                }

                Some(element.attr("href").unwrap().to_owned())
            })
            .collect::<HashSet<_>>()
    };

    let (results, errors) = fetch_many(0, 0, formats, |format| async move {
        let key = &make_key(&format);

        struct Teaser {
            id: usize,
            title: String,
            description: Option<String>,
            source_url: String,
        }

        let teasers = {
            let url = source
                .url
                .join(&format!("/de/aktuelles/mediathek{format}"))?;

            let text = client
                .fetch_text(source, format!("media-files-{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            document
                .select(&selectors.media_files_teasers)
                .map(|element| {
                    let id = element.attr("data-nid").unwrap().parse()?;

                    let title =
                        select_first_text(element, &selectors.media_files_teaser_title, "title")?;

                    let description = element
                        .select(&selectors.media_files_teaser_description)
                        .next()
                        .map(|element| collect_text(element.text()));

                    let href = element
                        .select(&selectors.media_files_teaser_source_url)
                        .next()
                        .ok_or_else(|| anyhow!("Missing source URL"))?
                        .attr("href")
                        .unwrap();

                    let source_url = source.url.join(href)?.into();

                    Ok(Teaser {
                        id,
                        title,
                        description,
                        source_url,
                    })
                })
                .collect::<Result<Vec<_>>>()?
        };

        let count = teasers.len();

        let (results, errors) = fetch_many(0, 0, teasers, |teaser| async move {
            let dataset = Dataset {
                title: teaser.title,
                description: teaser.description,
                types: smallvec![Type::Image],
                source_url: teaser.source_url,
                origins: source.origins.clone(),
                ..Default::default()
            };

            write_dataset(
                dir,
                client,
                source,
                format!("meda-file-{key}-{}", teaser.id),
                dataset,
            )
            .await
        })
        .await;

        Ok((count, results, errors))
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_background_information(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    const PAGES: &[&str] = &[
        "",
        "orientierung-beim-einkauf",
        "wissenschaftlich-erarbeitet",
        "umweltzeichen-mit-geschichte",
        "kooperationen-mit-anderen-labeln",
    ];

    let count = PAGES.len();

    let (results, errors) = fetch_many(0, 0, PAGES, |page| async move {
        let url = source.url.join(&format!(
            "/de/blauer-engel/unser-zeichen-fuer-die-umwelt/{page}"
        ))?;

        let key = format!("background-information-{page}");

        let mut title;
        let description;

        {
            let text = client
                .fetch_text(source, format!("{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            title = document
                .select(&selectors.background_breadcrumbs)
                .skip(1)
                .map(|element| collect_text(element.text()))
                .join(" - ");

            let title1 = select_first_text(&document, &selectors.background_title, "title")?;

            if !title.is_empty() {
                title.push_str(": ");
            }
            title.push_str(&title1);

            description = select_text(&document, &selectors.background_description);
        }

        let dataset = Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Editorial
            }],
            source_url: url.into(),
            origins: source.origins.clone(),
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

selectors! {
    product_categories: "div.m-bep_az-list__group li a[href]",
    product_category_title: "div.container h1",
    product_category_description: "div.m-bep_raluz__body, div.m-bep_raluz__advantage__inner",
    product_category_resources: "ul.m-quicknavi--level2 a[href]",
    publications_formats: "select#edit-field-publication-format-target-id option[value]",
    publications_links: "a.m-publications-teaser__link[href]",
    publications_title: "div.container h1",
    publications_description: "div.m-node--publication__teaser, div.m-node--publication__body",
    publications_resources: "div.m-attachments__downloads a[href], div.m-attachments__links a[href]",
    publications_tags: "div.m-node--publication__format a",
    media_files_formats: "a.m-form__viewsexposedform__button[href][data-target-id]",
    media_files_teasers: "div.m-media-teaser[data-nid]",
    media_files_teaser_title: "h3.m-media-teaser__title",
    media_files_teaser_description: "div.m-media-teaser__teaser",
    media_files_teaser_source_url: "div.m-media-teaser__downloads a[href], div.m-media-teaser__links a[href]",
    background_breadcrumbs: "div.m-breadcrumb__content li span",
    background_title: "h1.m-node--page__title",
    background_description: "div.m-node--page__body, div.m-paragraph--text",
}
