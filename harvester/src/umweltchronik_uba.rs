use anyhow::{Context, Result, anyhow};
use cap_std::fs::Dir;
use regex::Regex;
use serde::Deserialize;
use serde_roxmltree::{from_doc, roxmltree::Document};
use smallvec::{SmallVec, smallvec};
use time::{Date, format_description::FormatItem, macros::format_description};

use harvester::{
    Source, client::Client, fetch_many, suppress_errors, utilities::yesterday, write_dataset,
};
use metadaten::{
    dataset::{Dataset, Language, License, Resource, ResourceType, Tag, r#type::Type},
    umthes::extract_id as extract_umthes_id,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let id_format = &Regex::new(r"/chronik/(?<id>.+)").unwrap();

    let (count, results, errors) = fetch_page(dir, client, source, id_format, 1).await?;

    let pages = count.div_ceil(40);

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| async move {
        fetch_page(dir, client, source, id_format, page)
            .await
            .with_context(|| format!("Failed to fetch page {page}"))
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    id_format: &Regex,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let mut url = source.url.join("de/chronicle.rdf")?;

    url.query_pairs_mut().append_pair("page", &page.to_string());

    let text = client
        .fetch_text(source, format!("page-{}", page), &url)
        .await?;

    let document = Document::parse(&text)?;

    let page = from_doc::<Page>(&document)?;

    let count = page
        .entries
        .iter()
        .find_map(|entry| entry.total_results)
        .unwrap_or(0);

    let (results, errors) = fetch_many(0, 0, page.entries, |entry| async move {
        if entry.total_results.is_some() {
            return Ok((0, 0, 0));
        }

        let key = id_format
            .captures(&entry.about)
            .ok_or_else(|| anyhow!("Malformed about URL `{}`", entry.about))?
            .name("id")
            .unwrap()
            .as_str()
            .to_owned();

        let title = entry
            .pref_labels
            .into_iter()
            .find(|pref_label| pref_label.lang == Some("de"))
            .ok_or_else(|| anyhow!("Missing title"))?
            .text
            .to_owned();

        let description = entry
            .definitions
            .into_iter()
            .find(|definition| definition.lang == Some("de"))
            .map(|definition| definition.text.to_owned());

        let mut time_ranges = SmallVec::new();

        if let Some(temporal) = entry.temporal {
            let start = Date::parse(temporal.description.start, DATE_FORMAT)?;
            let end = if !temporal.description.end.is_empty() {
                Date::parse(temporal.description.end, DATE_FORMAT)?
            } else {
                yesterday()
            };
            suppress_errors(|| time_ranges.push((start, end).into()));
        }

        let mut resources = entry
            .see_also
            .into_iter()
            .map(|see_also| {
                Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(see_also.description.title),
                    url: see_also.description.page.resource,
                    ..Default::default()
                }
                .guess_or_keep_type()
            })
            .collect::<SmallVec<[Resource; 4]>>();

        resources.push(Resource {
            url: {
                let mut url = entry.about.clone();
                url.push_str(".rdf");
                url
            },
            r#type: ResourceType::RdfXml,
            primary_content: true,
            ..Default::default()
        });

        let tags = entry
            .what_happened
            .into_iter()
            .filter_map(|what_happened| {
                extract_umthes_id(what_happened.resource)
                    .ok()
                    .map(Tag::Umthes)
            })
            .collect();

        let dataset = Dataset {
            title,
            description,
            types: smallvec![Type::Event],
            time_ranges,
            resources,
            tags,
            source_url: entry.about,
            language: Language::German,
            origins: source.origins.clone(),
            license: License::CcByNcNd40,
            machine_readable_source: true,
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

#[derive(Debug, Deserialize)]
struct Page<'a> {
    #[serde(rename = "Description", borrow)]
    entries: Vec<Entry<'a>>,
}

#[derive(Debug, Deserialize)]
struct Entry<'a> {
    #[serde(rename = "totalResults")]
    total_results: Option<usize>,
    about: String,
    #[serde(rename = "prefLabel", default, borrow)]
    pref_labels: Vec<PrefLabel<'a>>,
    #[serde(rename = "definition", default, borrow)]
    definitions: Vec<Definition<'a>>,
    #[serde(borrow)]
    temporal: Option<Temporal<'a>>,
    #[serde(rename = "seeAlso", default)]
    see_also: Vec<SeeAlso>,
    #[serde(rename = "whatHappened", default, borrow)]
    what_happened: Vec<WhatHappened<'a>>,
}

#[derive(Debug, Deserialize)]
struct PrefLabel<'a> {
    lang: Option<&'a str>,
    #[serde(rename = "$text")]
    text: &'a str,
}

#[derive(Debug, Deserialize)]
struct Definition<'a> {
    lang: Option<&'a str>,
    #[serde(rename = "$text")]
    text: &'a str,
}

#[derive(Debug, Deserialize)]
struct Temporal<'a> {
    #[serde(rename = "Description", borrow)]
    description: TemporalDescription<'a>,
}

#[derive(Debug, Deserialize)]
struct TemporalDescription<'a> {
    start: &'a str,
    end: &'a str,
}

#[derive(Debug, Deserialize)]
struct SeeAlso {
    #[serde(rename = "Description")]
    description: SeeAlsoDescription,
}

#[derive(Debug, Deserialize)]
struct SeeAlsoDescription {
    title: String,
    page: SeeAlsoPage,
}

#[derive(Debug, Deserialize)]
struct SeeAlsoPage {
    resource: String,
}

#[derive(Debug, Deserialize)]
struct WhatHappened<'a> {
    resource: &'a str,
}

const DATE_FORMAT: &[FormatItem] = format_description!(version = 2, "[year]-[month]-[day]");
