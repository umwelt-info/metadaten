use std::borrow::Cow;

use anyhow::{Result, ensure};
use cap_std::fs::Dir;
use serde::Deserialize;
use serde_json::from_slice;
use smallvec::{SmallVec, smallvec};
use time::{Date, macros::format_description};

use harvester::{
    Source, client::Client, fetch_many, utilities::point_like_bounding_box, write_dataset,
};
use metadaten::{
    dataset::{
        Dataset, Language, License, Organisation, OrganisationKey, OrganisationRole, Region,
        r#type::{Domain, Station, Type},
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let mut url = source.url.clone();
    url.query_pairs_mut().append_pair("page[limit]", "100");

    let bytes = client
        .fetch_bytes(source, "stations".to_owned(), &url)
        .await?;

    let stations = from_slice::<Stations>(&bytes)?;

    ensure!(stations.count < 100);

    let count = stations.count;

    let (results, errors) = fetch_many(0, 0, stations.data, |station| {
        translate_dataset(dir, client, source, station)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    station: PegelStation<'_>,
) -> Result<(usize, usize, usize)> {
    let key = format!("dataset-{}", station.id);

    let station_id = station.attributes.id.to_string();

    let title = format!(
        "Fließgewässergütedaten der Online-Messstelle {} ({})",
        station.attributes.name, station.attributes.gewaesser,
    );

    let description = format!(
        "Die Online-Messstation {} befindet sich am Fluss {} und wird betrieben von der LUBW.
        Es werden kontinuierlich Güteparameter gemessen.
        Die aktuellen Daten der vergangenen sechs Wochen der Messstationen finden Sie als Stundenmittelwerte unter 'Messwerte' über die Stationsauswahl auf der Website.
        Bitte beachten Sie, dass es sich bei den dargestellten Messwerten um ungeprüfte Rohdaten handelt.
        Die mehrjährigen Tagesmittelwerte der an den Online-Messstationen gemessenen Parameter können im Daten- und Kartendienst der LUBW , ehemals Jahresdatenkatalog genannt, interaktiv abgerufen werden.",
        station.attributes.name, station.attributes.gewaesser,
    );

    let mut types = SmallVec::new();

    const PARAMETERS: &[(&str, &str)] = &[
        ("temp", "Temperatur"),
        ("o2", "Sauerstoff"),
        ("pH", "pH-Wert"),
        ("leitfaehigkeit", "Leitfähigkeit"),
        ("truebung", "Trübung"),
    ];

    for &(parameter, measured_variable) in PARAMETERS {
        if station.attributes.parameter.contains(parameter) {
            types.push(Type::Measurements {
                domain: Domain::Rivers,
                station: Some(Station {
                    id: Some(station_id.as_str().into()),
                    ..Default::default()
                }),
                measured_variables: smallvec![measured_variable.to_owned()],
                methods: Default::default(),
            });
        }
    }

    let organisations = smallvec![Organisation::WikiData {
        identifier: OrganisationKey::LUBW,
        role: OrganisationRole::Operator,
    }];

    let bounding_boxes = smallvec![point_like_bounding_box(
        station.attributes.lat,
        station.attributes.lon
    )];

    let mut regions = smallvec![Region::BW];
    regions.extend(
        WISE.match_shape(station.attributes.lon, station.attributes.lat)
            .map(Region::Watershed),
    );

    let modified = Date::parse(
        station.attributes.ts_format_mez,
        format_description!("[day].[month].[year] [hour]:[minute]"),
    )?;

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        organisations,
        bounding_boxes,
        regions,
        modified: Some(modified),
        language: Language::German,
        license: License::OtherClosed,
        source_url: source.source_url().replace("{id}", &station_id),
        origins: source.origins.clone(),
        machine_readable_source: true,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Deserialize)]
struct Stations<'a> {
    count: usize,
    #[serde(borrow)]
    data: Vec<PegelStation<'a>>,
}

#[derive(Deserialize)]
struct PegelStation<'a> {
    id: &'a str,
    #[serde(borrow)]
    attributes: Attributes<'a>,
}

#[derive(Deserialize)]
struct Attributes<'a> {
    #[serde(borrow)]
    name: Cow<'a, str>,
    #[serde(borrow)]
    gewaesser: Cow<'a, str>,
    lon: f64,
    lat: f64,
    id: u32,
    parameter: &'a str,
    #[serde(rename = "ts-format-mez")]
    ts_format_mez: &'a str,
}
