use anyhow::Result;
use cap_std::fs::Dir;
use itertools::Itertools;
use miniproj::{Projection, get_projection};
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_json::from_str as from_json_str;
use smallvec::{SmallVec, smallvec};
use time::{Date, format_description::FormatItem, macros::format_description};
use url::Url;

use harvester::{
    Source,
    client::{Client, TrackedResponse},
    fetch_many, selectors,
    utilities::{collect_text, make_key, point_like_bounding_box, select_text},
    write_dataset,
};
use metadaten::{
    dataset::{
        Dataset, Language, License, Region, Resource, ResourceType,
        r#type::{Domain, Station, Type},
    },
    proj_rect,
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let (count, results, errors) = fetch_links(dir, client, source).await?;

    let (count1, results1, errors1) = fetch_api(dir, client, source).await?;

    Ok((count + count1, results + results1, errors + errors1))
}

async fn fetch_links(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let mut all_links = fetch_mainlinks(client, source, selectors).await?;

    let sublinks = fetch_sublinks(client, source, &all_links, selectors).await?;

    all_links.extend(sublinks);

    all_links.sort_unstable();
    all_links.dedup();

    let count = all_links.len();

    let (results, errors) = fetch_many(0, 0, all_links, |link| {
        write_datasets(dir, client, source, selectors, link)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_mainlinks(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<Item>> {
    let text = client
        .fetch_text(source, "mainlinks".to_owned(), &source.url)
        .await?;

    let document = Html::parse_document(&text);

    let links: Vec<_> = document
        .select(&selectors.mainlink_selector)
        .filter_map(|element| {
            let link = element.attr("href").unwrap();

            if !link.starts_with("/auskunftssysteme") && !link.starts_with("/fachverfahren") {
                return None;
            }

            let title = collect_text(element.text());

            Some(Item {
                link: link.to_owned(),
                title,
            })
        })
        .collect();

    Ok(links)
}

async fn fetch_sublinks(
    client: &Client,
    source: &Source,
    items: &[Item],
    selectors: &Selectors,
) -> Result<Vec<Item>> {
    let mut sublinks = Vec::new();

    for item in items {
        let url = source.url.join(&item.link)?;
        let key = make_key(&item.link).into_owned();

        let text = client.fetch_text(source, key.to_owned(), &url).await?;
        let document = Html::parse_document(&text);

        sublinks.extend(document.select(&selectors.sublink_selector).map(|element| {
            let link = element.attr("href").unwrap();

            let mut title = select_text(element, &selectors.sublink_title);

            if title.is_empty() {
                title = collect_text(element.text());
            }

            Item {
                link: link.to_owned(),
                title,
            }
        }));
    }

    Ok(sublinks)
}

async fn write_datasets(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let Item { title, link } = item;

    let key = make_key(&link).into_owned();

    let dataset = {
        let url = source.url.join(&link)?;

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;
        let document = Html::parse_document(&text);

        let description = select_text(&document, &selectors.link_description);

        let resources = document
            .select(&selectors.link_resources)
            .filter_map(|element| {
                let href = element.attr("href").unwrap();

                if href.starts_with("javascript:") {
                    return None;
                }

                let description = collect_text(element.text()).replace("Mehr", "");

                if description.is_empty() {
                    return None;
                }

                Some((description, href))
            })
            .map(|(description, href)| {
                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        Dataset {
            title,
            description: Some(description),
            resources,
            regions: smallvec![Region::RLP],
            language: Language::German,
            license: License::AllRightsReserved,
            origins: source.origins.clone(),
            source_url: url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_api(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let projection = get_projection(25832).unwrap();

    let geodienste_url =
        &Url::parse("https://geodienste-wasser.rlp-umwelt.de/geoserver/wfs").unwrap();

    let geodaten_url = &Url::parse("https://geodaten-wasser.rlp-umwelt.de/").unwrap();

    let surfacewater = {
        let mut surfacewater_url = geodienste_url.clone();

        surfacewater_url
            .query_pairs_mut()
            .append_pair("request", "GetFeature")
            .append_pair("typeName", "messstellen:fliessgewaesser")
            .append_pair("outputFormat", "application/json");

        let surfacewater_text = client
            .fetch_text(source, "messstellen_sw".to_owned(), &surfacewater_url)
            .await?;

        let surfacewater_json = from_json_str::<SWJson>(&surfacewater_text)?;

        surfacewater_json
            .features
            .into_iter()
            .map(|feature| {
                let id = feature.id.replace("fliessgewaesser.", "");

                Surfacewater {
                    name: feature.properties.name,
                    division_kind: feature.properties.division_kind,
                    id,
                    coordinates: feature.geometry.coordinates,
                }
            })
            .collect::<Vec<_>>()
    };

    let mut count = surfacewater.len();

    let (mut results, mut errors) = fetch_many(0, 0, surfacewater, |item| {
        write_datasets_surfacewater(dir, client, source, projection, geodaten_url, item)
    })
    .await;

    let groundwater = {
        let mut groundwater_url = geodienste_url.clone();

        groundwater_url
            .query_pairs_mut()
            .append_pair("request", "GetFeature")
            .append_pair("typeName", "messstellen:grundwasser")
            .append_pair("outputFormat", "application/json");

        let groundwater_text = client
            .fetch_text(source, "messstellen_gw".to_owned(), &groundwater_url)
            .await?;

        let groundwater_json = from_json_str::<GWJson>(&groundwater_text)?;

        groundwater_json
            .features
            .into_iter()
            .filter(|feature| {
                !(feature.properties.num_analysis.is_none()
                    && feature.properties.year_from.is_none())
            })
            .map(|feature| {
                let id = feature.id.replace("grundwasser.", "");

                Groundwater {
                    name: feature.properties.name,
                    kind_name: feature.properties.kind_name,
                    id,
                    coordinates: feature.geometry.coordinates,
                }
            })
            .collect::<Vec<_>>()
    };

    count += groundwater.len();

    (results, errors) = fetch_many(results, errors, groundwater, |item| {
        write_datasets_groundwater(dir, client, source, projection, geodaten_url, item)
    })
    .await;

    let lakes = {
        let lake_url = geodaten_url.join("api/data/seenatlas_stammdaten_seenliste")?;

        let lake_text = fetch_geodaten_text(client, source, "lakes".to_owned(), &lake_url).await?;

        let lakes_json = from_json_str::<Vec<GeoLake>>(&lake_text)?;

        lakes_json
            .into_iter()
            .map(|element| {
                let region = element
                    .district_number
                    .map(|number| format!("07{number}"))
                    .or(element.district_name)
                    .or(element.municipality);

                Ok(Lake {
                    id: element.lake_id,
                    name: element.lake_name,
                    number: element.lake_number,
                    natural_region: element.natural_region,
                    region,
                })
            })
            .collect::<Result<Vec<_>>>()?
    };

    count += lakes.len();

    (results, errors) = fetch_many(results, errors, lakes, |item| {
        write_datasets_lakes(dir, client, source, geodaten_url, item)
    })
    .await;

    let pegel = {
        let mut pegel_url = geodienste_url.clone();

        pegel_url
            .query_pairs_mut()
            .append_pair("request", "GetFeature")
            .append_pair("typeName", "messstellen:pegel")
            .append_pair("outputFormat", "application/json");

        let pegel_text = client
            .fetch_text(source, "messstellen_pegel".to_owned(), &pegel_url)
            .await?;

        let pegel_json = from_json_str::<PegelJson>(&pegel_text)?;

        pegel_json
            .features
            .into_iter()
            .map(|feature| {
                let id = feature.id.replace("pegel.", "");

                Pegel {
                    id,
                    name: feature.properties.name,
                    designation: feature.properties.designation,
                    coordinates: feature.geometry.coordinates,
                }
            })
            .collect::<Vec<_>>()
    };

    count += pegel.len();

    (results, errors) = fetch_many(results, errors, pegel, |item| {
        write_datasets_pegel(dir, client, source, projection, geodaten_url, item)
    })
    .await;

    Ok((count, results, errors))
}

async fn write_datasets_surfacewater(
    dir: &Dir,
    client: &Client,
    source: &Source,
    projection: &dyn Projection,
    geodaten_url: &Url,
    item: Surfacewater,
) -> Result<(usize, usize, usize)> {
    let Surfacewater {
        id,
        name,
        division_kind,
        coordinates,
    } = item;

    let parameters = {
        let mut analyses_url = geodaten_url.join("api/data/messstellen_chemie_parameterdaten")?;

        analyses_url
            .query_pairs_mut()
            .append_pair("w", &format!("messst_nr={id}"));

        let analyses_json = fetch_geodaten_text(
            client,
            source,
            format!("analyses_sw_{id}.isol"),
            &analyses_url,
        )
        .await?;

        let analyses = from_json_str::<Vec<SWAnalyses>>(&analyses_json)?;

        analyses
            .into_iter()
            .map(|element| element.parameter)
            .collect::<SmallVec<_>>()
    };

    let title = format!("Chemische {division_kind} {name} (Messstellen-Nr: {id})");

    let description = if !parameters.is_empty() {
        format!("An der Messstelle {name} werden Zeitreihen abiotischer Parameter gemessen.")
    } else {
        format!(
            "An der Messstelle {name} werden derzeit keine Zeitreihen abiotischer Parameter gemessen."
        )
    };

    let source_url = geodaten_url
        .join(&format!("chemie/{id}/stammdaten"))?
        .into();

    let types = smallvec![
        Type::Measurements {
            domain: Domain::Chemistry,
            station: Some(Station {
                id: Some(id.as_str().into()),
                ..Default::default()
            }),
            measured_variables: parameters.clone(),
            methods: Default::default(),
        },
        Type::Measurements {
            domain: Domain::Surfacewater,
            station: Some(Station {
                id: Some(id.as_str().into()),
                ..Default::default()
            }),
            measured_variables: parameters,
            methods: Default::default(),
        }
    ];

    let (longitude, latitude) = projection.projected_to_deg(coordinates.y, coordinates.x);
    let bounding_boxes = smallvec![point_like_bounding_box(latitude, longitude)];

    let mut regions = smallvec![Region::RLP];
    regions.extend(WISE.match_shape(longitude, latitude).map(Region::Watershed));

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        regions,
        bounding_boxes,
        language: Language::German,
        license: License::AllRightsReserved,
        origins: source.origins.clone(),
        source_url,
        machine_readable_source: true,
        ..Default::default()
    };

    write_dataset(dir, client, source, format!("sw-{id}"), dataset).await
}

async fn write_datasets_groundwater(
    dir: &Dir,
    client: &Client,
    source: &Source,
    projection: &dyn Projection,
    geodaten_url: &Url,
    item: Groundwater,
) -> Result<(usize, usize, usize)> {
    let Groundwater {
        id,
        name,
        kind_name,
        coordinates,
    } = item;

    let time_ranges = {
        let mut timerange_url =
            geodaten_url.join("api/data/messstellen_grundwasser_hauptwerte_gesamtzeitraum")?;

        timerange_url
            .query_pairs_mut()
            .append_pair("w", &format!("messst_nr=number:{}", &id));

        let timerange_json = fetch_geodaten_text(
            client,
            source,
            format!("time_range_gw_{id}.isol"),
            &timerange_url,
        )
        .await?;

        let timeranges = from_json_str::<Vec<GWTimeRange>>(&timerange_json)?;

        if let Some(timerange) = timeranges.into_iter().next() {
            let start_date = Date::parse(&timerange.start.unwrap(), DATE_FORMAT)?;
            let end_date = Date::parse(&timerange.end.unwrap(), DATE_FORMAT)?;

            smallvec![(start_date, end_date).into()]
        } else {
            SmallVec::new()
        }
    };

    let title = format!("Messstelle {name} (Messstellen-Nr: {id})");

    let parameters;

    let description = {
        let mut analyses_url = geodaten_url
            .join("api/data/messstellen_grundwasser_messwertequalitativ_parameterdaten")?;

        analyses_url
            .query_pairs_mut()
            .append_pair("w", &format!("messst_nr=number:{}", &id))
            .append_pair("w", "MESSST_ART=number:311");

        let analyses_json = fetch_geodaten_text(
            client,
            source,
            format!("analyses_gw_{id}.isol"),
            &analyses_url,
        )
        .await?;

        let analyses = from_json_str::<Vec<GWAnalyses>>(&analyses_json)?;

        parameters = analyses
            .into_iter()
            .map(|element| element.parameter)
            .collect::<SmallVec<_>>();

        if kind_name == "Quellen" && !parameters.is_empty() {
            format!(
                "Die Messstelle {name} dient der Überwachung von {kind_name}. An der Messstelle werden Zeitreihen abiotischer Parametern gemessen."
            )
        } else {
            format!(
                "Die Messstelle {name} dient der Überwachung von {kind_name}. Zeitreihen abiotische Parameter werden derzeit nicht gemessen."
            )
        }
    };

    let bounding_boxes = smallvec![proj_rect(
        point_like_bounding_box(coordinates.x, coordinates.y),
        projection
    )];

    let source_url = geodaten_url
        .join(&format!("grundwasser/{id}/stammdaten"))?
        .into();

    let mut types = SmallVec::new();
    if kind_name == "Quellen" {
        types.push(Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some(id.as_str().into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Quellschüttung".to_owned()],
            methods: Default::default(),
        });
    } else if kind_name == "Grundwasserstände" {
        types.push(Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some(id.as_str().into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Grundwasserstand".to_owned()],
            methods: Default::default(),
        });
    }
    if !parameters.is_empty() {
        types.push(Type::Measurements {
            domain: Domain::Chemistry,
            station: Some(Station {
                id: Some(id.as_str().into()),
                ..Default::default()
            }),
            measured_variables: parameters,
            methods: Default::default(),
        });
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        time_ranges,
        regions: smallvec![Region::RLP],
        bounding_boxes,
        language: Language::German,
        license: License::AllRightsReserved,
        origins: source.origins.clone(),
        source_url,
        machine_readable_source: true,
        ..Default::default()
    };

    write_dataset(dir, client, source, format!("gw-{id}"), dataset).await
}

async fn write_datasets_lakes(
    dir: &Dir,
    client: &Client,
    source: &Source,
    geodaten_url: &Url,
    item: Lake,
) -> Result<(usize, usize, usize)> {
    let Lake {
        id,
        number,
        name,
        natural_region,
        region,
    } = item;

    let characteristics = {
        let mut characteristics_url =
            geodaten_url.join("api/data/seenatlas_allgemeineinformationen_textallgemeines")?;

        characteristics_url
            .query_pairs_mut()
            .append_pair("w", &format!("see_nr=mixed:{}", &number));

        let characteristics_json = fetch_geodaten_text(
            client,
            source,
            format!("characteristics_lakes_{number}.isol"),
            &characteristics_url,
        )
        .await?;

        let lake_characteristics =
            from_json_str::<Vec<LakeCharacteristics>>(&characteristics_json)?;

        lake_characteristics
            .into_iter()
            .map(|element| element.characteristics_lake)
            .join(", ")
            .replace("<h2>", "")
            .replace("<div>", "")
            .replace("</div>", " ")
            .replace("</h2>", ". ")
            .replace(
                "Entstehung, geologische und naturräumliche Charakteristika.",
                "Entstehung, geologische und naturräumliche Charakteristika:",
            )
            .replace("Form und Ufer.", "Form und Ufer:")
            .replace(
                "Nutzung und Belastungsquellen.",
                "Nutzung und Belastungsquellen:",
            )
    };

    let hydrology = {
        let mut hydrology_url =
            geodaten_url.join("api/data/seenatlas_allgemeineinformationen_texthydrologie")?;

        hydrology_url
            .query_pairs_mut()
            .append_pair("w", &format!("see_nr=mixed:{number}"));

        let hydrology_json = fetch_geodaten_text(
            client,
            source,
            format!("hydrology_lakes_{number}.isol"),
            &hydrology_url,
        )
        .await?;

        let lake_hydrology = from_json_str::<Vec<LakeHydrology>>(&hydrology_json)?;

        lake_hydrology
            .into_iter()
            .filter_map(|element| element.hydrology_lake)
            .join(", ")
            .replace("<h2>", "")
            .replace("<div>", "")
            .replace("</div>", " ")
            .replace("</h2>", ". ")
            .replace("Hydrologie.", "Hydrologie:")
    };

    let title = format!("Standgewässer {name} (Messstellen-Nr: {id})");

    let description = format!(
        "Das Standgewässer {name} liegt im Naturraum {natural_region}. {characteristics} {hydrology}"
    );

    let region = region.map_or(Region::RLP, |region| Region::Other(region.into()));

    let source_url = geodaten_url
        .join(&format!("seenatlas/{number}/stammdaten"))?
        .into();

    let dataset = Dataset {
        title,
        description: Some(description),
        regions: smallvec![region],
        language: Language::German,
        license: License::AllRightsReserved,
        origins: source.origins.clone(),
        source_url,
        machine_readable_source: true,
        ..Default::default()
    };

    write_dataset(dir, client, source, format!("lake-{id}"), dataset).await
}

async fn write_datasets_pegel(
    dir: &Dir,
    client: &Client,
    source: &Source,
    projection: &dyn Projection,
    geodaten_url: &Url,
    item: Pegel,
) -> Result<(usize, usize, usize)> {
    let Pegel {
        id,
        name,
        designation,
        coordinates,
    } = item;

    let time_ranges = {
        let mut timerange_url = geodaten_url.join("api/data/messstellen_wasserstand_hauptwerte")?;

        timerange_url
            .query_pairs_mut()
            .append_pair("w", &format!("messstellennummer={id}"));

        let timerange_text = fetch_geodaten_text(
            client,
            source,
            format!("time_range_pegel_{id}.isol"),
            &timerange_url,
        )
        .await?;

        let timeranges = from_json_str::<Vec<PegelTimeRange>>(&timerange_text)?;

        if let Some(timerange) = timeranges.into_iter().next() {
            let start_date = Date::parse(&timerange.time_from, DATE_FORMAT)?;
            let end_date = Date::parse(&timerange.time_until, DATE_FORMAT)?;

            smallvec![(start_date, end_date).into()]
        } else {
            SmallVec::new()
        }
    };

    let title = format!("Pegel {name} (Messstellen-Nr: {id})");

    let description = format!(
        "'{designation}' ist eine Pegel-Messstelle und dient zur Überwachung von Oberflächengewässern."
    );

    let bounding_boxes = smallvec![proj_rect(
        point_like_bounding_box(coordinates.x, coordinates.y),
        projection
    )];

    let source_url = geodaten_url
        .join(&format!("wasserstand/{id}/stammdaten"))?
        .into();

    let dataset = Dataset {
        title,
        time_ranges,
        description: Some(description),
        regions: smallvec![Region::RLP],
        bounding_boxes,
        language: Language::German,
        license: License::AllRightsReserved,
        origins: source.origins.clone(),
        source_url,
        machine_readable_source: true,
        ..Default::default()
    };

    write_dataset(dir, client, source, format!("lake-{id}"), dataset).await
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
struct Item {
    title: String,
    link: String,
}

#[derive(Debug)]
struct Surfacewater {
    id: String,
    name: String,
    division_kind: String,
    coordinates: Coord,
}

#[derive(Debug)]
struct Groundwater {
    id: String,
    name: String,
    kind_name: String,
    coordinates: Coord,
}

#[derive(Debug)]
struct Lake {
    id: i32,
    name: String,
    number: String,
    natural_region: String,
    region: Option<String>,
}

#[derive(Debug)]
struct Pegel {
    id: String,
    name: String,
    designation: String,
    coordinates: Coord,
}

#[derive(Debug, Deserialize)]
struct SWJson {
    features: Vec<SWFeature>,
}

#[derive(Debug, Deserialize)]
struct SWFeature {
    id: String,
    properties: SWProperty,
    geometry: Geometry,
}

#[derive(Debug, Deserialize)]
struct SWProperty {
    #[serde(rename = "MESSST_BEZ")]
    name: String,
    #[serde(rename = "EINTEILUNG_BEZ")]
    division_kind: String,
}

#[derive(Debug, Deserialize)]
struct SWAnalyses {
    #[serde(rename = "param_name")]
    parameter: String,
}

#[derive(Debug, Deserialize)]
struct GWJson {
    features: Vec<GWFeature>,
}

#[derive(Debug, Deserialize)]
struct GWFeature {
    id: String,
    properties: GWProperty,
    geometry: Geometry,
}

#[derive(Debug, Deserialize)]
struct GWProperty {
    #[serde(rename = "MESSST_BEZ")]
    name: String,
    #[serde(rename = "MESSST_ART_BEZ")]
    kind_name: String,
    #[serde(rename = "REGELMAESSIG_BEOBACHT_VON_JAHR")]
    year_from: Option<i32>,
    #[serde(rename = "ANZAHL_ANALYSEN")]
    num_analysis: Option<i32>,
}

#[derive(Debug, Deserialize)]
struct GWTimeRange {
    #[serde(rename = "beginn")]
    start: Option<String>,
    #[serde(rename = "ende")]
    end: Option<String>,
}

#[derive(Debug, Deserialize)]
struct GWAnalyses {
    #[serde(rename = "param_name")]
    parameter: String,
}

#[derive(Debug, Deserialize)]
struct GeoLake {
    #[serde(rename = "id")]
    lake_id: i32,
    #[serde(rename = "see_bez")]
    lake_name: String,
    #[serde(rename = "see_nr")]
    lake_number: String,
    #[serde(rename = "naturraum")]
    natural_region: String,
    #[serde(rename = "gemeinde")]
    municipality: Option<String>,
    #[serde(rename = "landkreis_nr")]
    district_number: Option<String>,
    #[serde(rename = "landkreis_bez")]
    district_name: Option<String>,
}

#[derive(Debug, Deserialize)]
struct LakeCharacteristics {
    #[serde(rename = "text_allgemein")]
    characteristics_lake: String,
}

#[derive(Debug, Deserialize)]
struct LakeHydrology {
    #[serde(rename = "text_hydrologie")]
    hydrology_lake: Option<String>,
}

#[derive(Debug, Deserialize)]
struct PegelJson {
    features: Vec<PegelFeature>,
}

#[derive(Debug, Deserialize)]
struct PegelFeature {
    id: String,
    properties: PegelProperty,
    geometry: Geometry,
}

#[derive(Debug, Deserialize)]
struct PegelProperty {
    #[serde(rename = "MESSST_BEZ")]
    name: String,
    #[serde(rename = "BEZEICHNUNG")]
    designation: String,
}

#[derive(Debug, Deserialize)]
struct PegelTimeRange {
    #[serde(rename = "messzeitraum_bis")]
    time_until: String,
    #[serde(rename = "messzeitraum_von")]
    time_from: String,
}

#[derive(Debug, Deserialize)]
struct Geometry {
    coordinates: Coord,
}

#[derive(Debug, Deserialize)]
struct Coord {
    #[serde(rename = "0")]
    y: f64,
    #[serde(rename = "1")]
    x: f64,
}

// HACK: That API filters based on the referer even for non-CORS requests.
async fn fetch_geodaten_text(
    client: &Client,
    source: &Source,
    key: String,
    url: &Url,
) -> Result<TrackedResponse<String>> {
    client
        .make_request(source, key, Some(url), |client| async {
            client
                .get(url.clone())
                .header("Referer", "https://geodaten-wasser.rlp-umwelt.de/")
                .send()
                .await?
                .error_for_status()?
                .text()
                .await
        })
        .await
}

const DATE_FORMAT: &[FormatItem] = format_description!("[day].[month].[year]");

selectors! {
    mainlink_selector: "div.container ul.navbar-nav a[href]",
    sublink_selector: "section.container-wrap.py-0.my-0.frame.container-bg-none a[href]",
    sublink_title: "div.infoSpalte-content a, div.page-teaser-content h3, h1",
    link_description: "div.ce-bodytext p",
    link_description_alt: "div.page.container p",
    link_resources: "div.page-teaser-wrapper.equalHeight a[href], div.ce-bodytext a[href], div.infoSpalte-content a[href]",
}
