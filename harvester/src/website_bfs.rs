use anyhow::{Context, Result, anyhow, ensure};
use cap_std::fs::Dir;
use geo::{Coord, Rect};
use regex::Regex;
use scraper::{ElementRef, Html, Selector};
use smallvec::SmallVec;
use time::Date;

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors,
    utilities::{GermanDate, collect_text, make_key, select_text, strip_jsessionid},
    write_dataset,
};
use metadaten::dataset::{
    Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (count, results, errors) = fetch_page(dir, client, source, selectors, 1).await?;

    let pages = count.div_ceil(source.batch_size);

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| {
        fetch_page(dir, client, source, selectors, page)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let count;
    let items;

    {
        let mut url = source
            .url
            .join("SiteGlobals/Forms/Suche/BfS/DE/Expertensuche_Formular.html")?;

        url.query_pairs_mut()
            .append_pair("gtp", &format!("6050242_list%3D{page}"))
            .append_pair("resultsPerPage", &source.batch_size.to_string());

        let text = client
            .checked_fetch_text(source, generic_error_page, format!("serp-{page}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let count_text = document
            .select(&selectors.count)
            .next()
            .ok_or_else(|| anyhow!("Missing count"))?
            .text()
            .collect::<String>();

        count = selectors
            .count_value
            .captures(&count_text)
            .ok_or_else(|| anyhow!("Malformed count"))?
            .get(1)
            .unwrap()
            .as_str()
            .replace('.', "")
            .parse()?;

        items = document
            .select(&selectors.items)
            .map(|element| {
                let link = element
                    .select(&selectors.item_link)
                    .next()
                    .ok_or_else(|| anyhow!("Missing link"))?;

                let href = link.attr("href").unwrap().to_owned();

                let title = collect_text(link.text());

                let description = select_text(element, &selectors.item_teaser);

                let date = element
                    .select(&selectors.item_more)
                    .next()
                    .map(|element| collect_text(element.text()))
                    .and_then(|text| {
                        selectors
                            .item_date
                            .find(&text)
                            .map(|r#match| r#match.as_str().parse::<GermanDate>())
                    })
                    .transpose()?
                    .map(Into::into);

                Ok(Item {
                    href,
                    title,
                    description,
                    date,
                })
            })
            .collect::<Vec<Result<_>>>();
    }

    let (results, errors) = fetch_many(0, 0, items, |item| async move {
        fetch_item(dir, client, source, selectors, item?)
            .await
            .with_context(|| format!("Failed to fetch item from page {page}"))
    })
    .await;

    Ok((count, results, errors))
}

struct Item {
    href: String,
    title: String,
    description: String,
    date: Option<Date>,
}

async fn fetch_item(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    mut item: Item,
) -> Result<(usize, usize, usize)> {
    let mut url = source.url.join(&item.href)?;
    strip_jsessionid(&mut url);

    let key = make_key(url.path()).into_owned();

    let mut organisations = SmallVec::new();
    let mut regions = SmallVec::new();
    let mut bounding_boxes = SmallVec::new();
    let mut resources = SmallVec::new();
    let mut title = item.title;

    {
        let text = client
            .checked_fetch_text(source, generic_error_page, key.clone(), &url)
            .await?;

        let mut document = Html::parse_document(&text);

        if title.is_empty() {
            title = select_text(&document, &selectors.item_title);
        }

        let remove = document
            .select(&selectors.remove)
            .map(|element| element.id())
            .collect::<Vec<_>>();

        for id in remove {
            document.tree.get_mut(id).unwrap().detach();
        }

        let content = document
            .select(&selectors.content)
            .next()
            .ok_or_else(|| anyhow!("Missing content on {}", url.path()))?;

        if content.select(&selectors.login_form).next().is_some() {
            tracing::debug!("Item {} is behind a login form.", item.href);
            return Ok((1, 0, 0));
        }

        item.description = collect_text(content.text());

        for row in content.select(&selectors.table_rows) {
            let mut cols = row
                .select(&selectors.table_cols)
                .map(|col| collect_text(col.text()));

            let Some(key) = cols.next() else {
                continue;
            };
            let Some(val) = cols.next() else {
                continue;
            };

            match &*key {
                "Betreiber" => {
                    organisations.push(Organisation::Other {
                        name: val,
                        role: OrganisationRole::Operator,
                        websites: Default::default(),
                    });
                }
                "Ort" => {
                    regions.push(Region::Other(val.into()));
                }
                "Koordinaten" => {
                    if let Some(captures) = selectors.coords.captures(&val) {
                        let lat_deg = captures.name("lat_deg").unwrap().as_str().parse::<f64>()?;
                        let lat_min = captures.name("lat_min").unwrap().as_str().parse::<f64>()?;
                        let lat_sec = captures.name("lat_sec").unwrap().as_str().parse::<f64>()?;
                        let y = lat_deg + lat_min / 60.0 + lat_sec / 3600.0;

                        let lon_deg = captures.name("lon_deg").unwrap().as_str().parse::<f64>()?;
                        let lon_min = captures.name("lon_min").unwrap().as_str().parse::<f64>()?;
                        let lon_sec = captures.name("lon_sec").unwrap().as_str().parse::<f64>()?;
                        let x = lon_deg + lon_min / 60.0 + lon_sec / 3600.0;

                        bounding_boxes.push(Rect::new(Coord { x, y }, Coord { x, y }));
                    }
                }
                _ => (),
            }
        }

        for download in content.select(&selectors.downloads) {
            let url = source.url.join(download.attr("href").unwrap())?;
            let description = collect_text(download.text());

            resources.push(
                Resource {
                    r#type: ResourceType::Document,
                    description: Some(description),
                    url: url.into(),
                    primary_content: true,
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }

        for link in content.select(&selectors.external_links) {
            let url = source.url.join(link.attr("href").unwrap())?;
            let description = link.attr("title").map(|title| title.to_owned());

            resources.push(Resource {
                r#type: ResourceType::WebPage,
                description,
                url: url.into(),
                ..Default::default()
            });
        }

        for video in document.select(&selectors.videos) {
            let description = video
                .prev_siblings()
                .filter_map(ElementRef::wrap)
                .filter(|element| matches!(element.value().name(), "h1" | "h2" | "h3" | "h4"))
                .map(|element| collect_text(element.text()))
                .next();

            let video_source = video
                .select(&selectors.video_source)
                .next()
                .ok_or_else(|| anyhow!("Missing video source"))?;

            let url = source.url.join(video_source.attr("src").unwrap())?;

            resources.push(
                Resource {
                    r#type: ResourceType::Video,
                    description,
                    url: url.into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            )
        }
    }

    let language = if url.as_str().contains("/DE/service/leichte-sprache/") {
        Language::GermanEasy
    } else {
        Language::German
    };

    let dataset = Dataset {
        title,
        description: Some(item.description),
        issued: item.date,
        regions,
        bounding_boxes,
        organisations,
        resources,
        language,
        license: License::AllRightsReserved,
        origins: source.origins.clone(),
        source_url: url.into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

// HACK: Workaround for malformed responses due to database errors
fn generic_error_page(text: &str) -> Result<()> {
    ensure!(
        !text.contains(r#"<link href="http://download.gsb.bund.de/BIT/errorpage/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />"#),
        "Generic error page"
    );

    Ok(())
}

selectors! {
    count: "h2 span.SuchResultate",
    count_value: r"von ([\d\.]+) Ergebnissen" as Regex,
    items: "ol#searchResultList > li",
    item_link :"ol#searchResultList li a[href]",
    item_title :"h1",
    item_teaser: "p",
    item_more: "div.more span.linkInfo",
    item_date: r"\d{2}\.\d{2}\.\d{4}" as Regex,
    content: "div#content",
    login_form: "form[name='LoginForm']",
    table_rows: "table tbody tr",
    table_cols: "td",
    coords: r#"(?<lat_deg>\d+)°(?<lat_min>\d+)'(?<lat_sec>\d+)"\s+Nord\s+(?<lon_deg>\d+)°(?<lon_min>\d+)'(?<lon_sec>\d+)"\s+Ost"# as Regex,
    downloads: "p.downloadLink a[href]",
    external_links: "a.ExternalLink[href]",
    videos: "div.mejs-wrapper",
    video_source: "video > source[src]",
    remove: "div.sectionRelated, div.sectionRating, div.akkordeon, div.columns.medium-6",
}
