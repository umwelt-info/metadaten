use anyhow::{Context, Result, anyhow};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use hashbrown::HashMap;
use miniproj::{Projection, get_projection};
use poppler::Document;
use regex::Regex;
use serde::Deserialize;
use serde_json::from_str as from_json_str;
use smallvec::{SmallVec, smallvec};
use url::Url;

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors,
    utilities::{collect_pages, make_suffix_key, point_like_bounding_box},
    write_dataset,
};
use metadaten::{
    dataset::{
        Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
        TimeRange,
        r#type::{Domain, ReportingObligation, Station, Type},
    },
    proj_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    // Umweltkarten Niedersachsen apparently uses EPSG10288 as UTM 32N system subspecification.
    let proj = get_projection(10288).unwrap();

    let stations = AtomicRefCell::new(HashMap::new());
    let (results, errors) = fetch_parameters(client, source, &stations).await?;

    let mut reporting_obligations: ObligationsMap = HashMap::new();
    fetch_reporting_obligations(client, source, &mut reporting_obligations).await?;

    let stations = &stations.borrow();
    write_datasets_gw(
        dir,
        client,
        source,
        selectors,
        proj,
        &reporting_obligations,
        stations,
        results,
        errors,
    )
    .await
}

async fn fetch_reporting_obligations(
    client: &Client,
    source: &Source,
    reporting_obgligations: &mut ObligationsMap,
) -> Result<()> {
    let mut url = source.url.join("/rest/services/6/getAttributetablePage")?;

    url.query_pairs_mut().append_pair("lang", "de").append_pair(
        "layerID",
        "Grundwassermessstellen_mengenmaessiger_Zustand_BWP3",
    );

    let text = client
        .fetch_text(source, "grundwassermessstellen_gesamt".into(), &url)
        .await?;

    let document = from_json_str::<ResultSiteObligations>(&text)
        .context("Obligations deserialization error")?;

    for data_site in document.data {
        let site_id = data_site
            .eu_code_site
            .rsplit_once('_')
            .and_then(|(_, site_id)| site_id.parse().ok());

        match site_id {
            Some(site_id) => {
                reporting_obgligations.insert(site_id, data_site);
            }
            None => tracing::error!("Malformed eu_code_site {}", data_site.eu_code_site),
        }
    }

    Ok(())
}

async fn fetch_parameters(
    client: &Client,
    source: &Source,
    stations: &AtomicRefCell<StationMap>,
) -> Result<(usize, usize)> {
    let parameters = {
        let url = source
            .url
            .join("/rest/services/6/Hydrologie/CatalogServer")?;

        let text = client
            .fetch_text(source, "catalog".to_owned(), &url)
            .await?;

        let document =
            from_json_str::<ResultCatalog>(&text).context("Catalog deserialization error")?;

        document
            .results
            .root
            .children
            .into_iter()
            .find(|child| child.layer_id == "GrundwasserberichtGuete")
            .ok_or_else(|| anyhow!("Could not find layer GrundwasserberichtGuete"))?
            .features
            .into_iter()
            .map(|feature| feature.name)
            .collect::<Vec<_>>()
    };

    let (results, errors) = fetch_many(0, 0, parameters, |parameter| {
        fetch_chem_param(client, source, stations, parameter)
    })
    .await;

    Ok((results, errors))
}

async fn fetch_chem_param(
    client: &Client,
    source: &Source,
    stations: &AtomicRefCell<StationMap>,
    parameter: CompactString,
) -> Result<(usize, usize, usize)> {
    let mut url = source.url.join("/rest/services/6/getAttributetablePage")?;

    url.query_pairs_mut()
        .append_pair("lang", "de")
        .append_pair("layerID", &parameter);

    let text = client
        .fetch_text(source, format!("chem_param_{}", parameter), &url)
        .await?;

    let document = from_json_str::<ResultChemSite>(&text).context("Site deserialization error")?;

    for data_site in document.data {
        stations
            .borrow_mut()
            .entry(data_site.site_id)
            .or_default()
            .push(data_site);
    }

    Ok((1, 1, 0))
}

#[allow(clippy::too_many_arguments)]
async fn write_datasets_gw(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    proj: &dyn Projection,
    reporting_obligations: &ObligationsMap,
    stations: &StationMap,
    results: usize,
    errors: usize,
) -> Result<(usize, usize, usize)> {
    let gw_data = {
        let mut gw_sites_url = source.url.join("/rest/services/6/getAttributetablePage")?;

        gw_sites_url
            .query_pairs_mut()
            .append_pair("lang", "de")
            .append_pair("layerID", "Grundwasserstandsmessstellen");

        let text = client
            .fetch_text(source, "messstellen_gw-layer".to_owned(), &gw_sites_url)
            .await?;

        let document = from_json_str::<ResultGroundwaterLevelSite>(&text)
            .context("Groundwater site deserialization error")?;

        document.data
    };

    let count = gw_data.len();

    let (results, errors) = fetch_many(results, errors, gw_data, |feature| async move {
        let title = format!("Grundwassermessstelle {} des NLWKN", feature.site_name);

        let organisations =
            if let Some(capture) = selectors.site_operator.captures(&feature.href_hauptdaten) {
                smallvec![Organisation::Other {
                    name: format!("NLWKN Betriebsstelle {}", &capture[1]),
                    role: OrganisationRole::Operator,
                    websites: Default::default(),
                }]
            } else {
                Default::default()
            };

        let mut resources = smallvec![
            Resource {
                url: feature.href_ganglinie.clone(),
                description: Some(format!("Ganglinie der Messstelle {}", feature.site_name)),
                primary_content: true,
                r#type: ResourceType::Pdf,
                ..Default::default()
            },
            Resource {
                url: feature.href_profil,
                description: Some(format!("Profil der Messstelle {}", feature.site_name)),
                r#type: ResourceType::Pdf,
                ..Default::default()
            },
            Resource {
                url: "http://www.wasserdaten.niedersachsen.de/cadenza/".to_owned(),
                description: Some("Niedersächsische Landesdatenbank für wasserwirtschaftliche Daten".to_owned()),
                direct_link: false,
                r#type: ResourceType::WebPage,
                ..Default::default()
            },
            Resource {
                url: "https://www.umweltkarten-niedersachsen.de/Umweltkarten/".to_owned(),
                description: Some("Umweltkarten Niedersachsen".to_owned()),
                direct_link: false,
                r#type: ResourceType::WebPage,
                ..Default::default()
            },
            Resource {
                url: "https://www.umweltkarten-niedersachsen.de/rest/services/6/Hydrologie/CatalogServer".to_owned(),
                description: Some("API der Umweltkarten Niedersachsen u.a. zu Grundwassermessstellen".to_owned()),
                primary_content: true,
                r#type: ResourceType::ArcGisCatalogServer,
                ..Default::default()
            },
        ];

        let mut types = SmallVec::new();
        let mut time_ranges = SmallVec::new();
        let mut regions = SmallVec::new();

        let mut reporting_obligations_chemistry = SmallVec::new();
        let mut reporting_obligations_gw = SmallVec::new();

        let mut station_id = feature.site_id.to_string();
        let mut description = None;

        if let Some(report_obligation) = reporting_obligations.get(&feature.site_id) {
            station_id = report_obligation.eu_code_site.as_str().to_owned();
            description = Some(format!(
                "Art der Messstelle: {}.\nDie Messstelle Befindet sich im {}.\nSie gehört zum Koordinierungsraum {}.\nDer Grundwasserleiter heißt: {}.",
                report_obligation.kind,
                report_obligation.aquifer_type,
                report_obligation.coordination,
                report_obligation.gwk_name,
            ));
            regions.push(Region::Other(report_obligation.state.clone()));

            let reporting_obligations = report_obligation
                .reporting_obligation
                .split(";")
                .filter_map(|text| match text.trim() {
                    "WRRL (W)" => Some(ReportingObligation::Wrrl),
                    "Nitrat-RL (J)" => Some(ReportingObligation::Nitrat),
                    "EIONET = SOE (A)" => Some(ReportingObligation::SoeReport),
                    text => {
                        tracing::error!("Unknown reporting obligation {}", text);
                        None
                    }
                })
                .collect::<SmallVec<_>>();

            if report_obligation.reporting_categories.contains("Chemie") {
                reporting_obligations_chemistry = reporting_obligations.clone();
            }

            if report_obligation.reporting_categories.contains("Menge") {
                reporting_obligations_gw = reporting_obligations
            }
        }

        if let Some(station_chem) = stations.get(&feature.site_id) {
            for chemsite in station_chem {
                let url = selectors
                    .get_href
                    .captures(&chemsite.href_hauptdaten)
                    .map_or_else(
                        || chemsite.href_hauptdaten.clone(),
                        |match_| match_[1].to_owned(),
                    );
                resources.push(Resource {
                    url,
                    description: Some(format!("Ganglinie der Messung {}", chemsite.parameter_name)),
                    primary_content: true,
                    r#type: ResourceType::Pdf,
                    ..Default::default()
                });

                time_ranges.push(TimeRange::whole_year(chemsite.year)?);

                let station_chem = Station {
                    id: Some(station_id.as_str().into()),
                    measurement_frequency: None,
                    reporting_obligations: reporting_obligations_chemistry.clone(),
                };

                let measured_variables = stations
                    .get(&feature.site_id)
                    .into_iter()
                    .flat_map(|station_chem| {
                        station_chem
                            .iter()
                            .map(|data_site| data_site.parameter_name.clone())
                    })
                    .collect::<SmallVec<_>>();

                types.push(Type::Measurements {
                    domain: Domain::Chemistry,
                    station: Some(station_chem),
                    measured_variables,
                    methods: Default::default(),
                });
            }
        }

        match pdf_get_years(client, source, selectors, &feature.href_ganglinie).await {
            Ok(years) => if let (Some(from), Some(until)) = (years.iter().min(), years.iter().max()) {
                time_ranges.push(TimeRange::multiple_whole_years(*from, *until)?);
            },
            Err(err) => tracing::warn!(
                "Failed to fetch time period from {} for {}: {}",
                &feature.href_ganglinie,
                &feature.site_name,
                err
            ),
        }

        let station_gw = Station {
            id: Some(station_id.into()),
            measurement_frequency: None,
            reporting_obligations: reporting_obligations_gw.clone(),
        };

        types.push(Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(station_gw),
            measured_variables: smallvec!["Grundwasserstand".to_owned()],
            methods: Default::default(),
        });

        let bounding_boxes = smallvec![proj_rect(
            point_like_bounding_box(feature.east, feature.north),
            proj,
        )];

        if regions.is_empty() {
            regions.push(Region::NI);
        }

        let dataset = Dataset {
            title,
            description,
            resources,
            organisations,
            types,
            time_ranges,
            bounding_boxes,
            regions,
            source_url: feature.href_hauptdaten,
            origins: source.origins.clone(),
            language: Language::German,
            license: License::DlDeBy20,
            ..Default::default()
        };

        write_dataset(dir, client, source, feature.site_id.to_string(), dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn pdf_get_years(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    url: &str,
) -> Result<Vec<i32>> {
    let url = Url::parse(url)?;
    let key = make_suffix_key(url.as_str(), &source.url).into_owned();

    let bytes = client.fetch_pdf(source, key, &url).await?;
    let document = Document::from_bytes(&bytes, None)?;

    let text = collect_pages(&document, 0..1);

    let years = selectors
        .years
        .find_iter(&text)
        .map(|year| year.as_str().parse::<i32>())
        .collect::<Result<Vec<_>, _>>()?;

    Ok(years)
}

#[derive(Debug, Deserialize)]
struct ResultCatalog {
    results: Root,
}

#[derive(Debug, Deserialize)]
struct Root {
    root: TopLevelChildren,
}

#[derive(Debug, Deserialize)]
struct TopLevelChildren {
    children: Vec<Children>,
}

#[derive(Debug, Deserialize)]
struct Children {
    #[serde(rename = "children")]
    features: Vec<Feature>,
    #[serde(rename = "layerBodId")]
    layer_id: CompactString,
}

#[derive(Debug, Deserialize, Clone)]
struct Feature {
    #[serde(rename = "layerBodId")]
    name: CompactString,
}

#[derive(Debug, Deserialize)]
struct ResultChemSite {
    #[serde(rename = "Data")]
    data: Vec<ChemSite>,
}

#[derive(Debug, Deserialize, Clone)]
struct ChemSite {
    #[serde(rename = "d1")]
    href_hauptdaten: String,
    #[serde(rename = "d4")]
    site_id: u32,
    #[serde(rename = "d7")]
    parameter_name: String,
    #[serde(rename = "d9")]
    year: i32,
}

#[derive(Debug, Deserialize)]
struct ResultGroundwaterLevelSite {
    #[serde(rename = "Data")]
    data: Vec<GroundwaterLevelSite>,
}

#[derive(Debug, Deserialize, Clone)]
struct GroundwaterLevelSite {
    #[serde(rename = "r1")]
    href_hauptdaten: String,
    #[serde(rename = "r2")]
    href_ganglinie: String,
    #[serde(rename = "r3")]
    href_profil: String,
    #[serde(rename = "r4")]
    site_id: u32,
    #[serde(rename = "r5")]
    site_name: CompactString,
    #[serde(rename = "d10")]
    north: f64,
    #[serde(rename = "d11")]
    east: f64,
}

#[derive(Debug, Deserialize, Default)]
struct ResultSiteObligations {
    #[serde(rename = "Data")]
    data: Vec<SiteObligations>,
}

#[derive(Debug, Deserialize, Clone, Default)]
struct SiteObligations {
    #[serde(rename = "r2")]
    gwk_name: CompactString,
    #[serde(rename = "r3")]
    eu_code_site: CompactString,
    #[serde(rename = "r5")]
    kind: CompactString,
    #[serde(rename = "r13")]
    reporting_obligation: CompactString,
    #[serde(rename = "r14")]
    reporting_categories: CompactString,
    #[serde(rename = "r15")]
    aquifer_type: CompactString,
    #[serde(rename = "r17")]
    coordination: CompactString,
    #[serde(rename = "r18")]
    state: CompactString,
}

type StationMap = HashMap<u32, Vec<ChemSite>>;
type ObligationsMap = HashMap<u32, SiteObligations>;

selectors! {
    site_operator: r"GW/(.*)_Haupttabelle" as Regex,
    years: r"\b(?:19|20)\d{2}\b" as Regex,
    get_href: r"href='(.*)'" as Regex,
}
