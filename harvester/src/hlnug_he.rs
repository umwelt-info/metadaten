use anyhow::{Result, anyhow};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use geo::{Coord, Rect};
use harvester::utilities::collect_text;
use hashbrown::{HashMap, HashSet};
use itertools::Itertools;
use regex::{Regex, RegexSet};
use scraper::{Html, Selector, selectable::Selectable};
use serde::Deserialize;
use serde_json::from_str;
use smallvec::{SmallVec, smallvec};
use time::{Date, OffsetDateTime};
use url::Url;

use harvester::{
    Source,
    client::Client,
    fetch_many, remove_datasets, selectors,
    utilities::{make_key, select_text},
    write_dataset,
};
use metadaten::{
    dataset::{
        Alternative, Dataset, Language, License, Person, PersonRole, Region, Resource,
        ResourceType, TimeRange,
        r#type::{Domain, Station, TextType, Type},
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let mut hrefs = HashSet::new();
    let written_datasets = AtomicRefCell::new(HashMap::new());

    let (mut count, _, _) = fetch_links(client, source, selectors, &mut hrefs).await?;

    let (mut results, mut errors) = fetch_many(0, 0, hrefs, |href| {
        write_article_dataset(dir, client, source, selectors, href, &written_datasets)
    })
    .await;

    for (_key, group) in written_datasets.into_inner() {
        if group.len() > 1 {
            if let Err(err) = merge_cluster(dir, client, source, selectors, group).await {
                tracing::error!("Failed merging dataset group: {err:#}");
            }
        }
    }

    let (count1, results1, errors1) =
        fetch_english_articles(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_air_stations(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_frankfurt_air_stations(dir, client, source).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_air_particle_stations(dir, client, source).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) =
        fetch_groundwater_stations(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_river_stations(dir, client, source).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_radio_biblis_stations(dir, client, source).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_radio_boden_stations(dir, client, source).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_daily_quakes(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_links(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    hrefs: &mut HashSet<String>,
) -> Result<(usize, usize, usize)> {
    let mut results = 0;
    let mut errors = 0;

    let mut new_links = vec![source.url.clone().into()];

    while let Some(href) = new_links.pop() {
        match fetch_sublinks(client, source, selectors, hrefs, &mut new_links, &href).await {
            Ok(()) => {
                results += 1;
            }
            Err(err) => {
                tracing::error!("Failed to fetch sublinks: {err:#}");

                errors += 1;
            }
        }
    }

    Ok((hrefs.len(), results, errors))
}

async fn fetch_sublinks(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    hrefs: &mut HashSet<String>,
    new_links: &mut Vec<String>,
    href: &str,
) -> Result<()> {
    let url = source.url.join(href)?;
    let id = format!("page-{}", make_key(href));

    let text = client.fetch_text(source, id, &url).await?;
    let document = Html::parse_document(&text);

    for elem in document.select(&selectors.links_l1) {
        let link = elem.attr("href").unwrap();

        if !selectors.exclude_pages.is_match(link) {
            // we need to use full path as otherwise links to foej.hlnug.de won't work
            let href: String = url.join(link)?.into();

            if hrefs.insert(href.clone()) {
                new_links.push(href);
            }
        }
    }

    Ok(())
}

async fn write_article_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    href: String,
    datasets: &AtomicRefCell<HashMap<usize, Vec<String>>>,
) -> Result<(usize, usize, usize)> {
    tracing::trace!("Fetching dataset at {}", &href);

    let url = source.url.join(&href)?;
    let id = format!("page-{}", make_key(&href));

    let dataset = {
        let text = client.fetch_text(source, id.clone(), &url).await?;
        let document = Html::parse_document(&text);

        let mut title = select_text(&document, &selectors.themen_title);
        let breadcrumb = document
            .select(&selectors.breadcrumb)
            .skip(2)
            .map(|elem| collect_text(elem.text()))
            .filter(|text| text != &title)
            .join("/");
        if !breadcrumb.is_empty() {
            title = format!("{}: {}", breadcrumb, title)
        }

        let mut description = select_text(&document, &selectors.default_description);

        let mut resources = SmallVec::<[Resource; 4]>::new();

        for outer_elem in document.select(&selectors.right_info) {
            for elem in outer_elem.select(&selectors.default_links) {
                let res_href = elem.attr("href").unwrap();

                let description = collect_text(elem.text());
                let description = if description != res_href {
                    // If the link is also in the text description, remove it.
                    Some(description.replace(res_href, ""))
                } else {
                    None
                };

                if !description
                    .as_ref()
                    .is_some_and(|description| selectors.is_internal_link.is_match(description))
                {
                    resources.push(
                        Resource {
                            r#type: ResourceType::WebPage,
                            description,
                            url: source.url.join(res_href)?.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }
            }
        }

        for elem in document.select(&selectors.panels) {
            if !select_text(elem, &selectors.panel_titles).contains("Literatur") {
                continue;
            }

            for lit_elem in elem.select(&selectors.panel_par) {
                if let Some(href_lit_elem) = lit_elem.select(&selectors.default_links).next() {
                    let href_lit = href_lit_elem.attr("href").unwrap();

                    let description = collect_text(lit_elem.text());
                    let description = if description != href_lit {
                        Some(description.replace(href_lit, ""))
                    } else {
                        None
                    };

                    resources.push(
                        Resource {
                            r#type: ResourceType::WebPage,
                            description,
                            url: source.url.join(href_lit)?.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }
            }
        }

        clean_resources(selectors, &mut resources);

        let (persons, mut alternatives) = resource_extract_extras(selectors, &mut resources)?;

        for elem in document.select(&selectors.links_english) {
            let href = elem.attr("href").unwrap();

            alternatives.push(Alternative::Language {
                language: Language::English,
                url: url.join(href)?.into(),
            });
        }
        if !alternatives.is_empty() {
            description =
                description.replace("Click here for the English version of this content.", "");
        }

        for key in selectors.group_dataset_on_url_match.matches(&href) {
            datasets
                .borrow_mut()
                .entry(key)
                .or_default()
                .push(id.clone());
        }

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Editorial,
            }],
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: url.into(),
            resources,
            persons,
            regions: smallvec![Region::HE],
            language: Language::German,
            alternatives,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, id, dataset).await
}

// Contacts and links to the alternative english page are sometimes misinterpreted as resources and get removed from the resources in these cases here.
fn resource_extract_extras(
    selectors: &Selectors,
    resources: &mut SmallVec<[Resource; 4]>,
) -> Result<(Vec<Person>, Vec<Alternative>)> {
    let contact_idx: Vec<usize> = resources
        .iter()
        .enumerate()
        .filter(|(_idx, resource)| selectors.is_contact.is_match(&resource.url))
        .map(|(idx, _resource)| idx)
        .collect();

    let contacts = contact_idx
        .into_iter()
        .rev()
        .map(|idx| {
            let resource = resources.swap_remove(idx);
            Person {
                name: resource.description.unwrap_or_else(|| "Kontakt".to_owned()),
                role: PersonRole::Contact,
                emails: smallvec![resource.url],
                ..Default::default()
            }
        })
        .collect();

    let alternative_idx: Vec<usize> = resources
        .iter()
        .enumerate()
        .filter(|(_idx, resource)| {
            resource
                .description
                .as_ref()
                .is_some_and(|description| selectors.is_english_description.is_match(description))
        })
        .map(|(idx, _resource)| idx)
        .collect();

    let alternatives = alternative_idx
        .into_iter()
        .rev()
        .map(|idx| {
            let resource = resources.swap_remove(idx);
            Alternative::Language {
                language: Language::English,
                url: resource.url,
            }
        })
        .collect();

    Ok((contacts, alternatives))
}

/// Merge a cluster of already written datasets into a single one.
///  All datasets that match the same RegEx of
///  selectors.group_dataset_on_url_match are grouped.
async fn merge_cluster(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    mut keys: Vec<String>,
) -> Result<()> {
    keys.sort_unstable();
    keys.dedup();

    let mut datasets = remove_datasets(dir, client, source, &keys).await?;

    let base_idx = keys
        .iter()
        .enumerate()
        .min_by_key(|(_, key)| key.len())
        .unwrap()
        .0;

    let base_key = keys.swap_remove(base_idx);
    let mut base_dataset = datasets.swap_remove(base_idx);

    if base_key.contains("altlasten") {
        datasets.sort_unstable_by_key(|ds| {
            if let Some(captures) = selectors.book_sorter.captures(&ds.source_url) {
                (
                    captures["book"].to_owned(),
                    captures["remainder"].to_owned(),
                )
            } else {
                Default::default()
            }
        });
    }

    for dataset in datasets {
        base_dataset.resources.extend(dataset.resources);
    }

    tracing::debug!(
        "Merged {} datasets into cluster based on {}",
        keys.len(),
        &base_dataset.title
    );

    write_dataset(dir, client, source, base_key, base_dataset).await?;

    Ok(())
}

async fn fetch_english_articles(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let art_hrefs = ["/service/english", "/service/english/english-contents"];

    let count = art_hrefs.len();

    let (results, errors) = fetch_many(0, 0, art_hrefs, |art_href| async move {
        let url = source.url.join(art_href)?;
        let id = format!("english-art-{}", make_key(art_href));

        let dataset = {
            let text = client
                .fetch_text(source, format!("{id}.isol"), &url)
                .await?;
            let document = Html::parse_document(&text);

            let title = select_text(&document, &selectors.default_title);
            let description = select_text(&document, &selectors.default_description);

            let resources = document
                .select(&selectors.default_links)
                .map(|elem| {
                    let href = elem.attr("href").unwrap();

                    Ok(Resource {
                        r#type: ResourceType::WebPage,
                        url: source.url.join(href)?.into(),
                        ..Default::default()
                    }
                    .guess_or_keep_type())
                })
                .collect::<Result<SmallVec<_>>>()?;

            Dataset {
                title,
                description: Some(description),
                types: smallvec![Type::Text {
                    text_type: TextType::Editorial,
                }],
                license: License::OtherClosed,
                origins: source.origins.clone(),
                source_url: url.into(),
                regions: smallvec![Region::HE],
                resources,
                language: Language::English,
                ..Default::default()
            }
        };

        write_dataset(dir, client, source, id, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_air_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let response = {
        let api_url = Url::parse("https://app.hlnug.de/json/lmw/getThemeStations/1")?;
        let text = client
            .fetch_text(source, "air-results.isol".to_owned(), &api_url)
            .await?;
        from_str::<Vec<StationResult>>(&text)?
    };

    let count = response.len();

    let (results, errors) = fetch_many(0, 0, response, |result| async move {
        let id = result.id;
        let station_id = result.station_id;
        let station_name = result.display_name;

        let measured_variables = fetch_meta_data(client, source, &station_id, "lmw").await?;

        let title = format!("Luftmessstelle Nr. {station_id} in {station_name}");

        let description = format!(r#"Dieser Datensatz enthält Informationen der Luftmessstelle Nr. {station_id} in {station_name}.

Es werden nur die an der Station erfassten Messwerte der letzten 20 Jahre publiziert.
Ältere Daten können auf Anfrage erhalten werden.
Auf der Webseite zur Messstelle ist ein Link zum Herunterladen der Rohdaten vorhanden."#);

        let landing_page = format!("https://www.hlnug.de/messwerte/datenportal/messstelle/2/1/{station_id}/14");

        let x = result.lon.parse::<f64>()?;
        let y = result.lat.parse::<f64>()?;
        let bounding_boxes = smallvec![Rect::new(Coord { x, y }, Coord { x, y })];

        let time_url = Url::parse(&format!(
            "https://app.hlnug.de/json/lmw/getStationMinMax/{station_id}/tstamp"
        ))?;
        let time_key = format!("air-station-{station_id}-tstamp.isol");
        let time_ranges = get_time_range(client, source, &time_url, time_key).await?;

        let resources_str = fetch_resource_str(client, source, &station_id, "lmw").await?;

        let mut resources = SmallVec::new();

        for resource_str in resources_str {
            if resource_str.ends_with(".pdf") {
                resources.push(Resource {
                    r#type: ResourceType::Pdf,
                    description: Some("Steckbrief der Messstelle".to_owned()),
                    url: source.url.join(&resource_str)?.into(),
                    primary_content: true,
                    ..Default::default()
                });
            } else if resource_str.ends_with(".jpg") || resource_str.ends_with(".JPG") {
                resources.push(Resource {
                    r#type: ResourceType::Jpeg,
                    description: Some("Bild der Messstelle".to_owned()),
                    url: source.url.join(&resource_str)?.into(),
                    ..Default::default()
                });
            }
        }

        clean_resources(selectors, &mut resources);

        let types = smallvec![Type::Measurements {
            domain: Domain::Air,
            station: Some(Station {
                id: Some(station_id),
                ..Default::default()
            }),
            measured_variables,
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: landing_page,
            bounding_boxes,
            resources,
            time_ranges,
            regions:  smallvec![Region::HE],
            language: Language::German,
            ..Default::default()
        };

        write_dataset(dir, client, source, format!("air-station-{id}"), dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_frankfurt_air_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
) -> Result<(usize, usize, usize)> {
    let response = {
        let api_url = Url::parse("https://app.hlnug.de/json/lmw/getThemeStations/4")?;
        let text = client
            .fetch_text(source, "frankfurt-results.isol".to_owned(), &api_url)
            .await?;
        from_str::<Vec<StationResult>>(&text)?
    };

    let count = response.len();

    let (results, errors) = fetch_many(0, 0, response, |result| async move {
        let id = result.id;
        let station_id = result.station_id;

        let measured_variables = fetch_meta_data(client, source, &station_id, "lmw").await?;


        let title = format!("Luftmessstelle Nr. {station_id} am Frankfurter Flughafen");

        let description = format!(r#"Dieser Datensatz enthält Informationen der Luftmessstelle Nr. {station_id} am Frankfurter Flughafen.
Die hier angezeigten Daten werden an Luftmessstationen auf dem Gelände des Flughafens Frankfurt im Auftrag des Umwelt- und Nachbarschaftshaus (UNH) erhoben.
Die Stationen sind nicht Bestandteil des regulären Luftmessnetzes zur Beurteilung der Luftqualität in Hessen.
Da es sich hierbei um Betriebsgelände handelt, auf das die Öffentlichkeit keinen Zugang hat, gelten für diese Standorte die durch die Luftqualitätsgesetzgebung (39. BImSchV) vorgeschriebenen Immissionsgrenzwerte zum Schutz der menschlichen Gesundheit nicht.
Es werden nur die an der Station erfassten Messwerte der letzten 20 Jahre publiziert.
Ältere Daten können auf Anfrage erhalten werden.
Auf der Webseite zur Messstelle ist ein Link zum Herunterladen der Rohdaten vorhanden."#);

        let landing_page = format!("https://www.hlnug.de/messwerte/datenportal/messstelle/2/4/{station_id}");

        let x = result.lon.parse::<f64>()?;
        let y = result.lat.parse::<f64>()?;
        let bounding_boxes = smallvec![Rect::new(Coord { x, y }, Coord { x, y })];

        let time_url = Url::parse(&format!(
            "https://app.hlnug.de/json/lmw/getStationMinMax/{station_id}/tstamp"
        ))?;
        let time_key = format!("frankfurt-station-{station_id}-tstamp.isol");
        let time_ranges = get_time_range(client, source, &time_url, time_key).await?;

        let types = smallvec![Type::Measurements {
            domain: Domain::Air,
            station: Some(Station {
                id: Some(station_id),
                ..Default::default()
            }),
            measured_variables,
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: landing_page,
            bounding_boxes,
            time_ranges,
            regions:  smallvec![Region::HE],
            language: Language::German,
            ..Default::default()
        };

        write_dataset(
            dir,
            client,
            source,
            format!("frankfurt-station-{id}"),
            dataset,
        )
        .await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_air_particle_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
) -> Result<(usize, usize, usize)> {
    let response = {
        let api_url = Url::parse("https://app.hlnug.de/json/lmw/getThemeStations/12")?;
        let text = client
            .fetch_text(source, "air-particle-results.isol".to_owned(), &api_url)
            .await?;
        from_str::<Vec<StationResult>>(&text)?
    };

    let count = response.len();

    let (results, errors) = fetch_many(0, 0, response, |result| async move {
        let id = result.id;
        let station_id = result.station_id;
        let station_name = result.display_name;

        let measured_variables = fetch_meta_data(client, source, &station_id, "lmw").await?;

        let title = format!("Luftpartikelmessstelle Nr. {station_id} in {station_name}");

        let description = format!(r#"Dieser Datensatz enthält Informationen der Luftmessstelle Nr. {station_id} in {station_name}.
Ultrafeine Partikel (UFP) bezeichnen Partikel, deren Durchmesser kleiner als 100 Nanometer (nm) ist.
Zurzeit existieren keine rechtlichen Verpflichtungen für das Messen von UFP, da es keine Anforderungen wie z. B. Grenz- oder Richtwerte zur Einhaltung bestimmter Konzentrationen gibt.
Die Messungen des HLNUG haben insofern einen rein fachlichen oder auch Forschungscharakter.
Deshalb werden Informationen zu den UFP an dieser Stelle auch bewusst abgesetzt vom regulären Luftmessnetz des Landes dargestellt.
Es werden nur die an der Station erfassten Messwerte der letzten 20 Jahre publiziert.
Ältere Daten können auf Anfrage erhalten werden.
Auf der Webseite zur Messstelle ist ein Link zum Herunterladen der Rohdaten vorhanden."#);

        let landing_page = format!("https://www.hlnug.de/messwerte/datenportal/messstelle/2/12/{station_id}/86");

        let x = result.lon.parse::<f64>()?;
        let y = result.lat.parse::<f64>()?;
        let bounding_boxes = smallvec![Rect::new(Coord { x, y }, Coord { x, y })];

        let time_url = Url::parse(&format!(
            "https://app.hlnug.de/json/lmw/getStationMinMax/{station_id}/tstamp"
        ))?;
        let time_key = format!("air-particle-station-{station_id}-tstamp.isol");
        let time_ranges = get_time_range(client, source, &time_url, time_key).await?;

        let types = smallvec![Type::Measurements {
            domain: Domain::Air,
            station: Some(Station {
                id: Some(station_id),
                ..Default::default()
            }),
            measured_variables,
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: landing_page,
            bounding_boxes,
            time_ranges,
            regions:  smallvec![Region::HE],
            language: Language::German,
            ..Default::default()
        };

        write_dataset(
            dir,
            client,
            source,
            format!("air-particle-station-{id}"),
            dataset,
        )
        .await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_radio_biblis_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
) -> Result<(usize, usize, usize)> {
    let response = {
        let api_url = Url::parse("https://app.hlnug.de/json/radio/getThemeStations/8")?;
        let text = client
            .fetch_text(source, "radio-biblis-results.isol".to_owned(), &api_url)
            .await?;
        from_str::<Vec<StationResult>>(&text)?
    };

    let count = response.len();

    let (results, errors) = fetch_many(0, 0, response, |result| async move {
        let id = result.id;
        let station_id = result.station_id;
        let station_name = result.display_name;

        let measured_variables = fetch_meta_data(client, source, &station_id, "radio").await?;

        let title = format!("Messstation für Radioaktivität Nr. {station_id} in {station_name}");

        let description = format!(r#"Dieser Datensatz enthält Informationen der Messstation Nr. {station_id} in {station_name}.
Auf der Webseite zur Messstelle ist ein Link zum Herunterladen der Rohdaten vorhanden."#);

        let landing_page = format!("https://www.hlnug.de/messwerte/datenportal/messstelle/6/8/{station_id}/");

        let x = result.lon.parse::<f64>()?;
        let y = result.lat.parse::<f64>()?;
        let bounding_boxes = smallvec![Rect::new(Coord { x, y }, Coord { x, y })];

        let time_url = Url::parse(&format!(
            "https://app.hlnug.de/json/radio/getStationMinMax/{station_id}/tstamp"
        ))?;
        let time_key = format!("radio-biblis-station-{station_id}-tstamp.isol");
        let time_ranges = get_time_range(client, source, &time_url, time_key).await?;

        let types = smallvec![Type::Measurements {
            domain: Domain::Radioactivity,
            station: Some(Station {
                id: Some(station_id),
                ..Default::default()
            }),
            measured_variables,
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: landing_page,
            bounding_boxes,
            time_ranges,
            regions:  smallvec![Region::HE],
            language: Language::German,
            ..Default::default()
        };

        write_dataset(
            dir,
            client,
            source,
            format!("radio-biblis-station-{id}"),
            dataset,
        )
        .await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_radio_boden_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
) -> Result<(usize, usize, usize)> {
    let response = {
        let api_url = Url::parse("https://app.hlnug.de/json/insitu/getThemeStations/16")?;
        let text = client
            .fetch_text(source, "radio-boden-results.isol".to_owned(), &api_url)
            .await?;
        from_str::<Vec<StationResult>>(&text)?
    };

    let count = response.len();

    let (results, errors) = fetch_many(0, 0, response, |result| async move {
        let id = result.id;
        let station_id = result.station_id;
        let station_name = result.display_name;

        let measured_variables = fetch_meta_data(client, source, &station_id, "insitu").await?;

        let title = format!("Messstation für Radioaktivität Nr. {station_id} in {station_name}");

        let description = format!(r#"Dieser Datensatz enthält Informationen der Messstation Nr. {station_id} in {station_name}.
Auf der Webseite zur Messstelle ist ein Link zum Herunterladen der Rohdaten vorhanden."#);

        let landing_page = format!("https://www.hlnug.de/messwerte/datenportal/messstelle/6/16/{station_id}/");

        let x = result.lon.parse::<f64>()?;
        let y = result.lat.parse::<f64>()?;
        let bounding_boxes = smallvec![Rect::new(Coord { x, y }, Coord { x, y })];

        let time_url = Url::parse(&format!(
            "https://app.hlnug.de/json/insitu/getStationMinMax/{station_id}/tstamp"
        ))?;
        let time_key = format!("radio-boden-station-{station_id}-tstamp.isol");
        let time_ranges = get_time_range(client, source, &time_url, time_key).await?;

        let types = smallvec![Type::Measurements {
            domain: Domain::Radioactivity,
            station: Some(Station {
                id: Some(station_id.clone()),
                ..Default::default()
            }),
            measured_variables: measured_variables.clone(),
            methods: Default::default(),
        },
        Type::Measurements {
            domain: Domain::Soil,
            station: Some(Station {
                id: Some(station_id),
                ..Default::default()
            }),
            measured_variables,
            methods: Default::default(),
        }
        ];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: landing_page,
            bounding_boxes,
            time_ranges,
            regions:  smallvec![Region::HE],
            language: Language::German,
            ..Default::default()
        };

        write_dataset(
            dir,
            client,
            source,
            format!("radio-boden-station-{id}"),
            dataset,
        )
        .await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_groundwater_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let response = {
        let api_url = Url::parse("https://app.hlnug.de/json/grdw/getThemeStations/10")?;
        let text = client
            .fetch_text(source, "groundwater-results.isol".to_owned(), &api_url)
            .await?;
        from_str::<Vec<StationResult>>(&text)?
    };

    let count = response.len();

    let (results, errors) = fetch_many(0, 0, response, |result| async move {
        let id = result.id;
        let station_id = result.station_id;
        let station_name = result.display_name;

        let title = format!("Grundwassermessstelle {station_id} in {station_name}");

        let description = format!(r#"Dieser Datensatz enthält Informationen zur Grundwassermessstelle {station_id} in {station_name}.
Die Messgröße ist der Grundwasserstand.
Auf der Webseite zur Messstelle ist ein Link zum Herunterladen der Rohdaten vorhanden."#);

        let landing_page = format!("https://www.hlnug.de/messwerte/datenportal/messstelle/4/10/{station_id}/106");

        let x = result.lon.parse::<f64>()?;
        let y = result.lat.parse::<f64>()?;
        let bounding_boxes = smallvec![Rect::new(Coord { x, y }, Coord { x, y })];

        let mut resources = smallvec![Resource {
            r#type: ResourceType::WebPage,
            description: Some("Weitere Informationen zur Messstelle beim Landesgrundwasserdienst".to_owned()),
            url: format!("https://lgd.hessen.de/mapapps/resources/apps/lgd/index.html?lang=de&identify=%7b%22store%22:%22messstellen%22,%22attributeName%22:%22ID%22,%22value%22:%22{station_id}%22%7d"),
            ..Default::default()
        }];

        let resources_str = fetch_resource_str(client, source, &station_id, "grdw").await?;

        for resource_str in resources_str {
            if resource_str.ends_with(".jpg") {
                resources.push(
                    Resource {
                        r#type: ResourceType::Jpeg,
                        description: Some("Bild der Messstelle".to_owned()),
                        url: source.url.join(&resource_str)?.into(),
                        ..Default::default()
                    }
                    .guess_or_keep_type(),
                );
            }
        }

        clean_resources(selectors, &mut resources);

        let time_url = Url::parse(&format!(
            "https://app.hlnug.de/json/grdw/getStationMinMax/{station_id}/tstamp"
        ))?;
        let time_key = format!("groundwater-station-{station_id}-tstamp.isol");
        let mut time_ranges = get_time_range(client, source, &time_url, time_key).await?;

        // The following is a hack as the API apparently gives wrong start dates for Grundwasserstationen.
        // On the website data of the last three years can be accessed.
        for time_range in &mut time_ranges {
            if time_range.from.year() == 2015 {
                time_range.from = Date::from_ordinal_date(time_range.until.year() - 2, 1)?;
            }
        }

        let types = smallvec![Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some(station_id),
                ..Default::default()
            }),
            measured_variables: smallvec!["Grundwasserstand".to_owned()],
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            regions:  smallvec![Region::HE],
            bounding_boxes,
            time_ranges,
            resources,
            language: Language::German,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: landing_page,
            machine_readable_source: true,
            ..Default::default()
        };

        write_dataset(dir, client, source, format!("groundwater-{id}"), dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_river_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
) -> Result<(usize, usize, usize)> {
    let response = {
        let api_url = Url::parse("https://app.hlnug.de/json/wasser/getThemeStations/6")?;
        let text = client
            .fetch_text(source, "river-results.isol".to_owned(), &api_url)
            .await?;
        from_str::<Vec<StationResult>>(&text)?
    };

    let count = response.len();

    let (results, errors) = fetch_many(0, 0, response, |result| async move {
        let id = result.id;
        let station_id = result.station_id;
        let station_name = result.display_name;

        let measured_variables = fetch_meta_data(client, source, &station_id, "wasser").await?;

        let title = format!("Flussmessstelle Nr. {station_id} in {station_name}");

        let description = format!(r#"Dieser Datensatz enthält Informationen zur Flussmessstelle Nr. {station_id} in {station_name}.
Auf der Webseite zur Messstelle ist ein Link zum Herunterladen der Rohdaten vorhanden."#);

        let landing_page = format!("https://www.hlnug.de/messwerte/datenportal/messstelle/4/6/{station_id}/");

        let x = result.lon.parse::<f64>()?;
        let y = result.lat.parse::<f64>()?;
        let bounding_boxes = smallvec![Rect::new(Coord { x, y }, Coord { x, y })];

        let mut regions = smallvec![Region::HE];
        regions.extend(WISE.match_shape(x, y).map(Region::Watershed));

        let time_url = Url::parse(&format!(
            "https://app.hlnug.de/json/wasser/getStationMinMax/{station_id}/tstamp"
        ))?;
        let time_key = format!("river-station-{station_id}-tstamp.isol");
        let time_ranges = get_time_range(
            client,
            source,
            &time_url,
            time_key,
        )
        .await?;

        let types = smallvec![
            Type::Measurements {
                domain: Domain::Rivers,
                station: Some(Station {
                    id: Some(station_id.clone()),
                    ..Default::default()
                }),
                measured_variables: measured_variables.clone(),
                methods: Default::default(),
            },
            Type::Measurements {
                domain: Domain::Chemistry,
                station: Some(Station {
                    id: Some(station_id),
                    ..Default::default()
                }),
                measured_variables,
                methods: Default::default(),
            }
        ];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            regions,
            bounding_boxes,
            time_ranges,
            language: Language::German,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: landing_page,
            ..Default::default()
        };

        write_dataset(dir, client, source, format!("river-station-{id}"), dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_daily_quakes(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join("/themen/geologie/erdbeben")?;
    let id = "erdbeben-taeglich".to_owned();

    let dataset = {
        let text = client
            .fetch_text(source, format!("{id}.isol"), &url)
            .await?;
        let document = Html::parse_document(&text);

        let title = select_text(&document, &selectors.title_with_subtitles);
        let description = select_text(&document, &selectors.default_description);

        let mut resources = document
            .select(&selectors.default_links)
            .map(|elem| {
                let href = elem.attr("href").unwrap();

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .chain(
                document
                    .select(&selectors.default_images)
                    .filter_map(|elem| {
                        let src = elem.attr("src").unwrap();

                        if !src.contains("/HLNUG-Logo.png") {
                            Some(src)
                        } else {
                            None
                        }
                    })
                    .map(|src| {
                        Ok(Resource {
                            r#type: ResourceType::Image,
                            url: source.url.join(src)?.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type())
                    }),
            )
            .collect::<Result<SmallVec<_>>>()?;

        clean_resources(selectors, &mut resources);

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Measurements {
                domain: Domain::Geophysics,
                station: Default::default(),
                measured_variables: smallvec!["Erdbebenstärke".to_owned()],
                methods: Default::default(),
            }],
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: url.into(),
            resources,
            regions: smallvec![Region::HE],
            language: Language::German,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, id, dataset).await
}

async fn fetch_meta_data(
    client: &Client,
    source: &Source,
    station_id: &str,
    measure_type: &str,
) -> Result<SmallVec<[String; 1]>> {
    let api_url = Url::parse(&format!(
        "https://app.hlnug.de/json/{measure_type}/getStation/{station_id}?join"
    ))?;

    let response = {
        let text = client
            .fetch_text(
                source,
                format!("measure-{station_id}-results.isol"),
                &api_url,
            )
            .await?;
        from_str::<MetaDataResponse>(&text)?
    };

    let mut measure_vec = Vec::new();

    if measure_type == "lmw" {
        let measures = response
            .latest
            .as_ref()
            .ok_or_else(|| anyhow!("Missing entry 'latest' in API"))?;

        for measure in measures {
            if !measure.paramname.is_empty() {
                measure_vec.push(&*measure.paramname);
            }
        }
    } else if measure_type == "wasser" || measure_type == "radio" || measure_type == "insitu" {
        for measure in &response.parameters {
            measure_vec.push(measure.display_name.trim());
        }
    }

    let measured_variables = measure_vec
        .iter()
        .map(|&measured_variable| measured_variable.to_owned())
        .collect();

    if measure_vec.is_empty() {
        measure_vec.push("keine Messgrößen vorhanden");
    }

    Ok(measured_variables)
}

async fn fetch_resource_str(
    client: &Client,
    source: &Source,
    station_id: &str,
    measure_type: &str,
) -> Result<Vec<String>> {
    let api_url = Url::parse(&format!(
        "https://app.hlnug.de/json/{measure_type}/getStation/{station_id}?join"
    ))?;

    let response = {
        let text = client
            .fetch_text(
                source,
                format!("measure-{station_id}-results.isol"),
                &api_url,
            )
            .await?;
        from_str::<MetaDataResponse>(&text)?
    };

    let mut resources = Vec::new();

    for part in response.material.split('"') {
        if part.ends_with(".pdf")
            || part.ends_with(".PDF")
            || part.ends_with(".jpg")
            || part.ends_with(".JPG")
        {
            resources.push(part.replace('\\', ""))
        }
    }

    Ok(resources)
}

fn clean_resources(selectors: &Selectors, resources: &mut SmallVec<[Resource; 4]>) {
    resources.retain(|resource| !selectors.excluded_resources.is_match(&resource.url))
}

async fn get_time_range(
    client: &Client,
    source: &Source,
    time_url: &Url,
    key: String,
) -> Result<SmallVec<[TimeRange; 1]>> {
    let text = client.fetch_text(source, key, time_url).await?;
    let response = from_str::<TstampResult>(&text)?;

    let start_date = OffsetDateTime::from_unix_timestamp(response.min)?;
    let end_date = OffsetDateTime::from_unix_timestamp(response.max)?;

    let time_range = (start_date.date(), end_date.date()).into();

    Ok(smallvec![time_range])
}

#[derive(Debug, Deserialize)]
struct MetaDataResponse {
    material: String,
    latest: Option<Vec<MetaDataResult>>,
    parameters: Vec<ParameterResult>,
}

#[derive(Debug, Deserialize)]
struct MetaDataResult {
    paramname: String,
}

#[derive(Debug, Deserialize)]
struct ParameterResult {
    #[serde(rename = "displayName")]
    display_name: String,
}

#[derive(Debug, Deserialize)]
struct StationResult {
    id: i16,
    #[serde(rename = "stationId")]
    station_id: CompactString,
    #[serde(rename = "displayName")]
    display_name: String,
    lon: String,
    lat: String,
}

#[derive(Debug, Deserialize)]
struct TstampResult {
    min: i64,
    max: i64,
}

selectors! {
    default_title: "div h1",
    default_description: "main div p, main div td, .ce-bodytext li",
    default_links: "p a[href], .ce-above a[href], .ce-bodytext li a[href], .externer-link-mit-icon",
    default_date: "b",
    default_images: ".img-responsive[src]",

    title_with_subtitles: "h2 , .colPos-0 .main-header",

    quakes_main: "table.tx_hlnugerdbeben tr",
    quakes_link: ".keine-auszeichnung[href]",
    quakes_next_page_link: ".next a",

    themen_title: ".colPos-0 .main-header",
    themen_link: ".px-4 a[href]",

    breadcrumb:".breadcrumb a",
    search_links: "h3 a[href]",
    links_l1: ".nav-link[href]",
    links_sidebar: "#maincontent .nav-link[href]",
    links_english: ".link-english[href]",
    panels: ".frame-wrap",
    panel_titles: ".panel-heading",
    panel_par: "p",
    right_info: ".right, #maincontent",

    is_contact: "(kontaktformular)" as Regex,
    is_internal_link: ".*intern.*Link" as Regex,
    exclude_pages: r"^/datenschutzerklaerung",
    r"^https://www.hlnug.de/netiquette",
    r"verkauf-gebrauchter-gegenstaende",
    r"^/impressum",
    r"^https://www.hlnug.de#",
    r"^https://www.hlnug.de/#.*",
    r"^/$",
    r"#$",
    r"^https://www.hlnug.de/medien/boden/fisbo/erfstd/index.html$",
    r"^https://emissionskataster.hlnug.de$",
    r"^http://www.hlug.de/medien/boden/fisbo/wbsa/start.htm$",
    r"^\#$",
    r"^#tab\-pane_",
    r"^https://www\.hlnug\.de/\#.*"
    as RegexSet,
    excluded_resources: r"carousel",
    r"collapse" as RegexSet,
    is_english_description: r"Hessian Database for Waste Transports",
    r"for the English version of this content",
    r"Click here" as RegexSet,

    quake_bulletin: r"(?<date>[\d\.]*)\s+(?<time>[\d\.:]+)\s+(?<place>.*?)\s*\((?<state>.*?),\D*(?<magnitude>\S+)\s+(?<lat>\S+)\s+(?<lon>\S+)\s+(?<depth>\S+)" as Regex,
    book_sorter: r"band-(?P<book>\d+)(?P<remainder>.*)" as Regex,
    group_dataset_on_url_match: r"altlasten/arbeitshilfen(-des-hlnug$|/band-(?P<book>\d+)(?P<remainder>.*))",
    r"qualitaetssicherung-von-29b-messstellen"
    as RegexSet,
}
