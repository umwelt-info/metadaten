mod aktion_flaeche_uba;
mod ars_ags;
mod atkis;
mod beteiligung_bge;
mod blauer_engel_uba;
mod cheminfo_uba;
mod ckan;
mod compositerunoff_grdc_bfg;
mod csw;
mod diffuse_quellen_uba;
mod doris_bfs;
mod elwas_nrw;
mod elwis_gdws;
mod endlagersuche_base;
mod energieatlas_st;
mod envir_acts_bfn;
mod envir_acts_bmuv;
mod external;
mod ffh_vp_info_bfn;
mod fis_wasser_mv;
mod flora_web_bfn;
mod geonames;
mod geoportal_wasser_sl;
mod gewaesserberechnung_uba;
mod gewaesserbewertung_uba;
mod giir_uba;
mod gkd_by;
mod gld_gw_st;
mod gw_lu_bw;
mod gw_mst_lfu_bb;
mod gw_mst_tlubn_th;
mod gw_sukw_hb;
mod hlnug_he;
mod hnd_by;
mod hochwasservorhersagezentrale_st;
mod hvz_bw;
mod kaikki;
mod klivo_bmuv;
mod kommunales_abwasser_uba;
mod korina_ufu;
mod lanuv_nrw;
mod lfu_rlp;
mod lhw_datenportal_st;
mod lu_bw;
mod luftdaten_uba;
mod luis_gw_sn;
mod manual;
mod messpegel_de;
mod mudab_uba;
mod naturdetektive_bfn;
mod nid_by;
mod niz_bw;
mod nlwkn_db_ni;
mod nlwkn_grundwasser_ni;
mod nlwkn_ni;
mod oai;
mod odl_info_bfs;
mod pegelonline_gdws;
mod piveau_search;
mod portal_grdc_bfg;
mod probas_uba;
mod rekis;
mod ressourceneffizienz_bmuv;
mod rote_liste_zentrum_bfn;
mod runder_tisch_meeresmuell_uba;
mod sar_bfs;
mod sensor_community;
mod smart_finder;
mod stadt_land_plus_uba;
mod sturmflutwarndienst_bsh;
mod suche_st;
mod thru_de_uba;
mod ufordat_uba;
mod uip_uba;
mod umweltchronik_uba;
mod umweltportal_gw_sh;
mod undine_bfg;
mod upb_uba;
mod upb_website_uba;
mod uvk_be;
mod uvp_portal_uba;
mod uvp_verbund;
mod wasser_de_bfg;
mod wasserblick_bfg;
mod wasserportal_be;
mod wasserportal_rlp;
mod website_base;
mod website_bfn;
mod website_bfs;
mod website_bge;
mod website_grdc_bfg;
mod website_publications_uba;
mod website_uba;
mod wikidata;
mod wise;
mod wisia_bfn;

use std::future::Future;
use std::pin::Pin;
use std::sync::Arc;
use std::time::SystemTime;

use anyhow::{Context, Result, anyhow, ensure};
use cap_std::{ambient_authority, fs::Dir};
use glib::{log_set_default_handler, rust_log_handler};
use tokio::task::JoinSet;
use tracing_subscriber::{filter::DynFilterFn, layer::SubscriberExt, util::SubscriberInitExt};

use harvester::{
    Config, Source,
    client::{BaseClient, Client},
    write_dataset,
};
use metadaten::{data_path_from_env, dataset::Dataset, metrics::Harvest as HarvestMetrics};

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::registry()
        .with(DynFilterFn::new(|_, cx| {
            cx.lookup_current()
                .is_none_or(|span| span.name() != "suppress_errors")
        }))
        .with(tracing_subscriber::EnvFilter::from_default_env())
        .with(tracing_subscriber::fmt::layer().without_time())
        .init();

    log_set_default_handler(rust_log_handler);

    let config = Config::read()?;

    let count = config.sources.len();
    tracing::info!("Harvesting {} sources", count);

    let data_path = data_path_from_env();

    let dir = Dir::open_ambient_dir(&data_path, ambient_authority())
        .with_context(|| format!("Failed to open data directory at '{}'", data_path.display()))?;

    let client = Arc::new(BaseClient::new(&config, &dir)?);

    {
        let _ = dir.remove_dir_all("datasets.old");
        let dir_old = dir.open_dir("datasets").ok().map(Arc::new);

        let _ = dir.remove_dir_all("datasets.new");
        dir.create_dir("datasets.new")?;
        let dir = Arc::new(dir.open_dir("datasets.new")?);

        let start = SystemTime::now();

        let mut tasks = JoinSet::new();

        for source in config.sources {
            let dir = dir.clone();
            let dir_old = dir_old.clone();
            let client = Client::new(client.clone(), &source)?;

            tasks.spawn(async move {
                let result = harvest(&dir, &client, &source)
                    .await
                    .with_context(|| format!("Failed to harvest source {}", source.name));

                if result.is_err() {
                    if config.keep_failed_sources {
                        if let Some(dir_old) = dir_old {
                            if let Err(err) = keep(&dir_old, &dir, &client, &source).await {
                                tracing::warn!("Failed to keep source {}: {:#}", source.name, err);
                            }
                        }
                    }

                    if config.merge_duplicates {
                        client.base.duplicates.remove_source(&source.name);
                    }

                    client
                        .base
                        .metrics
                        .lock()
                        .record_failed_harvest(source.name);
                }

                result
            });
        }

        let mut errors = 0;

        while let Some(res) = tasks.join_next().await {
            if let Err(err) = res.unwrap() {
                tracing::error!("{:#}", err);

                errors += 1;
            }
        }

        if config.merge_duplicates {
            client.duplicates.merge_duplicates(&dir).await;
        }

        let duration = start.elapsed()?;

        if errors != 0 {
            tracing::error!("Failed to harvest {errors} out of {count} sources");
        }

        client.metrics.lock().last_harvest = Some((start, duration));
    }

    let _ = dir.rename("datasets", &dir, "datasets.old");
    dir.rename("datasets.new", &dir, "datasets")?;

    let mut client = Arc::into_inner(client).unwrap();
    client.write_tag_defs().await?;
    client.metrics.get_mut().write(&dir)?;

    client.duplicates.write_duplicate_landing_pages(&dir)?;

    Ok(())
}

macro_rules! dispatch_harvester {
    ( $($harvester:ident)+ ) => {
        fn dispatch_harvester<'a>(
            dir: &'a Dir,
            client: &'a Client,
            source: &'a Source,
        ) -> Result<Pin<Box<dyn Future<Output = Result<(usize, usize, usize)>> + Send + 'a>>> {
            match &*source.r#type {
                $(
                    stringify!($harvester) => Ok(Box::pin($harvester::harvest(dir, client, source))),
                )+

                r#type if r#type.starts_with("external:") => Ok(Box::pin(external::harvest(&dir, client, source, &r#type[9..]))),

                r#type => Err(anyhow!("Unknow harvester type `{}`", r#type)),
            }
        }
    };
}

dispatch_harvester!(
    wikidata atkis ars_ags geonames kaikki wise
    manual ckan csw smart_finder oai piveau_search
    uvp_verbund
    website_bfn wisia_bfn flora_web_bfn naturdetektive_bfn rote_liste_zentrum_bfn ffh_vp_info_bfn envir_acts_bfn
    aktion_flaeche_uba cheminfo_uba diffuse_quellen_uba gewaesserbewertung_uba gewaesserberechnung_uba giir_uba kommunales_abwasser_uba probas_uba ufordat_uba umweltchronik_uba thru_de_uba stadt_land_plus_uba website_uba luftdaten_uba mudab_uba runder_tisch_meeresmuell_uba blauer_engel_uba upb_uba upb_website_uba uip_uba uvp_portal_uba website_publications_uba
    portal_grdc_bfg website_grdc_bfg compositerunoff_grdc_bfg wasser_de_bfg wasserblick_bfg undine_bfg
    website_bfs doris_bfs sar_bfs odl_info_bfs
    website_base endlagersuche_base
    elwis_gdws pegelonline_gdws
    wasserportal_be uvk_be
    lu_bw gw_lu_bw hvz_bw niz_bw
    hlnug_he
    rekis
    suche_st energieatlas_st hochwasservorhersagezentrale_st lhw_datenportal_st gld_gw_st
    envir_acts_bmuv klivo_bmuv ressourceneffizienz_bmuv
    korina_ufu
    sturmflutwarndienst_bsh
    elwas_nrw lanuv_nrw
    wasserportal_rlp lfu_rlp
    nlwkn_grundwasser_ni nlwkn_ni nlwkn_db_ni
    gkd_by hnd_by nid_by
    gw_mst_lfu_bb
    umweltportal_gw_sh
    fis_wasser_mv
    gw_sukw_hb
    geoportal_wasser_sl
    luis_gw_sn
    gw_mst_tlubn_th
    sensor_community messpegel_de
    website_bge beteiligung_bge
);

#[tracing::instrument(skip(dir, client))]
async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<()> {
    tracing::debug!("Harvesting source {}", source.name);

    let dir_name = source.datasets_dir_name();

    dir.create_dir(&*dir_name)?;
    let dir = dir.open_dir(&*dir_name)?;

    let (deleted, age) = client.clean_responses(source)?;

    let start = SystemTime::now();

    let (count, transmitted, failed) = dispatch_harvester(&dir, client, source)?.await?;

    let duration = start.elapsed()?;

    ensure!(
        client.check_fuse(source).is_ok(),
        "Failing source after fuse was blown"
    );

    ensure!(transmitted != 0, "No datasets were recorded");

    if failed != 0 {
        tracing::warn!("{failed} errors while harvesting {transmitted} out of {count} datasets");
    }

    client.base.metrics.lock().record_harvest(
        source.name.clone(),
        HarvestMetrics {
            deleted,
            age,
            duration,
            count,
            transmitted,
            failed,
        },
    );

    Ok(())
}

#[tracing::instrument(skip(dir_old, dir, client))]
async fn keep(dir_old: &Dir, dir: &Dir, client: &Client, source: &Source) -> Result<()> {
    tracing::debug!("Keeping source {}", source.name);

    let dir_name = source.datasets_dir_name();

    let _ = dir.remove_dir_all(&*dir_name);

    let dir_old = dir_old.open_dir(&*dir_name)?;

    dir.create_dir(&*dir_name)?;
    let dir = dir.open_dir(&*dir_name)?;

    if source.not_indexed {
        for entry in dir_old.entries()? {
            let entry = entry?;

            let file_name = entry.file_name();
            dir_old.hard_link(&file_name, &dir, &file_name)?;
        }

        return Ok(());
    }

    let mut dataset;
    let mut buf = Vec::new();

    for entry in dir_old.entries()? {
        let entry = entry?;

        let id = entry.file_name().into_string().unwrap();
        let file = entry.open()?;

        (dataset, buf) = Dataset::async_read_with(file, buf).await?;

        write_dataset(&dir, client, source, id, dataset).await?;
    }

    Ok(())
}
