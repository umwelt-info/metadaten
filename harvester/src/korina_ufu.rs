#![allow(clippy::ptr_arg)]

use std::mem::take;

use anyhow::{Context, Result, anyhow, ensure};
use cap_std::fs::Dir;
use geo::{Rect, algorithm::BoundingRect};
use geozero::{ToGeo, geojson::GeoJson};
use hashbrown::HashSet;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{SmallVec, smallvec};
use time::Date;
use url::Url;

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors,
    utilities::{
        GermanDate, collect_text, make_key, parse_attributes, select_first_text, select_text,
    },
    write_dataset,
};
use metadaten::dataset::{
    Dataset, Language, License, Organisation, OrganisationKey, OrganisationRole, Person,
    PersonRole, Resource, ResourceType, TimeRange,
    r#type::{TextType, Type},
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let organisations = smallvec![Organisation::WikiData {
        // according to https://www.korina.info/impressum/
        identifier: OrganisationKey::UFU,
        role: OrganisationRole::Provider,
    }];

    let persons = vec![Person {
        // according to https://www.korina.info/kontakt/
        name: "Max Neumann".to_string(),
        role: PersonRole::Contact,
        emails: smallvec!["max.neumann@ufu.de".to_string()],
        ..Default::default()
    }];

    let (mut count, mut results, mut errors) =
        fetch_species(dir, client, source, selectors, &organisations, &persons).await?;

    let (count1, results1, errors1) =
        fetch_publications(dir, client, source, selectors, &organisations, &persons).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) =
        fetch_seminars(dir, client, source, selectors, &organisations, &persons).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) =
        fetch_workshops(dir, client, source, selectors, &organisations, &persons).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) =
        fetch_news(dir, client, source, selectors, &organisations, &persons).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) =
        fetch_teaching_mat(dir, client, source, selectors, &organisations, &persons).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_species(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    organisations: &SmallVec<[Organisation; 2]>,
    persons: &Vec<Person>,
) -> Result<(usize, usize, usize)> {
    let (count, results, errors) =
        fetch_species_page(dir, client, source, selectors, organisations, persons, 1).await?;

    let pages = count.div_ceil(5);

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| {
        fetch_species_page(dir, client, source, selectors, organisations, persons, page)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_species_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    organisations: &SmallVec<[Organisation; 2]>,
    persons: &Vec<Person>,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let count;
    let names;

    {
        let url = source.url.join(&format!("arten/page/{page}/"))?;

        let text = client
            .fetch_text(source, format!("species-{page}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        count = document.select(&selectors.species_options).count();

        names = parse_attributes(
            &document,
            &selectors.species_details,
            &selectors.species_name,
            "href",
            "species name",
        )
        .collect::<Result<Vec<String>>>()?;
    }

    ensure!(
        !names.is_empty(),
        "Species page {page} contains no links to details"
    );

    let (results, errors) = fetch_many(0, 0, names, |name| async move {
        fetch_species_details(
            dir,
            client,
            source,
            selectors,
            &name,
            organisations,
            persons,
        )
        .await
        .with_context(|| format!("Failed to fetch details of species {name}"))
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_species_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    name: &str,
    organisations: &SmallVec<[Organisation; 2]>,
    persons: &Vec<Person>,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join(&format!("arten/{name}/"))?;

    let common_name;
    let scientific_name;
    let mut description = None;
    let mut resources = SmallVec::new();
    let mut bounding_boxes = SmallVec::new();
    let synonyms;
    let mut distribution_url = None;

    {
        let text = client
            .fetch_text(source, format!("species-details-{name}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        common_name = select_first_text(&document, &selectors.species_common_name, "common name")?;
        ensure!(!common_name.is_empty(), "Empty common name");

        scientific_name = select_first_text(
            &document,
            &selectors.species_scientific_name,
            "scientific name",
        )?;
        ensure!(!scientific_name.is_empty(), "Empty scientific name");

        for element in document.select(&selectors.species_description) {
            let key = select_text(element, &selectors.species_description_key);

            if key.contains("Allgemeine Information") {
                description = Some(select_text(element, &selectors.species_description_value));
            }
        }

        synonyms = select_text(&document, &selectors.species_synonyms);

        if let Some(element) = document.select(&selectors.species_identification).next() {
            let href = element.attr("href").unwrap();
            let url = source.url.join(href)?;

            resources.push(
                Resource {
                    r#type: ResourceType::WebPage,
                    description: Some("Bestimmungshilfe".to_owned()),
                    url: url.into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }

        for element in document.select(&selectors.species_identification_alternative) {
            if let Some(link) = element
                .select(&selectors.species_identification_alternative_link)
                .next()
            {
                let text = collect_text(element.text());

                if text.contains("Bestimmungshilfe") {
                    let href = link.attr("href").unwrap();
                    let url = source.url.join(href)?;

                    resources.push(
                        Resource {
                            r#type: ResourceType::WebPage,
                            description: Some(text),
                            url: url.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }
            }
        }

        if let Some(element) = document.select(&selectors.species_distribution).next() {
            let href = element.attr("href").unwrap();
            let url = source.url.join(href)?;

            resources.push(Resource {
                r#type: ResourceType::WebPage,
                description: Some("Verbreitungskarte".to_owned()),
                url: url.into(),
                ..Default::default()
            });

            if let Some(captures) = selectors.species_distribution_id.captures(href) {
                let mut url = source.url.join("atlas/geojson/generategeojson.php")?;

                url.query_pairs_mut().append_pair("taxnr", &captures["id"]);

                distribution_url = Some(url.clone());

                resources.push(Resource {
                    r#type: ResourceType::GeoJson,
                    description: Some("Verbreitungskarte (GeoJSON)".to_owned()),
                    url: url.into(),
                    ..Default::default()
                });
            }
        }

        for element in document.select(&selectors.species_figure) {
            let description = element
                .select(&selectors.species_figure_caption)
                .next()
                .map(|element| collect_text(element.text()));

            let image = element
                .select(&selectors.species_figure_image)
                .next()
                .ok_or_else(|| anyhow!("Missing image"))?;
            let url = source.url.join(image.attr("src").unwrap())?;

            resources.push(Resource {
                r#type: ResourceType::Image,
                description,
                url: url.into(),
                ..Default::default()
            });
        }

        for element in document.select(&selectors.species_identification_alternative) {
            let Some(link) = element.select(&selectors.link_without_google).next() else {
                continue;
            };

            let href = link.attr("href").unwrap();
            let url = source.url.join(href)?;

            if resources
                .iter()
                .any(|resource: &Resource| resource.url == url.as_str())
            {
                continue;
            }

            let description = collect_text(element.text());
            let primary_content =
                description.contains("Steckbrief") && description.contains("Liste");

            let description = if !description.is_empty() {
                Some(description)
            } else {
                None
            };

            resources.push(
                Resource {
                    r#type: ResourceType::WebPage,
                    description,
                    url: url.into(),
                    primary_content,
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }
    }

    let title = format!("{common_name} ({scientific_name})");

    if let Some(url) = distribution_url {
        match fetch_species_bounding_box(client, source, name, &url).await {
            Ok(bounding_box) => bounding_boxes.push(bounding_box),
            Err(err) => tracing::debug!("Failed to fetch bounding box of {name}: {:#}", err),
        }
    }

    let dataset = Dataset {
        title,
        description,
        types: smallvec![Type::Taxon {
            scientific_name,
            common_names: smallvec![common_name],
        }],
        comment: Some(synonyms),
        resources,
        bounding_boxes,
        language: Language::German,
        license: License::OtherOpen,
        source_url: url.into(),
        origins: source.origins.clone(),
        organisations: organisations.clone(),
        persons: persons.clone(),
        ..Default::default()
    };

    write_dataset(
        dir,
        client,
        source,
        format!("species-details-{name}"),
        dataset,
    )
    .await
}

async fn fetch_species_bounding_box(
    client: &Client,
    source: &Source,
    name: &str,
    url: &Url,
) -> Result<Rect> {
    let text = client
        .fetch_text(source, format!("species-bounding-box-{name}.isol"), url)
        .await?;

    let geometry = GeoJson(&text).to_geo()?;

    geometry
        .bounding_rect()
        .ok_or_else(|| anyhow!("Missing geometry"))
}

async fn fetch_publications(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    organisations: &SmallVec<[Organisation; 2]>,
    persons: &Vec<Person>,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join("publikationen/publikationsliste/")?;

    let datasets;

    {
        let text = client
            .fetch_text(source, "publications".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        datasets = document
            .select(&selectors.publications_content)
            .filter_map({
                let mut year = None;

                move |element| {
                    let text = collect_text(element.text());

                    match (element.value().name(), year) {
                        ("h1", _) => {
                            year = text.parse().ok();
                            None
                        }
                        ("p", Some(year)) => Some((element, year, text)),
                        _ => None,
                    }
                }
            })
            .filter(|(_, _, title)| !title.is_empty())
            .map(|(element, year, title)| {
                let mut date = Date::from_ordinal_date(year, 1)?;
                if let Some(captures) = selectors.publications_title_date.captures(&title) {
                    date = captures["date"].parse::<GermanDate>()?.into();
                }

                let link = element
                    .select(&selectors.publications_link)
                    .next()
                    .map(|element| element.attr("href").unwrap())
                    .map(|href| source.url.join(href))
                    .transpose()?;

                let resources = link
                    .into_iter()
                    .map(|link| {
                        Resource {
                            r#type: ResourceType::Pdf,
                            description: None,
                            url: link.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type()
                    })
                    .collect();

                Ok(Dataset {
                    title,
                    types: smallvec![Type::Text {
                        text_type: TextType::Publication,
                    }],
                    issued: Some(date),
                    resources,
                    language: Language::German,
                    license: License::OtherOpen,
                    source_url: url.clone().into(),
                    origins: source.origins.clone(),
                    organisations: organisations.clone(),
                    persons: persons.clone(),
                    ..Default::default()
                })
            })
            .collect::<Vec<Result<_>>>();
    }

    write_datasets(dir, client, source, datasets, |index| {
        format!("publication-{index}")
    })
    .await
}

async fn fetch_seminars(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    organisations: &SmallVec<[Organisation; 2]>,
    persons: &Vec<Person>,
) -> Result<(usize, usize, usize)> {
    let url = &source.url.join("info/veranstaltungsarchiv/seminare/")?;

    let datasets;

    {
        let text = client
            .fetch_text(source, "seminars".to_owned(), url)
            .await?;

        let document = Html::parse_document(&text);

        datasets = document
            .select(&selectors.seminars_content)
            .filter_map({
                let mut year = None;

                move |element| {
                    let text = collect_text(element.text());

                    match (element.value().name(), year) {
                        ("h1" | "h2" | "h3", _) => {
                            year = text.parse().ok();
                            None
                        }
                        ("div", Some(year)) => Some((element, year)),
                        _ => None,
                    }
                }
            })
            .flat_map(|(element, year)| {
                element
                    .select(&selectors.seminars_item)
                    .filter_map(|element| {
                        let title = collect_text(element.text());

                        if !title.is_empty() {
                            Some((element, title))
                        } else {
                            None
                        }
                    })
                    .map(move |(element, title)| {
                        let time_range = TimeRange::whole_year(year)?;
                        let time_ranges = smallvec![time_range];

                        let link = element
                            .select(&selectors.seminars_item_link)
                            .next()
                            .map(|element| element.attr("href").unwrap())
                            .map(|href| source.url.join(href))
                            .transpose()?;

                        let resources = link
                            .into_iter()
                            .map(|link| {
                                Resource {
                                    r#type: ResourceType::Pdf,
                                    description: None,
                                    url: link.into(),
                                    ..Default::default()
                                }
                                .guess_or_keep_type()
                            })
                            .collect();

                        Ok(Dataset {
                            title,
                            types: smallvec![Type::Event],
                            time_ranges,
                            resources,
                            language: Language::German,
                            license: License::OtherOpen,
                            source_url: url.clone().into(),
                            origins: source.origins.clone(),
                            organisations: organisations.clone(),
                            persons: persons.clone(),
                            ..Default::default()
                        })
                    })
            })
            .collect::<Vec<Result<_>>>();
    }

    write_datasets(dir, client, source, datasets, |index| {
        format!("seminar-{index}")
    })
    .await
}

async fn fetch_workshops(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    organisations: &SmallVec<[Organisation; 2]>,
    persons: &Vec<Person>,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join("info/veranstaltungsarchiv/workshops/")?;

    let mut datasets = Vec::new();

    let make_dataset = |title, description, resources| {
        Ok(Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Event],
            resources,
            language: Language::German,
            license: License::OtherOpen,
            source_url: url.clone().into(),
            origins: source.origins.clone(),
            organisations: organisations.clone(),
            persons: persons.clone(),
            ..Default::default()
        })
    };

    {
        let text = client
            .fetch_text(source, "workshops".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        let mut title = String::new();
        let mut description = String::new();
        let mut resources = SmallVec::<[Resource; 4]>::new();

        for element in document.select(&selectors.workshops_content) {
            let text = collect_text(element.text());

            if element.value().name() == "h1" {
                if !title.is_empty() {
                    datasets.push(make_dataset(
                        take(&mut title),
                        take(&mut description),
                        take(&mut resources),
                    ));
                }

                title = text;
            } else if text.starts_with("Vorträge") {
                for element in element.select(&selectors.workshops_material) {
                    let text = collect_text(element.text());

                    let href = element
                        .select(&selectors.workshops_material_link)
                        .next()
                        .ok_or_else(|| anyhow!("Missing link"))?
                        .attr("href")
                        .unwrap();

                    resources.push(
                        Resource {
                            r#type: ResourceType::Pdf,
                            description: Some(text),
                            url: source.url.join(href)?.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }
            } else if !title.is_empty() {
                description.push_str(&text);
            }
        }

        if !title.is_empty() {
            datasets.push(make_dataset(title, description, resources));
        }
    }

    write_datasets(dir, client, source, datasets, |index| {
        format!("workshop-{index}")
    })
    .await
}

async fn fetch_news(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    organisations: &SmallVec<[Organisation; 2]>,
    persons: &Vec<Person>,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join("info/korina/news/")?;

    let datasets;

    {
        let text = client.fetch_text(source, "news".to_owned(), &url).await?;

        let document = Html::parse_document(&text);

        datasets = document
            .select(&selectors.news_item)
            .map(|element| {
                let title = select_first_text(element, &selectors.news_title, "title")?;

                if title.is_empty() {
                    tracing::warn!("Empty title in news item");
                    return Ok(None);
                }

                let description =
                    select_first_text(element, &selectors.news_description, "description")?;

                let captures = selectors
                    .news_date
                    .captures(&title)
                    .ok_or_else(|| anyhow!("Missing date in title"))?;

                let date = captures["date"].parse::<GermanDate>()?;

                let title = title[captures.get(0).unwrap().range().end..].to_owned();

                Ok(Some(Dataset {
                    title,
                    description: Some(description),
                    types: smallvec![Type::Text {
                        text_type: TextType::News,
                    }],
                    issued: Some(date.into()),
                    language: Language::German,
                    license: License::OtherOpen,
                    source_url: url.clone().into(),
                    origins: source.origins.clone(),
                    organisations: organisations.clone(),
                    persons: persons.clone(),
                    ..Default::default()
                }))
            })
            .filter_map(Result::transpose)
            .collect::<Vec<Result<_>>>();
    }

    write_datasets(dir, client, source, datasets, |index| {
        format!("news-{index}")
    })
    .await
}

async fn fetch_teaching_mat(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    organisations: &SmallVec<[Organisation; 2]>,
    persons: &Vec<Person>,
) -> Result<(usize, usize, usize)> {
    let links;

    {
        let url = source.url.join("bildung/materialien/")?;

        let text = client
            .fetch_text(source, "teaching-mat".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        links = document
            .select(&selectors.teaching_mat_teaser_links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<HashSet<_>>();
    }

    let count = links.len();

    let (results, errors) = fetch_many(0, 0, links, |href| async move {
        let url = source.url.join(&href)?;
        let key = make_key(&href);

        let title;
        let description;
        let resources;

        {
            let text = client
                .fetch_text(source, format!("teaching-mat-{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            title = select_first_text(&document, &selectors.teaching_mat_title, "title")?;

            let element = document
                .select(&selectors.teaching_mat_description)
                .find(|element| {
                    element
                        .select(&selectors.teaching_mat_description_nav)
                        .next()
                        .is_none()
                })
                .ok_or_else(|| anyhow!("Missing description"))?;

            description = collect_text(element.text());

            resources = element
                .select(&selectors.teaching_mat_description_links)
                .map(|element| {
                    let url = source.url.join(element.attr("href").unwrap())?;

                    Ok(Resource {
                        r#type: ResourceType::Pdf,
                        url: url.into(),
                        ..Default::default()
                    }
                    .guess_or_keep_type())
                })
                .collect::<Result<SmallVec<_>>>()?;
        }

        let dataset = Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::LearningResource],
            resources,
            language: Language::German,
            license: License::OtherOpen,
            source_url: url.clone().into(),
            origins: source.origins.clone(),
            organisations: organisations.clone(),
            persons: persons.clone(),
            ..Default::default()
        };

        write_dataset(dir, client, source, format!("teaching-mat-{key}"), dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn write_datasets(
    dir: &Dir,
    client: &Client,
    source: &Source,
    datasets: Vec<Result<Dataset>>,
    key: impl Fn(usize) -> String,
) -> Result<(usize, usize, usize)> {
    let count = datasets.len();

    let datasets = datasets
        .into_iter()
        .rev()
        .enumerate()
        .map(|(index, dataset)| (key(index), dataset));

    let (results, errors) = fetch_many(0, 0, datasets, |(key, dataset)| async {
        write_dataset(dir, client, source, key, dataset?).await
    })
    .await;

    Ok((count, results, errors))
}

selectors! {
    species_options: "select#species-select-latein option:not([value=''])",
    species_details: "a.more[href*='/arten/']",
    species_name: r"/arten/([a-z\-]+)/" as Regex,
    species_common_name: "nav.breadcrumb span.current-page",
    species_scientific_name: "div.headline span.subtitle, p.erstezeile",
    species_description: "div.row div.col-md-12",
    species_description_key: "h2",
    species_description_value: "p",
    species_identification: "a.korinalink[href*='estimmungshilfe']",
    species_identification_alternative: "div.row div.col-md-12 p",
    species_identification_alternative_link: "a[href*='.pdf']",
    species_distribution: "a.korinalink[href*='funde/atlas/?schnellart=']",
    species_distribution_id: r"funde/atlas/\?schnellart=(?<id>\d+)" as Regex,
    species_synonyms: "div.headline span.brief_description",
    species_figure: "figure",
    species_figure_image: "img[src]",
    species_figure_caption: "figcaption",
    publications_content: "div.row div.col-md-12 > *",
    publications_link: "a[href]",
    publications_title_date: r"(?<date>\d{1,2}\.\d{1,2}\.\d{4})" as Regex,
    seminars_content: "div.row div.col-md-12 > *",
    seminars_item: "p",
    seminars_item_link: "a[href]",
    workshops_content: "div.row div.col-md-12 > *",
    workshops_material: "p",
    workshops_material_link: "a[href]",
    news_item: "div.panel.panel-default",
    news_title: "div.panel-heading",
    news_description: "div.panel-body",
    news_date: r"(?<date>\d{1,2}\.\d{2}\.\d{4}):\s+" as Regex,
    teaching_mat_teaser_links: "article div.teaser a[href]",
    teaching_mat_title: "div.col-md-8 div.headline",
    teaching_mat_description: "div.row div.col-md-12",
    teaching_mat_description_nav: "nav.breadcrumb",
    teaching_mat_description_links: "a[href]:not([href^='mailto'])",
    link_without_google: "[href]:not([href*='google'])",
}
