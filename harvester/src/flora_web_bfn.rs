use std::fmt::Write;
use std::ops::Deref;

use anyhow::{Context, Result, anyhow, ensure};
use cap_std::fs::Dir;
use csv::Reader;
use geo::{Coord, Rect};
use hashbrown::HashSet;
use regex::Regex;
use reqwest::StatusCode;
use scraper::{Element, ElementRef, Html, Node, Selector};
use smallvec::{SmallVec, smallvec};

use harvester::{
    Source,
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, parse_attributes, select_first_text},
    write_dataset,
};
use metadaten::dataset::{
    Dataset, Language, License, Organisation, OrganisationKey, OrganisationRole, Person,
    PersonRole, Resource, ResourceType, r#type::Type,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let species = fetch_species_ids(client, source, selectors).await?;
    let taxons = fetch_taxon_ids(client, source, selectors).await?;
    let count = species.len() + taxons.len();

    let organisations = &smallvec![Organisation::WikiData {
        // according to https://www.floraweb.de/impressum.html
        identifier: OrganisationKey::BFN,
        role: OrganisationRole::Publisher,
    }];

    let persons = &vec![Person {
        // according to https://www.floraweb.de/kontakt.html
        name: "FloraWeb".to_string(),
        role: PersonRole::Contact,
        emails: smallvec!["floraweb@bfn.de".to_string()],
        ..Default::default()
    }];

    let (results, errors) = fetch_many(0, 0, species, |id| async move {
        fetch_species_details(dir, client, source, selectors, id, organisations, persons)
            .await
            .with_context(|| format!("Failed to fetch species details {id}"))
    })
    .await;

    let (results, errors) = fetch_many(results, errors, taxons, |id| async move {
        fetch_taxon_details(dir, client, source, selectors, id, organisations, persons)
            .await
            .with_context(|| format!("Failed to fetch taxon details {id}"))
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_species_ids(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<u64>> {
    let mut species = Vec::new();

    let mut todo = vec![60000_u64];
    let mut done = HashSet::new();

    while let Some(id) = todo.pop() {
        if !done.insert(id) {
            continue;
        }

        let mut url = source.url.join("php/systematik1.php")?;

        url.query_pairs_mut()
            .append_pair("taxon-id", &id.to_string());

        let text = client
            .checked_fetch_text(source, database_error, format!("species-tree-{id}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        for id in parse_attributes(
            &document,
            &selectors.species_tree_link,
            &selectors.species_id,
            "href",
            "species tree ID",
        ) {
            todo.push(id?);
        }

        for id in parse_attributes(
            &document,
            &selectors.species_link,
            &selectors.species_id,
            "href",
            "species ID",
        ) {
            species.push(id?);
        }
    }

    species.sort_unstable();
    species.dedup();

    ensure!(!species.is_empty(), "Did not collect any species ID");

    Ok(species)
}

#[tracing::instrument(skip(dir, client, source, selectors))]
async fn fetch_species_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    id: u64,
    organisations: &SmallVec<[Organisation; 2]>,
    persons: &Vec<Person>,
) -> Result<(usize, usize, usize)> {
    let mut url = source.url.join("php/artenhome.php")?;

    url.query_pairs_mut()
        .append_pair("name-use-id", &id.to_string());

    let scientific_name;

    let title;
    let mut description = String::new();

    let mut resources = smallvec![
        Resource {
            r#type: ResourceType::WebPage,
            url: source
                .url
                .join(&format!("webkarten/karte.html?taxnr={id}"))?
                .into(),
            description: Some("Interaktive Kartendarstellung der Verbreitungsdaten".to_owned()),
            direct_link: false,
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::Kml,
            url: source
                .url
                .join(&format!("php/writekml_tkq.php?sipnr={id}"))?
                .into(),
            description: Some("Verbreitungsdaten im KML-Format".to_owned()),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::Csv,
            url: source
                .url
                .join(&format!("php/download_tkq.php?suchnr={id}"))?
                .into(),
            description: Some("Verbreitungsdaten im CSV-Format".to_owned()),
            direct_link: false,
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            url: source
                .url
                .join(&format!("shiny/florakarte/?taxonid={id}"))?
                .into(),
            description: Some("Interaktive Kartendarstellung der Verbreitungsdaten".to_owned()),
            direct_link: false,
            ..Default::default()
        },
    ];

    {
        let text = client
            .checked_fetch_text(
                source,
                database_error,
                format!("species-details-{id}.isol"),
                &url,
            )
            .await?;

        let document = Html::parse_document(&text);

        scientific_name =
            select_first_text(&document, &selectors.species_scientific_name, "search name")?;

        let taxonomy_tree =
            select_first_text(&document, &selectors.species_taxonomy_tree, "taxonomy tree")?;

        title = format!(
            "Artinformation zu {}",
            select_first_text(&document, &selectors.species_title, "title")?
        );

        if let Some(element) = document.select(&selectors.species_description).next() {
            description = collect_following_text(element);
        }

        write!(&mut description, " Taxonomie: {taxonomy_tree}")?;

        let details = [
            (
                &selectors.species_details,
                [
                    (&selectors.species_bluehmonate, "Blühmonate"),
                    (&selectors.species_lebensform, "Lebensform"),
                    (&selectors.species_fortpflanzung, "Fortpflanzung"),
                ],
            ),
            (
                &selectors.species_status,
                [
                    (&selectors.species_flor_status, "Floristischer Status"),
                    (&selectors.species_rl_status, "Gefährdung"),
                    (&selectors.species_prot_status, "Schutzstatus"),
                ],
            ),
        ];

        for (selector, sub_selectors) in details {
            for element in document.select(selector) {
                for (sub_selector, label) in sub_selectors {
                    if let Some(sub_element) = element.select(sub_selector).next() {
                        if let Some(next_element) = sub_element.next_sibling_element() {
                            let text = collect_text(next_element.text());

                            if !text.is_empty() {
                                write!(&mut description, " {label}: {text}")?;
                            }
                        }

                        break;
                    }
                }
            }
        }

        if let Some(element) = document.select(&selectors.species_photos).next() {
            let href = element.attr("href").unwrap();

            resources.push(Resource {
                r#type: ResourceType::Image,
                url: source.url.join(href)?.into(),
                description: Some("Sammlung von (Hyperlinks zu) Bildern der Art".to_owned()),
                direct_link: false,
                ..Default::default()
            });
        }
    }

    let bounding_boxes = match fetch_species_bounding_box(client, source, id).await {
        Ok(bounding_boxes) => bounding_boxes,
        Err(err) => {
            tracing::error!("Failed to fetch bounding box: {:#}", err);
            Default::default()
        }
    };

    let dataset = Dataset {
        types: smallvec![Type::Taxon {
            scientific_name,
            common_names: Default::default(),
        }],
        title,
        description: Some(description),
        resources,
        bounding_boxes,
        language: Language::German,
        license: License::AllRightsReserved,
        origins: source.origins.clone(),
        source_url: url.into(),
        organisations: organisations.clone(),
        persons: persons.clone(),
        ..Default::default()
    };

    write_dataset(dir, client, source, format!("species-{id}"), dataset).await
}

async fn fetch_species_bounding_box(
    client: &Client,
    source: &Source,
    id: u64,
) -> Result<SmallVec<[Rect; 1]>> {
    let url = source.url.join("php/writecsv4taxon.php")?;

    let text = client
        .make_request(
            source,
            format!("species-bounding-box-csv-{id}.isol"),
            Some(&url),
            |client| async {
                let resp = client
                    .post(url.clone())
                    .form(&[("sipnr", &id.to_string())])
                    .send()
                    .await?;

                if resp.status() == StatusCode::INTERNAL_SERVER_ERROR {
                    // HACK: Status code 500 indicates that no data is available.
                    return Ok(String::new());
                }

                resp.error_for_status()?.text().await
            },
        )
        .await?;

    if text.is_empty() {
        return Ok(SmallVec::new());
    }

    let pos = text
        .find("NAMNR,VOLLNAME,GRIDCODE,LAT,LON")
        .ok_or_else(|| anyhow!("Failed to find start of coordinates"))?;
    let text = &text[pos..];

    let pos = text
        .find("\n\n")
        .ok_or_else(|| anyhow!("Failed to find end of coordinates"))?;
    let text = &text[..pos];

    let mut min_lat = f64::MAX;
    let mut max_lat = f64::MIN;
    let mut min_lon = f64::MAX;
    let mut max_lon = f64::MIN;

    let reader = Reader::from_reader(text.as_bytes());

    for record in reader.into_records() {
        let record = record?;

        let lat = record
            .get(3)
            .ok_or_else(|| anyhow!("Missing latitude in {}", record.as_slice()))?
            .replace(',', ".")
            .parse::<f64>()?;

        let lon = record
            .get(4)
            .ok_or_else(|| anyhow!("Missing longitude in {}", record.as_slice()))?
            .replace(',', ".")
            .parse::<f64>()?;

        min_lat = min_lat.min(lat);
        max_lat = max_lat.max(lat);
        min_lon = min_lon.min(lon);
        max_lon = max_lon.max(lon);
    }

    if (min_lat, max_lat, min_lon, max_lon) != (f64::MAX, f64::MIN, f64::MAX, f64::MIN) {
        Ok(smallvec![Rect::new(
            Coord {
                x: min_lon,
                y: min_lat
            },
            Coord {
                x: max_lon,
                y: max_lat
            }
        )])
    } else {
        Ok(SmallVec::new())
    }
}

async fn fetch_taxon_ids(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<u64>> {
    let url = source
        .url
        .join("php/PflGesAuswahlliste.php?section_start=AA&section_end=Z,Ä,Ö,ÜA")?;

    let text = client
        .checked_fetch_text(source, database_error, "taxons-register".to_owned(), &url)
        .await?;

    let document = Html::parse_document(&text);

    let mut taxons = parse_attributes(
        &document,
        &selectors.taxon_link,
        &selectors.taxon_id,
        "href",
        "taxon ID",
    )
    .collect::<Result<Vec<_>>>()?;

    taxons.sort_unstable();
    taxons.dedup();

    ensure!(!taxons.is_empty(), "Did not collect any taxon ID");

    Ok(taxons)
}

#[tracing::instrument(skip(dir, client, source, selectors))]
async fn fetch_taxon_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    id: u64,
    organisations: &SmallVec<[Organisation; 2]>,
    persons: &Vec<Person>,
) -> Result<(usize, usize, usize)> {
    let mut url = source.url.join("php/PflGesHomepageLayout.php")?;

    url.query_pairs_mut()
        .append_pair("taxon_id", &id.to_string());

    let name;
    let mut common_names = SmallVec::new();

    let title;
    let mut description = "Informationen zu Syntaxonomie und Gefährdung in FloraWeb.".to_owned();
    let comment;

    {
        let text = client
            .checked_fetch_text(
                source,
                database_error,
                format!("taxon-details-{id}.isol"),
                &url,
            )
            .await?;

        let document = Html::parse_document(&text);

        let element = document
            .select(&selectors.taxon_name)
            .next()
            .ok_or_else(|| anyhow!("Missing name for taxon {id}"))?;

        name = collect_following_text(element);

        title = format!("Informationen zur Pflanzengesellschaft {name}");

        for element in document.select(&selectors.taxon_synonyms) {
            let label = element.text().collect::<String>();

            match label.as_str() {
                "Wissenschaftlicher Name" => {
                    let name = collect_following_text(element);

                    description.push_str(" / ");
                    description.push_str(&name);
                }
                "Deutsche Namen" => {
                    let name = collect_following_text(element);

                    description.push_str(" / ");
                    description.push_str(&name);

                    common_names.push(name);
                }
                _ => (),
            }
        }

        if let Some(element) = document.select(&selectors.taxon_scientific_synonyms).next() {
            description.push_str(" / ");
            description.push_str(&collect_text(element.text()));
        }

        comment = document
            .select(&selectors.taxon_comment)
            .next()
            .map(|element| collect_following_text(element));
    }

    let description = (!description.is_empty()).then_some(description);

    let dataset = Dataset {
        title,
        description,
        types: smallvec![Type::Taxon {
            scientific_name: name,
            common_names,
        }],
        comment,
        language: Language::German,
        license: License::AllRightsReserved,
        origins: source.origins.clone(),
        source_url: url.into(),
        organisations: organisations.clone(),
        persons: persons.clone(),
        ..Default::default()
    };

    write_dataset(dir, client, source, format!("taxon-{id}"), dataset).await
}

fn collect_following_text(element: ElementRef) -> String {
    let text = collect_text(
        element
            .next_siblings()
            .filter_map(|node| match node.value() {
                Node::Text(text) => Some(text),
                Node::Element(_) => node.first_child().and_then(|node| node.value().as_text()),
                _ => None,
            })
            .map(|text| text.text.deref()),
    );

    text.strip_prefix(":\u{a0}")
        .or_else(|| text.strip_prefix(": "))
        .unwrap_or(&text)
        .to_owned()
}

// HACK: Workaround for malformed responses due to database errors
fn database_error(text: &str) -> Result<()> {
    ensure!(
        !text.contains("Es gab ein Problem beim Datenzugriff"),
        "Database error"
    );

    Ok(())
}

selectors! {
    species_tree_link: "a[href^='/php/systematik1.php?taxon-id=']",
    species_link: "a[href^='/php/artenhome.php?name-use-id=']",
    species_id: r"(?:name\-use|taxon)\-id=(\d+)" as Regex,
    species_title: "h1#inhalt",
    species_description: "div#morphology > p > a",
    species_scientific_name: "span.taxname-main",
    species_taxonomy_tree: "div.taxonomy-tree > ul",
    species_details: "div#morphology table tr",
    species_bluehmonate: "td:has(a[href$='#data_bluehmonate'])",
    species_lebensform: "td:has(a[href$='#data_lebensform'])",
    species_fortpflanzung: "td:has(a[href^='/php/biologie.php'])",
    species_status: "table tr:has(td.label-column)",
    species_flor_status: "td:has(a[href$='#data_florstatus'])",
    species_rl_status: "td:has(a[href$='#data_rl2018'])",
    species_prot_status: "td:has(a[href$='#data_schutz'])",
    species_photos: "a[href^='/php/foto.php?taxon-id=']",
    taxon_link: "a[href^='PflGesHomepageLayout.php?taxon_id=']",
    taxon_id: r"taxon_id=(\d+)" as Regex,
    taxon_name: "p > a[href='/glossar.html#bges']",
    taxon_synonyms: "p > a[href='/glossar.html#syntax']",
    taxon_scientific_synonyms: "div.col2-grid-container",
    taxon_comment: "p > a[href='/glossar.html#anmerkung']",
}
