use std::fmt::Write;

use anyhow::Result;
use cap_std::fs::Dir;
use miniproj::{Projection, get_projection};
use serde::Deserialize;
use serde_roxmltree::{from_doc, roxmltree::Document};
use smallvec::smallvec;
use time::{Date, macros::format_description};

use harvester::{
    Source,
    client::Client,
    fetch_many,
    utilities::{contains_or, point_like_bounding_box},
    write_dataset,
};

use metadaten::{
    dataset::{
        Dataset, Language, License, Organisation, OrganisationRole, Region,
        r#type::{Domain, Station, Type},
    },
    proj_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let proj = get_projection(25832).unwrap();

    let text = {
        let mut url = source.url.clone();
        url.query_pairs_mut()
            .append_pair("service", "WFS")
            .append_pair("request", "GetFeature")
            .append_pair("typeNames", "tlubn:GW_MST");

        client.fetch_text(source, "wfs".to_owned(), &url).await?
    };

    let document = Document::parse(&text)?;

    let response = from_doc::<FeatureCollection>(&document)?;

    let (results, errors) = fetch_many(0, 0, response.members, |member| {
        translate_record(dir, client, source, proj, member.record)
    })
    .await;

    Ok((results, results, errors))
}

#[derive(Debug, Deserialize)]
struct FeatureCollection<'a> {
    #[serde(rename = "member", borrow)]
    members: Vec<Member<'a>>,
}

#[derive(Debug, Deserialize)]
struct Member<'a> {
    #[serde(rename = "GW_MST", borrow)]
    record: Record<'a>,
}

#[derive(Debug, Deserialize)]
struct Record<'a> {
    id: String,
    #[serde(rename = "URL")]
    url: String,
    #[serde(rename = "MST_NAME")]
    name: &'a str,
    #[serde(rename = "CHARAKTERISTIK")]
    construction: Option<&'a str>,
    #[serde(rename = "GEMEINDE")]
    community: &'a str,
    #[serde(rename = "GEMARKUNG")]
    parish: &'a str,
    #[serde(rename = "UTM_E")]
    ostwert: f64,
    #[serde(rename = "UTM_N")]
    nordwert: f64,
    #[serde(rename = "ENDSTRAT")]
    horizon: Option<&'a str>,
    #[serde(rename = "FLAG_MENGE")]
    gw_amount: &'a str,
    #[serde(rename = "FLAG_GUETE")]
    gw_quality: &'a str,
    #[serde(rename = "GWK")]
    gw_body: Option<&'a str>,
    #[serde(rename = "HAUPTGWL")]
    aquifer: Option<&'a str>,
    #[serde(rename = "LASTDATE_MENGE")]
    last_record: Option<&'a str>,
}

async fn translate_record(
    dir: &Dir,
    client: &Client,
    source: &Source,
    proj: &dyn Projection,
    record: Record<'_>,
) -> Result<(usize, usize, usize)> {
    let mut title = format!("Grundwassermessstelle {} in {}", record.id, record.name);

    if !record.name.contains(record.parish) {
        write!(&mut title, " ({})", record.parish)?;
    }

    let mut description = format!(
        "Dieser Datensatz enthält die Messdaten der Messstelle {}.\n",
        record.name
    );

    let mut add_line = |label, value: Option<&str>| -> Result<_> {
        if let Some(value) = value {
            writeln!(&mut description, " {label}: {value}.")?;
        }
        Ok(())
    };

    add_line("Horizont", record.horizon)?;
    add_line("Leiter", record.aquifer)?;
    add_line("Grundwasserkörper", record.gw_body)?;
    add_line("Messstellen-Art", record.construction)?;

    let region = contains_or(record.community, Region::TH);

    let bounding_box = proj_rect(
        point_like_bounding_box(record.nordwert, record.ostwert),
        proj,
    );

    let modified = record
        .last_record
        .map(|last_record| Date::parse(last_record, format_description!("[year]-[month]-[day]Z")))
        .transpose()?;

    let organisations = smallvec![Organisation::Other {
        name: "Thüringer Landesamt für Umwelt, Bergbau und Naturschutz (TLUBN)".to_owned(),
        role: OrganisationRole::Provider,
        websites: smallvec!["https://tlubn.thueringen.de/service/kontakt".to_owned()],
    }];

    let mut types = smallvec![Type::Measurements {
        domain: Domain::Groundwater,
        station: Some(Station {
            id: Some(record.name.into()),
            ..Default::default()
        }),
        measured_variables: if record.gw_amount.contains("ja") {
            smallvec!["Grundwasserstand".to_owned()]
        } else {
            Default::default()
        },
        methods: Default::default(),
    }];

    if record.gw_quality.contains("ja") {
        types.push(Type::Measurements {
            domain: Domain::Chemistry,
            station: Some(Station {
                id: Some(record.name.into()),
                ..Default::default()
            }),
            measured_variables: Default::default(),
            methods: Default::default(),
        });
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        organisations,
        regions: smallvec![region],
        bounding_boxes: smallvec![bounding_box],
        modified,
        language: Language::German,
        license: License::DlDeBy20, // stated in the field "Abstract" https://antares.thueringen.de/geoserver/tlubn/wfs?request=GetCapabilities
        origins: source.origins.clone(),
        source_url: record.url,
        machine_readable_source: true,
        ..Default::default()
    };

    write_dataset(dir, client, source, record.id, dataset).await
}
