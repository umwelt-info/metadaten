pub mod auto_classify;
mod cache;
pub mod similar_terms;

use std::borrow::Cow;
use std::env::var;
use std::fs::read;
use std::iter::once;
use std::ops::Deref;
use std::sync::Arc;
use std::time::SystemTime;

use anyhow::{Result, anyhow, bail};
use arc_swap::access::{Access, Map as MapAccess};
use bincode::deserialize;
use concread::hashmap::HashMap as CowHashMap;
use hashbrown::HashMap;
use once_cell::sync::Lazy;
use regex::{Regex, escape};
use serde::{Deserialize, Serialize};
use smallvec::SmallVec;
use tantivy::schema::Facet;
use tantivy_fst::Map;
use url::Url;

use crate::{
    data_path_from_env,
    vocabulary::{Vocabulary, VocabularyInner},
};

static BASE_URL: Lazy<Url> = Lazy::new(|| {
    var("UMTHES_BASE_URL")
        .ok()
        .and_then(|url| Url::parse(&url).ok())
        .unwrap_or_else(|| Url::parse("https://sns.uba.de/umthes/").unwrap())
});

pub static TAGS: Lazy<Vocabulary<TagsInner>> = Lazy::new(Vocabulary::open);

pub struct TagsInner {
    values: CowHashMap<u64, Arc<TagDefinition>>,
    lowercase_labels: Map<Vec<u8>>,
}

impl Default for TagsInner {
    fn default() -> Self {
        let values = CowHashMap::new();
        let lowercase_labels = recompute_lowercase_labels(&values);

        Self {
            values,
            lowercase_labels,
        }
    }
}

impl VocabularyInner for TagsInner {
    const NAME: &str = "UMTHES tags";

    type Key = u64;
    type Value = TagDefinition;

    fn placeholder(id: u64) -> TagDefinition {
        TagDefinition {
            label: format!("UMTHES/{id:08x}").into(),
            alt_labels: Default::default(),
            facets: vec![Facet::from(&format!("/UMTHES/{id:08x}"))],
            depth: 0,
            modified: SystemTime::UNIX_EPOCH,
            parents: SmallVec::new(),
        }
    }

    fn open() -> Result<Self> {
        let buf = read(data_path_from_env().join("auto-classify/tags"))?;

        let values = TagDefinition::parse(&buf)?;
        let lowercase_labels = recompute_lowercase_labels(&values);

        Ok(Self {
            values,
            lowercase_labels,
        })
    }

    fn resolve(&self, id: u64) -> Option<Arc<TagDefinition>> {
        self.values.read().get(&id).cloned()
    }
}

impl Vocabulary<TagsInner> {
    pub fn lowercase_labels(&self) -> impl Deref<Target = Map<Vec<u8>>> + use<'_> {
        MapAccess::new(self.inner(), |inner: &TagsInner| &inner.lowercase_labels).load()
    }
}

fn recompute_lowercase_labels(values: &CowHashMap<u64, Arc<TagDefinition>>) -> Map<Vec<u8>> {
    let mut labels = values
        .read()
        .iter()
        .flat_map(|(id, tag_def)| {
            tag_def
                .labels()
                .map(|label| (label.to_lowercase().into_bytes(), *id))
        })
        .collect::<Vec<_>>();

    labels.sort_unstable_by(|(lhs, _), (rhs, _)| lhs.cmp(rhs));
    labels.dedup_by(|(lhs, _), (rhs, _)| lhs == rhs);

    Map::from_iter(labels).unwrap()
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TagDefinition {
    pub label: Box<str>,
    pub alt_labels: Box<[Box<str>]>,
    pub facets: Vec<Facet>,
    pub depth: u8,
    modified: SystemTime,
    parents: SmallVec<[u64; 2]>,
}

#[derive(Debug, Serialize, Deserialize)]
struct OldTagDefinition {
    label: Box<str>,
    alt_labels: Box<[Box<str>]>,
    facets: Vec<Facet>,
    modified: SystemTime,
    parents: SmallVec<[u64; 2]>,
}

const CURR_VER: u8 = 1;
const OLD_VER: u8 = 0;

impl TagDefinition {
    pub fn labels(&self) -> impl Iterator<Item = &'_ str> + '_ {
        once(&*self.label).chain(self.alt_labels.iter().map(|alt_label| &**alt_label))
    }

    fn parse<S, T>(buf: &[u8]) -> Result<S>
    where
        S: FromIterator<(u64, T)>,
        T: From<Self>,
    {
        let vals = match buf.split_last() {
            Some((&CURR_VER, buf)) => deserialize::<HashMap<u64, Self>>(buf)?
                .into_iter()
                .map(|(id, val)| (id, val.into()))
                .collect(),
            Some((&OLD_VER, buf)) => deserialize::<HashMap<u64, OldTagDefinition>>(buf)?
                .into_iter()
                .map(|(id, old_val)| {
                    let depth = old_val
                        .facets
                        .iter()
                        .map(|facet| {
                            facet
                                .encoded_str()
                                .as_bytes()
                                .iter()
                                .filter(|byte| **byte == 0)
                                .count() as u8
                        })
                        .min()
                        .unwrap_or(0);

                    let val = Self {
                        label: old_val.label,
                        alt_labels: old_val.alt_labels,
                        facets: old_val.facets,
                        depth,
                        modified: old_val.modified,
                        parents: old_val.parents,
                    };

                    (id, val.into())
                })
                .collect(),
            _ => bail!("Missing or invalid tag definitions format version"),
        };

        Ok(vals)
    }
}

fn strip_disambiguation<T>(term: &str) -> T
where
    T: for<'a> From<Cow<'a, str>>,
{
    static DISAMBIGUATION: Lazy<Regex> = Lazy::new(|| Regex::new(r"\s+\[.+\]").unwrap()); // removes e.g. "[benutze Unterbegriffe]"

    DISAMBIGUATION.replace_all(term, "").into()
}

pub fn extract_id(link: &str) -> Result<u64> {
    static LINK: Lazy<Regex> = Lazy::new(|| {
        let base_url = escape(BASE_URL.as_str());

        Regex::new(&format!(
            r"(?:https?://(?:sns\.uba|(?:www\.)?semantic-network)\.de/(?:umthes/)?|{base_url})_([[:xdigit:]]+)"
        ))
        .unwrap()
    });

    let captures = LINK
        .captures(link)
        .ok_or_else(|| anyhow!("Failed to match link: {}", link))?;

    let id = u64::from_str_radix(&captures[1], 16)?;

    Ok(id)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn keep_results_but_remove_disambiguations() {
        assert_eq!(strip_disambiguation::<String>("Aller [Fluss]"), "Aller");

        assert_eq!(
            strip_disambiguation::<String>("Binnenschifffahrt"),
            "Binnenschifffahrt"
        );
    }

    #[test]
    fn extract_id_works() {
        assert_eq!(
            extract_id("https://sns.uba.de/umthes/_00013251").unwrap(),
            78417
        );

        assert_eq!(
            extract_id("https://sns.uba.de/umthes/_0e52de84").unwrap(),
            240311940
        );

        assert_eq!(
            extract_id("https://www.semantic-network.de/umthes/_00052284").unwrap(),
            336516
        );

        assert_eq!(
            extract_id("https://sns.uba.de/_00018829@Ozon").unwrap(),
            100393
        );
    }
}
