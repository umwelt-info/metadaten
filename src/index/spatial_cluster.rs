use geo::{Contains, Coord, MapCoords, Rect};
use itertools::zip_eq;
use serde::Serialize;
use tantivy::{
    DocId, Result as TantivyResult, Score, SegmentOrdinal, SegmentReader,
    collector::{Collector, SegmentCollector},
    fastfield::Column,
};
use utoipa::ToSchema;

use crate::{
    dataset::{BoundingBox, Corner},
    merge_rect,
};

#[derive(Debug, Clone, Serialize, ToSchema)]
pub struct SpatialCluster {
    #[schema(value_type = Corner)]
    pub position: Coord<f32>,
    #[schema(value_type = [BoundingBox])]
    pub bounding_box: Rect<f32>,
    pub count: usize,
}

impl SpatialCluster {
    fn distance(&self, other: &Self) -> f32 {
        let difference = other.position - self.position;

        difference.x.abs() + difference.y.abs()
    }

    fn merge(&mut self, other: Self) {
        let weight = other.count as f32 / (other.count + self.count) as f32;

        let difference = other.position - self.position;

        self.position.x += weight * difference.x;
        self.position.y += weight * difference.y;

        merge_rect(&mut self.bounding_box, other.bounding_box);

        self.count += other.count;
    }
}

#[derive(Default)]
pub struct SpatialClusters(Vec<SpatialCluster>);

impl SpatialClusters {
    fn add(&mut self, other: SpatialCluster, min_distance: f32) {
        if self.0.is_empty() {
            self.0.push(other);
            return;
        }

        let mut smallest_distance = self.0[0].distance(&other);
        let mut closest_idx = 0;

        for idx in 1..self.0.len() {
            let distance = self.0[idx].distance(&other);

            if smallest_distance > distance {
                smallest_distance = distance;
                closest_idx = idx;
            }
        }

        if smallest_distance > min_distance {
            self.0.push(other);
        } else {
            self.0[closest_idx].merge(other);
        }
    }

    fn merge(&mut self, other: Self, min_distance: f32) {
        for cluster in other.0 {
            self.add(cluster, min_distance);
        }
    }
}

pub struct SpatialClustersCollector {
    min_x: &'static str,
    min_y: &'static str,
    max_x: &'static str,
    max_y: &'static str,
    bounding_box: Rect<f32>,
    min_distance: f32,
}

impl SpatialClustersCollector {
    pub fn new(
        min_x: &'static str,
        min_y: &'static str,
        max_x: &'static str,
        max_y: &'static str,
        bounding_box: Rect,
    ) -> Self {
        let bounding_box = bounding_box.map_coords(|Coord { x, y }| Coord {
            x: x as f32,
            y: y as f32,
        });

        let min_distance = 0.1 * (bounding_box.width() + bounding_box.height());

        Self {
            min_x,
            min_y,
            max_x,
            max_y,
            bounding_box,
            min_distance,
        }
    }
}

impl Collector for SpatialClustersCollector {
    type Fruit = Vec<SpatialCluster>;

    type Child = SpatialClustersSegmentCollector;

    fn requires_scoring(&self) -> bool {
        false
    }

    fn for_segment(
        &self,
        _segment_ord: SegmentOrdinal,
        reader: &SegmentReader,
    ) -> TantivyResult<Self::Child> {
        let fast_fields = reader.fast_fields();
        let min_x = fast_fields.f64(self.min_x)?;
        let min_y = fast_fields.f64(self.min_y)?;
        let max_x = fast_fields.f64(self.max_x)?;
        let max_y = fast_fields.f64(self.max_y)?;

        Ok(SpatialClustersSegmentCollector {
            min_x,
            min_y,
            max_x,
            max_y,
            bounding_box: self.bounding_box,
            min_distance: self.min_distance,
            clusters: Default::default(),
        })
    }

    fn merge_fruits(
        &self,
        child_fruits: Vec<<Self::Child as SegmentCollector>::Fruit>,
    ) -> TantivyResult<Self::Fruit> {
        let clusters = child_fruits
            .into_iter()
            .reduce(|mut clusters, segment_clusters| {
                clusters.merge(segment_clusters, self.min_distance);
                clusters
            })
            .unwrap_or_default();

        Ok(clusters.0)
    }
}

pub struct SpatialClustersSegmentCollector {
    min_x: Column<f64>,
    min_y: Column<f64>,
    max_x: Column<f64>,
    max_y: Column<f64>,
    bounding_box: Rect<f32>,
    min_distance: f32,
    clusters: SpatialClusters,
}

impl SegmentCollector for SpatialClustersSegmentCollector {
    type Fruit = SpatialClusters;

    fn collect(&mut self, doc: DocId, _score: Score) {
        let min_x = self.min_x.values_for_doc(doc);
        let min_y = self.min_y.values_for_doc(doc);
        let max_x = self.max_x.values_for_doc(doc);
        let max_y = self.max_y.values_for_doc(doc);

        for ((min_x, min_y), (max_x, max_y)) in zip_eq(zip_eq(min_x, min_y), zip_eq(max_x, max_y)) {
            let bounding_box = Rect::new(
                Coord {
                    x: min_x as f32,
                    y: min_y as f32,
                },
                Coord {
                    x: max_x as f32,
                    y: max_y as f32,
                },
            );

            if !self.bounding_box.contains(&bounding_box) {
                continue;
            }

            let position = bounding_box.center();

            let cluster = SpatialCluster {
                position,
                bounding_box,
                count: 1,
            };

            self.clusters.add(cluster, self.min_distance);
        }
    }

    fn harvest(self) -> Self::Fruit {
        self.clusters
    }
}
