use geo::Rect;
use moka::sync::Cache;
use openssl::sha::Sha256;
use tantivy::schema::Facet;

use crate::{FacetWeight, dataset::TimeRange, index::QueryRepr, value_from_env};

#[derive(PartialEq, Eq, Hash)]
pub struct Key([u8; 32]);

pub struct QueryCache<R>(Cache<Key, R>);

impl<R> QueryCache<R>
where
    R: Clone + Send + Sync + 'static,
{
    pub fn new(max_capacity_key: &str) -> Self {
        let max_capacity = value_from_env(max_capacity_key);

        Self(Cache::new(max_capacity))
    }

    pub fn get(&self, key: &Key) -> Option<R> {
        self.0.get(key)
    }

    pub fn insert(&self, key: Key, results: &R) {
        self.0.insert(key, results.clone());
    }

    pub fn entries(&self) -> u64 {
        self.0.entry_count()
    }
}

#[allow(clippy::too_many_arguments)]
pub fn search_key(
    query: &dyn QueryRepr,
    types_root: &Facet,
    topics_root: &Facet,
    origins_root: &Facet,
    licenses_root: &Facet,
    languages_root: &Facet,
    resource_types_root: &Facet,
    bounding_box: &Option<Rect>,
    bounding_box_contains: bool,
    bounding_box_spatial_clusters: bool,
    time_range: &Option<TimeRange>,
    origin_weights: &[FacetWeight],
    generation_id: u64,
) -> Key {
    let mut ctx = Sha256::new();

    query.hash(&mut ctx);

    let mut hash_facet = |facet: &Facet| {
        let encoded = facet.encoded_str().as_bytes();
        ctx.update(&encoded.len().to_ne_bytes());
        ctx.update(encoded);
    };

    hash_facet(types_root);
    hash_facet(topics_root);
    hash_facet(origins_root);
    hash_facet(licenses_root);
    hash_facet(languages_root);
    hash_facet(resource_types_root);

    match bounding_box {
        None => ctx.update(&[0]),
        Some(bounding_box) => {
            let min = bounding_box.min();
            let max = bounding_box.max();

            ctx.update(&[
                1,
                bounding_box_contains as u8,
                bounding_box_spatial_clusters as u8,
            ]);
            ctx.update(&min.x.to_ne_bytes());
            ctx.update(&min.y.to_ne_bytes());
            ctx.update(&max.x.to_ne_bytes());
            ctx.update(&max.y.to_ne_bytes());
        }
    };

    match time_range {
        None => ctx.update(&[0]),
        Some(TimeRange { from, until }) => {
            let from = (from.year() << 9) | from.ordinal() as i32;
            let until = (until.year() << 9) | until.ordinal() as i32;

            ctx.update(&[1]);
            ctx.update(&from.to_ne_bytes());
            ctx.update(&until.to_ne_bytes());
        }
    };

    ctx.update(&origin_weights.len().to_ne_bytes());

    for FacetWeight { facet, weight } in origin_weights {
        let encoded = facet.encoded_str().as_bytes();
        ctx.update(&encoded.len().to_ne_bytes());
        ctx.update(encoded);
        ctx.update(&weight.to_ne_bytes());
    }

    ctx.update(&generation_id.to_ne_bytes());

    Key(ctx.finish())
}

pub fn complete_key(query: &dyn QueryRepr, generation_id: u64) -> Key {
    let mut ctx = Sha256::new();

    query.hash(&mut ctx);

    ctx.update(&generation_id.to_ne_bytes());

    Key(ctx.finish())
}
