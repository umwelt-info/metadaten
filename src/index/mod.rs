mod bounding_box;
pub(crate) mod collector;
mod indexer;
mod scorer;
mod searcher;
mod spatial_cluster;
mod split_compound_nouns;
mod time_range;

use std::fs::read;
use std::path::Path;
use std::sync::{Arc, Weak};

use anyhow::Result;
use bincode::deserialize;
use serde::Deserialize;
use tantivy::{
    Index, IndexReader, Result as TantivyResult, Warmer,
    schema::{
        FAST, FacetOptions, Field, INDEXED, IndexRecordOption, STORED, STRING, Schema,
        TextFieldIndexing, TextOptions,
    },
    tokenizer::{
        Language, LowerCaser, RawTokenizer, RemoveLongFilter, SimpleTokenizer, Stemmer,
        StopWordFilter, TextAnalyzer,
    },
};

pub use indexer::Indexer;
pub use searcher::{Clause, Occur, QueryRepr, SearchResults, Searcher, StructQuery, TextQuery};
pub use spatial_cluster::SpatialCluster;

use crate::index::{
    bounding_box::BoundingBoxes, split_compound_nouns::SplitCompoundNouns, time_range::TimeRanges,
};

fn schema() -> Schema {
    let text = TextOptions::default().set_indexing_options(
        TextFieldIndexing::default()
            .set_index_option(IndexRecordOption::WithFreqsAndPositions)
            .set_tokenizer("de_stem"),
    );

    let lowercase = TextOptions::default()
        .set_indexing_options(TextFieldIndexing::default().set_tokenizer("lowercase"));

    let proper_name = TextOptions::default().set_indexing_options(
        TextFieldIndexing::default()
            .set_index_option(IndexRecordOption::WithFreqsAndPositions)
            .set_tokenizer("proper_name"),
    );

    let mut schema = Schema::builder();

    schema.add_text_field("source", STRING | STORED);
    schema.add_text_field("id", STRING | STORED);
    schema.add_bytes_field("content", STORED);

    schema.add_text_field("title", text.clone());
    schema.add_text_field("description", text.clone());
    schema.add_facet_field("type", FacetOptions::default());
    schema.add_text_field("measurements", text.clone());
    schema.add_text_field("names", proper_name.clone());

    schema.add_text_field("organisation", proper_name.clone());
    schema.add_text_field("person", proper_name);

    schema.add_facet_field("topic", FacetOptions::default());
    schema.add_facet_field("origin", FacetOptions::default());
    schema.add_facet_field("license", FacetOptions::default());
    schema.add_facet_field("language", FacetOptions::default());
    schema.add_facet_field("resource_type", FacetOptions::default());

    schema.add_text_field("origin_text", text);
    schema.add_text_field("type_text", lowercase.clone());

    schema.add_text_field("tags", lowercase.clone());
    schema.add_text_field("region", lowercase);

    schema.add_bool_field("mandatory_registration", INDEXED);

    schema.add_u64_field("umthes_id", INDEXED);

    schema.add_u64_field("wikidata_id", INDEXED);

    schema.add_u64_field("geonames_id", INDEXED);
    schema.add_u64_field("regional_key", INDEXED);
    schema.add_u64_field("atkis_key", INDEXED);
    schema.add_text_field("watershed_id", STRING);

    schema.add_text_field("cas_rn", STRING);

    schema.add_f64_field("inherent_score", FAST);
    schema.add_bool_field("eligible_for_random", INDEXED);

    schema.add_f64_field("bounding_box_min_x", FAST);
    schema.add_f64_field("bounding_box_min_y", FAST);
    schema.add_f64_field("bounding_box_max_x", FAST);
    schema.add_f64_field("bounding_box_max_y", FAST);

    schema.add_i64_field("time_range_from", FAST);
    schema.add_i64_field("time_range_until", FAST);

    schema.build()
}

struct Fields {
    source: Field,
    id: Field,
    content: Field,
    title: Field,
    description: Field,
    r#type: Field,
    measurements: Field,
    names: Field,
    organisation: Field,
    person: Field,
    topic: Field,
    origin: Field,
    license: Field,
    language: Field,
    resource_type: Field,
    origin_text: Field,
    type_text: Field,
    tags: Field,
    region: Field,
    mandatory_registration: Field,
    umthes_id: Field,
    wikidata_id: Field,
    geonames_id: Field,
    regional_key: Field,
    atkis_key: Field,
    watershed_id: Field,
    cas_rn: Field,
    inherent_score: Field,
    eligible_for_random: Field,
    bounding_box_min_x: Field,
    bounding_box_min_y: Field,
    bounding_box_max_x: Field,
    bounding_box_max_y: Field,
    time_range_from: Field,
    time_range_until: Field,
}

impl Fields {
    fn new(schema: &Schema) -> Self {
        let source = schema.get_field("source").unwrap();
        let id = schema.get_field("id").unwrap();
        let content = schema.get_field("content").unwrap();

        let title = schema.get_field("title").unwrap();
        let description = schema.get_field("description").unwrap();
        let r#type = schema.get_field("type").unwrap();
        let measurements = schema.get_field("measurements").unwrap();
        let names = schema.get_field("names").unwrap();

        let organisation = schema.get_field("organisation").unwrap();
        let person = schema.get_field("person").unwrap();

        let topic = schema.get_field("topic").unwrap();
        let origin = schema.get_field("origin").unwrap();
        let license = schema.get_field("license").unwrap();
        let language = schema.get_field("language").unwrap();
        let resource_type = schema.get_field("resource_type").unwrap();

        let origin_text = schema.get_field("origin_text").unwrap();
        let type_text = schema.get_field("type_text").unwrap();

        let tags = schema.get_field("tags").unwrap();
        let region = schema.get_field("region").unwrap();

        let mandatory_registration = schema.get_field("mandatory_registration").unwrap();

        let umthes_id = schema.get_field("umthes_id").unwrap();

        let wikidata_id = schema.get_field("wikidata_id").unwrap();

        let geonames_id = schema.get_field("geonames_id").unwrap();
        let regional_key = schema.get_field("regional_key").unwrap();
        let atkis_key = schema.get_field("atkis_key").unwrap();
        let watershed_id = schema.get_field("watershed_id").unwrap();

        let cas_rn = schema.get_field("cas_rn").unwrap();

        let inherent_score = schema.get_field("inherent_score").unwrap();
        let eligible_for_random = schema.get_field("eligible_for_random").unwrap();

        let bounding_box_min_x = schema.get_field("bounding_box_min_x").unwrap();
        let bounding_box_min_y = schema.get_field("bounding_box_min_y").unwrap();
        let bounding_box_max_x = schema.get_field("bounding_box_max_x").unwrap();
        let bounding_box_max_y = schema.get_field("bounding_box_max_y").unwrap();

        let time_range_from = schema.get_field("time_range_from").unwrap();
        let time_range_until = schema.get_field("time_range_until").unwrap();

        Self {
            source,
            id,
            content,
            title,
            description,
            r#type,
            measurements,
            names,
            organisation,
            person,
            topic,
            origin,
            license,
            language,
            resource_type,
            origin_text,
            type_text,
            tags,
            region,
            mandatory_registration,
            umthes_id,
            wikidata_id,
            geonames_id,
            regional_key,
            atkis_key,
            watershed_id,
            cas_rn,
            inherent_score,
            eligible_for_random,
            bounding_box_min_x,
            bounding_box_min_y,
            bounding_box_max_x,
            bounding_box_max_y,
            time_range_from,
            time_range_until,
        }
    }
}

fn index_reader(
    index: &Index,
    bounding_boxes: &Arc<BoundingBoxes>,
    time_ranges: &Arc<TimeRanges>,
) -> TantivyResult<IndexReader> {
    index
        .reader_builder()
        .warmers(vec![
            Arc::downgrade(bounding_boxes) as Weak<dyn Warmer>,
            Arc::downgrade(time_ranges) as Weak<dyn Warmer>,
        ])
        .try_into()
}

fn register_tokenizers(data_path: &Path, index: &Index) -> Result<Option<SplitCompoundNouns>> {
    let tokenizers = index.tokenizers();

    tokenizers.register(
        "lowercase",
        TextAnalyzer::builder(RawTokenizer::default())
            .filter(LowerCaser)
            .build(),
    );

    tokenizers.register(
        "proper_name",
        TextAnalyzer::builder(SimpleTokenizer::default())
            .filter(RemoveLongFilter::limit(40))
            .filter(LowerCaser)
            .build(),
    );

    let de_stem_base = TextAnalyzer::builder(SimpleTokenizer::default())
        .filter(RemoveLongFilter::limit(40))
        .filter(LowerCaser)
        .filter(StopWordFilter::new(Language::German).unwrap());

    match init_compound_noun_token_filter(data_path) {
        Ok(split_compound_nouns) => {
            tokenizers.register(
                "de_stem",
                de_stem_base
                    .filter(split_compound_nouns.clone())
                    .filter(Stemmer::new(Language::German))
                    .build(),
            );

            Ok(Some(split_compound_nouns))
        }
        Err(err) => {
            tracing::error!(
                "Failed to load nouns used for splitting compounds: {:#}",
                err
            );

            tokenizers.register(
                "de_stem",
                de_stem_base.filter(Stemmer::new(Language::German)).build(),
            );

            Ok(None)
        }
    }
}

fn init_compound_noun_token_filter(data_path: &Path) -> Result<SplitCompoundNouns> {
    let buf = read(data_path.join("datasets/kaikki.not_indexed/nouns.bin"))?;

    #[derive(Deserialize)]
    struct Nouns<'a> {
        #[serde(borrow)]
        base: Vec<&'a str>,
        #[serde(borrow)]
        forms: Vec<&'a str>,
    }

    let nouns = deserialize::<Nouns>(&buf)?;

    let filter = SplitCompoundNouns::new(&nouns.base, &nouns.forms)?;

    Ok(filter)
}
