use aho_corasick::{AhoCorasick, AhoCorasickBuilder, Anchored, Input, MatchKind, StartKind};
use anyhow::Result;
use tantivy::{
    schema::{Field, FieldType, Schema},
    tokenizer::{Token, TokenFilter, TokenStream, Tokenizer},
};
use tantivy_query_grammar::{Occur, UserInputAst, UserInputLeaf, UserInputLiteral};

#[derive(Clone)]
pub struct SplitCompoundNouns {
    base: AhoCorasick,
    forms: AhoCorasick,
}

impl SplitCompoundNouns {
    pub fn new(base: &[&str], forms: &[&str]) -> Result<Self> {
        let base = AhoCorasickBuilder::new()
            .match_kind(MatchKind::LeftmostLongest)
            .build(base)?;

        let forms = AhoCorasickBuilder::new()
            .match_kind(MatchKind::LeftmostLongest)
            .start_kind(StartKind::Anchored)
            .build(forms)?;

        Ok(Self { base, forms })
    }
}

impl TokenFilter for SplitCompoundNouns {
    type Tokenizer<T: Tokenizer> = SplitCompoundNounsFilter<T>;

    fn transform<T: Tokenizer>(self, tokenizer: T) -> SplitCompoundNounsFilter<T> {
        SplitCompoundNounsFilter {
            base: self.base,
            forms: self.forms,
            inner: tokenizer,
            cuts: Vec::new(),
            parts: Vec::new(),
        }
    }
}

#[derive(Clone)]
pub struct SplitCompoundNounsFilter<T> {
    base: AhoCorasick,
    forms: AhoCorasick,
    inner: T,
    cuts: Vec<usize>,
    parts: Vec<Token>,
}

impl<T: Tokenizer> Tokenizer for SplitCompoundNounsFilter<T> {
    type TokenStream<'a> = SplitCompoundNounsTokenStream<'a, T::TokenStream<'a>>;

    fn token_stream<'a>(&'a mut self, text: &'a str) -> Self::TokenStream<'a> {
        self.cuts.clear();
        self.parts.clear();
        SplitCompoundNounsTokenStream {
            base: self.base.clone(),
            forms: self.forms.clone(),
            tail: self.inner.token_stream(text),
            cuts: &mut self.cuts,
            parts: &mut self.parts,
        }
    }
}

pub struct SplitCompoundNounsTokenStream<'a, T> {
    base: AhoCorasick,
    forms: AhoCorasick,
    tail: T,
    cuts: &'a mut Vec<usize>,
    parts: &'a mut Vec<Token>,
}

impl<T: TokenStream> SplitCompoundNounsTokenStream<'_, T> {
    // Will use `self.cuts` to fill `self.parts` if `self.tail.token()`
    // can fully be split into consecutive matches against `self.base` and `self.forms`.
    fn split(&mut self) {
        let token = self.tail.token();

        split(
            &self.base,
            &self.forms,
            self.cuts,
            token.text.as_str(),
            |text| self.parts.push(Token { text, ..*token }),
        );
    }
}

impl<T: TokenStream> TokenStream for SplitCompoundNounsTokenStream<'_, T> {
    fn advance(&mut self) -> bool {
        self.parts.pop();

        if !self.parts.is_empty() {
            return true;
        }

        if !self.tail.advance() {
            return false;
        }

        // Will yield either `self.parts.last()` or
        // `self.tail.token()` if it could not be split.
        self.split();
        true
    }

    fn token(&self) -> &Token {
        self.parts.last().unwrap_or_else(|| self.tail.token())
    }

    fn token_mut(&mut self) -> &mut Token {
        self.parts
            .last_mut()
            .unwrap_or_else(|| self.tail.token_mut())
    }
}

impl SplitCompoundNouns {
    pub fn rewrite(&self, schema: &Schema, default_fields: &[Field], ast: &mut UserInputAst) {
        match ast {
            UserInputAst::Clause(clauses) => {
                for (_occur, clause) in clauses {
                    self.rewrite(schema, default_fields, clause);
                }
            }
            UserInputAst::Leaf(leaf) => {
                if let UserInputLeaf::Literal(lit) = &**leaf {
                    if let Some(field_name) = &lit.field_name {
                        if let Ok(field) = schema.get_field(field_name) {
                            let field_entry = schema.get_field_entry(field);

                            if let FieldType::Str(options) = field_entry.field_type() {
                                if let Some(options) = options.get_indexing_options() {
                                    if options.tokenizer() != "de_stem" {
                                        return;
                                    }
                                }
                            }
                        }
                    }

                    let mut parts = Vec::new();

                    split(
                        &self.base,
                        &self.forms,
                        &mut Vec::new(),
                        &lit.phrase,
                        |part| parts.push(part),
                    );

                    if !parts.is_empty() {
                        if lit.field_name.is_some() {
                            // We checked above that `field` uses the `de_stem` tokenizer,
                            // so we build a conjuction of parts.
                            *ast = UserInputAst::Clause(
                                parts
                                    .into_iter()
                                    .map(|phrase| {
                                        let leaf = UserInputLeaf::Literal(UserInputLiteral {
                                            phrase,
                                            ..lit.clone()
                                        });

                                        (Some(Occur::Must), UserInputAst::Leaf(Box::new(leaf)))
                                    })
                                    .collect(),
                            );
                        } else {
                            // We have to search the phrase in the default fields,
                            // some use split compound nouns and some do not,
                            // hence we build a disjunction over them,
                            // some as-is, others as a conjuction of parts.
                            *ast = UserInputAst::Clause(
                                default_fields
                                    .iter()
                                    .copied()
                                    .map(|field| {
                                        let field_entry = schema.get_field_entry(field);

                                        if let FieldType::Str(options) = field_entry.field_type() {
                                            if let Some(options) = options.get_indexing_options() {
                                                if options.tokenizer() != "de_stem" {
                                                    // `field` uses a different tokenizer,
                                                    // so we forward the phrase as-is.
                                                    let leaf =
                                                        UserInputLeaf::Literal(UserInputLiteral {
                                                            field_name: Some(
                                                                field_entry.name().to_owned(),
                                                            ),
                                                            ..lit.clone()
                                                        });

                                                    return (
                                                        Some(Occur::Should),
                                                        UserInputAst::Leaf(Box::new(leaf)),
                                                    );
                                                }
                                            }
                                        }

                                        // `field` uses the `de_stem` tokenizer,
                                        // so we build a conjunction of parts.
                                        let clauses = parts
                                            .iter()
                                            .map(|phrase| {
                                                let leaf =
                                                    UserInputLeaf::Literal(UserInputLiteral {
                                                        field_name: Some(
                                                            field_entry.name().to_owned(),
                                                        ),
                                                        phrase: phrase.clone(),
                                                        ..lit.clone()
                                                    });

                                                (
                                                    Some(Occur::Must),
                                                    UserInputAst::Leaf(Box::new(leaf)),
                                                )
                                            })
                                            .collect();

                                        (Some(Occur::Should), UserInputAst::Clause(clauses))
                                    })
                                    .collect(),
                            );
                        }
                    }
                }
            }
            UserInputAst::Boost(ast, _boost) => self.rewrite(schema, default_fields, ast),
        }
    }
}

fn split<F>(
    base: &AhoCorasick,
    forms: &AhoCorasick,
    cuts: &mut Vec<usize>,
    mut text: &str,
    mut f: F,
) where
    F: FnMut(String),
{
    cuts.clear();
    let mut pos = 0;

    for match_ in base.find_iter(text) {
        if pos != match_.start() {
            break;
        }

        cuts.push(pos);
        pos = match_.end();

        let rest = Input::new(&text[pos..]).anchored(Anchored::Yes);

        if forms.is_match(rest) {
            cuts.push(pos);
            pos = text.len();
        }
    }

    if pos == text.len() {
        // Fill `self.parts` in reverse order,
        // so that `self.parts.pop()` yields
        // the tokens in their original order.
        for pos in cuts.iter().rev() {
            let (head, tail) = text.split_at(*pos);

            text = head;
            f(tail.to_owned());
        }
    }
}
