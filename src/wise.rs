use std::fs::read;
use std::ops::ControlFlow;
use std::path::Path;
use std::sync::Arc;

use anyhow::{Context, Result, bail};
use arc_swap::access::Access;
use bincode::{deserialize, serialize};
use cap_std::fs::Dir;
use compact_str::CompactString;
use geo::{
    Coord, LineString, Point, Polygon,
    algorithm::{BoundingRect, Distance as _, Euclidean},
};
use hashbrown::HashMap;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use sif_rtree::{DEF_NODE_LEN, Distance, Object, RTree};
use tantivy_fst::Map;

use crate::{
    data_path_from_env,
    vocabulary::{Vocabulary, VocabularyInner},
};

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct Database {
    pub entries: HashMap<u64, Arc<Entry>>,
}

const CURR_VER: u8 = 1;

impl Database {
    pub fn write(&self, dir: &Dir) -> Result<()> {
        let mut buf = serialize(self)?;
        buf.push(CURR_VER);

        dir.write("wise.bin", &buf)?;

        Ok(())
    }

    fn read(path: &Path) -> Result<Self> {
        let buf = read(path)?;

        let res = match buf.split_last() {
            Some((&CURR_VER, buf)) => deserialize::<Self>(buf),
            _ => bail!("Missing or invalid WISE/WFD format version"),
        };

        res.context("Invalid WISE/WFD format")
    }
}

#[derive(Serialize, Deserialize)]
pub struct Entry {
    pub name: String,
    pub id: CompactString,
    pub shape: Polygon,
}

pub static WISE: Lazy<Vocabulary<WiseInner>> = Lazy::new(Vocabulary::open);

impl Vocabulary<WiseInner> {
    pub fn match_name(&self, name: &str) -> Option<u64> {
        self.inner().load().match_name(name)
    }

    pub fn match_shape(&self, x: f64, y: f64) -> Option<u64> {
        self.inner().load().match_shape(x, y)
    }
}

pub struct WiseInner {
    database: Database,
    name_index: Map<Vec<u8>>,
    shape_index: Option<RTree<ShapeIndexItem>>,
}

impl Default for WiseInner {
    fn default() -> Self {
        let database = Database::default();
        let name_index = recompute_name_index(&database.entries);
        let shape_index = recompute_shape_index(&database.entries);

        Self {
            database,
            name_index,
            shape_index,
        }
    }
}

impl WiseInner {
    fn match_name(&self, name: &str) -> Option<u64> {
        self.name_index.get(name.to_uppercase())
    }

    fn match_shape(&self, x: f64, y: f64) -> Option<u64> {
        let mut id = None;

        if let Some(shape_index) = &self.shape_index {
            shape_index.look_up_at_point(&[x, y], |item| {
                id = Some(item.id);
                ControlFlow::Break(())
            });
        }

        id
    }
}

impl VocabularyInner for WiseInner {
    const NAME: &str = "WISE/WFD";

    type Key = u64;
    type Value = Entry;

    fn placeholder(id: u64) -> Entry {
        Entry {
            name: format!("WISE/{id}"),
            id: "DE????".into(),
            shape: Polygon::new(LineString::new(Vec::new()), Vec::new()),
        }
    }

    fn open() -> Result<Self> {
        let path = data_path_from_env().join("datasets/wise.not_indexed/wise.bin");

        let database = Database::read(&path)?;
        let name_index = recompute_name_index(&database.entries);
        let shape_index = recompute_shape_index(&database.entries);

        Ok(Self {
            database,
            name_index,
            shape_index,
        })
    }

    fn resolve(&self, id: u64) -> Option<Arc<Entry>> {
        self.database.entries.get(&id).cloned()
    }
}

fn recompute_name_index(entries: &HashMap<u64, Arc<Entry>>) -> Map<Vec<u8>> {
    let mut names = entries
        .iter()
        .map(|(id, entry)| (entry.name.clone().into_bytes(), *id))
        .collect::<Vec<_>>();

    names.sort_unstable_by(|(lhs, _), (rhs, _)| lhs.cmp(rhs));
    names.dedup_by(|(lhs, _), (rhs, _)| lhs == rhs);

    Map::from_iter(names).unwrap()
}

fn recompute_shape_index(entries: &HashMap<u64, Arc<Entry>>) -> Option<RTree<ShapeIndexItem>> {
    let items = entries
        .iter()
        .map(|(id, entry)| {
            let rect = entry.shape.bounding_rect().unwrap();
            let aabb = ([rect.min().x, rect.min().y], [rect.max().x, rect.max().y]);

            ShapeIndexItem {
                entry: entry.clone(),
                aabb,
                id: *id,
            }
        })
        .collect::<Vec<_>>();

    if !items.is_empty() {
        Some(RTree::new(DEF_NODE_LEN, items))
    } else {
        None
    }
}

struct ShapeIndexItem {
    entry: Arc<Entry>,
    aabb: ([f64; 2], [f64; 2]),
    id: u64,
}

impl Object for ShapeIndexItem {
    type Point = [f64; 2];

    fn aabb(&self) -> (Self::Point, Self::Point) {
        self.aabb
    }
}

impl Distance<[f64; 2]> for ShapeIndexItem {
    fn distance_2(&self, &[x, y]: &[f64; 2]) -> f64 {
        Euclidean::distance(&self.entry.shape, &Point(Coord { x, y }))
    }
}
