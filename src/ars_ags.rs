use std::fs::read;
use std::path::Path;
use std::sync::Arc;

use anyhow::{Context, Result};
use arc_swap::access::Access;
use bincode::{deserialize, serialize};
use cap_std::fs::Dir;
use geo::{MultiPolygon, Rect};
use hashbrown::hash_map::HashMap;
use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};

use crate::{
    data_path_from_env,
    vocabulary::{Vocabulary, VocabularyInner},
};

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct Database {
    pub ars: HashMap<u64, Arc<Entry>>,
    pub ags: HashMap<u64, u64>,
}

impl Database {
    pub fn write(&self, dir: &Dir) -> Result<()> {
        let buf = serialize(self)?;

        dir.write("ars_ags.bin", &buf)?;

        Ok(())
    }

    fn read(path: &Path) -> Result<Self> {
        let buf = read(path)?;

        deserialize(&buf).context("Invalid ARS/AGS format")
    }
}

#[derive(Serialize, Deserialize)]
pub struct Entry {
    pub name: Box<str>,
    pub bounding_box: Option<Rect>,
    pub shape: Option<MultiPolygon>,
}

pub static ARS_AGS: Lazy<Vocabulary<ArsAgsInner>> = Lazy::new(Vocabulary::open);

impl Vocabulary<ArsAgsInner> {
    pub fn extract(&self, val: &str) -> Option<u64> {
        self.inner().load().extract(val)
    }
}

#[derive(Default)]
pub struct ArsAgsInner {
    database: Database,
}

impl ArsAgsInner {
    fn extract(&self, val: &str) -> Option<u64> {
        /// Amtlicher Regionalschlüssel
        static ARS: Lazy<Regex> =
            Lazy::new(|| Regex::new(r"(\d{1,2}\s*\d\s*\d{2}\s*\d{4}\s*\d{3})").unwrap());

        /// Amtlicher Gemeindeschlüssel
        static AGS: Lazy<Regex> =
            Lazy::new(|| Regex::new(r"(\d{1,2}\s*\d\s*\d{2}\s*\d{3})").unwrap());

        let ars = ARS
            .captures(val)
            .map(|caps| {
                let val = caps[1]
                    .chars()
                    .filter(|char_| !char_.is_whitespace())
                    .collect::<String>();

                val.parse::<u64>().unwrap()
            })
            .filter(|key| self.database.ars.contains_key(key));

        let ags = AGS
            .captures(val)
            .map(|caps| {
                let val = caps[1]
                    .chars()
                    .filter(|char_| !char_.is_whitespace())
                    .collect::<String>();

                val.parse::<u64>().unwrap()
            })
            .and_then(|key| self.database.ags.get(&key).copied());

        ars.or(ags)
    }
}

impl VocabularyInner for ArsAgsInner {
    const NAME: &str = "ARS/AGS";

    type Key = u64;
    type Value = Entry;

    fn placeholder(key: u64) -> Entry {
        Entry {
            name: format!("ARS/{key:012}").into(),
            bounding_box: None,
            shape: None,
        }
    }

    fn open() -> Result<Self> {
        let path = data_path_from_env().join("datasets/ars-ags.not_indexed/ars_ags.bin");

        let database = Database::read(&path)?;

        Ok(Self { database })
    }

    fn resolve(&self, key: u64) -> Option<Arc<Entry>> {
        self.database.ars.get(&key).cloned()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn extract_works() {
        let mut ars = HashMap::new();
        let mut ags = HashMap::new();

        ars.insert(
            120530000000,
            Arc::new(Entry {
                name: "Frankfurt (Oder), Stadt".into(),
                bounding_box: None,
                shape: None,
            }),
        );
        ars.insert(
            120735305201,
            Arc::new(Entry {
                name: "Gerswalde".into(),
                bounding_box: None,
                shape: None,
            }),
        );
        ars.insert(
            40110000000,
            Arc::new(Entry {
                name: "Bremen, Stadt".into(),
                bounding_box: None,
                shape: None,
            }),
        );
        ars.insert(
            40120000000,
            Arc::new(Entry {
                name: "Bremerhaven, Stadt".into(),
                bounding_box: None,
                shape: None,
            }),
        );
        ars.insert(
            81365001088,
            Arc::new(Entry {
                name: "Aalen, Stadt".into(),
                bounding_box: None,
                shape: None,
            }),
        );

        ags.insert(4011000, 40110000000);
        ags.insert(4012000, 40120000000);
        ags.insert(8136088, 81365001088);

        let ars_ags = ArsAgsInner {
            database: Database { ars, ags },
        };

        assert_eq!(ars_ags.extract("120530000000"), Some(120530000000));

        assert_eq!(
            ars_ags.extract("Frankfurt (Oder), Stadt (120530000000)"),
            Some(120530000000)
        );

        assert_eq!(ars_ags.extract("Frankfurt (Oder), Stadt"), None);

        assert_eq!(ars_ags.extract("12 0 73 5305 201"), Some(120735305201));

        assert_eq!(
            ars_ags.extract("Gerswalde (12 0 73 5305 201)"),
            Some(120735305201)
        );

        assert_eq!(ars_ags.extract("Bremen (04011000)"), Some(40110000000));

        assert_eq!(
            ars_ags.extract("Bremerhaven (04 0 12 000)"),
            Some(40120000000)
        );

        assert_eq!(
            ars_ags.extract("8136088; Gemeindeschlüssel Stadt Aalen"),
            Some(81365001088),
        );
    }
}
