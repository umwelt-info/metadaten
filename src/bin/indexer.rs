use anyhow::Result;
use bincode::deserialize;
use cap_std::{ambient_authority, fs::Dir};
use hashbrown::HashSet;
use parking_lot::Mutex;
use rayon::iter::{IntoParallelIterator, ParallelIterator};
#[cfg(feature = "regression-test")]
use serde_json::to_writer_pretty;
use string_cache::DefaultAtom;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

#[cfg(not(feature = "regression-test"))]
use metadaten::index::Indexer;
use metadaten::{
    data_path_from_env,
    dataset::{Dataset, quality::Quality},
    metrics::Metrics,
    stats::Stats,
};

fn main() -> Result<()> {
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::from_default_env())
        .with(tracing_subscriber::fmt::layer().without_time())
        .init();

    let data_path = data_path_from_env();

    #[cfg(not(feature = "regression-test"))]
    let indexer = Indexer::start(&data_path)?;

    let dir = Dir::open_ambient_dir(data_path, ambient_authority())?;

    let mut metrics = Mutex::new(Metrics::read(&dir)?);

    let stats = Stats::read(&dir)?;

    metrics.get_mut().clear_datasets();

    let sources = dir.read_dir("datasets")?.collect::<Result<Vec<_>, _>>()?;

    let duplicate_landing_pages = match read_duplicate_landing_pages(&dir) {
        Ok(val) => val,
        Err(err) => {
            tracing::error!("Failed to read duplicate landing pages: {err:#}");

            Default::default()
        }
    };

    sources
        .into_par_iter()
        .try_for_each(|source| -> Result<()> {
            let source_name = DefaultAtom::from(source.file_name().into_string().unwrap());

            if source_name.ends_with(".not_indexed") {
                return Ok(());
            }

            let accesses = stats.accesses.get(source_name.as_ref());

            let source = source.open_dir()?;
            let datasets = source.entries()?.collect::<Result<Vec<_>, _>>()?;

            datasets
                .into_par_iter()
                .try_for_each(|dataset| -> Result<()> {
                    let dataset_id = dataset.file_name().into_string().unwrap();

                    let mut content = Vec::new();
                    let mut dataset = Dataset::read_with(dataset.open()?, &mut content)?;

                    dataset.quality = Quality::of(&dataset, &duplicate_landing_pages);

                    metrics.lock().record_dataset(&source_name, &dataset);

                    let accesses = *accesses
                        .and_then(|accesses| accesses.get(&dataset_id))
                        .unwrap_or(&0);

                    #[cfg(not(feature = "regression-test"))]
                    {
                        dataset.store(&mut content)?;

                        indexer.add_document(
                            source_name.as_ref().to_owned(),
                            dataset_id,
                            content,
                            dataset,
                            accesses,
                        )?;
                    }

                    #[cfg(feature = "regression-test")]
                    {
                        let _accesses = accesses;

                        content.clear();
                        to_writer_pretty(&mut content, &dataset)?;
                        content.push(b'\n');

                        let mut file_name = dataset_id;
                        source.remove_file(&file_name)?;

                        file_name.push_str(".json");
                        source.write(&file_name, &content)?;
                    }

                    Ok(())
                })
        })?;

    #[cfg(not(feature = "regression-test"))]
    indexer.commit()?;

    metrics.get_mut().write(&dir)?;

    Ok(())
}

fn read_duplicate_landing_pages(dir: &Dir) -> Result<HashSet<[u8; 32]>> {
    let buf = dir.read("duplicate_landing_pages")?;

    let val = deserialize(&buf)?;

    Ok(val)
}
