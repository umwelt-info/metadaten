use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::mem::replace;
use std::sync::Arc;
use std::time::{Duration, Instant};

use anyhow::Result;
use arc_swap::{ArcSwap, access::Access};
use concread::hashmap::HashMap as CowHashMap;
use parking_lot::Mutex;

pub struct Vocabulary<V>
where
    V: VocabularyInner,
{
    inner: ArcSwap<(V, Instant)>,
    placeholders: CowHashMap<V::Key, Arc<V::Value>>,
    refresh: Mutex<bool>,
}

pub trait VocabularyInner: Default {
    const NAME: &str;

    type Key: Debug + Display + Clone + Copy + Send + Sync + Eq + Hash + 'static;
    type Value: Send + Sync + 'static;

    fn placeholder(key: Self::Key) -> Self::Value;

    fn open() -> Result<Self>;

    fn resolve(&self, key: Self::Key) -> Option<Arc<Self::Value>>;
}

impl<V> Vocabulary<V>
where
    V: VocabularyInner,
{
    #[cold]
    pub fn open() -> Self {
        let (val, log_once) = match V::open() {
            Ok(val) => (val, false),
            Err(err) => {
                tracing::error!("Failed to open {}: {:#}", V::NAME, err);

                (Default::default(), true)
            }
        };

        Self {
            inner: ArcSwap::from_pointee((val, Instant::now())),
            placeholders: Default::default(),
            refresh: Mutex::new(log_once),
        }
    }

    pub fn inner(&self) -> impl Access<V> + use<'_, V> {
        self.inner.map(|(inner, _): &(V, _)| inner)
    }

    pub fn resolve(&self, key: V::Key) -> Arc<V::Value> {
        if let Some(val) = self.try_resolve(key) {
            return val;
        }

        if let Some(val) = self.placeholders.read().get(&key) {
            return val.clone();
        }

        self.refresh(key)
    }

    fn try_resolve(&self, key: V::Key) -> Option<Arc<V::Value>> {
        let (inner, last_refresh) = &**self.inner.load();

        if last_refresh.elapsed() > Duration::from_secs(24 * 60 * 60) {
            let mut placeholders = self.placeholders.write();
            placeholders.clear();
            placeholders.commit();

            return None;
        }

        inner.resolve(key)
    }

    #[cold]
    fn refresh(&self, key: V::Key) -> Arc<V::Value> {
        let placeholder = || {
            let val = Arc::new(V::placeholder(key));

            let mut placeholders = self.placeholders.write();
            placeholders.insert(key, val.clone());
            placeholders.commit();

            val
        };

        let mut log_once = self.refresh.lock();

        if let Some(val) = self.try_resolve(key) {
            return val;
        }

        match V::open() {
            Ok(val) => {
                let res = match val.resolve(key) {
                    Some(val) => val,
                    None => {
                        tracing::error!("Failed to resolve {} in {}", key, V::NAME);

                        placeholder()
                    }
                };

                self.inner.store(Arc::new((val, Instant::now())));

                *log_once = false;

                res
            }
            Err(err) => {
                if !replace(&mut log_once, true) {
                    tracing::error!("Failed to refresh {}: {:#}", V::NAME, err);
                }

                placeholder()
            }
        }
    }
}
