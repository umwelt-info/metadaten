use std::fs::read;
use std::path::Path;
use std::sync::Arc;

use anyhow::{Context, Result};
use arc_swap::access::Access;
use bincode::{deserialize, serialize};
use cap_std::fs::Dir;
use geo::Rect;
use hashbrown::hash_map::HashMap;
use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};

use crate::{
    data_path_from_env,
    vocabulary::{Vocabulary, VocabularyInner},
};

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct Database {
    pub entries: HashMap<u64, Arc<Entry>>,
}

impl Database {
    pub fn write(&self, dir: &Dir) -> Result<()> {
        let buf = serialize(self)?;

        dir.write("atkis.bin", &buf)?;

        Ok(())
    }

    fn read(path: &Path) -> Result<Self> {
        let buf = read(path)?;

        deserialize(&buf).context("Invalid ATKIS format")
    }
}

#[derive(Serialize, Deserialize)]
pub struct Entry {
    pub name: Box<str>,
    pub bounding_box: Option<Rect>,
}

pub static ATKIS: Lazy<Vocabulary<AtkisInner>> = Lazy::new(Vocabulary::open);

impl Vocabulary<AtkisInner> {
    pub fn extract(&self, val: &str) -> Option<u64> {
        self.inner().load().extract(val)
    }
}

#[derive(Default)]
pub struct AtkisInner {
    database: Database,
}

impl AtkisInner {
    fn extract(&self, val: &str) -> Option<u64> {
        static TK10: Lazy<Regex> = Lazy::new(|| Regex::new(r"(\d{4})-([A-Z]{2})").unwrap());

        let tk10 = TK10
            .captures(val)
            .map(|caps| {
                let val = caps[1].chars().collect::<String>();

                val.parse().unwrap()
            })
            .filter(|key| self.database.entries.contains_key(key));

        tk10
    }
}

impl VocabularyInner for AtkisInner {
    const NAME: &str = "ATKIS";

    type Key = u64;
    type Value = Entry;

    fn placeholder(key: u64) -> Entry {
        Entry {
            name: format!("ATKIS/{key:04}").into(),
            bounding_box: None,
        }
    }

    fn open() -> Result<Self> {
        let path = data_path_from_env().join("datasets/atkis.not_indexed/atkis.bin");

        let database = Database::read(&path)?;

        Ok(Self { database })
    }

    fn resolve(&self, key: u64) -> Option<Arc<Entry>> {
        self.database.entries.get(&key).cloned()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn extract_atkis_works() {
        let mut entries = HashMap::new();

        entries.insert(
            3652,
            Arc::new(Entry {
                name: "Jacobsdorf".into(),
                bounding_box: None,
            }),
        );

        let atkis = AtkisInner {
            database: Database { entries },
        };

        assert_eq!(
            atkis.extract("Digitale Topographische Karte 1 : 10 000 - 3652-SW Jacobsdorf"),
            Some(3652)
        );

        assert_eq!(atkis.extract("ARS 100213652876"), None);
    }
}
