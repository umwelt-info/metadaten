use std::fs::read;
use std::path::Path;
use std::sync::Arc;

use anyhow::{Context, Result, bail};
use arc_swap::access::Access;
use bincode::{deserialize, serialize};
use cap_std::fs::Dir;
use hashbrown::HashMap;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use tantivy_fst::Map;

use crate::{
    data_path_from_env,
    vocabulary::{Vocabulary, VocabularyInner},
};

pub static ORGANISATIONS: Lazy<Vocabulary<OrganisationsInner>> = Lazy::new(Vocabulary::open);

impl Vocabulary<OrganisationsInner> {
    pub fn r#match(&self, name: &str) -> Option<u64> {
        self.inner().load().r#match(name)
    }
}

pub struct OrganisationsInner {
    database: OrganisationsDatabase,
    index: Map<Vec<u8>>,
}

impl Default for OrganisationsInner {
    fn default() -> Self {
        let database = OrganisationsDatabase::default();
        let index = recompute_organisations_index(&database.entries);

        Self { database, index }
    }
}

impl OrganisationsInner {
    fn r#match(&self, name: &str) -> Option<u64> {
        if name.is_empty() {
            return None;
        }

        self.index.get(name.to_lowercase())
    }
}

impl VocabularyInner for OrganisationsInner {
    const NAME: &str = "WikiData organisations";

    type Key = u64;
    type Value = Organisation;

    fn placeholder(identifier: u64) -> Organisation {
        Organisation {
            label: format!("WikiData/Q{identifier}").into(),
            aliases: Default::default(),
            websites: Default::default(),
        }
    }

    fn open() -> Result<Self> {
        let path = data_path_from_env().join("datasets/wikidata.not_indexed/organisations.bin");

        let database = OrganisationsDatabase::read(&path)?;
        let index = recompute_organisations_index(&database.entries);

        Ok(Self { database, index })
    }

    fn resolve(&self, identifier: u64) -> Option<Arc<Organisation>> {
        self.database.entries.get(&identifier).cloned()
    }
}

#[derive(Default, Serialize, Deserialize)]
pub struct OrganisationsDatabase {
    pub entries: HashMap<u64, Arc<Organisation>>,
}

#[derive(Deserialize)]
struct OldOrganisationsDatabase {
    entries: HashMap<u64, OldOrganisation>,
}

const CURR_VER: u8 = 3;
const OLD_VER: u8 = 2;

impl OrganisationsDatabase {
    pub fn write(&self, dir: &Dir) -> Result<()> {
        let mut buf = serialize(self)?;
        buf.push(CURR_VER);

        dir.write("organisations.bin", &buf)?;

        Ok(())
    }

    fn read(path: &Path) -> Result<Self> {
        let buf = read(path)?;

        let res = match buf.split_last() {
            Some((&CURR_VER, buf)) => deserialize::<Self>(buf),
            Some((&OLD_VER, buf)) => {
                deserialize::<OldOrganisationsDatabase>(buf).map(|old_val| Self {
                    entries: old_val
                        .entries
                        .into_iter()
                        .map(|(id, old_val)| {
                            let val = Arc::new(Organisation {
                                label: old_val.label,
                                aliases: old_val.aliases,
                                websites: old_val.websites,
                            });

                            (id, val)
                        })
                        .collect(),
                })
            }
            _ => bail!("Missing or invalid WikiData organisations format version"),
        };

        res.context("Invalid WikiData organisations format")
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Organisation {
    pub label: Box<str>,
    pub aliases: Box<[Box<str>]>,
    pub websites: Box<[Box<str>]>,
}

#[derive(Deserialize)]
struct OldOrganisation {
    label: Box<str>,
    _description: Option<Box<str>>,
    aliases: Box<[Box<str>]>,
    websites: Box<[Box<str>]>,
}

fn recompute_organisations_index(entries: &HashMap<u64, Arc<Organisation>>) -> Map<Vec<u8>> {
    let mut labels = entries
        .iter()
        .fold(Vec::new(), |mut labels, (id, organisation)| {
            labels.push((organisation.label.to_lowercase().into_bytes(), *id));

            for alias in &organisation.aliases {
                labels.push((alias.to_lowercase().into_bytes(), *id));
            }

            labels
        });

    labels.sort_unstable_by(|(lhs, _), (rhs, _)| lhs.cmp(rhs));
    labels.dedup_by(|(lhs, _), (rhs, _)| lhs == rhs);

    Map::from_iter(labels).unwrap()
}
