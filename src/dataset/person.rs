use std::fmt;

use serde::{
    Deserialize, Serialize,
    ser::{SerializeStruct, Serializer},
};
use smallvec::SmallVec;
use utoipa::ToSchema;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Deserialize, ToSchema, Default, Clone)]
#[serde(deny_unknown_fields)]
/// A person affiliated with the dataset
pub struct Person {
    pub name: String,
    pub role: PersonRole,
    /// Addresses to reach this person via electronic mail
    #[serde(default)]
    pub emails: SmallVec<[String; 1]>,
    /// Addresses to reach this person using a web form
    #[serde(default)]
    pub forms: SmallVec<[String; 1]>,
}

impl Person {
    pub fn hyperlinks(&self) -> impl Iterator<Item = (&str, &str)> + '_ {
        self.emails
            .iter()
            .map(|email| ("mailto:", email.as_str()))
            .chain(self.forms.iter().map(|url| ("", url.as_str())))
    }
}

impl_minified_serialize!(Person, name, role, emails if is_empty, forms if is_empty);

/// The role of a person in relation to the dataset containing the reference
#[derive(
    Debug, Default, PartialEq, Eq, PartialOrd, Ord, Deserialize, Serialize, ToSchema, Clone, Copy,
)]
pub enum PersonRole {
    #[default]
    Unknown,
    Contact,
    Creator,
    Contributor,
    Manager,
}

impl fmt::Display for PersonRole {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            Self::Unknown => "unbekannt",
            Self::Contact => "Kontakt",
            Self::Creator => "Verfasser*in",
            Self::Contributor => "Beitragende*r",
            Self::Manager => "Leiter*in",
        };
        fmt.write_str(val)
    }
}
