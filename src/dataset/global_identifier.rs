use std::fmt;
use std::hash::{Hash, Hasher};

use compact_str::CompactString;
use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};
use url::Url;
use utoipa::ToSchema;
use uuid::Uuid;

use crate::TrimExt;

/// The optional global identifier of the dataset.
///
/// ## Usage
/// This identifier is used to reliably find dupliclate datasets over multiple sources.
///
/// ## Comparison to other standards
/// * In sources following the [DCAT standards][vocab-dcat], this is equivalent to
/// ```text
/// dct:guid
/// ```
/// and if this field does not exist, then to
/// ```text
/// dct:identifier
/// ```
///
/// See [DCAT-AP.de: Erkennung von Dubletten][erkennung-von-dubletten] for further information.
///
/// * In sources following the [OGC CWS standard][ogc-cat], this is equivalent to
/// ```text
/// identificationInfo.MD_DataIdentification.uuid
/// ```
/// and if this field does not exists, then to
/// ```text
/// identificationInfo.MD_DataIdentification.citation.CI_Citation.identifier.MD_Identifier.code
/// ```
///
/// [vocab-dcat]: https://www.w3.org/TR/vocab-dcat-3/
/// [ogc-cat]: https://www.ogc.org/standard/cat/
/// [erkennung-von-dubletten]: https://www.dcat-ap.de/def/dcatde/2.0/implRules/#erkennung-von-dubletten
#[derive(Clone, Eq, Serialize, Deserialize, ToSchema)]
pub enum GlobalIdentifier {
    Uuid(Uuid),
    UuidWithProvider(Provider, Uuid),
    #[schema(value_type = (String, String))]
    UrnNbn(CompactString, CompactString),
    Doi(String),
    Other(String),
    Url(Url),
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize, ToSchema)]
pub enum Provider {
    #[schema(value_type = String)]
    GdiDe(CompactString),
    Bfn,
    GeoSeaPortal,
}

impl fmt::Debug for GlobalIdentifier {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Uuid(uuid) => fmt.debug_tuple("Uuid").field(uuid).finish(),
            Self::UuidWithProvider(provider, uuid) => fmt
                .debug_tuple("UuidWithProvider")
                .field(provider)
                .field(uuid)
                .finish(),
            Self::UrnNbn(namespace, id) => fmt
                .debug_tuple("UrnNbn")
                .field(namespace)
                .field(id)
                .finish(),
            Self::Doi(doi) => fmt.debug_tuple("Doi").field(doi).finish(),
            Self::Other(val) => fmt.debug_tuple("Other").field(val).finish(),
            Self::Url(url) => fmt.debug_tuple("Url").field(&url.as_str()).finish(),
        }
    }
}

impl GlobalIdentifier {
    pub fn is_short(&self) -> bool {
        match self {
            Self::Uuid(_)
            | Self::UuidWithProvider(_, _)
            | Self::UrnNbn(_, _)
            | Self::Doi(_)
            | Self::Url(_) => false,
            Self::Other(val) => val.len() < 32,
        }
    }

    pub fn parse<S>(val: S) -> Option<Self>
    where
        S: TrimExt + Into<String>,
    {
        let val = val.trim();

        if val.is_empty() || &*val == "http://portalu.de/igc_2/TEST" {
            None
        } else if let Ok(uuid) = Uuid::try_parse(&val) {
            Some(Self::Uuid(uuid))
        } else if let Some(identifier) = extract_well_known(&val) {
            Some(identifier)
        } else if let Ok(url) = Url::parse(&val) {
            Some(Self::Url(url))
        } else {
            Some(Self::Other(val.into()))
        }
    }

    pub fn extract(val: &str) -> Option<Self> {
        extract_well_known(val.trim())
    }
}

impl PartialEq for GlobalIdentifier {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Uuid(lhs), Self::Uuid(rhs))
            | (Self::Uuid(lhs), Self::UuidWithProvider(_, rhs))
            | (Self::UuidWithProvider(_, lhs), Self::Uuid(rhs))
            | (Self::UuidWithProvider(_, lhs), Self::UuidWithProvider(_, rhs)) => lhs == rhs,
            (Self::UrnNbn(namespace1, id1), Self::UrnNbn(namespace2, id2)) => {
                (namespace1, id1) == (namespace2, id2)
            }
            (Self::Doi(lhs), Self::Doi(rhs)) => lhs == rhs,
            (Self::Other(lhs), Self::Other(rhs)) => lhs == rhs,
            (Self::Url(lhs), Self::Url(rhs)) => lhs == rhs,
            _ => false,
        }
    }
}

impl Hash for GlobalIdentifier {
    fn hash<H>(&self, hasher: &mut H)
    where
        H: Hasher,
    {
        let discriminant = match self {
            Self::Uuid(_) | Self::UuidWithProvider(_, _) => 0,
            Self::UrnNbn(_, _) => 1,
            Self::Doi(_) => 2,
            Self::Other(_) => 3,
            Self::Url(_) => 4,
        };

        discriminant.hash(hasher);

        match self {
            Self::Uuid(uuid) | Self::UuidWithProvider(_, uuid) => uuid.hash(hasher),
            Self::UrnNbn(namespace, id) => (namespace, id).hash(hasher),
            Self::Doi(doi) => doi.hash(hasher),
            Self::Other(val) => val.hash(hasher),
            Self::Url(url) => url.hash(hasher),
        }
    }
}

fn extract_well_known(val: &str) -> Option<GlobalIdentifier> {
    struct Pattern {
        gdi_de: Regex,
        bfn: Regex,
        /// The URN:NBN is described in <https://www.rfc-editor.org/rfc/rfc3188>.
        urn_nbn: Regex,
        doi: Regex,
        geoseaportal: Regex,
    }

    impl Default for Pattern {
        fn default() -> Self {
            Self {
                gdi_de: Regex::new(r"^https?://registry\.gdi\-de\.org/id/([^/]+)/\s*([^/\s]+)\s*/?$").unwrap(),
                bfn: Regex::new(r"^https?://geodienste\.bfn\.de/md/\s*([^/\s]+)\s*/?$").unwrap(),
                urn_nbn: Regex::new(r"(?x)
                    ^(?:https?\://nbn-resolving\.(?:org|de)/)? # The URN might be prefixed with a valid URL
                    (urn\:nbn\:[\w:]*)\:                       # We only care for the URN:NBN identifier and subnamespaces that are seperated by colons and end with a colon.
                    ([\w-]+)                                   # This part is the NBN identifier that has to be unique only within the same namespace.
                    (?:\/.*|)$                                 # There might be a 'fragment' part leading to subproperties of the resolved url.
                ").unwrap(),
                doi: Regex::new(r"(?x)
                    (?:(?:https?\://)?doi\.org/)               # The DOI needds to be prefixed with a valid doi.org as otherwise it's really hard to guess.
                    ([[:alnum:]\-\._\~!\$\&'\(\)\*\+,;=:@%/]+) # We only extract URL-safe characters as there is often some fluff around the actual DOI.
                ").unwrap(),
                geoseaportal: Regex::new(
                    r"(?x)
                    ^https?://www\.geoseaportal\.de/csw/record/ # This part is the URL of the GeoSeaPortal
                    ([^/\s]+)\s*/?                              # This should be a valid UUID
                ").unwrap(),
            }
        }
    }

    static PATTERN: Lazy<Pattern> = Lazy::new(Pattern::default);

    let pattern = &*PATTERN;

    if let Some(captures) = pattern.gdi_de.captures(val) {
        let provider = captures[1].into();
        let uuid = Uuid::try_parse(&captures[2]).ok()?;

        Some(GlobalIdentifier::UuidWithProvider(
            Provider::GdiDe(provider),
            uuid,
        ))
    } else if let Some(captures) = pattern.urn_nbn.captures(val) {
        let namespace = captures[1].into();
        let id = captures[2].into();

        Some(GlobalIdentifier::UrnNbn(namespace, id))
    } else if let Some(captures) = pattern.doi.captures(val) {
        let id = captures[1].into();

        Some(GlobalIdentifier::Doi(id))
    } else if let Some(captures) = pattern.bfn.captures(val) {
        let uuid = Uuid::try_parse(&captures[1]).ok()?;

        Some(GlobalIdentifier::UuidWithProvider(Provider::Bfn, uuid))
    } else if let Some(captures) = pattern.geoseaportal.captures(val) {
        let uuid = Uuid::try_parse(&captures[1]).ok()?;

        Some(GlobalIdentifier::UuidWithProvider(
            Provider::GeoSeaPortal,
            uuid,
        ))
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use uuid::uuid;

    #[test]
    fn extract_well_known_works() {
        assert_eq!(
            extract_well_known("https://registry.gdi-de.org/id/de.bund.bfg.csw/e0de19e8-f4c4-4e1d-9adb-1fa9aa9951b4").unwrap(),
            GlobalIdentifier::UuidWithProvider(Provider::GdiDe("de.bund.bfg.csw".into()), uuid!("e0de19e8-f4c4-4e1d-9adb-1fa9aa9951b4")),
        );

        assert_eq!(
            extract_well_known(
                "https://geodienste.bfn.de/md/d46a49fe-f9ac-4c23-bf5c-888291b244f5/"
            )
            .unwrap(),
            GlobalIdentifier::UuidWithProvider(
                Provider::Bfn,
                uuid!("d46a49fe-f9ac-4c23-bf5c-888291b244f5")
            ),
        );

        assert_eq!(
            extract_well_known("http://nbn-resolving.de/urn:nbn:de:0221-201203027611").unwrap(),
            GlobalIdentifier::UrnNbn("urn:nbn:de".into(), "0221-201203027611".into()),
        );

        assert_eq!(
            extract_well_known("https://nbn-resolving.org/urn:nbn:de:hbz:b219-16618").unwrap(),
            GlobalIdentifier::UrnNbn("urn:nbn:de:hbz".into(), "b219-16618".into()),
        );

        assert_eq!(
            extract_well_known("urn:nbn:de:bvb:91-diss20060308-1417541491").unwrap(),
            GlobalIdentifier::UrnNbn("urn:nbn:de:bvb".into(), "91-diss20060308-1417541491".into()),
        );

        assert_eq!(
            extract_well_known("https://doi.org/10.19217/skr622").unwrap(),
            GlobalIdentifier::Doi("10.19217/skr622".into()),
        );

        assert_eq!(
            extract_well_known(
                "https://www.geoseaportal.de/csw/record/7ed3206c-c996-46ff-b824-d58c245a065c"
            )
            .unwrap(),
            GlobalIdentifier::UuidWithProvider(
                Provider::GeoSeaPortal,
                uuid!("7ed3206c-c996-46ff-b824-d58c245a065c")
            )
        );

        assert_eq!(extract_well_known("WasserBLIcK"), None);
    }

    #[test]
    fn comparing_global_identifiers_works() {
        assert_eq!(
            extract_well_known(
                "https://registry.gdi-de.org/id/de.bund.bfg.csw/e0de19e8-f4c4-4e1d-9adb-1fa9aa9951b4"
            ).unwrap(),
            extract_well_known(
                "https://www.geoseaportal.de/csw/record/e0de19e8-f4c4-4e1d-9adb-1fa9aa9951b4"
            ).unwrap(),
        );

        assert_ne!(
            extract_well_known(
                "http://nbn-resolving.de/urn:nbn:de:e0de19e8-f4c4-4e1d-9adb-1fa9aa9951b4"
            )
            .unwrap(),
            extract_well_known(
                "https://www.geoseaportal.de/csw/record/e0de19e8-f4c4-4e1d-9adb-1fa9aa9951b4"
            )
            .unwrap(),
        );
    }
}
