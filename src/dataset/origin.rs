use std::env::var_os;
use std::ffi::OsString;
use std::fs::read_to_string;
use std::sync::OnceLock;

use anyhow::Result;
use glob::Pattern;
use hashbrown::HashMap;
use serde::{Deserialize, Serialize};
use toml::from_str;
use utoipa::ToSchema;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Deserialize, Serialize, ToSchema)]
#[serde(deny_unknown_fields)]
pub struct Origin {
    /// The name of the data provider
    pub name: String,
    #[schema(format = Uri)]
    /// Hyperlink to the about page
    pub about_url: Option<String>,
    #[schema(format = Uri)]
    /// Hyperink to the contact page
    pub contact_url: String,
    /// Contact via electronic mail
    pub email: Option<String>,
    /// Background information on the data provider
    pub description: String,
    #[serde(skip)]
    pub pattern: Option<Pattern>,
}

pub type Origins = HashMap<String, Origin>;

pub static ORIGINS_PATH: OnceLock<OsString> = OnceLock::new();

pub fn load_origins() -> Result<Origins> {
    let path = ORIGINS_PATH
        .get_or_init(|| var_os("ORIGINS_PATH").expect("Environment variable ORIGINS_PATH not set"));

    let contents = read_to_string(path)?;
    let mut origins = from_str::<Origins>(&contents)?;

    for (id, origin) in &mut origins {
        if &Pattern::escape(id) != id {
            let pattern = Pattern::new(id)?;

            origin.pattern = Some(pattern);
        }
    }

    Ok(origins)
}

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::{Context, ensure};

    #[test]
    fn validate_origins() -> Result<()> {
        let contents = read_to_string("deployment/origins.toml")?;
        let origins = from_str::<Origins>(&contents)?;

        for (id, origin) in origins {
            ensure!(!id.starts_with('/'), "Origin {id} starts with slash");

            ensure!(!origin.name.is_empty(), "Origin {id} has empty name");
            ensure!(
                !origin.description.is_empty(),
                "Origin {id} has empty description"
            );
            ensure!(
                !origin.contact_url.is_empty(),
                "Origin {id} has empty contact_url"
            );

            if Pattern::escape(&id) != id {
                Pattern::new(&id).with_context(|| format!("Invalid glob pattern {id}"))?;
            }
        }

        Ok(())
    }
}
