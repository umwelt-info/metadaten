use std::fmt::Debug;

use compact_str::CompactString;
use serde::{
    Deserialize, Serialize,
    ser::{SerializeStruct, Serializer},
};
use smallvec::SmallVec;
use utoipa::ToSchema;

use crate::dataset::tag::Tag;

/// This is the categorization of the types assigned to each data set.
///
/// The categorization uses the metadata terms of the [Dublin Core™ Metadata Initiative](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) where suitable and extended here where necessary.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize, ToSchema)]
pub enum Type {
    /// <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/dcmitype/Event>
    Event,
    /// <https://www.dublincore.org/specifications/lrmi/lrmi_terms/2022-06-14/#LearningResource>
    LearningResource,
    /// <https://dwc.tdwg.org/list/#dwc_Taxon>
    Taxon {
        /// <https://dwc.tdwg.org/list/#dwc_acceptedNameUsage>
        scientific_name: String,
        /// <https://dwc.tdwg.org/list/#dwc_vernacularName>
        common_names: SmallVec<[String; 1]>,
    },
    Measurements {
        domain: Domain,
        station: Option<Station>,
        /// Indicates which variables are part of the measured data.
        measured_variables: SmallVec<[String; 1]>,
        /// Indicates which method was used to retrieve the data.
        ///
        /// This can be a specific procedure, but can also indicate more generally if the data is measured, estimated or modelled.
        methods: SmallVec<[String; 1]>,
    },
    SupportProgram,
    MapService,
    /// <https://www.umweltbundesamt.de/themen/nachhaltigkeit-strategien-internationales/umweltpruefungen>
    Uvp,
    Text {
        text_type: TextType,
    },
    /// <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/dcmitype/Software>
    Software,
    /// <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/dcmitype/Image>
    Image,
    /// <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/dcmitype/Dataset>
    Dataset,
    /// <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/Jurisdiction>
    Legal,
    /// <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/dcmitype/MovingImage>
    Video,
    /// <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/dcmitype/Sound>
    Audio,
    /// <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/InteractiveResource>
    Form,
    ChemicalCompound {
        name: String,
        synonyms: Vec<String>,
        cas_registry_number: Option<String>,
    },
}

impl Type {
    pub fn with_tags<F>(&self, mut f: F)
    where
        F: FnMut(Tag),
    {
        if let Self::Measurements {
            domain,
            station:
                Some(Station {
                    reporting_obligations,
                    ..
                }),
            ..
        } = self
        {
            match domain {
                Domain::Unspecified => (),
                Domain::Air => f(Tag::AIR),
                Domain::Rivers => f(Tag::RIVER),
                Domain::Groundwater => f(Tag::GROUNDWATER),
                Domain::Geophysics => f(Tag::GEOPHYSICS),
                Domain::Radioactivity => f(Tag::RADIOACTIVITY),
                Domain::Surfacewater => f(Tag::SURFACEWATER),
                Domain::Chemistry => f(Tag::CHEMISTRY),
                Domain::Sea => f(Tag::SEA),
                Domain::Soil => f(Tag::SOIL),
                Domain::Biology => f(Tag::BIOLOGY),
                Domain::NonionisingRadiation => f(Tag::NONIONISINGRADIATION),
                Domain::Water => f(Tag::WATER),
            }

            for reporting_obligation in reporting_obligations {
                match reporting_obligation {
                    ReportingObligation::Wrrl => f(Tag::WRRL),
                    ReportingObligation::Ospar => f(Tag::OSPAR),
                    ReportingObligation::Helcom => f(Tag::HELCOM),
                    ReportingObligation::SoeReport => f(Tag::SOER),
                    ReportingObligation::Nitrat => f(Tag::NITRATRICHTLINIE),
                    ReportingObligation::Prtr => f(Tag::PRTR),
                }
            }
        }

        if let Self::Uvp = self {
            f(Tag::UVP);
        }
    }

    pub fn default_facet() -> &'static [&'static str] {
        &["unbekannt"]
    }

    pub fn facet(&self) -> &'static [&'static str] {
        match self {
            Self::Event => &["Ereignis"],
            Self::LearningResource => &["Lehrmaterial"],
            Self::Taxon { .. } => &["Taxon"],
            Self::Measurements {
                domain, station, ..
            } => match (domain, station) {
                (Domain::Unspecified, _) => &["Messwerte"],
                (Domain::Air, None) => &["Messwerte", "Luft"],
                (Domain::Air, Some(_)) => &["Messwerte", "Luft", "Messstellen"],
                (Domain::Rivers, None) => &["Messwerte", "Flüsse"],
                (Domain::Rivers, Some(_)) => &["Messwerte", "Flüsse", "Messstellen"],
                (Domain::Groundwater, None) => &["Messwerte", "Grundwasser"],
                (Domain::Groundwater, Some(_)) => &["Messwerte", "Grundwasser", "Messstellen"],
                (Domain::Geophysics, None) => &["Messwerte", "Geophysik"],
                (Domain::Geophysics, Some(_)) => &["Messwerte", "Geophysik", "Messstellen"],
                (Domain::Radioactivity, None) => &["Messwerte", "Radioaktivität"],
                (Domain::Radioactivity, Some(_)) => &["Messwerte", "Radioaktivität", "Messstellen"],
                (Domain::Surfacewater, None) => &["Messwerte", "Oberflächenwasser"],
                (Domain::Surfacewater, Some(_)) => {
                    &["Messwerte", "Oberflächenwasser", "Messstellen"]
                }
                (Domain::Chemistry, None) => &["Messwerte", "Chemie"],
                (Domain::Chemistry, Some(_)) => &["Messwerte", "Chemie", "Messstellen"],
                (Domain::Sea, None) => &["Messwerte", "Meer"],
                (Domain::Sea, Some(_)) => &["Messwerte", "Meer", "Messstellen"],
                (Domain::Soil, None) => &["Messwerte", "Boden"],
                (Domain::Soil, Some(_)) => &["Messwerte", "Boden", "Messstellen"],
                (Domain::Biology, None) => &["Messwerte", "Biologie"],
                (Domain::Biology, Some(_)) => &["Messwerte", "Biologie", "Messstellen"],
                (Domain::NonionisingRadiation, None) => {
                    &["Messwerte", "Nichtionisierende Strahlung"]
                }
                (Domain::NonionisingRadiation, Some(_)) => {
                    &["Messwerte", "Nichtionisierende Strahlung", "Messstellen"]
                }
                (Domain::Water, None) => &["Messwerte", "Wasser"],
                (Domain::Water, Some(_)) => &["Messwerte", "Wasser", "Messstellen"],
            },
            Self::SupportProgram => &["Förderprogramm"],
            Self::MapService => &["Kartendienst"],
            Self::Uvp => &["Umweltprüfung"],
            Self::Text { text_type } => match text_type {
                TextType::Unspecified => &["Text"],
                TextType::OfficialFile => &["Text", "Akte"],
                TextType::Manual => &["Text", "Handbuch"],
                TextType::Report => &["Text", "Bericht"],
                TextType::Publication => &["Text", "Fachpublikation"],
                TextType::BlogPost => &["Text", "Blogbeitrag"],
                TextType::PressRelease => &["Text", "Pressemitteilung"],
                TextType::News => &["Text", "News"],
                TextType::Editorial => &["Text", "Redaktioneller Beitrag"],
                TextType::Booklet => &["Text", "Broschüre"],
            },
            Self::Software => &["Software"],
            Self::Image => &["Bildmaterial"],
            Self::Dataset => &["Strukturierter Datensatz"],
            Self::Legal => &["Gesetzestext"],
            Self::Video => &["Videomaterial"],
            Self::Audio => &["Tonaufnahmen"],
            Self::Form => &["Formular"],
            Self::ChemicalCompound { .. } => &["Chemische Verbindung"],
        }
    }
}

/// Indicates the domain from which the measurments originate.
#[derive(
    Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Deserialize, Serialize, ToSchema,
)]
pub enum Domain {
    #[default]
    Unspecified,
    Air,
    Rivers,
    Groundwater,
    Geophysics,
    Radioactivity,
    Surfacewater,
    Chemistry,
    Sea,
    Soil,
    Biology,
    NonionisingRadiation,
    Water,
}

/// Indicates properties of the point of measurement where the data was collected.
#[derive(Debug, Default, Clone, Deserialize, PartialEq, Eq, PartialOrd, Ord, ToSchema)]
pub struct Station {
    #[schema(value_type = Option<String>)]
    pub id: Option<CompactString>,
    #[schema(value_type = Option<String>)]
    pub measurement_frequency: Option<CompactString>,
    pub reporting_obligations: SmallVec<[ReportingObligation; 1]>,
}

impl_minified_serialize!(
    Station,
    id if is_none,
    measurement_frequency if is_none,
    reporting_obligations if is_empty
);

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Deserialize, Serialize, ToSchema)]
pub enum ReportingObligation {
    /// Wasserrahmenrichtlinie
    Wrrl,
    /// Übereinkommen Oslo/Paris zur Überwachung der Nordsee
    Ospar,
    /// Helsinki-Konvention zur Überwachung der Ostsee
    Helcom,
    /// Umweltberichterstattung Meer (State of Environment Report Waterbase) WISE-6, https://rod.eionet.europa.eu/obligations/14
    SoeReport,
    /// Richtlinie 91/676/EWG zum Schutz der Gewässer vor Verunreinigung durch Nitrat aus landwirtschaftlichen Quellen
    Nitrat,
    /// Pollutant Release and Transfer Register
    Prtr,
}

/// Inidcates the kind of text provided by the dataset.
#[derive(
    Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Deserialize, Serialize, ToSchema,
)]
pub enum TextType {
    #[default]
    Unspecified,
    OfficialFile,
    Manual,
    Report,
    Publication,
    BlogPost,
    PressRelease,
    News,
    Editorial,
    Booklet,
}
