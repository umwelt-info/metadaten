use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

#[derive(
    Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize, Default, ToSchema, Clone,
)]
pub enum SourceUrlExplainer {
    #[default]
    None,
    Informational(String),
    CopyTitle(String),
    CopyStationId(String),
}

impl SourceUrlExplainer {
    pub fn is_none(&self) -> bool {
        matches!(self, Self::None)
    }
}

impl From<Option<String>> for SourceUrlExplainer {
    fn from(value: Option<String>) -> Self {
        match value {
            None => Self::None,
            Some(value) => Self::Informational(value),
        }
    }
}
