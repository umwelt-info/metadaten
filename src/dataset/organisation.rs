use std::cmp::Ordering;
use std::fmt;
use std::mem::take;
use std::ops::Deref;
use std::sync::Arc;

use cow_utils::CowUtils;
use hashbrown::Equivalent;
use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};
use smallvec::SmallVec;
use utoipa::ToSchema;

use crate::{
    TrimExt,
    wikidata::{ORGANISATIONS as WIKIDATA_ORGANISATIONS, Organisation as WikiDataOrganisation},
};

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize, ToSchema)]
#[serde(deny_unknown_fields)]
/// An organisation affiliated with the dataset
pub enum Organisation {
    Other {
        name: String,
        role: OrganisationRole,
        #[serde(default)]
        websites: SmallVec<[String; 1]>,
    },
    WikiData {
        identifier: u64,
        role: OrganisationRole,
    },
}

impl Organisation {
    pub fn key(&self) -> OrganisationKey<&str> {
        match self {
            Self::Other { name, .. } => OrganisationKey::Other(name),
            Self::WikiData { identifier, .. } => OrganisationKey::WikiData(*identifier),
        }
    }

    pub fn name(&self) -> OrganisationName<'_> {
        match self {
            Self::Other { name, .. } => OrganisationName::Other(name),
            Self::WikiData { identifier, .. } => {
                let wikidata_organisation = WIKIDATA_ORGANISATIONS.resolve(*identifier);

                OrganisationName::WikiData(wikidata_organisation)
            }
        }
    }

    pub fn role(&self) -> OrganisationRole {
        match self {
            Self::Other { role, .. } => *role,
            Self::WikiData { role, .. } => *role,
        }
    }

    pub fn with_token<F>(self, mut f: F)
    where
        F: FnMut(String),
    {
        match self {
            Self::Other { name, .. } => f(name),
            Self::WikiData { identifier, .. } => {
                let wikidata_organisation = WIKIDATA_ORGANISATIONS.resolve(identifier);

                f((*wikidata_organisation.label).to_owned());

                for alias in &wikidata_organisation.aliases {
                    f((**alias).to_owned());
                }
            }
        }
    }

    pub fn align(mut self) -> Option<Self> {
        if let Self::Other { name, role, .. } = &mut self {
            *name = take(name).trim();

            if name.is_empty() {
                return None;
            }

            static REDUNDANCIES: Lazy<Regex> = Lazy::new(|| {
                Regex::new(
                    r#"(?x)
                (\s+\([A-Z]+\)) # abbreviations like " (BMUV)"
                |(^Land\s+) # prefixes like "Land "
                |(^Stadt\s+)
                |(\s+gGmbH$) # legal forms
            "#,
                )
                .unwrap()
            });

            let name = REDUNDANCIES
                .replace_all(name, "")
                .cow_replace("e.V.", "e. V.")
                .cow_replace("eingetragener Verein", "e. V.")
                .into_owned();

            if let Some(identifier) = WIKIDATA_ORGANISATIONS.r#match(&name) {
                self = Self::WikiData {
                    identifier,
                    role: *role,
                };
            }
        }

        Some(self)
    }

    pub fn merge(&mut self, other: &mut Self) {
        match (self, other) {
            (
                Self::Other { role, websites, .. },
                Self::Other {
                    role: other_role,
                    websites: other_websites,
                    ..
                },
            ) => {
                *role = *other_role;

                websites.extend(take(other_websites));
                websites.sort_unstable();
                websites.dedup();
            }
            (
                Self::WikiData { role, .. },
                Self::WikiData {
                    role: other_role, ..
                }
                | Self::Other {
                    role: other_role, ..
                },
            ) => *role = *other_role,
            (this @ Self::Other { .. }, other @ Self::WikiData { .. }) => *this = other.clone(),
        }
    }
}

#[derive(
    Debug, Default, PartialEq, Eq, PartialOrd, Ord, Deserialize, Serialize, ToSchema, Clone, Copy,
)]
pub enum OrganisationRole {
    #[default]
    Unknown,
    Publisher,
    Operator,
    Organiser,
    Management,
    Provider,
    Owner,
    Contributor,
}

impl fmt::Display for OrganisationRole {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            Self::Unknown => "unbekannt",
            Self::Publisher => "Herausgeber*in",
            Self::Operator => "Betreiber*in",
            Self::Organiser => "Veranstalter*in",
            Self::Management => "Leitung",
            Self::Provider => "Bereitsteller*in",
            Self::Owner => "Eigentümer*in",
            Self::Contributor => "Mitwirkende",
        };
        fmt.write_str(val)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub enum OrganisationKey<S = String> {
    Other(S),
    WikiData(u64),
}

impl OrganisationKey {
    pub const UBA: u64 = 2491722;
    pub const LUBW: u64 = 1802082;
    pub const LANUV: u64 = 1315485;
    pub const BFN: u64 = 1005480;
    pub const UFU: u64 = 1430103;
}

impl Equivalent<OrganisationKey> for OrganisationKey<&str> {
    fn equivalent(&self, other: &OrganisationKey) -> bool {
        match (self, other) {
            (Self::Other(name), OrganisationKey::Other(other_name)) => name == other_name,
            (Self::WikiData(identifier), OrganisationKey::WikiData(other_identifier)) => {
                identifier == other_identifier
            }
            _ => false,
        }
    }
}

impl From<&OrganisationKey<&str>> for OrganisationKey {
    fn from(val: &OrganisationKey<&str>) -> Self {
        match val {
            OrganisationKey::Other(name) => Self::Other((*name).to_owned()),
            OrganisationKey::WikiData(identifier) => Self::WikiData(*identifier),
        }
    }
}

impl<S> OrganisationKey<S> {
    pub fn url(&self) -> Option<String> {
        match self {
            Self::Other(_) => None,
            Self::WikiData(identifier) => Some(format!(
                "https://www.wikidata.org/wiki/Special:EntityData/Q{identifier}"
            )),
        }
    }
}

impl<S> fmt::Display for OrganisationKey<S>
where
    S: Deref<Target = str>,
{
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Other(name) => fmt.write_str(name),
            Self::WikiData(identifier) => {
                let wikidata_organisation = WIKIDATA_ORGANISATIONS.resolve(*identifier);

                fmt.write_str(&wikidata_organisation.label)
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum OrganisationName<'a> {
    Other(&'a str),
    WikiData(Arc<WikiDataOrganisation>),
}

impl Deref for OrganisationName<'_> {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        match self {
            Self::Other(name) => name,
            Self::WikiData(wikidata_organisation) => &wikidata_organisation.label,
        }
    }
}

impl PartialEq for OrganisationName<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.deref().eq(other.deref())
    }
}

impl Eq for OrganisationName<'_> {}

impl PartialOrd for OrganisationName<'_> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for OrganisationName<'_> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.deref().cmp(other.deref())
    }
}

impl fmt::Display for OrganisationName<'_> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_str(self)
    }
}
