use std::env::var;
use std::fs::{read_to_string, write};

use phf_codegen::Map;
use toml::Table;

fn main() {
    let top_level_facets = read_to_string("src/dataset/top_level_facets.toml")
        .unwrap()
        .parse::<Table>()
        .unwrap_or_else(|err| panic!("{:#}", err));

    let mut map = Map::<String>::new();

    for (first_level, second_level) in top_level_facets {
        for (second_level, facets) in second_level.as_table().unwrap() {
            let mut key = first_level.to_owned();
            if !second_level.is_empty() {
                key.push('\0');
                key.push_str(&second_level.replace('/', "\0"));
            }

            let mut value = "&[".to_owned();
            for facet in facets.as_array().unwrap() {
                value.push_str(facet.as_str().unwrap());
                value.push(',');
            }
            value.push(']');

            map.entry(key, &value);
        }
    }

    let code = format!(
        "static TOP_LEVEL_FACETS: PhfMap<&str, &[&str]> = {};\n",
        map.build(),
    );

    write(
        format!("{}/top_level_facets.rs", var("OUT_DIR").unwrap()),
        code,
    )
    .unwrap();
}
