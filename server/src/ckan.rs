use std::borrow::Cow;

use axum::{Json, Router, extract::State, routing::get};
use axum_extra::extract::Query;
use base64::{Engine, engine::general_purpose::URL_SAFE};
use geo::geometry::{Geometry, GeometryCollection};
use geozero::ToJson;
use itertools::{Either, Itertools};
use once_cell::sync::Lazy;
use parking_lot::Mutex;
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json::to_string as to_json_string;
use tantivy::schema::Facet;
use utoipa::{IntoParams, ToSchema};

use crate::{Cache, ServerError, State as ServerState};
use metadaten::{
    dataset::{
        Alternative, GlobalIdentifier, License, OrganisationRole, PersonRole, Provider, Region,
        ResourceType, UniquelyIdentifiedDataset,
    },
    index::{SearchResults, Searcher, TextQuery},
    stats::Stats,
};

pub fn ckan_routes() -> Router<&'static ServerState> {
    Router::new()
        .route("/package_search", get(package_search))
        .route("/package_show", get(package_show))
        .route("/help_show", get(help_show))
}

#[utoipa::path(
    get,
    path = "/ckan/api/3/action/package_search",
    params(PackageSearchParams),
    responses(
        (status = 200, description =  "Result of the given query", body = PackageSearchPage),
    ),
)]
/// Search datasets in CKAN format
pub async fn package_search(
    State(searcher): State<&'static Searcher>,
    State(cache): State<&'static Cache>,
    State(stats): State<&'static Mutex<Stats>>,
    Query(params): Query<PackageSearchParams>,
) -> Result<Json<PackageSearchPage>, ServerError> {
    if params.rows == 0 || params.rows > 1000 {
        return Err(ServerError::BadRequest(
            "Number of rows must not be zero or larger than 1000",
        ));
    }

    let results = searcher.search(
        &cache.stats.load(),
        &params.q,
        &Facet::root(),
        &Facet::root(),
        &Facet::root(),
        &Facet::root(),
        &Facet::root(),
        &Facet::root(),
        None,
        false,
        false,
        None,
        Default::default(),
        params.rows,
        params.start,
    )?;

    tracing::debug!("Found {} documents", results.count);

    stats.lock().record_query(
        &results.terms,
        &Facet::root(),
        &Facet::root(),
        &Facet::root(),
        &Facet::root(),
        &Facet::root(),
        &Facet::root(),
        &results.results,
    );

    let page = PackageSearchPage {
        help: "/api/3/action/help_show?name=package_search",
        success: true,
        result: results.into(),
    };

    Ok(Json(page))
}

/// Modelled after <https://docs.ckan.org/en/2.11/api/#ckan.logic.action.get.package_search>
#[derive(Deserialize, IntoParams)]
pub struct PackageSearchParams {
    #[serde(default)]
    #[param(value_type = String)]
    /// Query following the syntax described in <https://docs.rs/tantivy/latest/tantivy/query/struct.QueryParser.html>
    q: TextQuery,
    #[serde(default = "default_rows")]
    #[param(minimum = 1, maximum = 1000)]
    /// Number of rows to be returned
    rows: usize,
    #[serde(default = "default_start")]
    #[param(minimum = 0)]
    /// Zero-based offset from where rows should be returned.
    start: usize,
}

fn default_start() -> usize {
    0
}

fn default_rows() -> usize {
    10
}

#[derive(Serialize, ToSchema)]
pub struct PackageSearchPage {
    help: &'static str,
    success: bool,
    result: CkanSearchResults,
}

#[derive(Serialize, ToSchema)]
pub(super) struct CkanSearchResults {
    count: usize,
    facets: Vec<String>,
    search_facets: Vec<String>,
    sort: &'static str,
    results: Vec<CkanDataset>,
}

impl From<SearchResults> for CkanSearchResults {
    fn from(val: SearchResults) -> Self {
        Self {
            count: val.count,
            facets: Default::default(),
            search_facets: Default::default(),
            sort: "score desc",
            results: val
                .results
                .into_iter()
                .map(|scored_dataset| scored_dataset.value.into())
                .collect(),
        }
    }
}

#[utoipa::path(
    get,
    path ="/ckan/api/3/action/package_show",
    params(PackageShowParams),
    responses(
        (status = 200, description = "The requested dataset", body = PackageShowPage),
    ),
)]
/// Request single datasets in CKAN format
pub async fn package_show(
    Query(params): Query<PackageShowParams>,
    State(searcher): State<&'static Searcher>,
    State(stats): State<&'static Mutex<Stats>>,
) -> Result<Json<PackageShowPage>, ServerError> {
    let id = String::from_utf8(URL_SAFE.decode(&params.id)?)?;

    let Some((source, id)) = id.split_once('/') else {
        return Err(ServerError::BadRequest("Invalid dataset ID"));
    };

    let Some(dataset) = searcher.read(source, id)? else {
        return Err(ServerError::DatasetNotFound(
            source.to_owned(),
            id.to_owned(),
        ));
    };

    stats.lock().record_access(source, id);

    let page = PackageShowPage {
        help: "/api/3/action/help_show?name=package_show",
        success: true,
        result: dataset.into(),
    };

    Ok(Json(page))
}

/// Modelled after <https://docs.ckan.org/en/2.11/api/#ckan.logic.action.get.package_show>
#[derive(Deserialize, IntoParams)]
pub struct PackageShowParams {
    /// Unique identifier
    id: String,
}

#[derive(Serialize, ToSchema)]
pub struct PackageShowPage {
    help: &'static str,
    success: bool,
    result: CkanDataset,
}

/// Representation of a dataset in CKAN compatible format.
///
/// The main model is taken from <https://docs.ckan.org/en/2.11/api/#ckan.logic.action.create.package_create>,
/// and the usage of keys in the 'extras' field follows as much as possible their usage by the GovData CKAN API.
///
/// The following optional parameters are not implemented as they have no reprensentation in the dataset schema:
///
/// - metadata_created,
/// - metadata_modified,
/// - geographical_coverage,
/// - version,
/// - state
/// - type
/// - vocabularies,
/// - organization (GovData uses a field "organization" that describes one organization responsible for the dataset. It provides informational context to "owner_org" field),
/// - plugin_data,
/// - relationships_as_object,
/// - relationships_as_subject,
/// - groups,
/// - owner_org,
#[derive(Serialize, ToSchema)]
pub struct CkanDataset {
    /// Unique identifier
    id: String,
    /// Name of the dataset in the form {source_id}/{dataset_id}
    name: String,
    /// Title of the dataset
    title: String,
    /// If True, marks a private dataset. There are no private datasets in here, so it is always false.
    private: bool,
    /// Name of the dataset’s author (optional)
    author: Option<String>,
    /// Email address of the dataset’s author (optional)
    author_email: Option<String>,
    /// Name of the dataset’s maintainer (optional)
    maintainer: Option<String>,
    /// Email address of the dataset’s maintainer (optional)
    maintainer_email: Option<String>,
    /// URL of the dataset’s license
    license_url: Option<String>,
    /// Name of the dataset's license
    license_title: Option<String>,
    /// Description of the dataset (optional)
    notes: Option<String>,
    /// URL for the dataset’s source (optional)
    url: String,
    /// the resources attached to the dataset, see <https://docs.ckan.org/en/2.11/api/#ckan.logic.action.create.resource_create> for the format of resource dictionaries (optional)
    resources: Vec<CkanResource>,
    /// the tags associated with the dataset, see <https://docs.ckan.org/en/2.11/api/#ckan.logic.action.create.tag_create> for the format of tag dictionaries (optional)
    tags: Vec<CkanTag>,
    /// (list of dataset extra dictionaries) – extra information on the dataset (optional), extras are arbitrary (key: value) metadata items that can be added to datasets, each extra dictionary should have keys 'key' (a string), 'value' (a string)
    extras: Vec<CkanExtra>,
}

/// Representation of a resource in CKAN compatible format.
///
/// Modelled after <https://docs.ckan.org/en/2.11/api/#ckan.logic.action.create.resource_create>
///
/// The folowing additional fields are included to following GovData:
///
/// - download_url
///
/// The following optional fields are ommitted as they have no representation in our Resource:
///
/// - license,
/// - mimetype_inner
#[derive(Serialize, ToSchema)]
pub(super) struct CkanResource {
    /// URL of resource
    url: String,
    /// If the URL allows a direct download of the resource, this field is filled.
    download_url: Option<String>,
    /// Description of the resource content
    description: Option<String>,
    /// Long name of format
    format: Option<String>,
    /// mimetype if the format has a direct equivalent
    mimetype: Option<&'static str>,
    /// ID of package that the resource should be added to.
    package_id: String,
}

#[derive(Serialize, ToSchema)]
pub(super) struct CkanTag {
    id: Option<String>,
    name: String,
}

#[derive(Serialize, ToSchema, Debug)]
pub(super) struct CkanExtra {
    key: &'static str,
    value: String,
}

impl From<UniquelyIdentifiedDataset> for CkanDataset {
    fn from(val: UniquelyIdentifiedDataset) -> Self {
        let name = format!("{}/{}", val.source, val.id);
        let id = URL_SAFE.encode(&name);

        let mut extras = Vec::new();
        let mut push_extra = |key, value| extras.push(CkanExtra { key, value });

        let mut dataset = val.value;

        if let Some(global_identifier) = dataset.global_identifier {
            match global_identifier {
                GlobalIdentifier::Uuid(uuid) => {
                    push_extra("identifier", uuid.to_string());
                }
                GlobalIdentifier::UuidWithProvider(provider, uuid) => {
                    push_extra(
                        "guid",
                        match provider {
                            Provider::GdiDe(name) => {
                                format!("https://registry.gdi-de.org/id/{name}/{uuid}")
                            }
                            Provider::Bfn => format!("https://geodienste.bfn.de/md/{uuid}"),
                            Provider::GeoSeaPortal => {
                                format!("https://www.geoseaportal.de/csw/record/{uuid}")
                            }
                        },
                    );
                    push_extra("identifier", uuid.to_string());
                }
                GlobalIdentifier::UrnNbn(namespace, id) => {
                    push_extra(
                        "urn_nbn",
                        format!("https://nbn-resolving.org/{namespace}/{id}"),
                    );
                }
                GlobalIdentifier::Doi(doi) => {
                    push_extra("doi_url", format!("https://doi.org/{doi}"));
                    push_extra("doi", doi);
                }
                GlobalIdentifier::Other(id) => {
                    push_extra("other_identifier", id);
                }
                GlobalIdentifier::Url(url) => {
                    push_extra("url_identifier", url.into());
                }
            }
        }

        let mut push_alternative = |alternative| match alternative {
            Alternative::Source { source, url, .. } => {
                push_extra("alternate_harvested_source", source.as_ref().to_owned());
                push_extra("alternate_harvested_source_url", url);
            }
            Alternative::Language { language, url } => {
                push_extra("alternate_language", language.to_string());
                push_extra("alternate_language_url", url);
            }
        };

        let (mut other_languages, mut other_sources) = dataset
            .alternatives
            .into_iter()
            .partition_map::<Vec<_>, Vec<_>, _, _, _>(|alternative| match alternative {
                Alternative::Language { .. } => Either::Left(alternative),
                Alternative::Source { .. } => Either::Right(alternative),
            });

        if !other_languages.is_empty() {
            let alternative = other_languages.swap_remove(0);
            push_alternative(alternative);
        }
        if !other_sources.is_empty() {
            let alternative = other_sources.swap_remove(0);
            push_alternative(alternative);
        }

        if !other_languages.is_empty() {
            push_extra(
                "alternate_languages_others",
                to_json_string(&other_languages).unwrap(),
            )
        }
        if !other_sources.is_empty() {
            push_extra(
                "alternate_harvested_source_others",
                to_json_string(&other_sources).unwrap(),
            )
        }

        push_extra("quality", to_json_string(&dataset.quality).unwrap());

        push_extra(
            "machine_readable_source",
            dataset.machine_readable_source.to_string(),
        );

        push_extra("status", dataset.status.to_string());

        if let Some(comment) = dataset.comment {
            push_extra("comment", comment);
        }

        match &dataset.bounding_boxes[..] {
            [] => (),
            [rect] => {
                let spatial = Geometry::Rect(*rect).to_json().unwrap();

                push_extra("spatial", spatial);
            }
            rects => {
                let spatial = Geometry::GeometryCollection(GeometryCollection(
                    rects.iter().copied().map(Geometry::Rect).collect(),
                ))
                .to_json()
                .unwrap();

                push_extra("spatial", spatial);
            }
        }

        let regional_key_pos = dataset
            .regions
            .iter()
            .position(|region| matches!(region, Region::RegionalKey(_)));

        if let Some(regional_key_pos) = regional_key_pos {
            let region = dataset.regions.swap_remove(regional_key_pos);

            push_extra("region_type", "RegionalKey".to_owned());
            push_extra("geocodingText", region.to_string());
            push_extra(
                "politicalGeocodingURI",
                region.url().expect("Missing URI for RegionalKey"),
            );
        }

        if !dataset.regions.is_empty() {
            push_extra("regions_other", to_json_string(&dataset.regions).unwrap())
        };

        if let Some(issued) = dataset.issued {
            push_extra("issued", issued.to_string());
        }

        if let Some(modified) = dataset.modified {
            push_extra("modified", modified.to_string());
        }

        if !dataset.time_ranges.is_empty() {
            let start = dataset
                .time_ranges
                .iter()
                .map(|time_range| time_range.from)
                .min()
                .unwrap();
            let end = dataset
                .time_ranges
                .into_iter()
                .map(|time_range| time_range.until)
                .max()
                .unwrap();
            push_extra("temporal_start", start.to_string());
            push_extra("temporal_end", end.to_string());
        }

        push_extra(
            "language",
            match dataset.language.code() {
                code @ ("und" | "mul" | "deu_easy") => code.to_owned(),
                code => format!("https://iso639-3.sil.org/{}", code),
            },
        );

        let license_url = dataset.license.url().map(|url| url.to_owned());

        let license_title = if dataset.license != License::Unknown {
            Some(dataset.license.as_str().to_owned())
        } else {
            None
        };

        push_extra(
            "mandatory_registration",
            dataset.mandatory_registration.to_string(),
        );

        if !dataset.organisations.is_empty() {
            push_extra(
                "organizations",
                to_json_string(
                    &dataset
                        .organisations
                        .iter()
                        .map(|organisation| organisation.name().to_string())
                        .collect::<Vec<String>>(),
                )
                .unwrap(),
            )
        };

        if !dataset.persons.is_empty() {
            push_extra("persons", to_json_string(&dataset.persons).unwrap())
        };

        let author = {
            dataset
                .persons
                .iter()
                .find(|person| person.role == PersonRole::Creator)
                .map(|person| {
                    (
                        person.name.clone(),
                        person.emails.first().map(|email| email.to_owned()),
                        person.role.to_string(),
                    )
                })
                .or_else(|| {
                    dataset
                        .organisations
                        .iter()
                        .find(|organization| organization.role() == OrganisationRole::Publisher)
                        .map(|organisation| {
                            (
                                organisation.name().to_string(),
                                None,
                                organisation.role().to_string(),
                            )
                        })
                })
        };

        let (author, author_email, author_role) = match author {
            Some((name, email, role)) => (Some(name), email, Some(role)),
            None => (None, None, None),
        };

        if let Some(role) = author_role {
            push_extra("author_contacttype", role);
        }

        let maintainer = {
            dataset
                .persons
                .iter()
                .find(|person| person.role == PersonRole::Contact)
                .map(|person| {
                    (
                        person.name.clone(),
                        person.emails.first().map(|email| email.to_owned()),
                    )
                })
                .or_else(|| {
                    dataset
                        .organisations
                        .iter()
                        .find(|organization| {
                            matches!(
                                organization.role(),
                                OrganisationRole::Operator | OrganisationRole::Provider
                            )
                        })
                        .map(|organisation| (organisation.name().to_string(), None))
                })
        };

        let (maintainer, maintainer_email) = match maintainer {
            Some((name, email)) => (Some(name), email),
            None => (None, None),
        };

        let resources = dataset
            .resources
            .into_iter()
            .map(|resource| {
                let format = if resource.r#type != ResourceType::Unknown {
                    Some(resource.r#type.as_str().to_owned())
                } else {
                    None
                };

                let download_url = if resource.direct_link {
                    Some(resource.url.clone())
                } else {
                    None
                };

                CkanResource {
                    url: resource.url,
                    description: resource.description,
                    package_id: id.clone(),
                    format,
                    mimetype: resource.r#type.mimetype(),
                    download_url,
                }
            })
            .collect();

        let tags = dataset
            .tags
            .into_iter()
            .map(|tag| CkanTag {
                id: tag.url(),
                name: clean_tags(&tag.to_string()).into_owned(),
            })
            .collect();

        Self {
            id,
            name,
            title: dataset.title,
            notes: dataset.description,
            private: false,
            author,
            author_email,
            maintainer,
            maintainer_email,
            license_url,
            license_title,
            url: dataset.source_url,
            resources,
            tags,
            extras,
        }
    }
}

#[utoipa::path(
    get,
    path = "/ckan/api/3/action/help_show",
    params(HelpShowParams),
    responses(
        (status = 200, description = "CKAN help", body = HelpShowPage),
    ),
)]
pub async fn help_show(
    Query(params): Query<HelpShowParams>,
) -> Result<Json<HelpShowPage>, ServerError> {
    match params.name.as_deref() {
        None | Some("package_search") => Ok(Json(HelpShowPage {
            help: "/api/3/action/help_show?name=package_search",
            success: true,
            result: "See /swagger-ui/#/ckan/package_search for help",
        })),
        Some("package_show") => Ok(Json(HelpShowPage {
            help: "/api/3/action/help_show?name=package_show",
            success: true,
            result: "See /swagger-ui/#/ckan/package_show for help",
        })),
        Some(_) => Err(ServerError::BadRequest(
            "No help available for given action.",
        )),
    }
}

/// Modelled after <https://docs.ckan.org/en/2.11/api/#ckan.logic.action.get.help_show>
#[derive(Default, Deserialize, IntoParams)]
pub struct HelpShowParams {
    #[param(default = "package_search")]
    name: Option<String>,
}

#[derive(Serialize, ToSchema)]
pub struct HelpShowPage {
    help: &'static str,
    success: bool,
    result: &'static str,
}

fn clean_tags(text: &str) -> Cow<'_, str> {
    static CLEAN_TAGS: Lazy<Regex> = Lazy::new(|| Regex::new(r"[^[:space:]\w\-_\.]+").unwrap());

    CLEAN_TAGS.replace_all(text, " ")
}

#[cfg(test)]
mod tests {
    use crate::ckan::clean_tags;

    #[test]
    fn clean_tags_works() {
        assert_eq!(
            clean_tags("This is füll of difficült characters #\"-/_"),
            "This is füll of difficült characters  - _"
        );
    }
}
