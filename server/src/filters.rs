use std::fmt;
use std::time::{Duration, SystemTime};

use askama::{Error, Result};
use humantime::{FormattedDuration, format_duration};
use tantivy::schema::Facet;
use time::{OffsetDateTime, macros::format_description};

pub fn system_time(val: &SystemTime) -> Result<String> {
    let val = OffsetDateTime::from(*val)
        .format(format_description!("[day].[month].[year] [hour]:[minute]"))
        .unwrap();

    Ok(val)
}

pub fn duration(val: &Duration) -> Result<FormattedDuration> {
    Ok(format_duration(Duration::from_secs(val.as_secs())))
}

pub fn percentage(val: &f64) -> Result<FormattedPercentage> {
    Ok(FormattedPercentage(100.0 * val))
}

pub struct FormattedPercentage(f64);

impl fmt::Display for FormattedPercentage {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:.0} %", self.0)
    }
}

pub fn strip_root<'a>(facet: &'a Facet, root: &Facet) -> Result<&'a str> {
    facet
        .encoded_str()
        .strip_prefix(root.encoded_str())
        .map(|facet| facet.trim_start_matches('\0'))
        .ok_or_else(|| Error::Custom("Root is not a prefix".into()))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn strip_root_works() {
        assert_eq!(
            strip_root(
                &Facet::from_text("/foo/bar").unwrap(),
                &Facet::from_text("/foo").unwrap(),
            )
            .unwrap(),
            "bar"
        );

        assert_eq!(
            strip_root(
                &Facet::from_text("/foo/bar/baz").unwrap(),
                &Facet::from_text("/foo/bar").unwrap(),
            )
            .unwrap(),
            "baz"
        );

        assert_eq!(
            strip_root(
                &Facet::from_text("/foo").unwrap(),
                &Facet::from_text("/").unwrap(),
            )
            .unwrap(),
            "foo"
        );
    }
}
