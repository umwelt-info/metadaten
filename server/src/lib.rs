pub mod ckan;
pub mod counts;
pub mod dataset;
pub mod filters;
pub mod metrics;
pub mod origin;
pub mod prometheus;
pub mod search;
pub mod top_n;

use std::convert::Infallible;

use anyhow::{Error, Result};
use arc_swap::ArcSwap;
use askama::Template;
use axum::{
    body::Body,
    extract::{FromRef, FromRequestParts},
    http::header::CONTENT_TYPE,
    http::{StatusCode, header::ACCEPT, request::Parts},
    response::{Html, IntoResponse, Json, Response},
};
use cap_std::fs::Dir;
use futures_util::stream::{Stream, StreamExt};
use hashbrown::HashMap;
use once_cell::sync::Lazy;
use parking_lot::Mutex;
use reqwest::Client;
use serde::Serialize;
use serde_json::to_writer;
use utoipa::OpenApi;

use crate::prometheus::Prometheus;
use metadaten::{dataset::Origin, index::Searcher, metrics::Metrics, stats::Stats};

pub async fn api_doc() -> &'static str {
    #[derive(OpenApi)]
    #[openapi(paths(
        search::text_search,
        search::struct_search,
        search::text_complete,
        search::struct_complete,
        search::text_search_all,
        search::struct_search_all,
        dataset::single,
        dataset::redirect,
        dataset::all,
        dataset::random,
        top_n::datasets,
        top_n::tags,
        origin::origin,
        counts::now,
        counts::all,
        ckan::package_search,
        ckan::package_show,
        ckan::help_show,
    ))]
    struct ApiDoc;

    static API_DOC: Lazy<String> = Lazy::new(|| ApiDoc::openapi().to_json().unwrap());

    &API_DOC
}

pub struct State {
    pub searcher: Searcher,
    pub client: Client,
    pub dir: Dir,
    pub stats: Mutex<Stats>,
    pub cache: Cache,
    pub prometheus: Prometheus,
    pub origins: HashMap<String, Origin>,
}

#[derive(Default)]
pub struct Cache {
    pub stats: ArcSwap<Stats>,
    pub metrics: ArcSwap<Metrics>,
}

impl FromRef<&'static State> for &'static Searcher {
    fn from_ref(state: &&'static State) -> Self {
        &state.searcher
    }
}

impl FromRef<&'static State> for &'static Client {
    fn from_ref(state: &&'static State) -> Self {
        &state.client
    }
}

impl FromRef<&'static State> for &'static Dir {
    fn from_ref(state: &&'static State) -> Self {
        &state.dir
    }
}

impl FromRef<&'static State> for &'static Mutex<Stats> {
    fn from_ref(state: &&'static State) -> Self {
        &state.stats
    }
}

impl FromRef<&'static State> for &'static Cache {
    fn from_ref(state: &&'static State) -> Self {
        &state.cache
    }
}

impl FromRef<&'static State> for &'static Prometheus {
    fn from_ref(state: &&'static State) -> Self {
        &state.prometheus
    }
}

impl FromRef<&'static State> for &'static HashMap<String, Origin> {
    fn from_ref(state: &&'static State) -> Self {
        &state.origins
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Accept {
    Unspecified,
    Html,
    Json,
}

impl Accept {
    pub fn into_response<P>(self, page: P) -> Response
    where
        P: Template + Serialize,
    {
        match self {
            Accept::Unspecified | Accept::Html => Html(page.render().unwrap()).into_response(),
            Accept::Json => Json(page).into_response(),
        }
    }
}

impl<S> FromRequestParts<S> for Accept
where
    S: Send + Sync,
{
    type Rejection = Infallible;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        if let Some(accept) = parts
            .headers
            .get(ACCEPT)
            .and_then(|header| header.to_str().ok())
        {
            if accept.contains("text/html") {
                return Ok(Self::Html);
            } else if accept.contains("application/json") {
                return Ok(Self::Json);
            }
        }

        Ok(Self::Unspecified)
    }
}

pub fn into_ndjson<S, T>(stream: S) -> impl IntoResponse
where
    S: Stream<Item = Result<T>> + Send + 'static,
    T: Serialize + Send,
{
    let body = Body::from_stream(stream.chunks(100).map(|vals| -> Result<_> {
        let mut buf = Vec::new();

        for val in vals {
            to_writer(&mut buf, &val?)?;
            buf.push(b'\n');
        }

        Ok(buf)
    }));

    ([(CONTENT_TYPE, "application/x-ndjson")], body)
}

#[derive(Debug)]
pub enum ServerError {
    BadRequest(&'static str),
    DatasetNotFound(String, String),
    Internal(Error),
}

impl<E> From<E> for ServerError
where
    Error: From<E>,
{
    fn from(err: E) -> Self {
        Self::Internal(Error::from(err))
    }
}

impl IntoResponse for ServerError {
    fn into_response(self) -> Response {
        match self {
            Self::BadRequest(msg) => (StatusCode::BAD_REQUEST, msg).into_response(),
            Self::DatasetNotFound(source, id) => (
                StatusCode::NOT_FOUND,
                format!("Dataset {id} not found in {source}"),
            )
                .into_response(),
            Self::Internal(err) => {
                (StatusCode::INTERNAL_SERVER_ERROR, err.to_string()).into_response()
            }
        }
    }
}
