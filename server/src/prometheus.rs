use std::fmt::Write;
use std::time::Instant;

use anyhow::Result;
use axum::{
    body::Body,
    extract::{MatchedPath, State},
    http::Request,
    middleware::Next,
    response::Response,
};
use hashbrown::HashMap;
use parking_lot::Mutex;
use sketches_ddsketch::DDSketch;

use crate::{Cache, ServerError};
use metadaten::index::Searcher;

#[derive(Default)]
pub struct Prometheus {
    pub request_duration: Mutex<HashMap<String, DDSketch>>,
    pub similar_terms_request_duration: Mutex<DDSketch>,
}

pub async fn render_measurements(
    State(searcher): State<&'static Searcher>,
    State(cache): State<&'static Cache>,
    State(prometheus): State<&'static Prometheus>,
) -> Result<String, ServerError> {
    let mut rendered = String::with_capacity(8 << 10);

    macro_rules! render {
        ( $name:literal, $help:literal, $type_:literal, $( $tt:tt )+ ) => {
            rendered.push_str(concat!("# HELP ", $name, " ", $help, "\n# TYPE ", $name, " ", $type_, "\n"));

            render!( @value $name, $( $tt )+ );

            rendered.push('\n');
        };

        ( @value $name:literal, $value:expr $(,)? ) => {
            writeln!(
                &mut rendered,
                concat!($name, " {value}"),
                value = $value
            )?;
        };

        ( @value $name:literal, $first_label:ident $(, $more_labels:ident )* => $values:expr $(,)? ) => {
            for ( $first_label $(, $more_labels )* , value) in $values {
                writeln!(
                    &mut rendered,
                    concat!(
                        $name, "{{",
                        stringify!($first_label), "=\"{", stringify!($first_label), "}\"",
                        $( ",", stringify!($more_labels), "=\"{", stringify!($more_labels), "}\"", )*
                        "}} {value}"
                    ),
                    $first_label = $first_label,
                    $( $more_labels = $more_labels, )*
                    value = value,
                )?;
            }
        };
    }

    {
        let request_duration = prometheus.request_duration.lock();

        render!(
            "request_duration_count",
            "Request count by route.",
            "gauge",
            route =>
            request_duration
                .iter()
                .map(|(route, sketch)| (route, sketch.count())),
        );

        render!(
            "request_duration",
            "Request duration by route.",
            "gauge",
            route, quantile =>
            request_duration.iter().flat_map(|(route, sketch)| {
                [0.5, 0.95, 1.0].into_iter().filter_map(move |quantile| {
                    sketch
                        .quantile(quantile)
                        .unwrap()
                        .map(|value| (route, quantile, value))
                })
            })
        );
    }

    {
        let similar_terms_request_duration = prometheus.similar_terms_request_duration.lock();

        render!(
            "similar_terms_request_duration_count",
            "Request count towards SNS' SimilarTerms API.",
            "counter",
            similar_terms_request_duration.count(),
        );

        render!(
            "similar_terms_request_duration",
            "Request durations towards SNS' SimilarTerms API.",
            "gauge",
            quantile =>
            [0.5, 0.95, 1.0].into_iter().filter_map(|quantile| {
                similar_terms_request_duration
                    .quantile(quantile)
                    .unwrap()
                    .map(|value| (quantile, value))
            }),
        );
    }

    render!(
        "search_cache_entries",
        "Number of cached search queries.",
        "gauge",
        searcher.search_cache_entries()
    );

    render!(
        "complete_cache_entries",
        "Number of cached completions.",
        "gauge",
        searcher.complete_cache_entries()
    );

    let metrics = cache.metrics.load();

    render!("datasets", "Number of datasets.", "gauge", metrics.datasets);

    render!(
        "sources",
        "Number of sources.",
        "gauge",
        metrics.sources.len(),
    );

    render!(
        "providers",
        "Number of providers.",
        "gauge",
        metrics.providers.len(),
    );

    render!(
        "harvests",
        "Number of harvesters which collected datasets.",
        "gauge",
        metrics.harvests.len(),
    );

    render!(
        "failed_harvests",
        "Number of harvesters which failed completely.",
        "gauge",
        metrics.failed_harvests.len(),
    );

    if let Some((_start, duration)) = metrics.last_harvest {
        render!(
            "last_harvest_duration",
            "Overall duration of the last harvest.",
            "gauge",
            duration.as_secs_f64(),
        );
    }

    render!(
        "last_harvest_duration",
        "Duration of the last harvest.",
        "gauge",
        source =>
        metrics
            .harvests
            .iter()
            .map(|(source_name, harvest)| (source_name, harvest.duration.as_secs_f64())),
    );

    render!(
        "responses_deleted",
        "Number of deleted responses before harvest.",
        "gauge",
        source =>
        metrics
            .harvests
            .iter()
            .map(|(source_name, harvest)| (source_name, harvest.deleted)),
    );

    render!(
        "responses_age",
        "Age of stored responses before harvest.",
        "gauge",
        source =>
        metrics
            .harvests
            .iter()
            .map(|(source_name, harvest)| (source_name, harvest.age.as_secs_f64())),
    );

    render!(
        "datasets_available",
        "Number of datasets available in the source.",
        "gauge",
        source =>
        metrics
            .harvests
            .iter()
            .map(|(source_name, harvest)| (source_name, harvest.count)),
    );

    render!(
        "datasets_transmitted",
        "Number of datasets transmitted by the source.",
        "gauge",
        source =>
        metrics
            .harvests
            .iter()
            .map(|(source_name, harvest)| (source_name, harvest.transmitted)),
    );

    render!(
        "datasets_failed",
        "Number of datasets which failed to process.",
        "gauge",
        source =>
        metrics
            .harvests
            .iter()
            .map(|(source_name, harvest)| (source_name, harvest.failed)),
    );

    render!(
        "umthes_fetched_tags",
        "Number of requests towards SNS to retrieve tags for a dataset.",
        "counter",
        metrics.umthes.fetched_tags,
    );

    render!(
        "umthes_fetched_tag_definitions",
        "Number of requests towards SNS to retrieve the definition of a tag.",
        "counter",
        metrics.umthes.fetched_tag_definitions,
    );

    render!(
        "umthes_searched_tags",
        "Number of requests towards SNS to resolve unknown tags into UMTHES.",
        "counter",
        metrics.umthes.searched_tags,
    );

    render!(
        "umthes_tags",
        "Number of cached auto-classify results from SNS.",
        "gauge",
        metrics.umthes.tags,
    );

    render!(
        "umthes_tag_definitions",
        "Number of cached tag definitions from SNS.",
        "gauge",
        metrics.umthes.tag_definitions,
    );

    render!(
        "umthes_search_results",
        "Number of cached search results from SNS.",
        "gauge",
        metrics.umthes.search_results,
    );

    let stats = cache.stats.load();

    render!(
        "accesses",
        "Number of accesses of datasets.",
        "counter",
        source =>
        stats
            .accesses
            .iter()
            .map(|(source_name, accesses)| (source_name, accesses.values().sum::<u64>())),
    );

    Ok(rendered)
}

pub async fn measure_routes(
    State(prometheus): State<&'static Prometheus>,
    path: MatchedPath,
    req: Request<Body>,
    next: Next,
) -> Response {
    let start = Instant::now();

    let resp = next.run(req).await;

    prometheus
        .request_duration
        .lock()
        .entry_ref(path.as_str())
        .or_default()
        .add(start.elapsed().as_secs_f64());

    resp
}
