use axum::extract::{Path, State};
use serde::Deserialize;
use std::fs::{read_dir, read_to_string};
use toml::from_str;

use harvester::{CONFIG_PATH, Config};
use metadaten::dataset::{
    IdentifiedDataset,
    origin::{ORIGINS_PATH, load_origins},
};
use server::{Accept, origin::origin};

#[tokio::test]
async fn every_source_has_origin_metadata() {
    let _ = ORIGINS_PATH.set("../deployment/origins.toml".into());
    let origins = &*Box::leak(Box::new(load_origins().unwrap()));

    let _ = CONFIG_PATH.set("../deployment/harvester.toml".into());
    let config = Config::read().unwrap();

    for source in config.sources {
        if source.not_indexed || &*source.name == "manual" {
            continue;
        }

        let id = source.origins[0][1..].to_owned();

        origin(Path(id), Accept::Unspecified, State(origins))
            .await
            .unwrap();
    }
}

#[tokio::test]
async fn every_manual_dataset_has_origin_metadata() {
    let _ = ORIGINS_PATH.set("../deployment/origins.toml".into());
    let origins = &*Box::leak(Box::new(load_origins().unwrap()));

    for entry in read_dir("../deployment/manual_datasets").unwrap() {
        let entry = entry.unwrap();

        #[derive(Deserialize)]
        struct FileContents {
            datasets: Vec<IdentifiedDataset>,
        }

        let contents = from_str::<FileContents>(&read_to_string(entry.path()).unwrap()).unwrap();

        for dataset in &contents.datasets {
            let id = dataset.value.origins[0][1..].to_owned();

            origin(Path(id), Accept::Unspecified, State(origins))
                .await
                .unwrap();
        }
    }
}
